Example Title:           Service Request
                         
Example Filename:        ServiceRequest.sln
                         
Category:                VISA
                         
Description:             This example demonstrates how to use the service request event and
			 the service request status byte to determine when generated data is ready 
			 and how to read it.              
                         
Software Group:          Measurement Studio                          
                         
Required Software:       NI-VISA
                         
Language:                Visual Basic .NET, Visual C#
                         
Language Version:        7.0
                         
Hardware Group:          GPIB 
                         
Driver Name:             NI-488.2
                         
Driver Version:          Version appropriate for the selected GPIB board
                         
Required Hardware:       Any kind of GPIB board