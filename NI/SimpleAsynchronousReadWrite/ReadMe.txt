Example Title:           Simple Asynchronous Read/Write
                         
Example Filename:        SimpleAsynchronousReadWrite.sln
                         
Category:                VISA
                         
Description:             This example demonstrates how to use the VisaNS .NET Library to
                         perform simple asynchronous operations.  With asynchronous
                         operations, you first call a method to start the operation. You then 
                         call a second method to complete the operation.  The first method
                         returns a handle that you can use to check the progress of the operation.   
              
                         
Software Group:          Measurement Studio                          
                         
Required Software:       
                         
Language:                Visual Basic .NET, Visual C#
                         
Language Version:        7.0
                         
Hardware Group:          Any message-based device
                         
Driver Name:             NI-VISA
                         
Driver Version:          2.6 or greater
                         
Required Hardware:       Any message-based device