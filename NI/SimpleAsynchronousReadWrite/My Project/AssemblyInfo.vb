﻿Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

<Assembly: AssemblyTitle("VISA Sync Read Write")> 
<Assembly: AssemblyDescription("Synchronous VISA Read and Write")> 
<Assembly: AssemblyProduct("Visa.SyncReadWrite.VS2010")>
<Assembly: CLSCompliant(True)> 

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: ComVisible(False)> 


