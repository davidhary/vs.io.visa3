'================================================================================================== 
' 
' Title      : MainForm.vb 
' Purpose    : This program is much like VISAIC (VISA Interactive Control) that currently 
'				ships with NI-VISA, but it would use our .NET Interface to NI-VISA.  
'				This application takes an advantage of reflection to discover what is available on 
'				VISA.NET APIs. This program provides a simple UI to let the user of the example program 
'				interactively tweak properties and call methods on the API.  
' 
'================================================================================================== 

Imports System.Reflection
Imports System.Collections
Imports System.Text
Imports System.Text.RegularExpressions
Imports NationalInstruments.VisaNS

<System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")>
Public Class MainForm
    Inherits FormBase

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        InitializeComponent()
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
            If Not (session Is Nothing) Then
                session.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    Private session As Session
    Private prevLeftPanelWidth As Integer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Private WithEvents PropertyTypeLabel As System.Windows.Forms.Label
    Private WithEvents GetButton As System.Windows.Forms.Button
    Private WithEvents SetButton As System.Windows.Forms.Button
    Private WithEvents GetTextBox As System.Windows.Forms.TextBox
    Private WithEvents StatusLabel As System.Windows.Forms.Label
    Private WithEvents EventFeedbackTextBox As System.Windows.Forms.TextBox
    Private WithEvents ResponseLabel As System.Windows.Forms.Label
    Private WithEvents CleanStatusButton As System.Windows.Forms.Button
    Private WithEvents ClearResponseButton As System.Windows.Forms.Button
    Private WithEvents ReturnTypeLabel As System.Windows.Forms.Label
    Private WithEvents InvokeButton As System.Windows.Forms.Button
    Private WithEvents RegisterButton As System.Windows.Forms.Button
    Private WithEvents UnregisterButton As System.Windows.Forms.Button

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1815:OverrideEqualsAndOperatorEqualsOnValueTypes")>
    Structure ControlProperties
        Friend pointX As Integer
        Friend pointY As Integer
        Friend sizeX As Integer
        Friend sizeY As Integer
        Friend tabIndex As Integer
        Friend name As String
    End Structure

    Private ctrlProperties As ControlProperties

    ' Used only in publicPropertiesTabPage
    Private setControl As System.Windows.Forms.Control

    ' Used only in publicMethodsTabPage
    Private paramCtrlSet As ArrayList ' will be dynamically created
    Private paramLabelCtrlSet As ArrayList ' will be dynamically created
    Private returnValueControl As Control ' will be dynamically created
    Private executeButton As System.Windows.Forms.Button ' will be dynamically created
    Private feedbackLabel As System.Windows.Forms.Label
    Private Const SPACE_BETWEEN_CTRLS As Integer = 24
    Private Const HEIGHT_CTRL As Integer = 20
    Private Const LABEL_HEIGHT As Integer = 16
    Private Const BUTTON_WIDTH As Integer = 75
    Private Const BUTTON_HEIGHT As Integer = 25
    Private Const OFFSET As Integer = 16

    ' Regular expression that matches integers
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1802:UseLiteralsWhereAppropriate")>
    Private Shared ReadOnly IntegerPattern As String = "([+-]?\d+)"
    Private Shared ReadOnly IntegerMatcher As Regex = New Regex(IntegerPattern)

    ' Stores the last IAsyncResult returned from a function
    Private lastAsyncResult As IAsyncResult = Nothing

    ' VISA Delegates - used only in eventTabPage
    Private GpibCICEventHandler As GpibInterfaceControllerInChargeEventHandler
    Private SignalProcessorEventHandler As VxiSessionSignalProcessorEventHandler
    Private GpibInterfaceTriggerEventHandler As GpibInterfaceTriggerEventHandler
    Private VxiBackplaneTriggerEventHandler As VxiBackplaneTriggerEventHandler
    Private VxiSessionTriggerEventHandler As VxiSessionTriggerEventHandler
    Private VisaEHandler As VisaEventHandler
    Private GpibInterfaceEventHandler As GpibInterfaceEventHandler
    Private VxiBackplaneEventHandler As VxiBackplaneEventHandler
    Private MessageSessionEventHandler As MessageBasedSessionEventHandler
    Private SerialSessionEventHandler As SerialSessionEventHandler
    Private VxiSessionEventHandler As VxiSessionEventHandler
    Private PxiSessionEventHandler As PxiSessionEventHandler
    Private VxiVmeInterruptEventHandler As VxiSessionVxiVmeInterruptEventHandler
    Private UsbRawEventHandler As UsbRawEventHandler
    Private UsbRawInterfaceEventHandler As UsbRawInterruptEventHandler
    Private UsbSessionEventHandler As UsbSessionEventHandler
    Private UsbSessionInterfaceEventHandler As UsbSessionInterruptEventHandler
    Private WithEvents FormSplitter As System.Windows.Forms.Splitter
    Private WithEvents LeftPanel As System.Windows.Forms.Panel
    Private WithEvents ReturnTypeTextBox As System.Windows.Forms.TextBox
    Private WithEvents RightPanel As System.Windows.Forms.Panel
    Private WithEvents SessionTreeView As System.Windows.Forms.TreeView
    Private WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Private WithEvents ValueLabel As System.Windows.Forms.Label
    Private WithEvents PropertyTypeTextBox As System.Windows.Forms.TextBox
    Private WithEvents PromptLabel As System.Windows.Forms.Label

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(MainForm))
        Me.getTextBox = New System.Windows.Forms.TextBox
        Me.setButton = New System.Windows.Forms.Button
        Me.propertyTypeLabel = New System.Windows.Forms.Label
        Me.getButton = New System.Windows.Forms.Button
        Me.invokeButton = New System.Windows.Forms.Button
        Me.returnTypeLabel = New System.Windows.Forms.Label
        Me.unregisterButton = New System.Windows.Forms.Button
        Me.registerButton = New System.Windows.Forms.Button
        Me.statusLabel = New System.Windows.Forms.Label
        Me.eventFeedbackTextBox = New System.Windows.Forms.TextBox
        Me.responseLabel = New System.Windows.Forms.Label
        Me.cleanStatusButton = New System.Windows.Forms.Button
        Me.clearResponseButton = New System.Windows.Forms.Button
        Me.leftPanel = New System.Windows.Forms.Panel
        Me.statusTextBox = New System.Windows.Forms.TextBox
        Me.sessionTreeView = New System.Windows.Forms.TreeView
        Me.rightPanel = New System.Windows.Forms.Panel
        Me.propertyTypeTextBox = New System.Windows.Forms.TextBox
        Me.returnTypeTextBox = New System.Windows.Forms.TextBox
        Me.promptLabel = New System.Windows.Forms.Label
        Me.valueLabel = New System.Windows.Forms.Label
        Me.formSplitter = New System.Windows.Forms.Splitter
        Me.leftPanel.SuspendLayout()
        Me.rightPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'getTextBox
        '
        Me.getTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                      System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.getTextBox.Location = New System.Drawing.Point(96, 56)
        Me.getTextBox.Name = "getTextBox"
        Me.getTextBox.ReadOnly = True
        Me.getTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal
        Me.getTextBox.Size = New System.Drawing.Size(212, 20)
        Me.getTextBox.TabIndex = 5
        Me.getTextBox.Text = ""
        Me.getTextBox.Visible = False
        '
        'setButton
        '
        Me.setButton.Location = New System.Drawing.Point(8, 96)
        Me.setButton.Name = "setButton"
        Me.setButton.TabIndex = 4
        Me.setButton.Text = "Set"
        Me.setButton.Visible = False
        '
        'propertyTypeLabel
        '
        Me.propertyTypeLabel.Location = New System.Drawing.Point(8, 8)
        Me.propertyTypeLabel.Name = "propertyTypeLabel"
        Me.propertyTypeLabel.Size = New System.Drawing.Size(80, 16)
        Me.propertyTypeLabel.TabIndex = 2
        Me.propertyTypeLabel.Text = "Property Type:"
        Me.propertyTypeLabel.Visible = False
        '
        'getButton
        '
        Me.getButton.Location = New System.Drawing.Point(8, 56)
        Me.getButton.Name = "getButton"
        Me.getButton.TabIndex = 3
        Me.getButton.Text = "Get"
        Me.getButton.Visible = False
        '
        'invokeButton
        '
        Me.invokeButton.Location = New System.Drawing.Point(8, 40)
        Me.invokeButton.Name = "invokeButton"
        Me.invokeButton.TabIndex = 3
        Me.invokeButton.Text = "Invoke"
        Me.invokeButton.Visible = False
        '
        'returnTypeLabel
        '
        Me.returnTypeLabel.Location = New System.Drawing.Point(8, 9)
        Me.returnTypeLabel.Name = "returnTypeLabel"
        Me.returnTypeLabel.Size = New System.Drawing.Size(72, 16)
        Me.returnTypeLabel.TabIndex = 2
        Me.returnTypeLabel.Text = "Return Type:"
        Me.returnTypeLabel.Visible = False
        '
        'unregisterButton
        '
        Me.unregisterButton.Location = New System.Drawing.Point(96, 9)
        Me.unregisterButton.Name = "unregisterButton"
        Me.unregisterButton.TabIndex = 2
        Me.unregisterButton.Text = "Unregister"
        Me.unregisterButton.Visible = False
        '
        'registerButton
        '
        Me.registerButton.Location = New System.Drawing.Point(8, 9)
        Me.registerButton.Name = "registerButton"
        Me.registerButton.TabIndex = 1
        Me.registerButton.Text = "Register"
        Me.registerButton.Visible = False
        '
        'statusLabel
        '
        Me.statusLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.statusLabel.Location = New System.Drawing.Point(16, 384)
        Me.statusLabel.Name = "statusLabel"
        Me.statusLabel.Size = New System.Drawing.Size(48, 16)
        Me.statusLabel.TabIndex = 2
        Me.statusLabel.Text = "Status:"
        '
        'eventFeedbackTextBox
        '
        Me.eventFeedbackTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) Or
                                                System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.eventFeedbackTextBox.Location = New System.Drawing.Point(16, 488)
        Me.eventFeedbackTextBox.Multiline = True
        Me.eventFeedbackTextBox.Name = "eventFeedbackTextBox"
        Me.eventFeedbackTextBox.ReadOnly = True
        Me.eventFeedbackTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.eventFeedbackTextBox.Size = New System.Drawing.Size(296, 40)
        Me.eventFeedbackTextBox.TabIndex = 3
        Me.eventFeedbackTextBox.Text = ""
        '
        'responseLabel
        '
        Me.responseLabel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.responseLabel.Location = New System.Drawing.Point(16, 472)
        Me.responseLabel.Name = "responseLabel"
        Me.responseLabel.Size = New System.Drawing.Size(160, 16)
        Me.responseLabel.TabIndex = 4
        Me.responseLabel.Text = "Response from Event Handler:"
        '
        'cleanStatusButton
        '
        Me.cleanStatusButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cleanStatusButton.Location = New System.Drawing.Point(216, 448)
        Me.cleanStatusButton.Name = "cleanStatusButton"
        Me.cleanStatusButton.Size = New System.Drawing.Size(96, 23)
        Me.cleanStatusButton.TabIndex = 5
        Me.cleanStatusButton.Text = "Clear Status"
        '
        'clearResponseButton
        '
        Me.clearResponseButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.clearResponseButton.Location = New System.Drawing.Point(216, 536)
        Me.clearResponseButton.Name = "clearResponseButton"
        Me.clearResponseButton.Size = New System.Drawing.Size(96, 23)
        Me.clearResponseButton.TabIndex = 6
        Me.clearResponseButton.Text = "Clear Response"
        '
        'leftPanel
        '
        Me.leftPanel.Controls.Add(Me.statusTextBox)
        Me.leftPanel.Controls.Add(Me.responseLabel)
        Me.leftPanel.Controls.Add(Me.statusLabel)
        Me.leftPanel.Controls.Add(Me.eventFeedbackTextBox)
        Me.leftPanel.Controls.Add(Me.cleanStatusButton)
        Me.leftPanel.Controls.Add(Me.clearResponseButton)
        Me.leftPanel.Controls.Add(Me.sessionTreeView)
        Me.leftPanel.Dock = System.Windows.Forms.DockStyle.Left
        Me.leftPanel.Location = New System.Drawing.Point(0, 0)
        Me.leftPanel.Name = "leftPanel"
        Me.leftPanel.Size = New System.Drawing.Size(312, 566)
        Me.leftPanel.TabIndex = 8
        '
        'statusTextBox
        '
        Me.statusTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) Or
                                         System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.statusTextBox.Location = New System.Drawing.Point(16, 400)
        Me.statusTextBox.Multiline = True
        Me.statusTextBox.Name = "statusTextBox"
        Me.statusTextBox.ReadOnly = True
        Me.statusTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.statusTextBox.Size = New System.Drawing.Size(296, 40)
        Me.statusTextBox.TabIndex = 8
        Me.statusTextBox.Text = ""
        '
        'sessionTreeView
        '
        Me.sessionTreeView.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                            System.Windows.Forms.AnchorStyles.Left) Or
                                        System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.sessionTreeView.ImageIndex = -1
        Me.sessionTreeView.Location = New System.Drawing.Point(16, 9)
        Me.sessionTreeView.Name = "sessionTreeView"
        Me.sessionTreeView.SelectedImageIndex = -1
        Me.sessionTreeView.Size = New System.Drawing.Size(296, 368)
        Me.sessionTreeView.TabIndex = 7
        '
        'rightPanel
        '
        Me.rightPanel.Controls.Add(Me.propertyTypeTextBox)
        Me.rightPanel.Controls.Add(Me.returnTypeTextBox)
        Me.rightPanel.Controls.Add(Me.promptLabel)
        Me.rightPanel.Controls.Add(Me.getButton)
        Me.rightPanel.Controls.Add(Me.getTextBox)
        Me.rightPanel.Controls.Add(Me.setButton)
        Me.rightPanel.Controls.Add(Me.propertyTypeLabel)
        Me.rightPanel.Controls.Add(Me.returnTypeLabel)
        Me.rightPanel.Controls.Add(Me.invokeButton)
        Me.rightPanel.Controls.Add(Me.unregisterButton)
        Me.rightPanel.Controls.Add(Me.registerButton)
        Me.rightPanel.Controls.Add(Me.valueLabel)
        Me.rightPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rightPanel.Location = New System.Drawing.Point(312, 0)
        Me.rightPanel.Name = "rightPanel"
        Me.rightPanel.Size = New System.Drawing.Size(328, 566)
        Me.rightPanel.TabIndex = 9
        '
        'propertyTypeTextBox
        '
        Me.propertyTypeTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                               System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.propertyTypeTextBox.Location = New System.Drawing.Point(96, 9)
        Me.propertyTypeTextBox.Name = "propertyTypeTextBox"
        Me.propertyTypeTextBox.ReadOnly = True
        Me.propertyTypeTextBox.Size = New System.Drawing.Size(216, 20)
        Me.propertyTypeTextBox.TabIndex = 9
        Me.propertyTypeTextBox.Text = ""
        Me.propertyTypeTextBox.Visible = False
        '
        'returnTypeTextBox
        '
        Me.returnTypeTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                             System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.returnTypeTextBox.Location = New System.Drawing.Point(96, 9)
        Me.returnTypeTextBox.Name = "returnTypeTextBox"
        Me.returnTypeTextBox.ReadOnly = True
        Me.returnTypeTextBox.Size = New System.Drawing.Size(212, 20)
        Me.returnTypeTextBox.TabIndex = 7
        Me.returnTypeTextBox.Text = ""
        Me.returnTypeTextBox.Visible = False
        '
        'promptLabel
        '
        Me.promptLabel.Location = New System.Drawing.Point(16, 192)
        Me.promptLabel.Name = "promptLabel"
        Me.promptLabel.Size = New System.Drawing.Size(296, 32)
        Me.promptLabel.TabIndex = 6
        Me.promptLabel.Text = "Please select a property, method, or event you would like to access on the left s" &
        "ide."
        '
        'valueLabel
        '
        Me.valueLabel.Location = New System.Drawing.Point(48, 59)
        Me.valueLabel.Name = "valueLabel"
        Me.valueLabel.Size = New System.Drawing.Size(40, 16)
        Me.valueLabel.TabIndex = 8
        Me.valueLabel.Text = "Value:"
        Me.valueLabel.Visible = False
        '
        'formSplitter
        '
        Me.formSplitter.Location = New System.Drawing.Point(312, 0)
        Me.formSplitter.Name = "formSplitter"
        Me.formSplitter.Size = New System.Drawing.Size(3, 566)
        Me.formSplitter.TabIndex = 10
        Me.formSplitter.TabStop = False
        '
        'MainForm
        '
        Me.ClientSize = New System.Drawing.Size(640, 566)
        Me.Controls.Add(Me.formSplitter)
        Me.Controls.Add(Me.rightPanel)
        Me.Controls.Add(Me.leftPanel)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(648, 600)
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "VISA Interactive Control for .NET"
        Me.leftPanel.ResumeLayout(False)
        Me.rightPanel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
#End Region


    Private Sub MainFormLoaded(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Using rlf As ResourceListForm = New ResourceListForm
            rlf.ShowDialog()
            session = rlf.OpenedSession
        End Using
        If Not (session Is Nothing) Then
            PopulateSessionTreeView()
            prevLeftPanelWidth = leftPanel.Size.Width
        Else
            Me.Close()
        End If
    End Sub

    Private Shared Function ReplaceCommonEscapeSequences(ByVal s As String) As String
        Return s.Replace("\n", vbLf).Replace("\r", vbCr)
    End Function

    Private Shared Function InsertCommonEscapeSequences(ByVal s As String) As String
        Return s.Replace(vbLf, "\n").Replace(vbCr, "\r")
    End Function

    Private Shared Function ParseByteArray(ByVal s As String) As Byte()
        Dim matches As MatchCollection = IntegerMatcher.Matches(s)
        Dim parsedArray(matches.Count - 1) As Byte
        Dim i As Integer
        For i = 0 To matches.Count - 1
            parsedArray(i) = Byte.Parse(matches(i).Value)
        Next i
        Return parsedArray
    End Function

    Private Shared Function ByteArrayToString(ByVal byteArray() As Byte) As String
        If byteArray Is Nothing Or byteArray.Length = 0 Then
            Return String.Empty
        End If

        Dim byteArraySB As New StringBuilder
        Dim i As Integer
        For i = 0 To byteArray.Length - 1
            If i > 0 Then
                byteArraySB.Append(" ")
            End If
            byteArraySB.Append(byteArray(i).ToString())
        Next i
        Return byteArraySB.ToString()
    End Function

    Private Sub PopulateSessionTreeView()
        sessionTreeView.Nodes.Clear()

        Dim sessionNode As TreeNode = New TreeNode(session.ResourceName)
        sessionTreeView.Nodes.Add(sessionNode)

        Dim propertyNode As TreeNode = New TreeNode("Public Properties")
        PopulatePublicProperties(propertyNode)
        sessionNode.Nodes.Add(propertyNode)

        Dim methodNode As TreeNode = New TreeNode("Public Methods")
        PopulatePublicMethods(methodNode)
        sessionNode.Nodes.Add(methodNode)

        Dim eventNode As TreeNode = New TreeNode("Events")
        PopulateEvents(eventNode)
        sessionNode.Nodes.Add(eventNode)

        sessionNode.Expand()
    End Sub

    Private Function GetCtrlWidth() As Integer
        Return Me.Size.Width - (sessionTreeView.Size.Width + OFFSET * 3)
    End Function

    Private Sub FormSplitter_LocationChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles formSplitter.LocationChanged
        Me.MinimumSize = New System.Drawing.Size(Me.MinimumSize.Width + (leftPanel.Size.Width - prevLeftPanelWidth), Me.MinimumSize.Height)
        prevLeftPanelWidth = leftPanel.Size.Width
    End Sub

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub SessionTree_AfterCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles sessionTreeView.AfterCollapse
        Try
            If sessionTreeView.SelectedNode.Parent.Text = "Public Properties" Then
                ShowPublicPropertiesCtrls(False)
            ElseIf (sessionTreeView.SelectedNode.Parent.Text = "Public Methods") Then
                ShowPublicMethodsCtrls(False)
            ElseIf (sessionTreeView.SelectedNode.Parent.Text = "Events") Then
                ShowEventsCtrls(False)
            Else
                ShowPublicPropertiesCtrls(False)
                ShowPublicMethodsCtrls(False)
                ShowEventsCtrls(False)
            End If
        Catch ex As Exception ' session node was collapsed
            GetInnerException(ex)
        End Try
    End Sub

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub SessionTreeView_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles sessionTreeView.AfterSelect
        promptLabel.Visible = False
        Try
            If sessionTreeView.SelectedNode.Parent Is Nothing Then
                Return
            End If

            If sessionTreeView.SelectedNode.Parent.Text = "Public Properties" Then
                PublicPropertySelected()
            ElseIf sessionTreeView.SelectedNode.Parent.Text = "Public Methods" Then
                PublicMethodSelected()
            ElseIf sessionTreeView.SelectedNode.Parent.Text = "Events" Then
                EventSelected()
            Else ' propertyNode, methodNode, or eventNode is selected
                ShowPublicPropertiesCtrls(False)
                ShowPublicMethodsCtrls(False)
                ShowEventsCtrls(False)
                promptLabel.Visible = True
            End If
            statusTextBox.Text = session.LastStatus.ToString()
        Catch ex As Exception
            GetInnerException(ex)
        End Try
    End Sub

    Private Sub SetNewControlProperties(ByVal ctrl As Control)
        ctrl.Location = New System.Drawing.Point(ctrlProperties.pointX, ctrlProperties.pointY)
        ctrl.Size = New System.Drawing.Size(ctrlProperties.sizeX, ctrlProperties.sizeY)
        ctrl.TabIndex = ctrlProperties.tabIndex
        ctrl.Name = ctrlProperties.name
    End Sub

    ' convert the string in the control to the type specified
    Private Function ConvertFromCtrlToType(ByVal ctrl As Control, ByVal type As Type) As Object
        Dim newValue As Object
        If type.IsEnum Then
            newValue = System.Enum.Parse(type, ctrl.Text)
        ElseIf type Is GetType(String) Then
            newValue = ReplaceCommonEscapeSequences(ctrl.Text)
        ElseIf type Is GetType(Byte()) Then
            newValue = ParseByteArray(ctrl.Text)
        ElseIf type Is GetType(IAsyncResult) Then
            newValue = lastAsyncResult
        Else
            Dim parameter(0) As Object ' parameter for Parse method
            parameter(0) = ctrl.Text
            newValue = type.InvokeMember("Parse", BindingFlags.Public Or BindingFlags.Static Or System.Reflection.BindingFlags.InvokeMethod, Nothing, Nothing, parameter)
        End If
        Return newValue
    End Function

    Private Sub CreateCtrl(ByVal type As Type, ByRef ctrl As Control)
        If type.Namespace.StartsWith("NationalInstruments.VisaNS") Or type Is GetType(Boolean) Then ' those are Enum defined in NationalInstruments.VisaNS
            CreateComboBox(type, ctrl)
        ElseIf type Is GetType(Short) Or type Is GetType(Integer) Or type Is GetType(Long) Or type Is GetType(Byte) Or type Is GetType(IntPtr) Then
            CreateNumericUpDown(type, ctrl)
        Else ' just create a text control
            CreateTextControl(ctrl)
        End If
    End Sub

    Private Sub CreateComboBox(ByVal type As Type, ByRef ctrl As Control)
        ' create combo box dynamically
        ctrl = New System.Windows.Forms.ComboBox
        SetNewControlProperties(ctrl)

        ' populate it with enum constants
        If type.IsEnum Then
            Dim enumNames As String() = System.Enum.GetNames(type)
            Dim enumName As String
            For Each enumName In enumNames
                CType(ctrl, ComboBox).Items.Add(enumName)
            Next
        ElseIf type Is GetType(Boolean) Then
            CType(ctrl, ComboBox).Items.Add("False")
            CType(ctrl, ComboBox).Items.Add("True")
        Else
            statusTextBox.Text = "The property type is not enum, boolean, or number. Unable to fill the combo box."
            Return
        End If
        CType(ctrl, ComboBox).SelectedIndex = 0
        CType(ctrl, ComboBox).DropDownStyle = ComboBoxStyle.DropDownList
    End Sub

    Private Sub CreateNumericUpDown(ByVal type As Type, ByRef ctrl As Control)
        ' create NumericUpDown dynamically
        ctrl = New System.Windows.Forms.NumericUpDown
        SetNewControlProperties(ctrl)
        If type Is GetType(IntPtr) Then
            CType(ctrl, NumericUpDown).Minimum = 0
            CType(ctrl, NumericUpDown).Maximum = IntPtr.Size
        ElseIf type Is GetType(Byte) Then  ' cannot convert object to int if type is Byte
            CType(ctrl, NumericUpDown).Minimum = Byte.MinValue
            CType(ctrl, NumericUpDown).Maximum = Byte.MaxValue
        Else ' Int16, or Int 32
            CType(ctrl, NumericUpDown).Minimum = CType(type.GetField("MinValue").GetValue(Nothing), Integer)
            CType(ctrl, NumericUpDown).Maximum = CType(type.GetField("MaxValue").GetValue(Nothing), Integer)
        End If
    End Sub

    Private Sub CreateTextControl(ByRef ctrl As Control)
        ' create text control dynamically
        ctrl = New System.Windows.Forms.TextBox
        SetNewControlProperties(ctrl)
        CType(ctrl, TextBox).ScrollBars = ScrollBars.Horizontal
    End Sub

    Private Sub GetInnerException(ByVal e As Exception)
        Dim prev As Exception = e
        Dim current As Exception = e.InnerException

        While Not (current Is Nothing)
            prev = current
            current = current.InnerException
        End While
        statusTextBox.Text = prev.Message
    End Sub

    Private Sub CleanStatusButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cleanStatusButton.Click
        statusTextBox.Text = ""
    End Sub

    Private Sub ClearResponseButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clearResponseButton.Click
        eventFeedbackTextBox.Text = ""
    End Sub

    '*************** Methods for Public Properties ***************//
#Region "Methods for Public Properties"

    ' This class implements IComparer so that PropertyInfo can be sorted in an alphabetical order for display.
    Private Class PropertyInfoComparer
        Implements IComparer
        Public Function Compare(ByVal ob1 As Object, ByVal ob2 As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim pi1 As PropertyInfo = CType(ob1, PropertyInfo)
            Dim pi2 As PropertyInfo = CType(ob2, PropertyInfo)
            If Not (pi1 Is Nothing) And Not (pi2 Is Nothing) Then
                Return pi1.Name.CompareTo(pi2.Name)
            Else
                MessageBox.Show("Compare function on Property Info failed!!", "Compare Failed", MessageBoxButtons.OK,
                                MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                Return 0
            End If
        End Function
    End Class

    Private Sub MainFormSizeChanges(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.SizeChanged
        ctrlProperties.pointY = setButton.Location.Y
        ctrlProperties.sizeX = getTextBox.Size.Width
    End Sub

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub PopulatePublicProperties(ByVal parent As TreeNode)
        Try
            Dim prpInfoArray As PropertyInfo() = session.GetType().GetProperties()
            Array.Sort(prpInfoArray, New PropertyInfoComparer)
            Dim pi As PropertyInfo
            For Each pi In prpInfoArray
                Dim tn As TreeNode = New TreeNode(pi.Name) With {
                    .Tag = pi
                }
                parent.Nodes.Add(tn)
            Next
            StatusTextBox.Text = session.LastStatus.ToString()
        Catch ex As Exception
            GetInnerException(ex)
        End Try
    End Sub

    Private Sub PublicPropertySelected()
        ShowPublicPropertiesCtrls(True)
        ShowPublicMethodsCtrls(False)
        ShowEventsCtrls(False)

        SetsetControlProperties()
        Dim propertyInfo As PropertyInfo = CType(SessionTreeView.SelectedNode.Tag, PropertyInfo)
        CreateControlForType(propertyInfo)
        PropertyTypeTextBox.Text = propertyInfo.PropertyType.Name.ToString()
        GetButton_Click(Nothing, Nothing)
    End Sub

    Private Sub SetsetControlProperties()
        ctrlProperties.pointX = GetTextBox.Location.X ' position of setControl
        ctrlProperties.pointY = SetButton.Location.Y
        ctrlProperties.sizeX = GetTextBox.Size.Width ' size of setControl
        ctrlProperties.sizeY = GetTextBox.Size.Height
        ctrlProperties.tabIndex = 5
        ctrlProperties.name = "ctrlPublicProperties"
    End Sub

    Private Sub ShowPublicPropertiesCtrls(ByVal show As Boolean)
        GetButton.Visible = show
        SetButton.Visible = show
        GetTextBox.Visible = show
        PropertyTypeTextBox.Visible = show
        PropertyTypeLabel.Visible = show
        ValueLabel.Visible = show

        If Not (setControl Is Nothing) Then
            RightPanel.Controls.Remove(setControl)
            setControl = Nothing
        End If
    End Sub

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub GetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GetButton.Click
        Try
            Dim propertyInfo As PropertyInfo = CType(SessionTreeView.SelectedNode.Tag, PropertyInfo)
            GetTextBox.Text = propertyInfo.GetValue(session, Nothing).ToString()
            StatusTextBox.Text = session.LastStatus.ToString
        Catch ex As Exception
            GetTextBox.Text = ""
            GetInnerException(ex)
        End Try
        SessionTreeView.Focus()
    End Sub

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub SetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SetButton.Click
        Try
            Dim propertyInfo As PropertyInfo = CType(SessionTreeView.SelectedNode.Tag, PropertyInfo)

            ' dynamically get the type of property, and invoke Parse method on it.
            Dim type As Type = propertyInfo.GetValue(session, Nothing).GetType()
            Dim newVal As Object = ConvertFromCtrlToType(setControl, type)
            propertyInfo.SetValue(session, newVal, Nothing)
            StatusTextBox.Text = session.LastStatus.ToString
        Catch ex As Exception
            GetInnerException(ex)
        End Try
        SessionTreeView.Focus()
    End Sub

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub CreateControlForType(ByVal propertyInfo As PropertyInfo)
        Try
            SuspendLayout()
            RightPanel.Controls.Remove(setControl)
            setControl = Nothing
            Dim type As Type = propertyInfo.GetValue(session, Nothing).GetType() ' dynamically get the type of property

            If Not (HideControls(propertyInfo)) Then ' property is not read only
                CreateCtrl(type, setControl)
                setControl.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left Or
                                     System.Windows.Forms.AnchorStyles.Right)
                RightPanel.Controls.Add(setControl)
            End If
        Catch ex As Exception
            GetInnerException(ex)
        Finally
            ResumeLayout(False)
        End Try
    End Sub

    Private Function HideControls(ByVal propertyInfo As PropertyInfo) As Boolean
        If Not (propertyInfo.CanWrite) Then 'read only
            SetButton.Hide()
            GetButton.Hide()
            Return True
        Else
            SetButton.Show()
            GetButton.Show()
            Return False
        End If
    End Function

#End Region

    '*************** Methods for public Methods  ***************//
#Region "Methods for public Methods"

    ' This class implements IComparer so that MethodInfo can be sorted in an alphabetical order for display.
    Private Class MethodInfoComparer
        Implements IComparer
        Public Function Compare(ByVal ob1 As Object, ByVal ob2 As Object) As Integer Implements System.Collections.IComparer.Compare
            Dim mi1 As MethodInfo = CType(ob1, MethodInfo)
            Dim mi2 As MethodInfo = CType(ob2, MethodInfo)
            If Not (mi1 Is Nothing) And Not (mi2 Is Nothing) Then
                Return GetMethodSignature(mi1).CompareTo(GetMethodSignature(mi2))
            Else
                MessageBox.Show("Compare function on Method Info failed!!", "Compare Failed", MessageBoxButtons.OK,
                                MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                Return 0
            End If
        End Function
    End Class

    ' This method returns a method signature, excluding the return type. Also, uses type name rather than a full name
    Public Shared Function GetMethodSignature(ByVal value As MethodBase) As String
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Dim rs As String = value.Name + "("
        Dim piArray As ParameterInfo() = value.GetParameters()
        Dim i As Integer = 0
        Dim pi As ParameterInfo
        For Each pi In piArray
            rs = rs + pi.ParameterType.Name.ToString() + " " + pi.Name
            If i <> piArray.Length - 1 Then
                rs += ", "
                i = i + 1
            End If
        Next
        rs += ")"
        Return rs
    End Function

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub PopulatePublicMethods(ByVal parent As TreeNode)
        Try
            Dim mdInfoArray As MethodInfo() = session.GetType().GetMethods()
            Array.Sort(mdInfoArray, New MethodInfoComparer)
            Dim mi As MethodInfo
            For Each mi In mdInfoArray
                If Not HasUnderscoreInMethodName(mi) Then
                    Dim tn As TreeNode = New TreeNode(GetMethodSignature(mi)) With {
                        .Tag = mi
                    }
                    parent.Nodes.Add(tn)
                End If
            Next

        Catch ex As Exception
            GetInnerException(ex)
        End Try
    End Sub

    Private Sub ShowPublicMethodsCtrls(ByVal show As Boolean)
        InvokeButton.Visible = show
        ReturnTypeLabel.Visible = show
        ReturnTypeTextBox.Visible = show
        RemoveMethodCtrls()

        If show Then
            Dim mi As MethodInfo = CType(SessionTreeView.SelectedNode.Tag, MethodInfo)
            ReturnTypeTextBox.Text = mi.ReturnType.Name.ToString()
        End If
    End Sub

    Private Sub PublicMethodSelected()
        ShowPublicPropertiesCtrls(False)
        ShowPublicMethodsCtrls(True)
        ShowEventsCtrls(False)
    End Sub

    ' There are methods that contain "_" and appear as public methods 
    ' using reflection. However, they should not show up in intellisense  
    ' or the object browser. This method removes those functions
    ' from the list box publicMethodsListBox.
    Private Shared Function HasUnderscoreInMethodName(ByVal value As MethodInfo) As Boolean
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        If value.Name.IndexOf("_") <> -1 Then
            Return True
        End If
        Return False
    End Function

    Private Sub OnKeyUpInpublicMethodsListBox(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyData = Keys.Enter Then
            InvokeButton_Click(Nothing, Nothing)
        End If
    End Sub

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub InvokeButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InvokeButton.Click
        RemoveMethodCtrls()
        Try
            paramCtrlSet = New ArrayList
            paramLabelCtrlSet = New ArrayList

            ' dynamically create a form with function parameter controls and also for the return value if appropriate.
            Me.CreateParamCtrls()
            StatusTextBox.Text = session.LastStatus.ToString
        Catch ex As Exception
            GetInnerException(ex)
        End Try
        SessionTreeView.Focus()
    End Sub

    Private Function GetPositionY(ByVal counter As Integer) As Integer ' returns the position y for parameter control
        Return CInt(InvokeButton.Location.Y * 5 / 4 + (counter + 1) * (SPACE_BETWEEN_CTRLS + HEIGHT_CTRL + LABEL_HEIGHT) - HEIGHT_CTRL)
    End Function

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Private Sub CreateLabelForParamControl(ByVal counter As Integer, ByVal name As String)
        Dim label As System.Windows.Forms.Label = New System.Windows.Forms.Label With {
            .Text = name,
            .Location = New System.Drawing.Point(InvokeButton.Location.X, GetPositionY(counter) - LABEL_HEIGHT),
            .Size = New System.Drawing.Size(GetCtrlWidth, LABEL_HEIGHT)
        }
        RightPanel.Controls.Add(label)
        paramLabelCtrlSet.Add(label)
    End Sub

    Private Function CreateParamCtrls() As Integer
        Dim mi As MethodInfo = CType(SessionTreeView.SelectedNode.Tag, MethodInfo)
        Dim piArray As ParameterInfo() = mi.GetParameters()

        Dim counter As Integer = 0
        Dim pi As ParameterInfo
        For Each pi In piArray
            CreateParamLabelAndCtrl(pi, counter)
            counter = counter + 1
        Next

        Dim rt As Type = mi.ReturnType
        If Not rt Is Type.GetType("System.Void") Then ' also add read-only control for return value
            AddReturnValueCtrl(counter)
            counter = counter + 1
            CType(paramLabelCtrlSet(paramLabelCtrlSet.Count - 1), Label).Text = "Return Value"
        End If

        AddExecuteButton(counter)
        counter = counter + 1
        AddFeedbackLabel(counter)

        Return paramCtrlSet.Count
    End Function

    Private Sub AddFeedbackLabel(ByVal counter As Integer)
        feedbackLabel = New System.Windows.Forms.Label With {
            .Size = New System.Drawing.Size(GetCtrlWidth, LABEL_HEIGHT * 2),
            .TabIndex = counter,
            .Name = "feedbackLabel",
            .Location = New System.Drawing.Point(InvokeButton.Location.X, GetPositionY(counter) - SPACE_BETWEEN_CTRLS - OFFSET)
        }
        RightPanel.Controls.Add(feedbackLabel)
    End Sub

    Private Sub CreateParamLabelAndCtrl(ByVal pi As ParameterInfo, ByVal counter As Integer)
        CreateLabelForParamControl(counter, pi.Name)

        ctrlProperties.pointX = InvokeButton.Location.X ' position of control
        ctrlProperties.pointY = GetPositionY(counter)
        ctrlProperties.sizeX = GetCtrlWidth()
        ctrlProperties.sizeY = HEIGHT_CTRL
        ctrlProperties.tabIndex = counter
        ctrlProperties.name = "Parameter" + (counter + 1).ToString()

        Dim parameterType As Type = pi.ParameterType
        Dim ctrl As Control = Nothing
        CreateCtrl(parameterType, ctrl)
        ctrl.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right)
        RightPanel.Controls.Add(ctrl)
        paramCtrlSet.Add(ctrl)

        If parameterType Is GetType(IAsyncResult) Then
            ctrl.Text = "Last stored Async Result Interface will be used."
            Dim tb As TextBox = CType(ctrl, TextBox)
            tb.ReadOnly = True
        End If
    End Sub

    Private Sub AddExecuteButton(ByVal counter As Integer)
        executeButton = New System.Windows.Forms.Button With {
            .Size = New System.Drawing.Size(BUTTON_WIDTH, BUTTON_HEIGHT),
            .TabIndex = counter
        }
        Dim pointX, pointY As Integer

        pointX = InvokeButton.Location.X
        pointY = CInt(GetPositionY(counter) - SPACE_BETWEEN_CTRLS / 2)
        executeButton.Name = "executeButton"
        executeButton.Text = "Execute"
        executeButton.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)
        AddHandler executeButton.Click, AddressOf ExecuteButton_Click

        executeButton.Location = New System.Drawing.Point(pointX, pointY)
        RightPanel.Controls.Add(executeButton)
    End Sub

    Private Sub AddReturnValueCtrl(ByVal counter As Integer)
        ctrlProperties.pointX = InvokeButton.Location.X ' position of control
        ctrlProperties.pointY = GetPositionY(counter)
        ctrlProperties.tabIndex = counter
        CreateLabelForParamControl(counter, "Return Value")

        returnValueControl = New Control
        CreateCtrl(GetType(System.String), returnValueControl)
        returnValueControl.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or System.Windows.Forms.AnchorStyles.Right)
        CType(returnValueControl, TextBox).ReadOnly = True
        RightPanel.Controls.Add(returnValueControl)
    End Sub

    ' Convert the string from the control to the type specified in piArray
    Private Function GetParamObjects(ByVal ctrl As Control, ByRef piArray As ParameterInfo(), ByVal count As Integer) As Object
        Dim type As Type = piArray(count).ParameterType
        Return ConvertFromCtrlToType(ctrl, type)
    End Function

    ' When the Execute button that was created dynamically is pressed, it executes
    ' the selected method with specified parameters.
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub ExecuteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim mi As MethodInfo = CType(SessionTreeView.SelectedNode.Tag, MethodInfo)
            Dim piArray As ParameterInfo() = mi.GetParameters()
            Dim parameters(paramCtrlSet.Count - 1) As Object
            Dim count As Integer = 0
            Dim ctrl As Control
            For Each ctrl In paramCtrlSet
                parameters(count) = GetParamObjects(ctrl, piArray, count)
                count = count + 1
            Next
            Dim rv As Object = mi.Invoke(session, parameters)
            If Not mi.ReturnType Is Type.GetType("System.Void") Then ' display return val
                If mi.ReturnType Is GetType(Byte()) Then
                    CType(returnValueControl, TextBox).Text = ByteArrayToString(CType(rv, Byte()))
                ElseIf TypeOf rv Is IAsyncResult Then
                    lastAsyncResult = CType(rv, IAsyncResult)
                    CType(returnValueControl, TextBox).Text = "Async Result Interface object stored for later use."
                Else
                    CType(returnValueControl, TextBox).Text = InsertCommonEscapeSequences(rv.ToString())
                End If
                feedbackLabel.Text = "Method " + mi.Name + " executed successfully."
                StatusTextBox.Text = session.LastStatus.ToString()
            End If
        Catch ex As Exception
            GetInnerException(ex)
        End Try
        SessionTreeView.Focus()
    End Sub

    ' When the Close button that was created dynamically is pressed, it exits methodsForm
    Private Sub RemoveMethodCtrls()
        If Not (paramCtrlSet Is Nothing) Then
            If paramCtrlSet.Count > 0 Then
                Dim ctrl As Control
                For Each ctrl In paramCtrlSet
                    RightPanel.Controls.Remove(ctrl)
                Next
                paramCtrlSet.RemoveRange(0, paramCtrlSet.Count)
            End If
        End If
        If Not (paramLabelCtrlSet Is Nothing) Then
            If paramLabelCtrlSet.Count > 0 Then
                Dim ctrl As Control
                For Each ctrl In paramLabelCtrlSet
                    RightPanel.Controls.Remove(ctrl)
                Next
                paramLabelCtrlSet.RemoveRange(0, paramLabelCtrlSet.Count)
            End If
        End If

        RightPanel.Controls.Remove(returnValueControl)
        RightPanel.Controls.Remove(executeButton)
        RightPanel.Controls.Remove(feedbackLabel)
        returnValueControl = Nothing
        executeButton = Nothing
        feedbackLabel = Nothing
    End Sub

#End Region

    '*************** Methods for Events ***************//
#Region "Methods for Events"

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub PopulateEvents(ByVal parent As TreeNode)
        Try
            Dim evInfoArray As EventInfo() = session.GetType().GetEvents()
            Dim ei As EventInfo
            For Each ei In evInfoArray
                Dim tn As TreeNode = New TreeNode(ei.Name) With {
                    .Tag = ei
                }
                parent.Nodes.Add(tn)
            Next

            InitializeDelegates()
        Catch ex As Exception
            GetInnerException(ex)
        End Try
    End Sub

    Private Sub EventSelected()
        ShowPublicPropertiesCtrls(False)
        ShowPublicMethodsCtrls(False)
        ShowEventsCtrls(True)
    End Sub

    Private Sub ShowEventsCtrls(ByVal show As Boolean)
        registerButton.Visible = show
        unregisterButton.Visible = show
    End Sub

    Private Sub InitializeDelegates()
        GpibCICEventHandler = New GpibInterfaceControllerInChargeEventHandler(AddressOf OnGpibInterfaceControllerInChargeEventHandler)
        SignalProcessorEventHandler = New VxiSessionSignalProcessorEventHandler(AddressOf OnVxiSignalProcessorEventHandler)
        GpibInterfaceTriggerEventHandler = New GpibInterfaceTriggerEventHandler(AddressOf OnGpibInterfaceTriggerEventHandler)
        VxiBackplaneTriggerEventHandler = New VxiBackplaneTriggerEventHandler(AddressOf OnVxiBackplaneTriggerEventHandler)
        VxiSessionTriggerEventHandler = New VxiSessionTriggerEventHandler(AddressOf OnVxiSessionTriggerEventHandler)
        VisaEHandler = New VisaEventHandler(AddressOf OnVisaEventHandler)
        GpibInterfaceEventHandler = New GpibInterfaceEventHandler(AddressOf OnGpibInterfaceEventHandler)
        VxiBackplaneEventHandler = New VxiBackplaneEventHandler(AddressOf OnVxiBackplaneEventHandler)
        MessageSessionEventHandler = New MessageBasedSessionEventHandler(AddressOf OnMessageSessionEventHandler)
        SerialSessionEventHandler = New SerialSessionEventHandler(AddressOf OnSerialSessionEventHandler)
        VxiSessionEventHandler = New VxiSessionEventHandler(AddressOf OnVxiSessionEventHandler)
        PxiSessionEventHandler = New PxiSessionEventHandler(AddressOf OnPxiSessionEventHandler)
        VxiVmeInterruptEventHandler = New VxiSessionVxiVmeInterruptEventHandler(AddressOf OnVxiVmeInterruptEventHandler)
        UsbRawEventHandler = New UsbRawEventHandler(AddressOf OnUsbRawEventHandler)
        UsbRawInterfaceEventHandler = New UsbRawInterruptEventHandler(AddressOf OnUsbRawInterfaceEventHandler)
        UsbSessionEventHandler = New UsbSessionEventHandler(AddressOf OnUsbSessionEventHandler)
        UsbSessionInterfaceEventHandler = New UsbSessionInterruptEventHandler(AddressOf OnUsbSessionInterfaceEventHandler)
    End Sub

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RegisterButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles registerButton.Click
        Try
            Dim ef As EventInfo = CType(sessionTreeView.SelectedNode.Tag, EventInfo)

            Dim handlerType As Type = ef.EventHandlerType
            If (handlerType Is GetType(GpibInterfaceControllerInChargeEventHandler)) Then
                ef.AddEventHandler(session, GpibCICEventHandler)
            ElseIf (handlerType Is GetType(VxiSessionSignalProcessorEventHandler)) Then
                ef.AddEventHandler(session, SignalProcessorEventHandler)
            ElseIf (handlerType Is GetType(GpibInterfaceTriggerEventHandler)) Then
                ef.AddEventHandler(session, GpibInterfaceTriggerEventHandler)
            ElseIf (handlerType Is GetType(VxiBackplaneTriggerEventHandler)) Then
                ef.AddEventHandler(session, VxiBackplaneTriggerEventHandler)
            ElseIf (handlerType Is GetType(VxiSessionTriggerEventHandler)) Then
                ef.AddEventHandler(session, VxiSessionTriggerEventHandler)
            ElseIf (handlerType Is GetType(VisaEventHandler)) Then
                ef.AddEventHandler(session, VisaEHandler)
            ElseIf (handlerType Is GetType(GpibInterfaceEventHandler)) Then
                ef.AddEventHandler(session, GpibInterfaceEventHandler)
            ElseIf (handlerType Is GetType(VxiBackplaneEventHandler)) Then
                ef.AddEventHandler(session, VxiBackplaneEventHandler)
            ElseIf (handlerType Is GetType(MessageBasedSessionEventHandler)) Then
                ef.AddEventHandler(session, MessageSessionEventHandler)
            ElseIf (handlerType Is GetType(SerialSessionEventHandler)) Then
                ef.AddEventHandler(session, SerialSessionEventHandler)
            ElseIf (handlerType Is GetType(VxiSessionEventHandler)) Then
                ef.AddEventHandler(session, VxiSessionEventHandler)
            ElseIf (handlerType Is GetType(PxiSessionEventHandler)) Then
                ef.AddEventHandler(session, PxiSessionEventHandler)
            ElseIf (handlerType Is GetType(VxiSessionVxiVmeInterruptEventHandler)) Then
                ef.AddEventHandler(session, VxiVmeInterruptEventHandler)
            ElseIf (handlerType Is GetType(UsbRawEventHandler)) Then
                ef.AddEventHandler(session, UsbRawEventHandler)
            ElseIf (handlerType Is GetType(UsbRawInterruptEventHandler)) Then
                ef.AddEventHandler(session, UsbRawInterfaceEventHandler)
            ElseIf (handlerType Is GetType(UsbSessionEventHandler)) Then
                ef.AddEventHandler(session, UsbSessionEventHandler)
            ElseIf (handlerType Is GetType(UsbSessionInterruptEventHandler)) Then
                ef.AddEventHandler(session, UsbSessionInterfaceEventHandler)
            End If
            statusTextBox.Text = session.LastStatus.ToString
        Catch ex As Exception
            GetInnerException(ex)
        End Try
        sessionTreeView.Focus()
    End Sub

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub UnregisterButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles unregisterButton.Click
        eventFeedbackTextBox.Text = ""
        Try
            Dim ef As EventInfo = CType(sessionTreeView.SelectedNode.Tag, EventInfo)

            Dim handlerType As Type = ef.EventHandlerType
            If (handlerType Is GetType(GpibInterfaceControllerInChargeEventHandler)) Then
                ef.RemoveEventHandler(session, GpibCICEventHandler)
            ElseIf (handlerType Is GetType(VxiSessionSignalProcessorEventHandler)) Then
                ef.RemoveEventHandler(session, SignalProcessorEventHandler)
            ElseIf (handlerType Is GetType(GpibInterfaceTriggerEventHandler)) Then
                ef.RemoveEventHandler(session, GpibInterfaceTriggerEventHandler)
            ElseIf (handlerType Is GetType(VxiBackplaneTriggerEventHandler)) Then
                ef.RemoveEventHandler(session, VxiBackplaneTriggerEventHandler)
            ElseIf (handlerType Is GetType(VxiSessionTriggerEventHandler)) Then
                ef.RemoveEventHandler(session, VxiSessionTriggerEventHandler)
            ElseIf (handlerType Is GetType(VisaEventHandler)) Then
                ef.RemoveEventHandler(session, VisaEHandler)
            ElseIf (handlerType Is GetType(GpibInterfaceEventHandler)) Then
                ef.RemoveEventHandler(session, GpibInterfaceEventHandler)
            ElseIf (handlerType Is GetType(VxiBackplaneEventHandler)) Then
                ef.RemoveEventHandler(session, VxiBackplaneEventHandler)
            ElseIf (handlerType Is GetType(MessageBasedSessionEventHandler)) Then
                ef.RemoveEventHandler(session, MessageSessionEventHandler)
            ElseIf (handlerType Is GetType(SerialSessionEventHandler)) Then
                ef.RemoveEventHandler(session, SerialSessionEventHandler)
            ElseIf (handlerType Is GetType(VxiSessionEventHandler)) Then
                ef.RemoveEventHandler(session, VxiSessionEventHandler)
            ElseIf (handlerType Is GetType(PxiSessionEventHandler)) Then
                ef.RemoveEventHandler(session, PxiSessionEventHandler)
            ElseIf (handlerType Is GetType(VxiSessionVxiVmeInterruptEventHandler)) Then
                ef.RemoveEventHandler(session, VxiVmeInterruptEventHandler)
            ElseIf (handlerType Is GetType(UsbRawEventHandler)) Then
                ef.RemoveEventHandler(session, UsbRawEventHandler)
            ElseIf (handlerType Is GetType(UsbRawInterruptEventHandler)) Then
                ef.RemoveEventHandler(session, UsbRawInterfaceEventHandler)
            ElseIf (handlerType Is GetType(UsbSessionEventHandler)) Then
                ef.RemoveEventHandler(session, UsbSessionEventHandler)
            ElseIf (handlerType Is GetType(UsbSessionInterruptEventHandler)) Then
                ef.RemoveEventHandler(session, UsbSessionInterfaceEventHandler)
            End If
            statusTextBox.Text = session.LastStatus.ToString
        Catch ex As Exception
            GetInnerException(ex)
        End Try
        sessionTreeView.Focus()
    End Sub

    Public Sub OnGpibInterfaceControllerInChargeEventHandler(ByVal sender As Object, ByVal e As GpibInterfaceControllerInChargeEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Controller In Charge Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnVxiSignalProcessorEventHandler(ByVal sender As Object, ByVal e As VxiSessionSignalProcessorEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Vxi Signal Processor Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnGpibInterfaceTriggerEventHandler(ByVal sender As Object, ByVal e As GpibInterfaceTriggerEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Gpib Interface Trigger Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnVxiBackplaneTriggerEventHandler(ByVal sender As Object, ByVal e As VxiBackplaneTriggerEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Vxi Backplane Trigger Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnVxiSessionTriggerEventHandler(ByVal sender As Object, ByVal e As VxiSessionTriggerEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Vxi Session Trigger Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnVisaEventHandler(ByVal sender As Object, ByVal e As VisaEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Visa Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnGpibInterfaceEventHandler(ByVal sender As Object, ByVal e As GpibInterfaceEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Gpib Interface Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnVxiBackplaneEventHandler(ByVal sender As Object, ByVal e As VxiBackplaneEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Vxi Backplane Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnMessageSessionEventHandler(ByVal sender As Object, ByVal e As MessageBasedSessionEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Message Based Session Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnSerialSessionEventHandler(ByVal sender As Object, ByVal e As SerialSessionEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Serial Session Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnVxiSessionEventHandler(ByVal sender As Object, ByVal e As VxiSessionEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Vxi Session Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnPxiSessionEventHandler(ByVal sender As Object, ByVal e As PxiSessionEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Pxi Session Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnVxiVmeInterruptEventHandler(ByVal sender As Object, ByVal e As VxiSessionVxiVmeInterruptEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Vme Interrupt Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnUsbRawEventHandler(ByVal sender As Object, ByVal e As UsbRawEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Usb Raw Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnUsbRawInterfaceEventHandler(ByVal sender As Object, ByVal e As UsbRawInterruptEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Usb Raw Interrupt Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnUsbSessionEventHandler(ByVal sender As Object, ByVal e As UsbSessionEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Usb Session Event Handler is caught. " + e.ToString()
        End If
    End Sub

    Public Sub OnUsbSessionInterfaceEventHandler(ByVal sender As Object, ByVal e As UsbSessionInterruptEventArgs)
        If e IsNot Nothing Then
            eventFeedbackTextBox.Text = "Usb Session Interrupt Event Handler is caught. " + e.ToString()
        End If
    End Sub
#End Region

End Class
