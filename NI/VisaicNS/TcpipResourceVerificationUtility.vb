'================================================================================================== 
' 
' Title      : TcpipResourceVerificationUtility.cs
' (c)  : National Instruments 2002. All Rights Reserved. 
' Author     : Mika Fukuchi 
' Purpose    : This application shows the user how to verify the existence of
'				TCP/IP resources. Property TcpipResourceNames is a string array that	
'				contains the verified TCP/IP resource names.
' 
'================================================================================================== 
Imports NationalInstruments.VisaNS

Public Class TcpipResourceVerificationUtilityForm
    Inherits TopDialogBase

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Private WithEvents InstrumentGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents SocketGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents BoardResourceLabel As System.Windows.Forms.Label
    Private WithEvents HostAddResourceLabel As System.Windows.Forms.Label
    Private WithEvents LANLabel As System.Windows.Forms.Label
    Private WithEvents VerifyResourceButton As System.Windows.Forms.Button
    Private WithEvents BoardSocketLabel As System.Windows.Forms.Label
    Private WithEvents HostAddSocketLabel As System.Windows.Forms.Label
    Private WithEvents PortNumberLabel As System.Windows.Forms.Label
    Private WithEvents VerifySocketButton As System.Windows.Forms.Button
    Private WithEvents TCPIPResourcesLabel As System.Windows.Forms.Label
    Private WithEvents OptionalLabel As System.Windows.Forms.Label
    Private WithEvents DeviceNameTextBox As System.Windows.Forms.TextBox
    Private WithEvents HostAddressInstrTextBox As System.Windows.Forms.TextBox
    Private WithEvents BoardInstrTextBox As System.Windows.Forms.TextBox
    Private WithEvents PortNumberTextBox As System.Windows.Forms.TextBox
    Private WithEvents HostAddressSocketTextBox As System.Windows.Forms.TextBox
    Private WithEvents BoardSocketTextBox As System.Windows.Forms.TextBox
    Private WithEvents ResourcesVerifiedListBox As System.Windows.Forms.ListBox
    Private WithEvents OKButton As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(TcpipResourceVerificationUtilityForm))
        Me.instrumentGroupBox = New System.Windows.Forms.GroupBox
        Me.optionalLabel = New System.Windows.Forms.Label
        Me.verifyResourceButton = New System.Windows.Forms.Button
        Me.deviceNameTextBox = New System.Windows.Forms.TextBox
        Me.hostAddressInstrTextBox = New System.Windows.Forms.TextBox
        Me.boardInstrTextBox = New System.Windows.Forms.TextBox
        Me.LANLabel = New System.Windows.Forms.Label
        Me.hostAddResourceLabel = New System.Windows.Forms.Label
        Me.boardResourceLabel = New System.Windows.Forms.Label
        Me.socketGroupBox = New System.Windows.Forms.GroupBox
        Me.portNumberTextBox = New System.Windows.Forms.TextBox
        Me.hostAddressSocketTextBox = New System.Windows.Forms.TextBox
        Me.boardSocketTextBox = New System.Windows.Forms.TextBox
        Me.verifySocketButton = New System.Windows.Forms.Button
        Me.portNumberLabel = New System.Windows.Forms.Label
        Me.hostAddSocketLabel = New System.Windows.Forms.Label
        Me.boardSocketLabel = New System.Windows.Forms.Label
        Me.TCPIPResourcesLabel = New System.Windows.Forms.Label
        Me.resourcesVerifiedListBox = New System.Windows.Forms.ListBox
        Me.OKButton = New System.Windows.Forms.Button
        Me.instrumentGroupBox.SuspendLayout()
        Me.socketGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'instrumentGroupBox
        '
        Me.instrumentGroupBox.Controls.Add(Me.optionalLabel)
        Me.instrumentGroupBox.Controls.Add(Me.verifyResourceButton)
        Me.instrumentGroupBox.Controls.Add(Me.deviceNameTextBox)
        Me.instrumentGroupBox.Controls.Add(Me.hostAddressInstrTextBox)
        Me.instrumentGroupBox.Controls.Add(Me.boardInstrTextBox)
        Me.instrumentGroupBox.Controls.Add(Me.LANLabel)
        Me.instrumentGroupBox.Controls.Add(Me.hostAddResourceLabel)
        Me.instrumentGroupBox.Controls.Add(Me.boardResourceLabel)
        Me.instrumentGroupBox.Location = New System.Drawing.Point(16, 16)
        Me.instrumentGroupBox.Name = "instrumentGroupBox"
        Me.instrumentGroupBox.Size = New System.Drawing.Size(256, 152)
        Me.instrumentGroupBox.TabIndex = 0
        Me.instrumentGroupBox.TabStop = False
        Me.instrumentGroupBox.Text = "TCP/IP Instrument Resource"
        '
        'optionalLabel
        '
        Me.optionalLabel.Location = New System.Drawing.Point(8, 104)
        Me.optionalLabel.Name = "optionalLabel"
        Me.optionalLabel.Size = New System.Drawing.Size(64, 16)
        Me.optionalLabel.TabIndex = 7
        Me.optionalLabel.Text = "(optional)"
        '
        'verifyResourceButton
        '
        Me.verifyResourceButton.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                                System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.verifyResourceButton.Location = New System.Drawing.Point(96, 120)
        Me.verifyResourceButton.Name = "verifyResourceButton"
        Me.verifyResourceButton.Size = New System.Drawing.Size(72, 24)
        Me.verifyResourceButton.TabIndex = 6
        Me.verifyResourceButton.Text = "Verify"
        '
        'deviceNameTextBox
        '
        Me.deviceNameTextBox.Location = New System.Drawing.Point(112, 88)
        Me.deviceNameTextBox.Name = "deviceNameTextBox"
        Me.deviceNameTextBox.Size = New System.Drawing.Size(128, 20)
        Me.deviceNameTextBox.TabIndex = 5
        Me.deviceNameTextBox.Text = ""
        '
        'hostAddressInstrTextBox
        '
        Me.hostAddressInstrTextBox.Location = New System.Drawing.Point(112, 56)
        Me.hostAddressInstrTextBox.Name = "hostAddressInstrTextBox"
        Me.hostAddressInstrTextBox.Size = New System.Drawing.Size(128, 20)
        Me.hostAddressInstrTextBox.TabIndex = 4
        Me.hostAddressInstrTextBox.Text = ""
        '
        'boardInstrTextBox
        '
        Me.boardInstrTextBox.Location = New System.Drawing.Point(112, 24)
        Me.boardInstrTextBox.Name = "boardInstrTextBox"
        Me.boardInstrTextBox.Size = New System.Drawing.Size(128, 20)
        Me.boardInstrTextBox.TabIndex = 3
        Me.boardInstrTextBox.Text = ""
        '
        'LANLabel
        '
        Me.LANLabel.Location = New System.Drawing.Point(8, 88)
        Me.LANLabel.Name = "LANLabel"
        Me.LANLabel.Size = New System.Drawing.Size(104, 16)
        Me.LANLabel.TabIndex = 2
        Me.LANLabel.Text = "LAN Device Name"
        '
        'hostAddResourceLabel
        '
        Me.hostAddResourceLabel.Location = New System.Drawing.Point(8, 56)
        Me.hostAddResourceLabel.Name = "hostAddResourceLabel"
        Me.hostAddResourceLabel.Size = New System.Drawing.Size(96, 16)
        Me.hostAddResourceLabel.TabIndex = 1
        Me.hostAddResourceLabel.Text = "Host Address"
        '
        'boardResourceLabel
        '
        Me.boardResourceLabel.Location = New System.Drawing.Point(8, 24)
        Me.boardResourceLabel.Name = "boardResourceLabel"
        Me.boardResourceLabel.Size = New System.Drawing.Size(104, 16)
        Me.boardResourceLabel.TabIndex = 0
        Me.boardResourceLabel.Text = "Board (optional)"
        '
        'socketGroupBox
        '
        Me.socketGroupBox.Controls.Add(Me.portNumberTextBox)
        Me.socketGroupBox.Controls.Add(Me.hostAddressSocketTextBox)
        Me.socketGroupBox.Controls.Add(Me.boardSocketTextBox)
        Me.socketGroupBox.Controls.Add(Me.verifySocketButton)
        Me.socketGroupBox.Controls.Add(Me.portNumberLabel)
        Me.socketGroupBox.Controls.Add(Me.hostAddSocketLabel)
        Me.socketGroupBox.Controls.Add(Me.boardSocketLabel)
        Me.socketGroupBox.Location = New System.Drawing.Point(16, 184)
        Me.socketGroupBox.Name = "socketGroupBox"
        Me.socketGroupBox.Size = New System.Drawing.Size(256, 152)
        Me.socketGroupBox.TabIndex = 1
        Me.socketGroupBox.TabStop = False
        Me.socketGroupBox.Text = "TCP/IP Socket Resource"
        '
        'portNumberTextBox
        '
        Me.portNumberTextBox.Location = New System.Drawing.Point(112, 88)
        Me.portNumberTextBox.Name = "portNumberTextBox"
        Me.portNumberTextBox.Size = New System.Drawing.Size(128, 20)
        Me.portNumberTextBox.TabIndex = 6
        Me.portNumberTextBox.Text = ""
        '
        'hostAddressSocketTextBox
        '
        Me.hostAddressSocketTextBox.Location = New System.Drawing.Point(112, 56)
        Me.hostAddressSocketTextBox.Name = "hostAddressSocketTextBox"
        Me.hostAddressSocketTextBox.Size = New System.Drawing.Size(128, 20)
        Me.hostAddressSocketTextBox.TabIndex = 5
        Me.hostAddressSocketTextBox.Text = ""
        '
        'boardSocketTextBox
        '
        Me.boardSocketTextBox.Location = New System.Drawing.Point(112, 24)
        Me.boardSocketTextBox.Name = "boardSocketTextBox"
        Me.boardSocketTextBox.Size = New System.Drawing.Size(128, 20)
        Me.boardSocketTextBox.TabIndex = 4
        Me.boardSocketTextBox.Text = ""
        '
        'verifySocketButton
        '
        Me.verifySocketButton.Location = New System.Drawing.Point(96, 120)
        Me.verifySocketButton.Name = "verifySocketButton"
        Me.verifySocketButton.TabIndex = 3
        Me.verifySocketButton.Text = "Verify"
        '
        'portNumberLabel
        '
        Me.portNumberLabel.Location = New System.Drawing.Point(8, 88)
        Me.portNumberLabel.Name = "portNumberLabel"
        Me.portNumberLabel.Size = New System.Drawing.Size(88, 24)
        Me.portNumberLabel.TabIndex = 2
        Me.portNumberLabel.Text = "Port Number"
        '
        'hostAddSocketLabel
        '
        Me.hostAddSocketLabel.Location = New System.Drawing.Point(8, 56)
        Me.hostAddSocketLabel.Name = "hostAddSocketLabel"
        Me.hostAddSocketLabel.Size = New System.Drawing.Size(96, 16)
        Me.hostAddSocketLabel.TabIndex = 1
        Me.hostAddSocketLabel.Text = "Host Address"
        '
        'boardSocketLabel
        '
        Me.boardSocketLabel.Location = New System.Drawing.Point(8, 24)
        Me.boardSocketLabel.Name = "boardSocketLabel"
        Me.boardSocketLabel.Size = New System.Drawing.Size(96, 16)
        Me.boardSocketLabel.TabIndex = 0
        Me.boardSocketLabel.Text = "Board (optional)"
        '
        'TCPIPResourcesLabel
        '
        Me.TCPIPResourcesLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                               System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TCPIPResourcesLabel.Location = New System.Drawing.Point(288, 24)
        Me.TCPIPResourcesLabel.Name = "TCPIPResourcesLabel"
        Me.TCPIPResourcesLabel.Size = New System.Drawing.Size(168, 16)
        Me.TCPIPResourcesLabel.TabIndex = 2
        Me.TCPIPResourcesLabel.Text = "TCP/IP Resources Verified"
        '
        'resourcesVerifiedListBox
        '
        Me.resourcesVerifiedListBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) Or
                                                     System.Windows.Forms.AnchorStyles.Left) Or
                                                 System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.resourcesVerifiedListBox.Location = New System.Drawing.Point(288, 48)
        Me.resourcesVerifiedListBox.Name = "resourcesVerifiedListBox"
        Me.resourcesVerifiedListBox.Size = New System.Drawing.Size(248, 251)
        Me.resourcesVerifiedListBox.TabIndex = 3
        '
        'OKButton
        '
        Me.OKButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OKButton.Location = New System.Drawing.Point(464, 312)
        Me.OKButton.Name = "OKButton"
        Me.OKButton.TabIndex = 4
        Me.OKButton.Text = "OK"
        '
        'TcpipResourceVerificationUtilityForm
        '
        Me.ClientSize = New System.Drawing.Size(560, 357)
        Me.Controls.Add(Me.OKButton)
        Me.Controls.Add(Me.resourcesVerifiedListBox)
        Me.Controls.Add(Me.TCPIPResourcesLabel)
        Me.Controls.Add(Me.socketGroupBox)
        Me.Controls.Add(Me.instrumentGroupBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(568, 384)
        Me.Name = "TcpipResourceVerificationUtilityForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "TCP/IP Resource Verification Utility"
        Me.instrumentGroupBox.ResumeLayout(False)
        Me.socketGroupBox.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub BtnVerifyInstr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles verifyResourceButton.Click
        Dim resourceName As String

        If boardInstrTextBox.TextLength = 0 Then
            resourceName = "TCPIP::" + hostAddressInstrTextBox.Text
        Else
            resourceName = "TCPIP" + boardInstrTextBox.Text + "::" + hostAddressInstrTextBox.Text
        End If

        If deviceNameTextBox.TextLength = 0 Then
            resourceName += "::INSTR"
        Else
            resourceName += "::" + deviceNameTextBox.Text + "::INSTR"
        End If

        VerifyAndUpdateResourcename(resourceName)
    End Sub

    Private Sub BtnVerifySocket_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles verifySocketButton.Click
        Dim resourceName As String
        If boardSocketTextBox.TextLength = 0 Then
            resourceName = "TCPIP::" + hostAddressSocketTextBox.Text
        Else
            resourceName = "TCPIP" + boardSocketTextBox.Text + "::" + hostAddressSocketTextBox.Text
        End If

        resourceName += "::" + portNumberTextBox.Text + "::SOCKET"
        VerifyAndUpdateResourcename(resourceName)
    End Sub

    Private Sub VerifyAndUpdateResourcename(ByVal resourceName As String)
        Dim resourceFullName As String = ValidResourceName(resourceName)
        If resourceFullName <> Nothing Then
            If Not resourcesVerifiedListBox.Items.Contains(resourceFullName) Then
                resourcesVerifiedListBox.Items.Add(resourceFullName)
            End If
        Else
            MessageBox.Show("Invalid Resource Name", "Invalid resource name", MessageBoxButtons.OK,
                            MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End If
    End Sub

    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Shared Function ValidResourceName(ByVal resourceName As String) As String
        Dim fullName As String = String.Empty
        Try
            Using session As Session = ResourceManager.GetLocalManager().Open(resourceName)
                fullName = session.ResourceName
            End Using
        Catch ex As VisaException
            ' Don't do anything
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Exception Occurred", MessageBoxButtons.OK,
                            MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
        Return fullName
    End Function

    Private Sub OKButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OKButton.Click
        Me.Close()
    End Sub

    Public ReadOnly Property TcpipResourceNames() As ObjectModel.ReadOnlyCollection(Of String)
        Get
            Dim resourceNames(resourcesVerifiedListBox.Items.Count - 1) As String
            resourcesVerifiedListBox.Items.CopyTo(resourceNames, 0)
            Return New ObjectModel.ReadOnlyCollection(Of String)(resourceNames)
        End Get
    End Property
End Class
