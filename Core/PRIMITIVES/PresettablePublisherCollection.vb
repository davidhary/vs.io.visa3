﻿Imports System.Threading
Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.EventHandlerExtensions
''' <summary> A collection of subsystem elements. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="01/21/2005" by="David" revision="1.0.1847.x"> Created. </history>
Public Class PresettablePublisherCollection
    Inherits Collections.ObjectModel.Collection(Of PresettablePublisherBase)
    Implements IPresettablePublisher, ITraceMessagePublisher

#Region " CONSTRUCTORS "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.SyncContext = SynchronizationContext.Current
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Clears the queues and resets all registers to zero. Sets the subsystem properties to
    ''' the following CLS default values:<para>
    ''' </para> </summary>
    Public Sub ClearExecutionState() Implements IPresettable.ClearExecutionState
        For Each element As IPresettable In MyBase.Items
            element.ClearExecutionState()
        Next
    End Sub

    ''' <summary> Performs a reset and additional custom setting for the subsystem:<para>
    ''' </para> </summary>
    Public Sub InitializeKnownState() Implements IPresettable.InitializeKnownState
        For Each element As IPresettable In MyBase.Items
            element.InitializeKnownState()
        Next
    End Sub

    ''' <summary> Gets subsystem to the following default system preset values:<para>
    ''' </para> </summary>
    Public Sub PresetKnownState() Implements IPresettable.PresetKnownState
        For Each element As IPresettable In MyBase.Items
            element.PresetKnownState()
        Next
    End Sub

    ''' <summary> Restore member properties to the following RST or System Preset values:<para>
    ''' </para> </summary>
    Public Sub ResetKnownState() Implements IPresettable.ResetKnownState
        For Each element As IPresettable In MyBase.Items
            element.ResetKnownState()
        Next
    End Sub

#End Region

#Region " PUBLISHER "

    ''' <summary> Gets or sets the publishable sentinel. </summary>
    ''' <value> The publishable. </value>
    Public Property Publishable As Boolean Implements IPublisher.Publishable

    ''' <summary> Publishes all values. </summary>
    Public Sub Publish() Implements IPublisher.Publish
        For Each element As IPublisher In MyBase.Items
            element.Publish()
        Next
    End Sub

    ''' <summary> Resume property events. </summary>
    Public Sub ResumePublishing() Implements IPublisher.ResumePublishing
        For Each element As IPublisher In MyBase.Items
            element.ResumePublishing()
            Me.Publishable = element.Publishable
        Next
    End Sub

    ''' <summary> Suspend publishing. </summary>
    Public Sub SuspendPublishing() Implements IPublisher.SuspendPublishing
        For Each element As IPublisher In MyBase.Items
            element.SuspendPublishing()
            Me.Publishable = element.Publishable
        Next
    End Sub

#End Region

#Region " TRACE MESSAGE AVAILABLE EVENT IMPLEMENTATION "

    ''' <summary> Propagates a sync context. </summary>
    ''' <value> The synchronization context. </value>
    Protected Property SyncContext As SynchronizationContext

    ''' <summary> Event queue for all listeners interested in TraceMessageAvailable events. </summary>
    Public Event TraceMessageAvailable As EventHandler(Of TraceMessageEventArgs) Implements ITraceMessagePublisher.TraceMessageAvailable

#Region " INVOKE "

    ''' <summary> Synchronously notifies (invokes) the <see cref="TraceMessageAvailable">trace message
    ''' available Event</see>. Must be called with the <see cref="SynchronizationContext">sync
    ''' context</see> </summary>
    ''' <param name="e"> The <see cref="TraceMessageEventArgs" /> instance containing the event data. </param>
    Private Sub SyncNotifyTraceMessageAvailable(ByVal e As TraceMessageEventArgs)
        Dim evt As EventHandler(Of TraceMessageEventArgs) = Me.TraceMessageAvailableEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (invokes) the <see cref="TraceMessageAvailable">trace message
    ''' available Event</see>. Must be called with the <see cref="SynchronizationContext">sync
    ''' context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub SyncNotifyTraceMessageAvailable(ByVal obj As Object)
        Me.SyncNotifyTraceMessageAvailable(CType(obj, TraceMessageEventArgs))
    End Sub

    ''' <summary> Asynchronously notifies (safe begin invoke targets) the
    ''' <see cref="TraceMessageAvailable">trace message available Event</see>. Must be called with
    ''' the <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="TraceMessageEventArgs" /> instance containing the event data. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub AsyncNotifyTraceMessageAvailable(ByVal e As TraceMessageEventArgs)
        Me.TraceMessageAvailableEvent.SafeBeginInvoke(Me, e)
    End Sub

#End Region

#Region " I TRACE MESSAGE PUBLISHER "

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <remarks> Override this method in cases the trace message needs to be used (e.g., displayed or logged) before it is raised. </remarks>
    ''' <param name="value"> The Trace Message to process. </param>
    Protected Overridable Sub OnTraceMessageAvailable(ByVal value As TraceMessage) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If value IsNot Nothing Then
            If Me.SyncContext Is Nothing Then
                ' It is possible that the current thread does not have a synchronization context object; or
                ' a synchronization context has not been set for this thread. Creating an instance of a 
                ' synchronization context can yield unexpected results if the Current property of the 
                ' synchronization context is checked from a thread other than the thread on which the UI 
                ' is running, in which case the context will be null. Rather, we try to check the current
                ' property when the event is called. 
                Me.SyncContext = SynchronizationContext.Current
            End If
            If Me.SyncContext Is Nothing Then
                Debug.Assert(Not Debugger.IsAttached, "lost the sync context")
                Me.AsyncNotifyTraceMessageAvailable(New TraceMessageEventArgs(value))
            Else
                Me.SyncContext.Post(New SendOrPostCallback(AddressOf Me.SyncNotifyTraceMessageAvailable), New TraceMessageEventArgs(value))
            End If
        End If
    End Sub

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> The event arguments. </returns>
    Protected Function OnTraceMessageAvailable(eventType As TraceEventType, ByVal id As Integer,
                                               format As String, ParamArray args() As Object) As TraceMessage Implements ITraceMessagePublisher.OnTraceMessageAvailable
        Dim e As New TraceMessage(eventType, id, format, args)
        Me.OnTraceMessageAvailable(e)
        Return e
    End Function

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnTraceMessageAvailable(ByVal e As TraceMessageEventArgs) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If e IsNot Nothing Then
            Me.OnTraceMessageAvailable(e.TraceMessage)
        End If
    End Sub

#End Region

#End Region

End Class