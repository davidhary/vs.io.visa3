Imports NationalInstruments
Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.EnumExtensions
Namespace Scpi

    ''' <summary> Defines the contract that must be implemented by a SCPI Source Subsystem. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1501:AvoidExcessiveInheritance")>
    Public MustInherit Class SourceSubsystemBase
        Inherits Visa.SourceSubsystemBase

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="SourceSubsystemBase" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">status subsystem</see>. </param>
        Protected Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " AUTO CLEAR "

        ''' <summary> Queries the source AutoClear state. </summary>
        ''' <returns> <c>True</c> if the Auto Clear is enabled; <c>False</c> if not enabled, or none if
        ''' unknown. </returns>
        Public Overrides Function QueryAutoClearEnabled() As Boolean?
            If Me.IsSessionOpen Then
                MyBase.AutoClearEnabled = Me.Session.Query(True, ":SOUR:CLE:AUTO?")
            End If
            Return MyBase.AutoClearEnabled
        End Function

        ''' <summary> Writes the state of the Source Auto Clear without reading back the value from the
        ''' device. </summary>
        ''' <param name="value"> Enable if <c>true</c>; otherwise, disable. </param>
        ''' <returns> <c>True</c> if the Auto Clear is enabled; <c>False</c> if not enabled, or none if
        ''' unknown. </returns>
        Public Overrides Function WriteAutoClearEnabled(ByVal value As Boolean) As Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SOUR:CLE:AUTO {0:'ON';'ON';'OFF'}", CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                MyBase.AutoClearEnabled = New Boolean?
            Else
                MyBase.AutoClearEnabled = value
            End If
            Return MyBase.AutoClearEnabled
        End Function

#End Region

#Region " AUTO DELAY "

        ''' <summary> Queries the Source Auto Delay state. </summary>
        ''' <returns> <c>True</c> if the Auto Delay is enabled; <c>False</c> if not enabled, or none if
        ''' unknown or not set. </returns>
        Public Overrides Function QueryAutoDelayEnabled() As Boolean?
            If Me.IsSessionOpen Then
                MyBase.AutoDelayEnabled = Me.Session.Query(True, ":SOUR:DEL:AUTO?")
            End If
            Return MyBase.AutoDelayEnabled
        End Function

        ''' <summary> Writes the state of the Source Auto Delay without reading back the value from the
        ''' device. </summary>
        ''' <param name="value"> Enable if <c>true</c>; otherwise, disable. </param>
        ''' <returns> <c>True</c> if the Auto Delay is enabled; <c>False</c> if not enabled, or none if
        ''' unknown or not set. </returns>
        Public Overrides Function WriteAutoDelayEnabled(ByVal value As Boolean) As Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SOUR:DEL:AUTO {0:'ON';'ON';'OFF'}", CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                MyBase.AutoDelayEnabled = New Boolean?
            Else
                MyBase.AutoDelayEnabled = value
            End If
            Return MyBase.AutoDelayEnabled
        End Function

#End Region

#Region " FUNCTION MODE "

        ''' <summary> Queries the Source Function Mode. </summary>
        ''' <returns> The <see cref="SourceFunctionMode">source Function Mode</see> or none if unknown. </returns>
        Public Overrides Function QueryFunctionMode() As SourceFunctionMode?
            Dim mode As String = Me.FunctionMode.ToString
            If Me.IsSessionOpen Then
                mode = Me.Session.QueryTrimEnd(":SOUR:FUNC?")
            End If
            If String.IsNullOrWhiteSpace(mode) Then
                Dim message As String = "Failed fetching source function mode"
                Debug.Assert(Not Debugger.IsAttached, message)
                MyBase.FunctionMode = New SourceFunctionMode?
            Else
                Dim se As New StringEnumerator(Of SourceFunctionMode)
                MyBase.FunctionMode = se.ParseContained(mode.BuildDelimitedValue)
            End If
            Return MyBase.FunctionMode
        End Function

        ''' <summary> Writes the source Function Mode without reading back the value from the device. </summary>
        ''' <param name="value"> The Function Mode. </param>
        ''' <returns> The <see cref="SourceFunctionMode">source Function Mode</see> or none if unknown. </returns>
        Public Overrides Function WriteFunctionMode(ByVal value As SourceFunctionMode) As SourceFunctionMode?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SOUR:FUNC {0}", value.ExtractBetween())
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                MyBase.FunctionMode = New SourceFunctionMode?
            Else
                MyBase.FunctionMode = value
            End If
            Return MyBase.FunctionMode
        End Function

#End Region

#Region " SWEEP POINTS "

        ''' <summary> Queries the current Sweep Points. </summary>
        ''' <returns> The SweepPoints or none if unknown. </returns>
        Public Overrides Function QuerySweepPoints() As Integer?
            If Me.IsSessionOpen Then
                MyBase.SweepPoints = Me.Session.Query(0I, ":SOUR:SWE:POIN??")
            End If
            Return MyBase.SweepPoints
        End Function

        ''' <summary> Sets back the source Sweep Points without reading back the value from the device. </summary>
        ''' <param name="value"> The current Sweep Points. </param>
        ''' <returns> The SweepPoints or none if unknown. </returns>
        Public Overrides Function WriteSweepPoints(ByVal value As Integer) As Integer?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SOUR:SWE:POIN? {0}", value)
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                MyBase.SweepPoints = New Integer?
            Else
                MyBase.SweepPoints = value
            End If
            Return MyBase.SweepPoints
        End Function

#End Region

    End Class

End Namespace
