Imports NationalInstruments
Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.EnumExtensions
Namespace Scpi

    ''' <summary> Defines the contract that must be implemented by a SCPI Output Subsystem. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1501:AvoidExcessiveInheritance")>
    Public MustInherit Class OutputSubsystemBase
        Inherits Visa.OutputSubsystemBase

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="OutputSubsystemBase" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">status subsystem</see>. </param>
        Protected Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " OFF MODE "

        ''' <summary> Queries the output Off Mode. Also sets the <see cref="OffMode"></see> cached value. </summary>
        ''' <returns> The output off mode or null if unknown. </returns>
        Public Overrides Function QueryOffMode() As OutputOffMode?
            Dim mode As String = Me.OffMode.ToString
            If Me.IsSessionOpen Then
                mode = Me.Session.QueryTrimEnd(":OUTP:SMOD?")
            End If
            If String.IsNullOrWhiteSpace(mode) Then
                Me.OffMode = New OutputOffMode?
            Else
                Dim se As New StringEnumerator(Of OutputOffMode)
                Me.OffMode = se.ParseContained(mode.BuildDelimitedValue)
            End If
            Return Me.OffMode
        End Function

        ''' <summary> Writes the output off mode. Does not read back from the instrument. </summary>
        ''' <param name="value"> The off mode. </param>
        ''' <returns> The output off mode or null if unknown. </returns>
        Public Overrides Function WriteOffMode(ByVal value As OutputOffMode) As OutputOffMode?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":OUTP:SMOD {0}", value.ExtractBetween())
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.OffMode = New OutputOffMode?
            Else
                Me.OffMode = value
            End If
            Return Me.OffMode
        End Function

#End Region

#Region " ON/OFF STATE "

        ''' <summary> Queries the output on/off state. Also sets the <see cref="OutputOnState">output
        ''' on</see> sentinel. </summary>
        ''' <returns> <c>True</c> if on; otherwise <c>False</c>. </returns>
        Public Overrides Function QueryOutputOnState() As Boolean?
            If Me.IsSessionOpen Then
                Me.OutputOnState = Me.Session.Query(True, ":OUTP:STAT?")
            End If
            Return Me.OutputOnState
        End Function

        ''' <summary> Writes the output on/off state. Does not read back from the instrument. </summary>
        ''' <param name="value"> if set to <c>True</c> [is on]. </param>
        ''' <returns> <c>True</c> if on; otherwise <c>False</c>. </returns>
        Public Overrides Function WriteOutputOnState(ByVal value As Boolean) As Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":OUTP:STAT {0:'1';'1';'0'}", CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.OutputOnState = New Boolean?
            Else
                Me.OutputOnState = value
            End If
            Return Me.OutputOnState
        End Function

#End Region

    End Class

End Namespace

