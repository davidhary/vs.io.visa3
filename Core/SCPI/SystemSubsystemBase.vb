Imports NationalInstruments
Imports System.ComponentModel

Namespace Scpi

    ''' <summary> Defines the contract that must be implemented by SCPI System Subsystem. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1501:AvoidExcessiveInheritance")>
    Public MustInherit Class SystemSubsystemBase
        Inherits Visa.SystemSubsystemBase

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="SystemSubsystemBase" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">status subsystem</see>. </param>
        Protected Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
            Me._AddPresettableValues()
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Adds Presettable values. </summary>
        Private Sub _AddPresettableValues()
            Me._ScpiRevision = New PresettableValue(Of Double)("ScpiRevision", New Double?)
            MyBase.AddValue(Me._ScpiRevision)
        End Sub

        ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
        ''' <remarks> Override this method to customize the reset.<para>
        ''' Additional Actions: </para><para>
        ''' Clears Error Queue.
        ''' </para> </remarks>
        Public Overrides Sub InitializeKnownState()
            MyBase.InitializeKnownState()
            Me.ClearErrorQueue()
        End Sub

        ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
        Public Overrides Sub ResetKnownState()
            MyBase.ResetKnownState()
        End Sub

#End Region

#Region " SCPI VERSION "

        ''' <summary> The SCPI revision. </summary>
        Private WithEvents _ScpiRevision As PresettableValue(Of Double)
        ''' <summary> Gets the cached version level of the SCPI standard implemented by the device. </summary>
        ''' <value> The SCPI revision. </value>
        Public ReadOnly Property ScpiRevision As PresettableValue(Of Double)
            Get
                Return Me._ScpiRevision
            End Get
        End Property

        ''' <summary> Scpi revision property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        Private Sub _ScpiRevision_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _ScpiRevision.PropertyChanged
            Me.SafePostPropertyChanged(e)
        End Sub

        ''' <summary> Queries the version level of the SCPI standard implemented by the device. </summary>
        ''' <returns> System.Nullable{System.Double}. </returns>
        ''' <remarks> Sends the ':SYST:VERS?' query. </remarks>
        Public Function QueryScpiRevision() As Double?
            If Me.IsSessionOpen AndAlso Me.ScpiRevision.Command.QuerySupported Then
                Me.ScpiRevision.Value = Me.Session.Query(0.0F, Me.ScpiRevision.Command.QueryCommand)
            End If
            Return Me.ScpiRevision.Value
        End Function

#End Region

    End Class

End Namespace
