Namespace Scpi

    ''' <summary> Defines the contract that must be implemented by a SCPI Thermo Stream Subsystem. </summary>
    ''' <license> (c) 2015 Integrated Scientific ReSenses, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="03/17/2015" by="David" revision="3.0.5554"> Created. </history>
    <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId:="ThermoStream")>
    <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1501:AvoidExcessiveInheritance")>
    Public MustInherit Class ThermoStreamSubsystemBase
        Inherits Visa.ThermoStreamSubsystemBase

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="ThermoStreamSubsystemBase" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">status subsystem</see>. </param>
        Protected Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

    End Class

End Namespace
