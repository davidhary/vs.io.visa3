﻿Imports NationalInstruments
Imports isr.Core.Agnostic.EnumExtensions
Imports isr.Core.Agnostic.StackTraceExtensions
''' <summary> Defines the contract that must be implemented by Status Subsystem. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
Public MustInherit Class StatusSubsystemBase
    Inherits SubsystemBase

#Region " CONSTRUCTION + CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="StatusSubsystemBase" /> class. </summary>
    ''' <param name="visaSession"> A reference to a <see cref="Session">message based session</see>. </param>
    Protected Sub New(ByVal visaSession As Session)
        MyBase.New(visaSession)

        Me._OperationCompleted = New Boolean?
        Me.DeviceErrorBuilder = New System.Text.StringBuilder
        Me._DeviceErrorQueue = New Queue(Of DeviceError)

        ' check for query and other errors reported by the standard event register
        Me.StandardDeviceErrorAvailableBits = StandardEvents.CommandError Or
            StandardEvents.DeviceDependentError Or
            StandardEvents.ExecutionError Or
            StandardEvents.QueryError

        If visaSession IsNot Nothing Then
            AddHandler visaSession.PropertyChanged, AddressOf Me.SessionPropertyChanged
        End If

    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged resources;
    ''' False if this method releases only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not MyBase.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called
                    If Me.IsSessionOpen Then
                        RemoveHandler Me.Session.PropertyChanged, AddressOf Me.SessionPropertyChanged
                    End If

                End If

                ' Free shared unmanaged resources
                If Me.DeviceErrorBuilder IsNot Nothing Then
                    Me.DeviceErrorBuilder.Clear()
                    Me.DeviceErrorBuilder = Nothing
                End If

                If Me._DeviceErrorQueue IsNot Nothing Then
                    Me._DeviceErrorQueue.Clear()
                    Me._DeviceErrorQueue = Nothing
                End If
            End If

        Finally

            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Clears the active state.
    '''           Issues selective device clear. </summary>
    Public Overridable Sub ClearActiveState()
        Me.ClearService()
    End Sub

    ''' <summary> Gets the clear execution state command. </summary>
    ''' <remarks> SCPI: "*CLS".
    ''' <see cref="Ieee488.Syntax.ClearExecutionStateCommand"> </see> </remarks>
    ''' <value> The clear execution state command. </value>
    Protected MustOverride ReadOnly Property ClearExecutionStateCommand As String

    ''' <summary> Sets subsystem values to their known execution clear state. </summary>
    ''' <remarks> Sends the '*CLS' message. 
    '''           '*CLS' clears the error queue. </remarks>
    Public Overrides Sub ClearExecutionState()
        MyBase.ClearExecutionState()
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.ClearExecutionStateCommand) Then
            Me.Session.WriteLine(Me.ClearExecutionStateCommand)
            Me.ReadServiceRequestStatus()
        End If
    End Sub

    ''' <summary> Returns the instrument registers to there preset power on state. </summary>
    ''' <remarks> SCPI: "*CLS".<para>
    ''' <see cref="Ieee488.Syntax.ClearExecutionStateCommand"> </see> </para><para>
    ''' When this command is sent, the SCPI event registers are affected as follows:<p>
    '''   1. All bits of the positive transition filter registers are set to one (1).</p><p>
    '''   2. All bits of the negative transition filter registers are cleared to zero (0).</p><p>
    '''   3. All bits of the following registers are cleared to zero (0):</p><p>
    '''      a. Operation Event Enable Register.</p><p>
    '''      b. Questionable Event Enable Register.</p><p>
    '''   4. All bits of the following registers are set to one (1):</p><p>
    '''      a. Trigger Event Enable Register.</p><p>
    '''      b. Arm Event Enable Register.</p><p>
    '''      c. Sequence Event Enable Register.</p><p>
    ''' Note: Registers not included in the above list are not affected by this command.</p> </para></remarks>
    Public Overrides Sub PresetKnownState()
        MyBase.PresetKnownState()
    End Sub

    ''' <summary> Gets or sets the reset known state command. </summary>
    ''' <remarks> SCPI: "*RST".
    ''' <see cref="Ieee488.Syntax.ResetKnownStateCommand"> </see> </remarks>
    ''' <value> The reset known state command. </value>
    Protected MustOverride ReadOnly Property ResetKnownStateCommand As String

    ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
    ''' <remarks> Sends the '*RST' message. </remarks>
    Public Overrides Sub ResetKnownState()
        MyBase.ResetKnownState()
        Me.Identity = ""
        Me.ServiceRequestEnableBitmask = 0
        Me.OperationCompleted = New Boolean?
        If Me.IsSessionOpen Then
            Me.Session.StandardServiceEnableCommandFormat = Me.StandardServiceEnableCommandFormat
            Me.Session.StandardEventQueryCommand = Me.StandardEventQueryCommand
            Me.Session.ErrorQueueQueryCommand = Me.ErrorQueueQueryCommand
            Me.Session.ErrorAvailableBits = Me.ErrorAvailableBits
            Me.Session.MeasurementAvailableBits = Me.MeasurementAvailableBits
            Me.Session.MessageAvailableBits = Me.MessageAvailableBits
            Me.Session.StandardEventAvailableBits = Me.StandardEventAvailableBits
            If Not String.IsNullOrWhiteSpace(Me.ResetKnownStateCommand) Then
                Me.Session.WriteLine(Me.ResetKnownStateCommand)
            End If
        End If
        Me.ReadServiceRequestStatus()
    End Sub

#End Region

#Region " SESSION "

    Private _IsDeviceOpen As Boolean
    ''' <summary> Gets or sets (Protected) a value indicating whether the device is open. This is
    '''           required when the device is used in emulation. </summary>
    ''' <value> <c>True</c> if the device has an open session; otherwise, <c>False</c>. </value>
    Public Property IsDeviceOpen As Boolean
        Get
            Return Me._IsDeviceOpen
        End Get
        Set(ByVal value As Boolean)
            If Not Me.IsDeviceOpen.Equals(value) Then
                Me._IsDeviceOpen = value
                Me.SafePostPropertyChanged("IsDeviceOpen")
            End If
        End Set
    End Property

    ''' <summary> Handles the Session property changed event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    Private Sub SessionPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If sender IsNot Nothing AndAlso e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) AndAlso Me.IsSessionOpen Then
            Me.OnSessionPropertyChanged(e)
        End If
    End Sub

    ''' <summary> Handles the property changed event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnSessionPropertyChanged(ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
            Select Case e.PropertyName
                Case "ServiceRequestStatus"
                    Me.SafeSendPropertyChanged("ServiceRequestStatus")
                Case "ErrorAvailable"
                    Me.SafeSendPropertyChanged("ErrorAvailable")
                Case "MeasurementAvailable"
                    Me.SafeSendPropertyChanged("MeasurementAvailable")
                Case "MessageAvailable"
                    Me.SafeSendPropertyChanged("MessageAvailable")
                Case "StandardEventAvailable"
                    Me.SafeSendPropertyChanged("StandardEventAvailable")
            End Select
        End If
    End Sub

#End Region

#Region " DEVICE CLEAR "

    ''' <summary> Clears the device. </summary>
    ''' <remarks> When communicating with a message-based device, particularly when you
    ''' are first developing your program, you may need to tell the device to clear its I/O buffers
    ''' so that you can start again. In addition, if a device has more information than you need, you
    ''' may want to read until you have everything you need and then tell the device to throw the
    ''' rest away. The <c>viClear()</c> operation performs these tasks. More specifically, the clear
    ''' operation lets a controller send the device clear command to the device it is associated with,
    ''' as specified by the interface specification and the type of device. The action that the
    ''' device takes depends on the interface to which it is connected. <para>
    ''' For a GPIB device, the controller sends the IEEE 488.1 SDC (04h) command.</para><para>
    ''' For a VXI or MXI device, the controller sends the Word Serial Clear (FFFFh) command.</para>
    ''' <para>
    ''' For the ASRL INSTR or TCPIP SOCKET resource, the controller sends the string "*CLS\n". The
    ''' I/O protocol must be set to VI_PROT_4882_STRS for this service to be available to these
    ''' resources.</para>
    ''' For more details on these clear commands, refer to your device documentation, the IEEE 488.1
    ''' standard, or the VXI bus specification. <para>
    ''' Source: NI-Visa HTML help.</para></remarks>
    Public Sub ClearService()
        If Me.IsSessionOpen Then
            Me.Session.Clear()
        End If
    End Sub

#End Region

#Region " DEVICE ERRORS "

    ''' <summary> Gets the clear error queue command. </summary>
    ''' <remarks> SCPI: ":STAT:QUE:CLEAR".
    ''' <see cref="Scpi.Syntax.ClearErrorQueueCommand"> </see> </remarks>
    ''' <value> The clear error queue command. </value>
    Protected MustOverride ReadOnly Property ClearErrorQueueCommand As String

    ''' <summary> Clears the error cache. </summary>
    Public Overridable Sub ClearErrorCache()
        Me.DeviceErrorBuilder = New System.Text.StringBuilder
        Me._DeviceErrorQueue = New Queue(Of DeviceError)
    End Sub

    ''' <summary> Clears the error queue. </summary>
    ''' <remarks> Sends the ':STAT:QUE:CLEAR' message. </remarks>
    Public Overridable Sub ClearErrorQueue()
        Me.ClearErrorCache()
        If Me.IsSessionOpen Then
            Me.Session.WriteLine(Me.ClearErrorQueueCommand)
        End If
    End Sub

    Private _DeviceErrorQueue As Queue(Of DeviceError)
    ''' <summary> Gets or sets the error queue. </summary>
    ''' <value> A Queue of device errors. </value>
    Protected ReadOnly Property DeviceErrorQueue As Queue(Of DeviceError)
        Get
            Return Me._DeviceErrorQueue
        End Get
    End Property

    ''' <summary> The device errors. </summary>
    Protected Property DeviceErrorBuilder As System.Text.StringBuilder

    ''' <summary> Gets or sets a report of the error stored in the cached error queue. </summary>
    ''' <value> The cached device errors. </value>
    Public Property DeviceErrors() As String
        Get
            Dim builder As New System.Text.StringBuilder(Me.DeviceErrorBuilder.ToString)
            If Me.StandardEventStatus.GetValueOrDefault(0) <> 0 Then
                Dim report As String = StatusSubsystemBase.BuildReport(Me.StandardEventStatus.Value, "; ")
                If Not String.IsNullOrWhiteSpace(report) Then
                    If builder.Length > 0 Then
                        builder.AppendLine()
                    End If
                    builder.Append(report)
                End If
            End If
            Return builder.ToString
        End Get
        Protected Set(ByVal value As String)
            Me.DeviceErrorBuilder.AppendLine(value)
            Me.SafePostPropertyChanged(NameOf(Me.DeviceErrors))
        End Set
    End Property

    ''' <summary> Gets the error queue query command. </summary>
    ''' <remarks> SCPI: ":STAT:QUE?".
    ''' <see cref="Scpi.Syntax.ErrorQueueQueryCommand"> </see> </remarks>
    ''' <value> The error queue query command. </value>
    Protected MustOverride ReadOnly Property ErrorQueueQueryCommand As String

    ''' <summary> Returns the queued error. </summary>
    ''' <remarks> Sends the ':SYST:QUE?' query . </remarks>
    Public Function QueryQueuedError() As DeviceError
        Dim err As New DeviceError()
        If Me.IsSessionOpen Then
            Me.ReadServiceRequestStatus()
            If Me.ErrorAvailable() Then
                err = New DeviceError(Me.Session.QueryTrimEnd(Me.ErrorQueueQueryCommand))
            End If
        End If
        Return err
    End Function

    ''' <summary> Reads the device errors. </summary>
    ''' <returns> The device errors. </returns>
    Public Function QueryDeviceErrors() As String
        Me.ClearErrorCache()
        If Me.IsSessionOpen Then
            Dim deviceError As DeviceError
            Do
                deviceError = Me.QueryQueuedError()
                If deviceError.IsError Then
                    Me.DeviceErrorQueue.Enqueue(deviceError)
                End If
            Loop While Me.ErrorAvailable()
            Dim message As New System.Text.StringBuilder
            If Me.DeviceErrorQueue IsNot Nothing AndAlso Me.DeviceErrorQueue.Count > 0 Then
                message.AppendFormat("{0} Device Errors:", Me.ResourceName)
                For Each e As DeviceError In Me.DeviceErrorQueue
                    message.AppendLine()
                    message.Append(e.ErrorMessage)
                Next
                Me.QueryStandardEventStatus()
            End If
            Me.DeviceErrors = message.ToString
        End If
        Return Me.DeviceErrors
    End Function

#End Region

#Region " DEVICE REGISTERS "

    ''' <summary> Reads the service request register. Also reads the standard event register if 
    '''           the service request register indicates the existence of a standard event. </summary>
    Public Overridable Sub ReadRegisters()
        Me.ReadServiceRequestStatus()
    End Sub

#End Region

#Region " IDENTITY "

    ''' <summary> The identity. </summary>
    Private _Identity As String

    ''' <summary> Gets or sets the device identity string (*IDN?). </summary>
    ''' <value> The identity. </value>
    Public Property Identity As String
        Get
            Return Me._Identity
        End Get
        Protected Set(ByVal value As String)
            If Not String.Equals(value, Me.Identity) Then
                Me._Identity = value
                Me.ParseVersionInfo(value)
                Me.SafePostPropertyChanged(NameOf(Me.Identity))
            End If
        End Set
    End Property

    ''' <summary> Gets the identity query command. </summary>
    ''' <remarks> SCPI: "*IDN?".
    ''' <see cref="Ieee488.Syntax.IdentityQueryCommand"> </see> </remarks>
    ''' <value> The identity query command. </value>
    Protected MustOverride ReadOnly Property IdentityQueryCommand As String

    ''' <summary> Queries the Identity. </summary>
    ''' <returns> System.String. </returns>
    ''' <remarks> Sends the '*IDN?' query. </remarks>
    Public Function QueryIdentity() As String
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.IdentityQueryCommand) Then
            Me.Identity = Me.Session.QueryTrimEnd(Me.IdentityQueryCommand)
        End If
        Return Me.Identity
    End Function

    ''' <summary> Parse version information. </summary>
    ''' <param name="value"> The value. </param>
    Public MustOverride Sub ParseVersionInfo(ByVal value As String)

#End Region

#Region " OPC "

    ''' <summary> The operation completed. </summary>
    Private _OperationCompleted As Boolean?

    ''' <summary> Gets or sets the cached value indicating whether the last operation completed. </summary>
    ''' <value> <c>null</c> if operation completed contains no value, <c>True</c> if operation
    ''' completed; otherwise, <c>False</c>. </value>
    Public Property OperationCompleted As Boolean?
        Get
            Return Me._OperationCompleted
        End Get
        Protected Set(ByVal value As Boolean?)
            Me._OperationCompleted = value
            Me.SafeSendPropertyChanged(NameOf(Me.OperationCompleted))
        End Set
    End Property

    ''' <summary> Gets or sets the operation completed query command. </summary>
    ''' <remarks> SCPI: "*OPC?".
    ''' <see cref="Ieee488.Syntax.OperationCompletedQueryCommand"> </see> </remarks>
    ''' <value> The operation completed query command. </value>
    Protected MustOverride ReadOnly Property OperationCompletedQueryCommand As String

    ''' <summary> Issues the operation completion query, waits and returns a reply. </summary>
    ''' <returns> <c>True</c> if successful <see cref="VisaNS.VisaStatusCode">VISA Status Code</see>
    ''' was returned and operation is complete; <c>False</c> otherwise. </returns>
    ''' <remarks> Sends the '*OPC?' query. </remarks>
    Public Function QueryOperationCompleted() As Boolean?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.OperationCompletedQueryCommand) Then
            Me.OperationCompleted = Me.Session.Query(True, Me.OperationCompletedQueryCommand)
        End If
        Return Me.OperationCompleted
    End Function

    ''' <summary> Enabled detection of completion. </summary>
    ''' <remarks> 3475. Add Or Visa.Ieee4882.ServiceRequests.OperationEvent. </remarks>
    Public Sub EnableWaitComplete()
        Me.EnableServiceRequestComplete(StandardEvents.All And Not StandardEvents.RequestControl, ServiceRequests.StandardEvent)
    End Sub

    ''' <summary> Awaits for service request. Use this method to wait for an operation to complete on
    ''' this device.  Set the time out value to negative value for infinite time out. 
    ''' Uses minimum poll delay of 10ms or 0.1 timeout.</summary>
    ''' <param name="statusByteBits"> Specifies the status byte which to check. </param>
    ''' <param name="timeout">        Specifies how long to wait for the service request before
    ''' throwing the timeout exception. Set to zero for an infinite (120 seconds) timeout. </param>
    Public Sub AwaitServiceRequest(ByVal statusByteBits As ServiceRequests, ByVal timeout As TimeSpan)
        Me.AwaitServiceRequest(statusByteBits, timeout, TimeSpan.FromSeconds(Math.Min(0.01, timeout.TotalSeconds / 10)))
    End Sub

    ''' <summary> Awaits for service request. Use this method to wait for an operation to complete on
    ''' this device.  Set the time out value to negative value for infinite time out. </summary>
    ''' <exception cref="TimeoutException"> Thrown when a Timeout error condition occurs. </exception>
    ''' <param name="statusByteBits"> Specifies the status byte which to check. </param>
    ''' <param name="timeout">        Specifies how long to wait for the service request before
    ''' throwing the timeout exception. Set to zero for an infinite (120 seconds) timeout. </param>
    ''' <param name="pollDelay">      Specifies time between serial polls. </param>
    Public Function TryAwaitServiceRequest(ByVal statusByteBits As ServiceRequests, ByVal timeout As TimeSpan, ByVal pollDelay As TimeSpan) As Boolean

        Dim endTime As Date

        ' check if time out is negative
        If timeout.TotalMilliseconds > 0 Then
            endTime = DateTime.Now.Add(timeout)
        Else
            ' if negative, set to 'infinite' value.
            endTime = DateTime.Now.AddMinutes(2)
        End If

        ' Clear the SRQ flag
        ' this clears the service request item (32) we are looking for!
        ' Me.ReadServiceRequestStatus()

        ' Loop until SRQ or Time out
        Do Until ((Me.ReadServiceRequestStatus And statusByteBits) > 0) OrElse (DateTime.Now.CompareTo(endTime) > 0)
            Dim endPoll As DateTime = DateTime.Now.Add(pollDelay)
            Do Until DateTime.Now.CompareTo(endPoll) > 0
                Windows.Forms.Application.DoEvents()
            Loop
        Loop

        Return (Me.ServiceRequestStatus And statusByteBits) <> 0

    End Function


    ''' <summary> Awaits for service request. Use this method to wait for an operation to complete on
    ''' this device.  Set the time out value to negative value for infinite time out. </summary>
    ''' <exception cref="TimeoutException"> Thrown when a Timeout error condition occurs. </exception>
    ''' <param name="statusByteBits"> Specifies the status byte which to check. </param>
    ''' <param name="timeout">        Specifies how long to wait for the service request before
    ''' throwing the timeout exception. Set to zero for an infinite (120 seconds) timeout. </param>
    ''' <param name="pollDelay">      Specifies time between serial polls. </param>
    Public Sub AwaitServiceRequest(ByVal statusByteBits As ServiceRequests, ByVal timeout As TimeSpan, ByVal pollDelay As TimeSpan)
        If Not Me.TryAwaitServiceRequest(statusByteBits, timeout, pollDelay) Then Throw New TimeoutException("Timeout awaiting service request.")
    End Sub

    ''' <summary> Waits for completion of last operation. </summary>
    ''' <remarks> Assumes requesting service event is registered with the instrument. </remarks>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    ''' completed. </param>
    Public Sub AwaitOperationCompleted(ByVal timeout As TimeSpan)
        If Me.IsSessionOpen Then
            Me.AwaitServiceRequest(ServiceRequests.RequestingService Or ServiceRequests.StandardEvent, timeout)
        End If
    End Sub

#End Region

#Region " STATUS REGISTER EVENTS: ERROR "

    ''' <summary> Gets the bits that would be set for detecting if an error is available. </summary>
    ''' <value> The error available bits. </value>
    Public MustOverride ReadOnly Property ErrorAvailableBits() As ServiceRequests

    ''' <summary> Gets a value indicating whether [Error available]. </summary>
    ''' <value> <c>True</c> if [Error available]; otherwise, <c>False</c>. </value>
    Public ReadOnly Property ErrorAvailable As Boolean
        Get
            If Me.IsSessionOpen Then
                Return Me.Session.ErrorAvailable
            Else
                Return False
            End If
        End Get
    End Property

#End Region

#Region " STATUS REGISTER EVENTS: MESSAGE "

    ''' <summary> Gets the bits that would be set for detecting if an Message is available. </summary>
    ''' <value> The Message available bits. </value>
    Public MustOverride ReadOnly Property MessageAvailableBits() As ServiceRequests

    ''' <summary> Gets a value indicating whether [Message available]. </summary>
    ''' <value> <c>True</c> if [Message available]; otherwise, <c>False</c>. </value>
    Public ReadOnly Property MessageAvailable As Boolean
        Get
            If Me.IsSessionOpen Then
                Return Me.Session.MessageAvailable
            Else
                Return False
            End If
        End Get
    End Property

    ''' <summary> Is message available. </summary>
    ''' <remarks> Delays looking for the message status by the status latency to make sure the
    ''' instrument had enough time to process the previous command. </remarks>
    ''' <param name="latency">     The latency. </param>
    ''' <param name="repeatCount"> Number of repeats. </param>
    ''' <returns> <c>True</c> if message is available. </returns>
    Public Function IsMessageAvailable(ByVal latency As TimeSpan, ByVal repeatCount As Integer) As Boolean
        If Me.IsSessionOpen Then
            Return Me.Session.IsMessageAvailable(latency, repeatCount)
        End If
        Return Me.MessageAvailable
    End Function

#End Region

#Region " STATUS REGISTER EVENTS: MEASUREMENT "

    ''' <summary> Gets the bits that would be set for detecting if an Measurement is available. </summary>
    ''' <value> The Measurement available bits. </value>
    Public MustOverride ReadOnly Property MeasurementAvailableBits() As ServiceRequests

    ''' <summary> Gets a value indicating whether [Measurement available]. </summary>
    ''' <value> <c>True</c> if [Measurement available]; otherwise, <c>False</c>. </value>
    Public ReadOnly Property MeasurementAvailable As Boolean
        Get
            If Me.IsSessionOpen Then
                Return Me.Session.MeasurementAvailable
            Else
                Return False
            End If
        End Get
    End Property

#End Region

#Region " STATUS REGISTER EVENTS: STANDARD EVENT "

    ''' <summary> Gets the bits that would be set for detecting if a Standard Event is available. </summary>
    ''' <value> The Standard Event available bits. </value>
    Public MustOverride ReadOnly Property StandardEventAvailableBits() As ServiceRequests

    ''' <summary> Gets a value indicating whether [StandardEvent available]. </summary>
    ''' <value> <c>True</c> if [StandardEvent available]; otherwise, <c>False</c>. </value>
    Public ReadOnly Property StandardEventAvailable As Boolean
        Get
            If Me.IsSessionOpen Then
                Return Me.Session.StandardEventAvailable
            Else
                Return False
            End If
        End Get
    End Property

#End Region

#Region " SERVICE REQUEST REGISTER EVENTS: REPORT "

    ''' <summary> Returns a detailed report of the service request register (SRQ) byte. </summary>
    ''' <param name="value">     Specifies the value that was read from the service request register. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> The structured report. </returns>
    Public Shared Function BuildReport(ByVal value As ServiceRequests, ByVal delimiter As String) As String

        If String.IsNullOrWhiteSpace(delimiter) Then
            delimiter = "; "
        End If

        Dim builder As New System.Text.StringBuilder

        For Each element As ServiceRequests In [Enum].GetValues(GetType(ServiceRequests))
            If element <> ServiceRequests.None AndAlso element <> ServiceRequests.All AndAlso (element And value) <> 0 Then
                If builder.Length > 0 Then
                    builder.Append(delimiter)
                End If
                builder.Append(element.Description)
            End If
        Next

        If builder.Length > 0 Then
            builder.Append(".")
            builder.Insert(0, String.Format(Globalization.CultureInfo.CurrentCulture,
                                            "The device service request register reported: 0x{0:X}{1}", value, Environment.NewLine))
        End If
        Return builder.ToString

    End Function

#End Region

#Region " SERVICE REQUEST REGISTER "

    ''' <summary> Gets the standard service enable command format. </summary>
    ''' <remarks> SCPI: "*CLS; *ESE {0:D}; *SRE {1:D}". 
    ''' <see cref="Ieee488.Syntax.StandardServiceEnableCommandFormat"> </see> </remarks>
    ''' <value> The standard service enable command format. </value>
    Protected MustOverride ReadOnly Property StandardServiceEnableCommandFormat As String

    ''' <summary> Program the device to issue an SRQ upon any of the SCPI events. Uses *ESE to select
    ''' (mask) the events that will issue SRQ and  *SRE to select (mask) the event registers to be
    ''' included in the bits that will issue an SRQ. </summary>
    ''' <param name="standardEventEnableBitmask">  Specifies standard events will issue an SRQ. </param>
    ''' <param name="serviceRequestEnableBitmask"> Specifies which status registers will issue an
    ''' SRQ. </param>
    Public Sub EnableServiceRequest(ByVal standardEventEnableBitmask As StandardEvents,
                                    ByVal serviceRequestEnableBitmask As ServiceRequests)
        If Me.IsSessionOpen Then
            Me.Session.ReadStatusByte()
        End If
        Me.ServiceRequestEnableBitmask = serviceRequestEnableBitmask
        Me.StandardEventEnableBitmask = standardEventEnableBitmask
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.StandardServiceEnableCommandFormat) Then
            Me.Session.WriteLine(Me.StandardServiceEnableCommandFormat, CInt(standardEventEnableBitmask), CInt(serviceRequestEnableBitmask))
        End If
    End Sub

    ''' <summary> Gets or sets the standard service enable and complete command format. </summary>
    ''' <remarks> SCPI: "*CLS; *ESE {0:D}; *SRE {1:D}; *OPC".
    ''' <see cref="Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat"> </see> </remarks>
    ''' <value> The standard service enable command and complete format. </value>
    Protected MustOverride ReadOnly Property StandardServiceEnableCompleteCommandFormat As String

    ''' <summary> Sets the device to issue an SRQ upon any of the SCPI events. Uses *ESE to select
    ''' (mask) the events that will issue SRQ and  *SRE to select (mask) the event registers to be
    ''' included in the bits that will issue an SRQ Uses a single line command to accomplish the
    ''' entire command so as to save time. Also issues *OPC. </summary>
    ''' <param name="standardEventEnableBitmask">  Specifies standard events will issue an SRQ. </param>
    ''' <param name="serviceRequestEnableBitmask"> Specifies which status registers will issue an SRQ. </param>
    Public Sub EnableServiceRequestComplete(ByVal standardEventEnableBitmask As StandardEvents,
                                            ByVal serviceRequestEnableBitmask As ServiceRequests)
        If Me.IsSessionOpen Then
            Me.Session.ReadStatusByte()
        End If
        Me.ServiceRequestEnableBitmask = serviceRequestEnableBitmask
        Me.StandardEventEnableBitmask = standardEventEnableBitmask
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.StandardServiceEnableCompleteCommandFormat) Then
            Me.Session.WriteLine(Me.StandardServiceEnableCompleteCommandFormat, CInt(standardEventEnableBitmask), CInt(serviceRequestEnableBitmask))
        End If

    End Sub

    ''' <summary> Queries the service request enable bit mask. </summary>
    ''' <returns> <c>null</c> if value is not known; otherwise <see cref="ServiceRequests">Service
    ''' Requests</see>. </returns>
    Public Function QueryServiceRequestEnableBitmask() As ServiceRequests?
        If Me.IsSessionOpen Then
            Me.ServiceRequestEnableBitmask = CType(Me.Session.Query(0I, Ieee488.Syntax.ServiceRequestEnableQueryCommand),
                                                   ServiceRequests?)
        End If
        Return Me.ServiceRequestEnableBitmask
    End Function

    ''' <summary> The service request enable bitmask. </summary>
    Private _ServiceRequestEnableBitmask As ServiceRequests?

    ''' <summary> Gets or sets the cached service request enable bit mask. </summary>
    ''' <value> <c>null</c> if value is not known; otherwise <see cref="ServiceRequests">Service
    ''' Requests</see>. </value>
    Public Property ServiceRequestEnableBitmask() As ServiceRequests?
        Get
            Return Me._ServiceRequestEnableBitmask
        End Get
        Protected Set(ByVal value As ServiceRequests?)
            If Not Me.ServiceRequestEnableBitmask.Equals(value) Then
                Me._ServiceRequestEnableBitmask = value
                Me.SafePostPropertyChanged(NameOf(Me.ServiceRequestEnableBitmask))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the service request enable command format. </summary>
    ''' <remarks> SCPI: "*SRE {0:D}". 
    ''' <see cref="Ieee488.Syntax.ServiceRequestEnableCommandFormat"> </see> </remarks>
    ''' <value> The service request enable command format. </value>
    Protected MustOverride ReadOnly Property ServiceRequestEnableCommandFormat As String
    '
    ''' <summary> Program the device to issue an SRQ upon any of the SCPI events. Uses *SRE to select
    ''' (mask) the event registers to be included in the bits that will issue an SRQ. </summary>
    ''' <param name="serviceRequestEnableBitmask"> Specifies which status registers will issue an
    ''' SRQ. </param>
    Public Sub EnableServiceRequest(ByVal serviceRequestEnableBitmask As ServiceRequests)
        Me.ServiceRequestEnableBitmask = serviceRequestEnableBitmask
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.ServiceRequestEnableCommandFormat) Then
            Me.Session.WriteLine(Me.ServiceRequestEnableCommandFormat, CInt(serviceRequestEnableBitmask))
        End If
    End Sub

    ''' <summary> Gets or sets the cached service request Status. </summary>
    ''' <value> <c>null</c> if value is not known; otherwise <see cref="ServiceRequests">Service
    ''' Requests</see>. </value>
    Public ReadOnly Property ServiceRequestStatus() As ServiceRequests
        Get
            If Me.IsSessionOpen Then
                Return Me.Session.ServiceRequestStatus
            Else
                Return ServiceRequests.None
            End If
        End Get
    End Property

    ''' <summary> Reads the service request Status. This method casts the
    ''' <see cref="VisaNS.MessageBasedSession.ReadStatusByte">Read Status Byte</see> to
    ''' <see cref="ServiceRequests">Service Requests</see>. </summary>
    ''' <returns> <c>null</c> if value is not known; otherwise <see cref="ServiceRequests">Service
    ''' Requests</see>. </returns>
    Public Function ReadServiceRequestStatus() As ServiceRequests
        If Me.IsSessionOpen Then
            Me.Session.ReadServiceRequestStatus()
        End If
        Return Me.ServiceRequestStatus
    End Function

#End Region

#Region " STANDARD EVENT REGISTER "

    ''' <summary> Gets or sets bits that would be set for detecting if an Standard Device Error is available. </summary>
    ''' <value> The Standard Device Error available bits. </value>
    Public Property StandardDeviceErrorAvailableBits() As StandardEvents

    ''' <summary> <c>True</c> if StandardDeviceError available. </summary>
    Private _StandardDeviceErrorAvailable As Boolean

    ''' <summary> Gets or sets a value indicating whether a Standard Device Error is available. </summary>
    ''' <value> <c>True</c> if [StandardDeviceError available]; otherwise, <c>False</c>. </value>
    Public Property StandardDeviceErrorAvailable As Boolean
        Get
            Return Me._StandardDeviceErrorAvailable
        End Get
        Protected Set(ByVal value As Boolean)
            Me._StandardDeviceErrorAvailable = value
            Me.SafeSendPropertyChanged(NameOf(Me.StandardDeviceErrorAvailable))
        End Set
    End Property

    ''' <summary> Returns a detailed report of the event status register (ESR) byte. </summary>
    ''' <param name="value">     Specifies the value that was read from the status register. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> Returns a detailed report of the event status register (ESR) byte. </returns>
    Public Shared Function BuildReport(ByVal value As StandardEvents, ByVal delimiter As String) As String

        If String.IsNullOrWhiteSpace(delimiter) Then
            delimiter = "; "
        End If

        Dim builder As New System.Text.StringBuilder

        For Each eventValue As StandardEvents In [Enum].GetValues(GetType(StandardEvents))
            If eventValue <> StandardEvents.None AndAlso eventValue <> StandardEvents.All AndAlso (eventValue And value) <> 0 Then
                If builder.Length > 0 Then
                    builder.Append(delimiter)
                End If
                builder.Append(eventValue.Description)
            End If
        Next

        If builder.Length > 0 Then
            builder.Append(".")
            builder.Insert(0, String.Format(Globalization.CultureInfo.CurrentCulture,
                                            "The device standard status register reported: 0x{0:X}{1}", value, Environment.NewLine))
        End If
        Return builder.ToString

    End Function

    ''' <summary> The standard event status. </summary>
    Private _StandardEventStatus As StandardEvents?

    ''' <summary> Gets or sets the cached Standard Event enable bit mask. </summary>
    ''' <value> <c>null</c> if value is not known; otherwise <see cref="StandardEvents">Standard
    ''' Events</see>. </value>
    Public Property StandardEventStatus() As StandardEvents?
        Get
            Return Me._StandardEventStatus
        End Get
        Set(ByVal value As StandardEvents?)
            Me._StandardEventStatus = value
            If value.HasValue Then
                Me.StandardDeviceErrorAvailable = (value.Value And Me.StandardDeviceErrorAvailableBits) <> 0
            Else
                Me.StandardDeviceErrorAvailable = False
            End If
            Me.SafePostPropertyChanged(NameOf(Me.StandardEventStatus))
        End Set
    End Property

    ''' <summary> Gets the standard event status query command. </summary>
    ''' <remarks> SCPI: "*ESR?".
    ''' <see cref="Ieee488.Syntax.StandardEventQueryCommand"> </see> </remarks>
    ''' <value> The standard event status query command. </value>
    Protected MustOverride ReadOnly Property StandardEventQueryCommand As String

    ''' <summary> Queries the Standard Event enable bit mask. </summary>
    ''' <returns> <c>null</c> if value is not known; otherwise <see cref="StandardEvents">Standard
    ''' Events</see>. </returns>
    Public Function QueryStandardEventStatus() As StandardEvents?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.StandardEventQueryCommand) Then
            Me.StandardEventStatus = CType(Me.Session.Query(0I, Me.StandardEventQueryCommand), StandardEvents?)
        End If
        Return Me.StandardEventStatus
    End Function

    ''' <summary> The standard event enable bitmask. </summary>
    Private _StandardEventEnableBitmask As StandardEvents?

    ''' <summary> Gets or sets the cached Standard Event enable bit mask. </summary>
    ''' <value> <c>null</c> if value is not known; otherwise <see cref="StandardEvents">Standard
    ''' Events</see>. </value>
    Public Property StandardEventEnableBitmask() As StandardEvents?
        Get
            Return Me._StandardEventEnableBitmask
        End Get
        Set(ByVal value As StandardEvents?)
            If Not Me.StandardEventEnableBitmask.Equals(value) Then
                Me._StandardEventEnableBitmask = value
                Me.SafePostPropertyChanged(NameOf(Me.StandardEventEnableBitmask))
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the standard event enable query command. </summary>
    ''' <remarks> SCPI: "*ESE?".
    ''' <see cref="Ieee488.Syntax.StandardEventEnableQueryCommand"> </see> </remarks>
    ''' <value> The standard event enable query command. </value>
    Protected MustOverride ReadOnly Property StandardEventEnableQueryCommand As String

    ''' <summary> Queries the Standard Event enable bit mask. </summary>
    ''' <returns> <c>null</c> if value is not known; otherwise <see cref="StandardEvents">Standard
    ''' Events</see>. </returns>
    Public Function QueryStandardEventEnableBitmask() As StandardEvents?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.StandardEventEnableQueryCommand) Then
            Me.StandardEventEnableBitmask = CType(Me.Session.Query(0I, Me.StandardEventEnableQueryCommand), StandardEvents?)
        End If
        Return Me.StandardEventEnableBitmask
    End Function

#End Region

#Region " CHECK AND THROW "

    ''' <summary> Checks and throws an exception if device errors occurred. Can only be used after
    ''' receiving a full reply from the device. </summary>
    ''' <exception cref="DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    Public Sub CheckThrowDeviceException(ByVal flushReadFirst As Boolean, ByVal format As String, ByVal ParamArray args() As Object)
        If Me.IsSessionOpen AndAlso (Me.ReadServiceRequestStatus And Me.ErrorAvailableBits) <> 0 Then
            If flushReadFirst Then
                Me.Session.DiscardUnreadData()
            End If
            Me.QueryStandardEventStatus()
            Me.QueryDeviceErrors()
            Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
            Throw New isr.IO.Visa.DeviceException(Me.ResourceName, "{0}. {1}.", details, Me.DeviceErrors)
        End If
    End Sub

    ''' <summary> Checks and throws an exception if a visa or device errors occurred.
    ''' Can only be used after receiving a full reply from the device. </summary>
    ''' <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    Public Sub CheckThrowVisaDeviceException(ByVal flushReadFirst As Boolean, ByVal format As String, ByVal ParamArray args() As Object)
        Me.CheckThrowVisaException(format, args)
        Me.CheckThrowDeviceException(flushReadFirst, format, args)
    End Sub

    ''' <summary> Checks and throws an exception if a visa or device errors occurred.
    ''' Can only be used after receiving a full reply from the device. </summary>
    ''' <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    Public Sub CheckThrowVisaDeviceException(ByVal nodeNumber As Integer, ByVal flushReadFirst As Boolean, ByVal format As String, ByVal ParamArray args() As Object)
        Me.CheckThrowVisaException(nodeNumber, format, args)
        Me.CheckThrowDeviceException(flushReadFirst, format, args)
    End Sub

#End Region

#Region " CHECK AND REPORT "

    ''' <summary> Check and reports visa or device error occurred. Can only be used after receiving a
    ''' full reply from the device. </summary>
    ''' <param name="nodeNumber"> Specifies the remote node number to validate. </param>
    ''' <param name="format">     Specifies the report format. </param>
    ''' <param name="args">       Specifies the report arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Overridable Function ReportVisaDeviceOperationOkay(ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return True
    End Function

    ''' <summary> Checks and reports if a visa or device error occurred. Can only be used after
    ''' receiving a full reply from the device. </summary>
    ''' <param name="nodeNumber">     Specifies the remote node number to validate. </param>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function ReportVisaDeviceOperationOkay(ByVal nodeNumber As Integer,
                                                  ByVal flushReadFirst As Boolean, ByVal format As String,
                                                  ByVal ParamArray args() As Object) As Boolean

        Dim success As Boolean = Me.ReportVisaOperationOkay(nodeNumber, format, args)
        If success Then
            Me.ReadServiceRequestStatus()
            success = Not Me.ErrorAvailable
            Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
            If success Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "{0} node {1} done {2}",
                                           Me.ResourceName, nodeNumber, details)
            Else
                If Me.IsSessionOpen AndAlso flushReadFirst Then
                    Me.Session.DiscardUnreadData()
                End If
                Me.QueryStandardEventStatus()
                Me.QueryDeviceErrors()
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                           "{0} node {1} had errors {2}. Details: {3}{4}{5}",
                                           Me.ResourceName, nodeNumber, Me.DeviceErrors, details,
                                           Environment.NewLine, New StackFrame(True).UserCallStack())
            End If
        End If
        Return success
    End Function

    ''' <summary> Checks and reports if a visa or device error occurred.
    ''' Can only be used after receiving a full reply from the device. </summary>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="format">         Describes the format to use. </param>
    ''' <param name="args">           A variable-length parameters list containing arguments. </param>
    ''' <returns><c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function ReportVisaDeviceOperationOkay(ByVal flushReadFirst As Boolean, ByVal format As String,
                                                  ByVal ParamArray args() As Object) As Boolean
        Dim success As Boolean = Me.ReportVisaOperationOkay(format, args)
        If success Then
            Me.ReadServiceRequestStatus()
            success = Not Me.ErrorAvailable
            Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
            If success Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "{0} done {1}",
                                           Me.ResourceName, details)
            Else
                If Me.IsSessionOpen AndAlso flushReadFirst Then
                    Me.Session.DiscardUnreadData()
                End If
                Me.QueryStandardEventStatus()
                Me.QueryDeviceErrors()
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                           "{0} had errors {1}. Details: {2}{3}{4}",
                                           Me.ResourceName, Me.DeviceErrors, details,
                                           Environment.NewLine, New StackFrame(True).UserCallStack())
            End If
        End If
        Return success
    End Function

    ''' <summary> Reports if a visa error occurred.
    '''           Can be used with queries. </summary>
    ''' <param name="format">   Describes the format to use. </param>
    ''' <param name="args">     A variable-length parameters list containing arguments. </param>
    ''' <returns><c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function ReportVisaOperationOkay(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Dim success As Boolean = Not Me.IsSessionOpen OrElse Me.Session.LastStatus >= NationalInstruments.VisaNS.VisaStatusCode.Success
        Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        If success Then
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "{0} done {1}", Me.ResourceName, details)
        Else
            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                       "{0} had a VISA error {1}. Details: {2}{3}{4}",
                                       Me.ResourceName, Me.Session.BuildVisaStatusDetails(), details,
                                       Environment.NewLine, New StackFrame(True).UserCallStack())
        End If
        Return success
    End Function

    ''' <summary> Reports if a visa error occurred. Can be used with queries. </summary>
    ''' <param name="nodeNumber">   Specifies the remote node number to validate. </param>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function ReportVisaOperationOkay(ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Dim success As Boolean = Not Me.IsSessionOpen OrElse Me.Session.LastStatus >= NationalInstruments.VisaNS.VisaStatusCode.Success
        Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        If success Then
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "{0} node {1} done {2}",
                                       Me.ResourceName, nodeNumber, details)
        Else
            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                       "{0} node {1} had a VISA error {2}. Details: {3}{4}{5}",
                                       Me.ResourceName, nodeNumber, Me.Session.BuildVisaStatusDetails(), details,
                                       Environment.NewLine, New StackFrame(True).UserCallStack())
        End If
        Return success
    End Function

#End Region

End Class

