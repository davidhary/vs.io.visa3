Namespace My

    ''' <summary> Values that represent project trace event identifiers. </summary>
    Public Enum ProjectTraceEventId As Integer
        <System.ComponentModel.Description("Not specified")> None
        <System.ComponentModel.Description("Core Library")> CoreLibrary = isr.Core.Agnostic.My.ProjectTraceEventId.VirtualInstruments
        <System.ComponentModel.Description("Instrument Library")> InstrumentLibrary = CoreLibrary + &H1

        <System.ComponentModel.Description("Handler Library")> HandlerLibrary = CoreLibrary + &H2
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multimeter")>
        <System.ComponentModel.Description("Multimeter Library")> MultimeterLibrary = CoreLibrary + &H3
        <System.ComponentModel.Description("Power Supply Library")> PowerSupplyLibrary = CoreLibrary + &H4
        <System.ComponentModel.Description("Prober Library")> ProberLibrary = CoreLibrary + &H5
        <System.ComponentModel.Description("Source Measure Library")> SourceMeasureLibrary = CoreLibrary + &H6
        <System.ComponentModel.Description("Switch Library")> SwitchLibrary = CoreLibrary + &H7
        <System.ComponentModel.Description("Tsp Library")> TspLibrary = CoreLibrary + &H8

        <System.ComponentModel.Description("Core Tester")> CoreTester = CoreLibrary + &H10
        <System.ComponentModel.Description("Multimeter Tester")> MultimeterTester = ProjectTraceEventId.CoreTester + &H1
        <System.ComponentModel.Description("Tsp Tester")> TspTester = ProjectTraceEventId.CoreTester + &H2
        <System.ComponentModel.Description("Ohmni Tester")> OhmniTester = ProjectTraceEventId.CoreTester + &H3

        <System.ComponentModel.Description("Core Units")> CoreUnitTest = CoreLibrary + &H1A
        <System.ComponentModel.Description("Multimeter Library Units")> LibraryUnitTest = CoreUnitTest + 1

    End Enum

End Namespace

