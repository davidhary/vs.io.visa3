''' <summary> Reports resources not found exception. </summary>
''' <remarks> Use this class to report an exception raised if resources were not found. </remarks>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/04/2013" by="David" revision="3.0.4955"> Created. </history>
<Serializable()> Public Class ResourcesNotFoundException
    Inherits ExceptionBase

#Region " CONSTRUCTION + CLEANUP "

    Private Const defaultMessage As String = "Resources not found."

    ''' <summary> Initializes a new instance of the <see cref="ResourcesNotFoundException" /> class.
    ''' Uses the default message. </summary>
    Public Sub New()
        Me.New(ResourcesNotFoundException.defaultMessage)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="ResourcesNotFoundException" /> class. </summary>
    ''' <param name="message"> The message. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="ResourcesNotFoundException" /> class. </summary>
    ''' <param name="message">        The message. </param>
    ''' <param name="innerException"> Specifies the exception that was trapped for throwing this
    ''' exception. </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="T:System.Exception" /> class with
    ''' serialized data. </summary>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    ''' that holds the serialized object data about the exception being thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    ''' that contains contextual information about the source or destination. </param>
    Protected Sub New(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

#End Region

End Class
