﻿Imports System.Runtime.CompilerServices
Imports NationalInstruments
Imports NationalInstruments.VisaNS

''' <summary> Resource Manager Extensions. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/10/2013" by="David" revision="3.0.5001.x"> Created. </history>
Public Module ResourceManagerExtensions

#Region " PARSE RESOURCES "

    ''' <summary> Try parse resource. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">           Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="resourceName">    Name of the resource. </param>
    ''' <param name="interfaceType">   [in,out] Type of the interface. </param>
    ''' <param name="interfaceNumber"> [in,out] The interface number. </param>
    ''' <returns> <c>True</c> if parsed or false if failed parsing. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#",
        Justification:="This is the normative implementation of this method.")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#",
        Justification:="This is the normative implementation of this method.")>
    <Extension()>
    Public Function TryParseResource(ByVal value As ResourceManager, ByVal resourceName As String,
                                     ByRef interfaceType As HardwareInterfaceType, ByRef interfaceNumber As Short) As Boolean

        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Try
            value.ParseResource(resourceName, interfaceType, interfaceNumber)
            Return interfaceType > 0
        Catch ex As VisaNS.VisaException
            interfaceType = 0
            interfaceNumber = -1
            Return False
        End Try

    End Function

    ''' <summary> Try parse a GPIB resource. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">           Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="resourceName">    Name of the resource. </param>
    ''' <param name="interfaceType">   [in,out] Type of the interface. </param>
    ''' <param name="interfaceNumber"> [in,out] The interface number. </param>
    ''' <param name="address">         [in,out] The address. </param>
    ''' <returns> <c>True</c> if parsed or false if failed parsing. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="4#",
        Justification:="This is the normative implementation of this method.")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#",
        Justification:="This is the normative implementation of this method.")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#",
        Justification:="This is the normative implementation of this method.")>
    <Extension()>
    Public Function TryParseResource(ByVal value As ResourceManager, ByVal resourceName As String,
                                     ByRef interfaceType As HardwareInterfaceType, ByRef interfaceNumber As Short, ByRef address As Integer) As Boolean

        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        If String.IsNullOrWhiteSpace(resourceName) Then
            Return False
        End If
        Try
            value.ParseResource(resourceName, interfaceType, interfaceNumber)
            If interfaceType = HardwareInterfaceType.Gpib Then
                Dim addressLocation As Int32 = resourceName.IndexOf("::", StringComparison.OrdinalIgnoreCase) + 2
                Dim addressWidth As Int32 = resourceName.LastIndexOf("::", StringComparison.OrdinalIgnoreCase) - addressLocation
                Return Integer.TryParse(resourceName.Substring(addressLocation, addressWidth), address)
            Else
                address = 0
                Return False
            End If
            Return interfaceType > 0
        Catch ex As VisaNS.VisaException
            interfaceType = 0
            interfaceNumber = -1
            Return False
        End Try

    End Function

#End Region

#Region " SEARCH PATTERNS "

    ''' <summary> A pattern specifying all resources search. </summary>
    Public ReadOnly AllResourcesSearchPattern As String = "?*"

    ''' <summary> A pattern specifying the Gpib, USB or TCPIP search. </summary>
    Public ReadOnly GpibUsbTcpIPSearchPattern As String = "(GPIB|USB|TCPIP)?*"

    ''' <summary> A pattern specifying the Gpib, USB or TCPIP instrument search. </summary>
    Public ReadOnly GpibUsbTcpIPInstrumentSearchPattern As String = "(GPIB|USB|TCPIP)?*INSTR"

    Private Const _InterfaceResourceFormat As String = "{0}{1}::INTFC"
    Private Const _InterfaceSearchString As String = "{0}?*INTFC"
    Private Const _InstrumentSearchPattern As String = "{0}?*INSTR"
    Private Const _InstrumentBoardSearchPattern As String = "{0}{1}?*INSTR"

    Private Const _FirewireResourceBaseName As String = "Firewire"
    Private Const _GpibResourceBaseName As String = "GPIB"
    Private Const _GpibVxiResourceBaseName As String = "USB"
    Private Const _SerialResourceBaseName As String = "ASRL"
    Private Const _TcpIPResourceBaseName As String = "TCPIP"
    Private Const _UsbResourceBaseName As String = "USB"
    Private Const _PxiResourceBaseName As String = "Pxi"
    Private Const _VxiResourceBaseName As String = "Vxi"

    ''' <summary> Returns the interface resource base name. </summary>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    ''' illegal values. </exception>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The interface base name. </returns>
    Public Function InterfaceResourceBaseName(ByVal interfaceType As HardwareInterfaceType) As String
        Select Case interfaceType
            Case HardwareInterfaceType.Firewire
                Return _FirewireResourceBaseName
            Case HardwareInterfaceType.Gpib
                Return _GpibResourceBaseName
            Case HardwareInterfaceType.GpibVxi
                Return _GpibVxiResourceBaseName
            Case HardwareInterfaceType.Pxi
                Return _PxiResourceBaseName
            Case HardwareInterfaceType.Serial
                Return _SerialResourceBaseName
            Case HardwareInterfaceType.Tcpip
                Return _TcpIPResourceBaseName
            Case HardwareInterfaceType.Usb
                Return _UsbResourceBaseName
            Case HardwareInterfaceType.Vxi
                Return _VxiResourceBaseName
            Case Else
                Throw New ArgumentException("Unhandled case", "interfaceType")
        End Select
    End Function

    ''' <summary> Returns the Interface resource name. </summary>
    ''' <param name="interfaceName"> The name of the interface. </param>
    ''' <param name="boardNumber">   The board number. </param>
    ''' <returns> The Interface resource name, e.g., 'GPIB?*INTFC'. </returns>
    Public Function InterfaceResourceName(ByVal interfaceName As String, ByVal boardNumber As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, ResourceManagerExtensions._InterfaceResourceFormat,
                             interfaceName, boardNumber)
    End Function

    ''' <summary> Returns the Interface resource name. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="boardNumber">   The board number. </param>
    ''' <returns> The Interface resource name, e.g., 'GPIB?*INTFC'. </returns>
    Public Function InterfaceResourceName(ByVal interfaceType As HardwareInterfaceType, ByVal boardNumber As Integer) As String
        Return ResourceManagerExtensions.InterfaceResourceName(ResourceManagerExtensions.InterfaceResourceBaseName(interfaceType), boardNumber)
    End Function

    ''' <summary> Returns the Interface search pattern. </summary>
    ''' <returns> The Interface search pattern '?*INTFC'. </returns>
    Public Function InterfaceSearchPattern() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, ResourceManagerExtensions._InterfaceSearchString, "")
    End Function

    ''' <summary> Returns the Interface search pattern. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The Interface search pattern, e.g., 'GPIB?*INTFC'. </returns>
    Public Function InterfaceSearchPattern(ByVal interfaceType As HardwareInterfaceType) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, ResourceManagerExtensions._InterfaceSearchString,
                     ResourceManagerExtensions.InterfaceResourceBaseName(interfaceType))
    End Function

    ''' <summary> Returns the Instrument search pattern. </summary>
    ''' <returns> The Instrument search pattern, e.g., '?*INSTR'. </returns>
    Public Function InstrumentSearchPattern() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, ResourceManagerExtensions._InstrumentSearchPattern, "")
    End Function

    ''' <summary> Returns the Instrument search pattern. </summary>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The Instrument search pattern, e.g., 'GPIB?*INSTR'. </returns>
    Public Function InstrumentSearchPattern(ByVal interfaceType As HardwareInterfaceType) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, ResourceManagerExtensions._InstrumentSearchPattern,
                             ResourceManagerExtensions.InterfaceResourceBaseName(interfaceType))
    End Function

    ''' <summary> Returns the Instrument search pattern. </summary>
    ''' <param name="interfaceType">   Type of the interface. </param>
    ''' <param name="interfaceNumber"> The interface number (e.g., board or port number). </param>
    ''' <returns> The Instrument search pattern, e.g., 'GPIB0?*INSTR'. </returns>
    Public Function InstrumentSearchPattern(ByVal interfaceType As HardwareInterfaceType, ByVal interfaceNumber As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, ResourceManagerExtensions._InstrumentBoardSearchPattern,
                             ResourceManagerExtensions.InterfaceResourceBaseName(interfaceType), interfaceNumber)
    End Function

#End Region

#Region " FIND RESOURCES "

    ''' <summary> Lists all resources. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> Reference to the <see cref="ResourceManager">resource manager</see>. </param>
    ''' <returns> List of all resources. </returns>
    <Extension()>
    Public Function FindResources(ByVal value As ResourceManager) As String()
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Return value.FindResources(ResourceManagerExtensions.AllResourcesSearchPattern)
    End Function

    ''' <summary> Build resource message. </summary>
    ''' <param name="ex"> Details of the exception. </param>
    ''' <returns> A an array with a single resource expressing the error. </returns>
    Private Function BuildResourceMessage(ByVal ex As ArgumentException) As String()
        If TypeOf ex.InnerException Is VisaNS.VisaException Then
            With CType(ex.InnerException, VisaNS.VisaException)
                If .ErrorCode = VisaNS.VisaStatusCode.ErrorResourceNotFound Then
                    ' Insufficient location information or the device or resource is not present in the system.  
                    ' VISA error code -1073807343 (0xBFFF0011), ErrorResourceNotFound
                    Return New String() {"Error: Resource Not Found"}
                Else
                    Return New String() {String.Format("{0} code={1}:{2}", .Message, CInt(.ErrorCode), .ErrorCode.ToString)}
                End If
            End With
        Else
            Return New String() {ex.Message}
        End If
    End Function

    ''' <summary> Tries to find resources. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">     Reference to the <see cref="ResourceManager">resource manager</see>. </param>
    ''' <param name="resources"> [in,out] The resources. </param>
    ''' <returns> <c>True</c> if resources were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#",
        Justification:="This is the normative implementation of this method.")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#",
        Justification:="This is the normative implementation of this method.")>
    <Extension()>
    Public Function TryFindResources(ByVal value As ResourceManager, ByRef resources As String()) As Boolean
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Try
            resources = value.FindResources()
            Return resources.Count > 0
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.buildResourceMessage(ex)
            Return False
        Catch ex As VisaNS.VisaException
            resources = New String() {ex.Message}
            Return False
        End Try
    End Function

    ''' <summary> Tries to find resources. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="searchPattern"> A pattern specifying the search. </param>
    ''' <param name="resources">     [in,out] The resources. </param>
    ''' <returns> <c>True</c> if resources were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#",
        Justification:="This is the normative implementation of this method.")>
    <Extension()>
    Public Function TryFindResources(ByVal value As ResourceManager, ByVal searchPattern As String, ByRef resources As String()) As Boolean
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Try
            resources = value.FindResources(searchPattern)
            Return resources.Count > 0
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.buildResourceMessage(ex)
            Return False
        Catch ex As VisaNS.VisaException
            resources = New String() {ex.ToString}
            Return False
        End Try
    End Function

    ''' <summary> Returns true if the specified resource exists. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">        Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="resourceName"> The resource name. </param>
    ''' <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
    <Extension()>
    Public Function Exists(ByVal value As ResourceManager, ByVal resourceName As String) As Boolean
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        If String.IsNullOrWhiteSpace(resourceName) Then
            Throw New ArgumentNullException("resourceName")
        End If

        Dim resources As String() = value.FindResources()
        Return resources.Contains(resourceName, StringComparer.CurrentCultureIgnoreCase)
    End Function

    ''' <summary> Searches for the interface. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">        Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="resourceName"> The interface resource name. </param>
    ''' <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
    <Extension()>
    Public Function FindInterface(ByVal value As ResourceManager, ByVal resourceName As String) As Boolean
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Dim interfaceType As HardwareInterfaceType
        Dim interfaceNumber As Short
        If value.TryParseResource(resourceName, interfaceType, interfaceNumber) Then
            Dim resources As String() = New String() {}
            If value.TryFindInterfaces(interfaceType, resources) Then
                Return resources.Contains(resourceName, StringComparer.CurrentCultureIgnoreCase)
            End If
        End If
        Return False
    End Function

    ''' <summary> Searches for all interfaces. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <returns> The found interface resource names. </returns>
    <Extension()>
    Public Function FindInterfaces(ByVal value As ResourceManager) As String()
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Return value.FindResources(ResourceManagerExtensions.InterfaceSearchPattern())
    End Function

    ''' <summary> Tries to find interfaces. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="resources">     [in,out] The resources. </param>
    ''' <returns> <c>True</c> if interfaces were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>. </returns>
    <Extension(),
    System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#",
        Justification:="This is the normative implementation of this method.")>
    Public Function TryFindInterfaces(ByVal value As ResourceManager, ByRef resources As String()) As Boolean
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Try
            resources = value.FindInterfaces()
            Return resources.Count > 0
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.buildResourceMessage(ex)
            Return False
        Catch ex As VisaNS.VisaException
            resources = New String() {ex.ToString}
            Return False
        End Try
    End Function

    ''' <summary> Searches for the interfaces. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found interface resource names. </returns>
    <Extension()>
    Public Function FindInterfaces(ByVal value As ResourceManager, ByVal interfaceType As HardwareInterfaceType) As String()
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Return value.FindResources(ResourceManagerExtensions.InterfaceSearchPattern(interfaceType))
    End Function

    ''' <summary> Tries to find interfaces. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="resources">     [in,out] The resources. </param>
    ''' <returns> <c>True</c> if interfaces were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>. </returns>
    <Extension(),
    System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#")>
    Public Function TryFindInterfaces(ByVal value As ResourceManager, ByVal interfaceType As HardwareInterfaceType,
                                      ByRef resources As String()) As Boolean
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Try
            resources = value.FindInterfaces(interfaceType)
            Return resources.Count > 0
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.buildResourceMessage(ex)
            Return False
        Catch ex As VisaNS.VisaException
            resources = New String() {ex.ToString}
            Return False
        End Try
    End Function

    ''' <summary> Searches for the instrument. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">        Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="resourceName"> The instrument resource name. </param>
    ''' <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
    <Extension()>
    Public Function FindInstrument(ByVal value As ResourceManager, ByVal resourceName As String) As Boolean
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Dim interfaceType As HardwareInterfaceType
        Dim interfaceNumber As Short
        If value.TryParseResource(resourceName, interfaceType, interfaceNumber) Then
            Dim resources As String() = New String() {}
            If value.TryFindInstruments(interfaceType, interfaceNumber, resources) Then
                Return resources.Contains(resourceName, StringComparer.CurrentCultureIgnoreCase)
            End If
        End If
        Return False
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <returns> The found instrument resource names. </returns>
    <Extension()>
    Public Function FindInstruments(ByVal value As ResourceManager) As String()
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Return value.FindResources(ResourceManagerExtensions.InstrumentSearchPattern())
    End Function

    ''' <summary> Tries to find instruments. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="resources">     [in,out] The resources. </param>
    ''' <returns> <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>. </returns>
    <Extension(),
    System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="1#",
        Justification:="This is the normative implementation of this method.")>
    Public Function TryFindInstruments(ByVal value As ResourceManager, ByRef resources As String()) As Boolean
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Try
            resources = value.FindInstruments()
            Return resources.Count > 0
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.buildResourceMessage(ex)
            Return False
        Catch ex As VisaNS.VisaException
            resources = New String() {ex.ToString}
            Return False
        End Try
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> The found instrument resource names. </returns>
    <Extension()>
    Public Function FindInstruments(ByVal value As ResourceManager, ByVal interfaceType As HardwareInterfaceType) As String()
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Return value.FindResources(ResourceManagerExtensions.InstrumentSearchPattern(interfaceType))
    End Function

    ''' <summary> Tries to find instruments. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="resources">     [in,out] The resources. </param>
    ''' <returns> <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="2#",
        Justification:="This is the normative implementation of this method.")>
    <Extension()>
    Public Function TryFindInstruments(ByVal value As ResourceManager, ByVal interfaceType As HardwareInterfaceType,
                                       ByRef resources As String()) As Boolean
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Try
            resources = value.FindInstruments(interfaceType)
            Return resources.Count > 0
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.buildResourceMessage(ex)
            Return False
        Catch ex As VisaNS.VisaException
            resources = New String() {ex.ToString}
            Return False
        End Try
    End Function

    ''' <summary> Searches for instruments. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">         Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <param name="boardNumber">   The board number. </param>
    ''' <returns> The found instrument resource names. </returns>
    <Extension()>
    Public Function FindInstruments(ByVal value As ResourceManager, ByVal interfaceType As HardwareInterfaceType, ByVal boardNumber As Integer) As String()
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Return value.FindResources(ResourceManagerExtensions.InstrumentSearchPattern(interfaceType, boardNumber))
    End Function

    ''' <summary> Tries to find instruments. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value">           Reference to the <see cref="ResourceManager">resource
    ''' manager</see>. </param>
    ''' <param name="interfaceType">   Type of the interface. </param>
    ''' <param name="interfaceNumber"> The interface number (e.g., board or port number). </param>
    ''' <param name="resources">       [in,out] The resources. </param>
    ''' <returns> <c>True</c> if instruments were located or false if failed or no instrument resources were
    ''' located. If exception occurred, the exception details are returned in the first element of the
    ''' <paramref name="resources"/>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId:="3#",
        Justification:="This is the normative implementation of this method.")>
    <Extension()>
    Public Function TryFindInstruments(ByVal value As ResourceManager, ByVal interfaceType As HardwareInterfaceType,
                                       ByVal interfaceNumber As Integer, ByRef resources As String()) As Boolean
        If value Is Nothing Then
            Throw New ArgumentNullException("value")
        End If
        Try
            resources = value.FindInstruments(interfaceType, interfaceNumber)
            Return resources.Count > 0
        Catch ex As ArgumentException
            resources = ResourceManagerExtensions.buildResourceMessage(ex)
            Return False
        Catch ex As VisaNS.VisaException
            resources = New String() {ex.ToString}
            Return False
        End Try
    End Function

#End Region

#Region " RESOURCE NAMES "

    ''' <summary> The Gpib instrument resource format. </summary>
    Private Const _GpibInstrumentResourceFormat As String = "GPIB{0}::{1}::INSTR"

    ''' <summary> Builds Gpib instrument resource. </summary>
    ''' <param name="boardNumber"> The board number. </param>
    ''' <param name="address">     The address. </param>
    ''' <returns> The resource name. </returns>
    Public Function BuildGpibInstrumentResource(ByVal boardNumber As Integer, ByVal address As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, ResourceManagerExtensions._GpibInstrumentResourceFormat,
                             boardNumber, address)
    End Function

    ''' <summary> Builds Gpib instrument resource. </summary>
    ''' <param name="boardNumber"> The board number. </param>
    ''' <param name="address">     The address. </param>
    ''' <returns> The resource name. </returns>
    Public Function BuildGpibInstrumentResource(ByVal boardNumber As String, ByVal address As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, ResourceManagerExtensions._GpibInstrumentResourceFormat,
                             boardNumber, address)
    End Function

    ''' <summary> The USB instrument resource format. </summary>
    Private Const _UsbInstrumentResourceFormat As String = "USB{0}::0x{1:X}::0x{2:X}::{3}::INSTR"

    ''' <summary> Builds Gpib instrument resource. </summary>
    ''' <param name="boardNumber"> The board number. </param>
    ''' <returns> The resource name. </returns>
    Public Function BuildUsbInstrumentResource(ByVal boardNumber As Integer, ByVal manufacturerId As Integer, ByVal modelNumber As Integer, ByVal serialNumber As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, ResourceManagerExtensions._UsbInstrumentResourceFormat,
                             boardNumber, manufacturerId, modelNumber, serialNumber)
    End Function

#End Region

End Module
