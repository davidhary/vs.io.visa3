﻿Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.DiagnosticsExtensions
Imports isr.Core.Agnostic.EventHandlerExtensions
''' <summary> Trace handler. </summary>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="1/21/2014" by="David" revision=""> Documented. </history>
Public Class TraceHandler
    Implements IDisposable

#Region " CONSTRUCTOR "

    ''' <summary> Constructor. </summary>
    Public Sub New()
        MyBase.New()
        Me.TraceShowLevel = TraceHandler.TraceLogLevel
    End Sub

#Region "IDisposable Support"

    ''' <summary> Gets the disposed sentinel. </summary>
    ''' <value> The disposed sentinel. </value>
    Private Property Disposed As Boolean

    ''' <summary> Performs application-defined tasks associated with freeing, releasing, or resetting
    ''' unmanaged resources. </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    ''' release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me.disposed Then
            If disposing Then
                Me.RemoveEventHandler(Me.TraceEvent)
            End If
        End If
        Me.disposed = True
    End Sub

    ''' <summary> Performs application-defined tasks associated with freeing, releasing, or resetting
    ''' unmanaged resources. </summary>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

#End Region

#Region " LOG AND SHOW SWITCHES "

    ''' <summary> Gets or sets the trace log level. The trace message is logged if the trace level is lower
    ''' than this value. </summary>
    ''' <value> The trace log level. </value>
    Private Shared ReadOnly Property TraceLogLevel As Diagnostics.TraceEventType
        Get
            Return My.Application.Log.TraceSource.Switch.TraceLevel
        End Get
    End Property

    ''' <summary> Determines if the specified event type should be logged. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> if the specified trace level should be logged. </returns>
    Public Shared Function ShouldLog(ByVal eventType As TraceEventType) As Boolean
        Return eventType <= TraceHandler.TraceLogLevel
    End Function

    ''' <summary> Gets or sets the trace show level. The trace message is displayed if the trace level is
    ''' lower than this value. </summary>
    ''' <value> The trace show level. </value>
    Public Property TraceShowLevel As Diagnostics.TraceEventType

    ''' <summary> Determines if the specified event type should be displayed. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> if the specified trace level should be displayed. </returns>
    Public Function ShouldShow(ByVal eventType As TraceEventType) As Boolean
        Return eventType <= Me.TraceShowLevel
    End Function

    ''' <summary> Determines if the specified event type should be logged or displayed. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> if the specified trace event type should be logged or displayed. </returns>
    Public Function ShouldTrace(ByVal eventType As TraceEventType) As Boolean
        Return TraceHandler.ShouldLog(eventType) OrElse Me.ShouldShow(eventType)
    End Function

#End Region

#Region " EVENTS "

    ''' <summary> Raises the event asking the parent to trace (log and or show) the trace message. </summary>
    Public Event Trace As EventHandler(Of TraceMessageEventArgs)

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As EventHandler(Of TraceMessageEventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.Trace, CType(d, EventHandler(Of TraceMessageEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

    ''' <summary> Raises the event log message to the parent. </summary>
    ''' <param name="e"> The log event arguments. </param>
    ''' <returns> The message details. </returns>
    Public Function OnTrace(ByVal e As TraceMessageEventArgs) As String
        If e IsNot Nothing AndAlso e.TraceMessage IsNot Nothing AndAlso Me.ShouldTrace(e.TraceMessage.EventType) Then
            If Me.ShouldTrace(e.TraceMessage.EventType) Then
                Dim evt As EventHandler(Of TraceMessageEventArgs) = Me.TraceEvent
                evt?.Invoke(Me, e)
            End If
            Return e.TraceMessage.Details
        Else
            Return ""
        End If
    End Function

    ''' <summary> Raises the event log message to the parent. </summary>
    ''' <param name="value"> The trace message. </param>
    ''' <returns> The message details. </returns>
    Public Function OnTrace(ByVal value As TraceMessage) As String
        If value IsNot Nothing Then
            If Me.ShouldTrace(value.EventType) Then
                Dim evt As EventHandler(Of TraceMessageEventArgs) = Me.TraceEvent
                evt?.Invoke(Me, New TraceMessageEventArgs(value))
            End If
            Return value.Details
        Else
            Return ""
        End If
    End Function

#End Region

#Region " LOG / SHOW METHODS "

    ''' <summary> Gets or sets the identifier of the trace event. </summary>
    ''' <value> The identifier of the trace event. </value>
    Public Property TraceEventId As Integer

    ''' <summary> Traces an error message. </summary>
    ''' <param name="message"> Specifies the message to log. </param>
    ''' <returns> The message details. </returns>
    Public Function TraceError(ByVal message As String) As String
        If Me.ShouldTrace(TraceEventType.Error) Then
            Me.OnTrace(New TraceMessage(TraceEventType.Error, TraceEventId, message))
        End If
        Return message
    End Function

    ''' <summary> Traces an error message. </summary>
    ''' <param name="format"> The format. </param>
    ''' <param name="args">   The arguments. </param>
    ''' <returns> The message details. </returns>
    Public Function TraceError(ByVal format As String, ByVal ParamArray args() As Object) As String
        If Me.ShouldTrace(TraceEventType.Error) Then
            Me.OnTrace(New TraceMessage(TraceEventType.Error, TraceEventId, format, args))
        End If
        Return String.Format(format, args)
    End Function

    ''' <summary> Traces an Information message. </summary>
    ''' <param name="message"> Specifies the message to log. </param>
    ''' <returns> The message details. </returns>
    Public Function TraceInformation(ByVal message As String) As String
        If Me.ShouldTrace(TraceEventType.Information) Then
            Me.OnTrace(New TraceMessage(TraceEventType.Information, TraceEventId, message))
        End If
        Return message
    End Function

    ''' <summary> Traces an Information message. </summary>
    ''' <param name="format"> The format. </param>
    ''' <param name="args">   The arguments. </param>
    ''' <returns> The message details. </returns>
    Public Function TraceInformation(ByVal format As String, ByVal ParamArray args() As Object) As String
        If Me.ShouldTrace(TraceEventType.Information) Then
            Me.OnTrace(New TraceMessage(TraceEventType.Information, TraceEventId, format, args))
        End If
        Return String.Format(format, args)
    End Function

    ''' <summary> Traces an Warning message. </summary>
    ''' <param name="message"> Specifies the message to log. </param>
    Public Function TraceWarning(ByVal message As String) As String
        If Me.ShouldTrace(TraceEventType.Warning) Then
            Me.OnTrace(New TraceMessage(TraceEventType.Warning, TraceEventId, message))
        End If
        Return message
    End Function

    ''' <summary> Traces an Warning message. </summary>
    ''' <param name="format"> The format. </param>
    ''' <param name="args">   The arguments. </param>
    ''' <returns> The message details. </returns>
    Public Function TraceWarning(ByVal format As String, ByVal ParamArray args() As Object) As String
        If Me.ShouldTrace(TraceEventType.Warning) Then
            Me.OnTrace(New TraceMessage(TraceEventType.Warning, TraceEventId, format, args))
        End If
        Return String.Format(format, args)
    End Function

    ''' <summary> Traces an Verbose message. </summary>
    ''' <param name="message"> Specifies the message to log. </param>
    ''' <returns> The message details. </returns>
    Public Function TraceVerbose(ByVal message As String) As String
        If Me.ShouldTrace(TraceEventType.Verbose) Then
            Me.OnTrace(New TraceMessage(TraceEventType.Verbose, TraceEventId, message))
        End If
        Return message
    End Function

    ''' <summary> Traces an Verbose message. </summary>
    ''' <param name="format"> The format. </param>
    ''' <param name="args">   The arguments. </param>
    ''' <returns> The message details. </returns>
    Public Function TraceVerbose(ByVal format As String, ByVal ParamArray args() As Object) As String
        If Me.ShouldTrace(TraceEventType.Verbose) Then
            Me.OnTrace(New TraceMessage(TraceEventType.Verbose, TraceEventId, format, args))
        End If
        Return String.Format(format, args)
    End Function

#End Region

End Class
