﻿Imports System.Drawing
Imports System.Windows.Forms
Imports System.ComponentModel
Imports isr.Core.Agnostic
''' <summary> A form that receives trace messages. </summary>
''' <remarks> Requires the use of the <see cref="TraceObserverFormBaseWrapper">wrapper base form</see> with the designer. 
''' <example> <code>
''' #Const designMode1 = True
''' #Region " BASE FROM WRAPPER "
'''     ' Designing requires changing the condition to True.
''' #If designMode Then
'''     Inherits TraceObserverFormBaseWrapper
''' #Else
'''     Inherits TraceObserverFormBase
''' #End If
''' #End Region
''' </code> </example> </remarks>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/31/2013" by="David" revision="6.1.4779.x"> created. </history>
Public MustInherit Class TraceObserverFormBase
    Inherits System.Windows.Forms.Form
    Implements ITraceMessageObserver

#Region " CONSTRUCTION + CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived classes. </summary>
    Protected Sub New()
        MyBase.New()
        Me.InitializeComponent()
    End Sub

    ''' <summary> Initializes the component. </summary>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(331, 341)
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "TraceObserverFormBase"
        Me.ResumeLayout(False)
    End Sub

#Region " CLASS STYLE "

    ''' <summary> The enable drop shadow version. </summary>
    Public Const EnableDropShadowVersion As Integer = 5

    ''' <summary> Gets or sets the class style. </summary>
    ''' <value> The class style. </value>
    Protected Property ClassStyle As ClassStyleConstants = ClassStyleConstants.None

    ''' <summary> Adds a drop shadow parameter. </summary>
    ''' <remarks> From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx. </remarks>
    ''' <value> Options that control the create. </value>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand,
                                                        Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CInt(Me.ClassStyle)
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " FORM EVENTS "

    ''' <summary> Gets or sets the sentinel indicating that the form loaded without an exception.
    '''           Should be set only if load did not fail. </summary>
    ''' <value> The is loaded. </value>
    Protected Property IsLoaded As Boolean

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Load" /> event after reading the
    ''' start position from the configuration file. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As System.EventArgs)
        Try
            If Not Me.DesignMode Then
            End If
            Me.IsLoaded = True
        Catch
            Throw
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

#End Region

#Region " I TRACE OBSERVER "

    ''' <summary> Displays a trace message described by value. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overridable Sub DisplayTraceMessage(ByVal value As TraceMessage)
        If value IsNot Nothing Then
            Dim s As String = value.ExtractSynopsis()
            If Not String.IsNullOrWhiteSpace(s) Then Me.DisplaySynopsis(s)
            Me.DisplayMessage(value)
        End If
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected MustOverride Sub DisplaySynopsis(ByVal value As String)

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected MustOverride Sub DisplayMessage(ByVal value As TraceMessage)

    ''' <summary> Observes the Trace event published by <see cref="ITraceMessagePublisher">trace publishers</see>. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="TraceMessageEventArgs" /> instance containing the event data. </param>
    Protected Overridable Sub OnTraceMessageAvailable(ByVal sender As Object, ByVal e As TraceMessageEventArgs) Implements ITraceMessageObserver.OnTraceMessageAvailable
        If sender IsNot Nothing AndAlso e IsNot Nothing Then
            Me.DisplayTraceMessage(e.TraceMessage)
        End If
    End Sub

    ''' <summary> Gets or sets the trace show level. The trace message is displayed if the trace level is lower
    ''' than this value. </summary>
    ''' <value> The trace show level. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected MustOverride ReadOnly Property TraceShowLevel As Diagnostics.TraceEventType Implements ITraceMessageObserver.TraceShowLevel

    ''' <summary> Determines if the trace event type should be displayed. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> if the trace event type should be displayed. </returns>
    Protected Function ShouldShow(ByVal eventType As TraceEventType) As Boolean Implements ITraceMessageObserver.ShouldShow
        Return eventType <= Me.TraceShowLevel
    End Function

#End Region

#Region " I TRACE OBSERVER HELPER METHODS "

    ''' <summary> Displays a trace message described by value. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    ''' <returns> A Trace Message. </returns>
    Private Function _DisplayTraceMessage(ByVal value As TraceMessage) As TraceMessage
        Me.DisplayTraceMessage(value)
        Return value
    End Function

    ''' <summary> Displays and logs the trace message. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier. </param>
    ''' <param name="format">    The format. </param>
    ''' <param name="args">      The arguments. </param>
    ''' <returns> The built <see cref="TraceMessage">trace message</see>. </returns>
    Protected Function OnTraceMessageAvailable(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As TraceMessage
        Return Me._DisplayTraceMessage(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Displays and logs the trace message. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="details">   The details. </param>
    ''' <returns> The built <see cref="TraceMessage">trace message</see>. </returns>
    Protected Function OnTraceMessageAvailable(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal details As String) As TraceMessage
        Return Me._DisplayTraceMessage(New TraceMessage(eventType, id, details))
    End Function

#End Region

End Class

''' <summary> Trace observer base form and wrapper. </summary>
''' <remarks> This class is required to permit the use of the
''' <see cref="TraceObserverFormBase">base form</see> with the designer. </remarks>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="1/28/2014" by="David"> Created. </history>
Public Class TraceObserverFormBaseWrapper
    Inherits TraceObserverFormBase

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overrides Sub DisplayMessage(value As TraceMessage)
        Debug.Assert(False, "Illegal call; reset design mode")
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected Overrides Sub DisplaySynopsis(value As String)
    End Sub

    ''' <summary> Gets the trace display level. </summary>
    ''' <value> The trace display level. </value>
    Public Property TraceDisplayLevel As TraceEventType = TraceEventType.Verbose

    ''' <summary> Gets or sets the trace show level. The trace message is displayed if the trace level is lower
    ''' than this value. </summary>
    ''' <value> The trace show level. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected Overrides ReadOnly Property TraceShowLevel As Diagnostics.TraceEventType
        Get
            Return Me.TraceDisplayLevel
        End Get
    End Property

End Class
