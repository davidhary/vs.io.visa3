﻿Imports System.Threading
Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.EventHandlerExtensions
Imports isr.Core.Agnostic.ExceptionExtensions
Imports isr.IO.Visa.EventHandlerExtensions
''' <summary> Defines the contract that must be implemented by trace and property publishers. </summary>
''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/30/14" by="David" revision="1.2.5143"> Based on legacy property publisher.
''' Added trace publisher. </history>
Public MustInherit Class TracePropertyChangedPublisherBase
    Inherits isr.Core.Agnostic.PublisherBase
    Implements IDisposable, INotifyPropertyChanged, ITraceMessagePublisher

#Region " CONSTRUCTION + CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TracePropertyPublisherBase" /> class. </summary>
    Protected Sub New()
        MyBase.New()
    End Sub

#Region " Disposable Support "

    ''' <summary> Performs application-defined tasks associated with freeing, releasing, or resetting
    ''' unmanaged resources. </summary>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> Gets or sets the dispose status sentinel of the base class.  This applies to the
    ''' derived class provided proper implementation. </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    ''' release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not Me.IsDisposed Then

                If disposing Then
                    Me.RemoveEventHandler(Me.PropertyChangedEvent)
                    Me.RemoveEventHandler(Me.TraceMessageAvailableEvent)
                End If

                ' Free shared unmanaged resources

            End If

        Finally

            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True

        End Try

    End Sub

#End Region

#End Region

#Region " SYNC CONTEXT "

    ''' <summary> Caches the synchronization context for threading functions. </summary>
    ''' <value> The captured synchronization context. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property CapturedSyncContext As Threading.SynchronizationContext

    ''' <summary> Applies the captured or a new synchronization context. </summary>
    Public Sub ApplyCapturedSyncContext()
        If SynchronizationContext.Current Is Nothing Then
            If Me.CapturedSyncContext Is Nothing Then Me._CapturedSyncContext = New SynchronizationContext
            Threading.SynchronizationContext.SetSynchronizationContext(Me.CapturedSyncContext)
        End If
    End Sub

    ''' <summary> Captures and applies synchronization context. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when the captured sync context is null. </exception>
    ''' <param name="syncContext"> Context for the synchronization. </param>
    Public Overridable Sub CaptureSyncContext(ByVal syncContext As Threading.SynchronizationContext)
        If syncContext Is Nothing Then Throw New ArgumentNullException(NameOf(syncContext))
        Me._CapturedSyncContext = syncContext
        Me.ApplyCapturedSyncContext()
    End Sub

#End Region

#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Removes the event handler. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangedEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    Public Function ChangedEvent() As PropertyChangedEventHandler
        Return Me.PropertyChangedEvent
    End Function

#Region " NOTIFY DEFAULT (POST DYNAMIC INVOKE)"

    ''' <summary>
    ''' Asynchronously notifies (default) property change on a different thread. Safe for cross
    ''' threading.
    ''' </summary>
    ''' <remarks>
    ''' The legacy default for property change notification was to posting a dynamic invoke. For
    ''' backward compatibility, this is set as the default.
    ''' </remarks>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overridable Sub NotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        ' the legacy default was posting property change was posting a dynamic invoke.
		Me.ApplyCapturedSyncContext()
        Me.PropertyChangedEvent.PostDynamicInvoke(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously (default) notifies property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Overridable Sub NotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.NotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#Region " ASYNC NOTIFY (POST"

    ''' <summary>
    ''' Asynchronously notifies (post dynamic invoke) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overridable Sub AsyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
		Me.ApplyCapturedSyncContext()
        Me.PropertyChangedEvent.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Asynchronously notifies (post dynamic invoke) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Overridable Sub AsyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.AsyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#Region " SYNC NOTIFY (SEND)"

    ''' <summary>
    ''' Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overridable Sub SyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
		Me.ApplyCapturedSyncContext()
        Me.PropertyChangedEvent.SafeSend(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Overridable Sub SyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#End Region

#End Region

#Region " TRACE MESSAGE AVAILABLE EVENT IMPLEMENTATION "

    ''' <summary> Event queue for all listeners interested in TraceMessageAvailable events. </summary>
    Public Event TraceMessageAvailable As EventHandler(Of TraceMessageEventArgs) Implements ITraceMessagePublisher.TraceMessageAvailable

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As EventHandler(Of TraceMessageEventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.TraceMessageAvailable, CType(d, EventHandler(Of TraceMessageEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#Region " I TRACE MESSAGE PUBLISHER "

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <remarks> Override this method in cases the trace message needs to be used (e.g., displayed or logged) before it is raised. </remarks>
    ''' <param name="value"> The Trace Message to process. </param>
    Protected Overridable Sub OnTraceMessageAvailable(ByVal value As TraceMessage) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If value IsNot Nothing Then
            Me.TraceMessageAvailableEvent.SafePost(Me, New TraceMessageEventArgs(value))
            Application.DoEvents()
        End If
    End Sub

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> The event arguments. </returns>
    Protected Function OnTraceMessageAvailable(ByVal eventType As TraceEventType, ByVal id As Integer,
                                               ByVal format As String, ByVal ParamArray args() As Object) As TraceMessage Implements ITraceMessagePublisher.OnTraceMessageAvailable
        Dim e As New TraceMessage(eventType, id, format, args)
        Me.OnTraceMessageAvailable(e)
        Return e
    End Function

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnTraceMessageAvailable(ByVal e As TraceMessageEventArgs) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If e IsNot Nothing Then
            Me.OnTraceMessageAvailable(e.TraceMessage)
        End If
    End Sub

#End Region

#End Region

End Class

#Region " UNUSED "
#If False Then
#Region " SAFE EVENTS "

#Region " TRACE MESSAGE AVAILABLE EVENT IMPLEMENTATION "

    ''' <summary> Event queue for all listeners interested in TraceMessageAvailable events. </summary>
    Public Event TraceMessageAvailable As EventHandler(Of TraceMessageEventArgs) Implements ITraceMessagePublisher.TraceMessageAvailable

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As EventHandler(Of TraceMessageEventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.TraceMessageAvailable, CType(d, EventHandler(Of TraceMessageEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) the <see cref="TraceMessageAvailable">trace message
    ''' available Event</see>. Must be called with the <see cref="SynchronizationContext">sync
    ''' context</see> </summary>
    ''' <param name="e"> The <see cref="TraceMessageEventArgs" /> instance containing the event data. </param>
    Private Sub InvokeTraceMessageAvailable(ByVal e As TraceMessageEventArgs)
        Dim evt As EventHandler(Of TraceMessageEventArgs) = Me.TraceMessageAvailableEvent
        evt?.Invoke(Me, e)
        Application.DoEvents()
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) the <see cref="TraceMessageAvailable">trace message
    ''' available Event</see>. Must be called with the <see cref="SynchronizationContext">sync
    ''' context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokeTraceMessageAvailable(ByVal obj As Object)
        Me.InvokeTraceMessageAvailable(CType(obj, TraceMessageEventArgs))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes)  the
    ''' <see cref="TraceMessageAvailable">trace message available Event</see>. </summary>
    ''' <param name="e"> The <see cref="TraceMessageEventArgs" /> instance containing the event data. </param>
    Private Sub SafeBeginInvokeTraceMessageAvailable(ByVal e As TraceMessageEventArgs)
        Me.TraceMessageAvailableEvent.SafeBeginInvoke(Me, e)
    End Sub

#End Region

#Region " I TRACE MESSAGE PUBLISHER "

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <remarks> Override this method in cases the trace message needs to be used (e.g., displayed or logged) before it is raised. </remarks>
    ''' <param name="value"> The Trace Message to process. </param>
    Protected Overridable Sub OnTraceMessageAvailable(ByVal value As TraceMessage) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If value IsNot Nothing Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokeTraceMessageAvailable(New TraceMessageEventArgs(value))
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf Me.InvokeTraceMessageAvailable),
                                                                   New TraceMessageEventArgs(value))
            End If
            Application.DoEvents()
        End If
    End Sub

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> The event arguments. </returns>
    Protected Function OnTraceMessageAvailable(ByVal eventType As TraceEventType, ByVal id As Integer,
                                               ByVal format As String, ByVal ParamArray args() As Object) As TraceMessage Implements ITraceMessagePublisher.OnTraceMessageAvailable
        Dim e As New TraceMessage(eventType, id, format, args)
        Me.OnTraceMessageAvailable(e)
        Return e
    End Function

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnTraceMessageAvailable(ByVal e As TraceMessageEventArgs) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If e IsNot Nothing Then
            Me.OnTraceMessageAvailable(e.TraceMessage)
        End If
    End Sub

#End Region

#End Region

#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Removes the event handler. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub RemoveEventHandler(value As PropertyChangedEventHandler)
        For Each d As [Delegate] In Me.PropertyChangedEvent.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then
            Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
            evt?.Invoke(Me, e)
            Application.DoEvents()
        End If
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokePropertyChanged(ByVal obj As Object)
        Me.InvokePropertyChanged(CType(obj, System.ComponentModel.PropertyChangedEventArgs))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    Protected Sub SafeInvokePropertyChanged(ByVal name As String)
        Me.SafeInvokePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then Me.PropertyChangedEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    Protected Sub SafeBeginInvokePropertyChanged(ByVal name As String)
        Me.SafeBeginInvokePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeBeginInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then Me.PropertyChangedEvent.SafeBeginInvoke(Me, e)
    End Sub

#End Region

#Region " SYNCHRONOUS NOTIFICATIONS "

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Value. </summary>
    Protected Sub SyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeInvokePropertyChanged(e)
            Else
                SynchronizationContext.Current.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged), e)
            End If
            Application.DoEvents()
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    Protected Sub SyncNotifyPropertyChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafeSendPropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Get value. </summary>
    ''' <remarks> Strips the "get_" prefix derived from using reflection to get the current function
    ''' name from a property Get construct. </remarks>
    ''' <param name="name"> The 'Get' property name. </param>
    Protected Sub SyncNotifyPropertyGetChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafeSendPropertyChanged(name.TrimStart("get_".ToCharArray()))
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Set value. </summary>
    ''' <remarks> Strips the "set_" prefix derived from using reflection to Set the current function
    ''' name from a property Set construct. </remarks>
    ''' <param name="name"> The 'Set' property name. </param>
    Protected Sub SyncNotifyPropertySetChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafeSendPropertyChanged(name.TrimStart("set_".ToCharArray()))
        End If
    End Sub

#End Region

#Region " ASYNCHRONOUS NOTIFICATIONS "

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="e"> Property Changed event information. </param>
    Protected Sub SafePostPropertyChanged(ByVal e As PropertyChangedEventArgs)
        If Me.Publishable Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokePropertyChanged(e)
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged), e)
            End If
            Application.DoEvents()
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    Protected Sub SafePostPropertyChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafePostPropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begin
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Get value. </summary>
    ''' <remarks> Strips the "get_" prefix derived from using reflection to get the current function
    ''' name from a property Get construct. </remarks>
    ''' <param name="name"> The 'Get' property name. </param>
    Protected Sub AsyncNotifyPropertyGetChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafePostPropertyChanged(name.TrimStart("get_".ToCharArray()))
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begin
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Set value. </summary>
    ''' <remarks> Strips the "set_" prefix derived from using reflection to Set the current function
    ''' name from a property Set construct. </remarks>
    ''' <param name="name"> The 'Set' property name. </param>
    Protected Sub AsyncNotifyPropertySetChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafePostPropertyChanged(name.TrimStart("set_".ToCharArray()))
        End If
    End Sub

#End Region

#End Region

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Sub SafeInvoke(ByVal handler As EventHandler(Of System.EventArgs))
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, System.EventArgs.Empty})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d . Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, System.EventArgs.Empty})
                Else
                    target.Invoke(d, New Object() {Me, System.EventArgs.Empty})
                End If
            End If
            Application.DoEvents()
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    Protected Sub SafeBeginInvoke(ByVal handler As EventHandler(Of System.EventArgs))
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {Me, System.EventArgs.Empty})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d . Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {Me, System.EventArgs.Empty})
                Else
                    target.BeginInvoke(d, New Object() {Me, System.EventArgs.Empty})
                End If
            End If
            Application.DoEvents()
        Next
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    Public Shared Sub SafeInvoke(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, System.EventArgs.Empty})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d . Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, System.EventArgs.Empty})
                Else
                    target.Invoke(d, New Object() {sender, System.EventArgs.Empty})
                End If
            End If
            Application.DoEvents()
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of System.EventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    Public Shared Sub SafeBeginInvoke(ByVal handler As EventHandler(Of System.EventArgs), ByVal sender As Object)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, System.EventArgs.Empty})
            Else
                Dim target As ISynchronizeInvoke = TryCast(d . Target, ISynchronizeInvoke)
                If target Is Nothing Then
                    d.DynamicInvoke(New Object() {sender, System.EventArgs.Empty})
                Else
                    target.BeginInvoke(d, New Object() {sender, System.EventArgs.Empty})
                End If
            End If
            Application.DoEvents()
        Next
    End Sub

    ''' <summary> Synchronously (Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of TEventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The arguments for the event. </param>
    ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
    Public Shared Sub SafeInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs),
                                                             ByVal sender As Object, ByVal e As TEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, e})
            Else
                Dim target As System.ComponentModel.ISynchronizeInvoke = TryCast(d . Target, System.ComponentModel.ISynchronizeInvoke)
                If target IsNot Nothing AndAlso target.InvokeRequired Then
                    ' asynchronously executes the delegate on the target thread.
                    target.BeginInvoke(d, New Object() {sender, e})
                Else
                    d.DynamicInvoke(New Object() {sender, e})
                End If
            End If
            Application.DoEvents()
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of TEventArgs)">Event</see>. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The arguments for the event. </param>
    ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
    Public Shared Sub SafeBeginInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs),
                                                              ByVal sender As Object, ByVal e As TEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, e})
            Else
                Dim target As System.ComponentModel.ISynchronizeInvoke = TryCast(d . Target, System.ComponentModel.ISynchronizeInvoke)
                If target IsNot Nothing AndAlso target.InvokeRequired Then
                    ' asynchronously executes the delegate on the target thread.
                    target.BeginInvoke(d, New Object() {sender, e})
                Else
                    d.DynamicInvoke(New Object() {sender, e})
                End If
            End If
            Application.DoEvents()
        Next
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies an
    ''' <see cref="EventHandler(Of TEventArgs)">Event</see> and waits for its completion. </summary>
    ''' <param name="handler"> The event handler. </param>
    ''' <param name="sender">  The sender of the event. </param>
    ''' <param name="e">       The arguments for the event. </param>
    ''' <typeparam name="TEventArgs"> The type of the event arguments. </typeparam>
    Public Shared Sub SafeBeginEndInvoke(Of TEventArgs As EventArgs)(ByVal handler As EventHandler(Of TEventArgs),
                                                                     ByVal sender As Object, ByVal e As TEventArgs)
        For Each d As [Delegate] In handler.SafeInvocationList
            If d.Target Is Nothing Then
                d.DynamicInvoke(New Object() {sender, e})
            Else
                Dim target As System.ComponentModel.ISynchronizeInvoke = TryCast(d . Target, System.ComponentModel.ISynchronizeInvoke)
                If target IsNot Nothing AndAlso target.InvokeRequired Then
                    ' asynchronously executes the delegate on the target thread.
                    Dim result As IAsyncResult = target.BeginInvoke(d, New Object() {sender, e})
                    If result IsNot Nothing Then
                        ' waits until the process ends.
                        target.EndInvoke(result)
                    End If
                Else
                    d.DynamicInvoke(New Object() {sender, e})
                End If
            End If
            Application.DoEvents()
        Next
    End Sub

#End Region

#End If
#End Region
