﻿Imports System.Drawing
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Threading
Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.EventHandlerExtensions
Imports isr.Core.Agnostic.ExceptionExtensions
''' <summary> A base control implementing property notifications and trace publishing. </summary>
''' <remarks> Requires the use of the <see cref="TracePublisherControlBaseWrapper">wrapper base control</see> with the designer. 
''' <example> <code>
''' #Const designMode1 = True
''' #Region " BASE FROM WRAPPER "
''' ' Designing requires changing the condition to True.
''' #If designMode Then
'''     ' Designing requires changing the condition to True.
'''     Inherits TracePublisherControlBaseWrapper
''' #Else
'''     Inherits TracePublisherControlBase
''' #End If
''' #End Region
''' </code> </example> </remarks>
''' <license> (c) 2014 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="1/27/2014" by="David" revision="2.0.5140"> Created. </history>
Public MustInherit Class TracePublisherControlBase
    Inherits System.Windows.Forms.UserControl
    Implements System.ComponentModel.INotifyPropertyChanged, ITraceMessagePublisher

#Region " CONSTRUCTION + CLEANUP "

    ''' <summary> A private constructor for this class making it not publicly creatable. This ensure
    ''' using the class as a singleton. </summary>
    Protected Sub New()
        MyBase.new()
        Me.InitializeComponent()
    End Sub

#Region " Windows Form Designer generated code "

    ''' <summary>
    ''' Releases the unmanaged resources used by the TracePublisherControlBase and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    Me.RemoveEventHandler(Me.PropertyChangedEvent)
                    Me.RemoveEventHandler(Me.TraceMessageAvailableEvent)
                    ' unable to use null conditional because it is not seen by code analysis
                    If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'MyUserControlBase
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit
        Me.Font = New Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, FontStyle.Regular, GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "MyUserControlBase"
        Me.Size = New System.Drawing.Size(175, 173)
        Me.ResumeLayout(False)

    End Sub

#End Region

#End Region

#Region " SYNC CONTEXT "

    ''' <summary> Caches the synchronization context for threading functions. </summary>
    ''' <value> The captured synchronization context. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property CapturedSyncContext As Threading.SynchronizationContext

    ''' <summary> Applies the captured or a new synchronization context. </summary>
    Public Sub ApplyCapturedSyncContext()
        If SynchronizationContext.Current Is Nothing Then
            If Me.CapturedSyncContext Is Nothing Then Me._CapturedSyncContext = New SynchronizationContext
            Threading.SynchronizationContext.SetSynchronizationContext(Me.CapturedSyncContext)
        End If
    End Sub

    ''' <summary> Captures and applies synchronization context. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when the captured sync context is null. </exception>
    ''' <param name="syncContext"> Context for the synchronization. </param>
    Public Overridable Sub CaptureSyncContext(ByVal syncContext As Threading.SynchronizationContext)
        If syncContext Is Nothing Then Throw New ArgumentNullException(NameOf(syncContext))
        Me._CapturedSyncContext = syncContext
        Me.ApplyCapturedSyncContext()
    End Sub

#End Region

#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Removes the event handler. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As PropertyChangedEventHandler)
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
            End Try
        Next
    End Sub

    Public Function ChangedEvent() As PropertyChangedEventHandler
        Return Me.PropertyChangedEvent
    End Function

#Region " NOTIFY DEFAULT (POST DYNAMIC INVOKE)"

    ''' <summary>
    ''' Asynchronously notifies (default) property change on a different thread. Safe for cross
    ''' threading.
    ''' </summary>
    ''' <remarks>
    ''' The legacy default for property change notification was to posting a dynamic invoke. For
    ''' backward compatibility, this is set as the default.
    ''' </remarks>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overridable Sub NotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
        ' the legacy default was posting property change was posting a dynamic invoke.
		Me.ApplyCapturedSyncContext()
        Me.PropertyChangedEvent.PostDynamicInvoke(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously (default) notifies property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Overridable Sub NotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.NotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#Region " ASYNC NOTIFY (POST"

    ''' <summary>
    ''' Asynchronously notifies (post dynamic invoke) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overridable Sub AsyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
		Me.ApplyCapturedSyncContext()
        Me.PropertyChangedEvent.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Asynchronously notifies (post dynamic invoke) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Overridable Sub AsyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.AsyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#Region " SYNC NOTIFY (SEND)"

    ''' <summary>
    ''' Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="e"> Property changed event information. </param>
    Protected Overridable Sub SyncNotifyPropertyChanged(ByVal e As PropertyChangedEventArgs)
		Me.ApplyCapturedSyncContext()
        Me.PropertyChangedEvent.SafeSend(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")>
    Protected Overridable Sub SyncNotifyPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SyncNotifyPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#End Region

#Region " TRACE MESSAGE AVAILABLE EVENT IMPLEMENTATION "

    ''' <summary> Event queue for all listeners interested in TraceMessageAvailable events. </summary>
    Public Event TraceMessageAvailable As EventHandler(Of TraceMessageEventArgs) Implements ITraceMessagePublisher.TraceMessageAvailable

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As EventHandler(Of TraceMessageEventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.TraceMessageAvailable, CType(d, EventHandler(Of TraceMessageEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#Region " I TRACE MESSAGE PUBLISHER "

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <remarks> Override this method in cases the trace message needs to be used (e.g., displayed or logged) before it is raised. </remarks>
    ''' <param name="value"> The Trace Message to process. </param>
    Protected Overridable Sub OnTraceMessageAvailable(ByVal value As TraceMessage) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If value IsNot Nothing Then
            Me.TraceMessageAvailableEvent.SafePost(Me, New TraceMessageEventArgs(value))
            Application.DoEvents()
        End If
    End Sub

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> The event arguments. </returns>
    Protected Function OnTraceMessageAvailable(ByVal eventType As TraceEventType, ByVal id As Integer,
                                               ByVal format As String, ByVal ParamArray args() As Object) As TraceMessage Implements ITraceMessagePublisher.OnTraceMessageAvailable
        Dim e As New TraceMessage(eventType, id, format, args)
        Me.OnTraceMessageAvailable(e)
        Return e
    End Function

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnTraceMessageAvailable(ByVal e As TraceMessageEventArgs) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If e IsNot Nothing Then
            Me.OnTraceMessageAvailable(e.TraceMessage)
        End If
    End Sub

#End Region

#Region " TRACE MESSAGE PUBLISHER OVERRIDABLE "

    ''' <summary> Displays a trace message described by value. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overridable Sub DisplayTraceMessage(ByVal value As TraceMessage)
        If value IsNot Nothing Then
            Dim s As String = value.ExtractSynopsis()
            If Not String.IsNullOrEmpty(s) Then DisplaySynopsis(s)
            Me.DisplayMessage(value)
        End If
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected MustOverride Sub DisplaySynopsis(ByVal value As String)

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected MustOverride Sub DisplayMessage(ByVal value As TraceMessage)

#End Region

#End Region

#Region " TOOL TIP WORK AROUND "

    ''' <summary> Sets the Tool tip for all form controls that inherit a <see cref="Control">control base.</see> </summary>
    ''' <param name="parent">  Reference to the parent form or control. </param>
    ''' <param name="toolTip"> The parent form or control tool tip. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")>
    Public Shared Sub ToolTipSetter(ByVal parent As Control, ByVal toolTip As ToolTip)
        If parent Is Nothing Then Return
        If toolTip Is Nothing Then Return
        If TypeOf parent Is TracePublisherControlBase Then
            CType(parent, TracePublisherControlBase).ToolTipSetter(toolTip)
        ElseIf parent.HasChildren Then
            For Each control As Control In parent.Controls
                ToolTipSetter(control, toolTip)
            Next
        End If
    End Sub

    ''' <summary> Sets a tool tip for all controls on the user control. Uses the message already set
    ''' for this control. </summary>
    ''' <remarks> This is required because setting a tool tip from the parent form does not show the
    ''' tool tip if hovering above children controls hosted by the user control. </remarks>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    Protected Sub ToolTipSetter(ByVal toolTip As ToolTip)
        If toolTip IsNot Nothing Then
            applyToolTipToChildControls(Me, toolTip, toolTip.GetToolTip(Me))
        End If
    End Sub

    ''' <summary> Sets a tool tip for all controls on the user control. </summary>
    ''' <remarks> This is required because setting a tool tip from the parent form does not show the
    ''' tool tip if hovering above children controls hosted by the user control. </remarks>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    ''' <param name="message"> The tool tip message to apply to all the children controls and their
    ''' children. </param>
    Protected Sub ToolTipSetter(ByVal toolTip As ToolTip, ByVal message As String)
        applyToolTipToChildControls(Me, toolTip, message)
    End Sub

    ''' <summary> Applies the tool tip to all control hosted by the parent as well as all the children
    ''' with these control. </summary>
    ''' <param name="parent">  The parent control. </param>
    ''' <param name="toolTip"> The tool tip control from the parent form. </param>
    ''' <param name="message"> The tool tip message to apply to all the children controls and their
    ''' children. </param>
    Private Sub ApplyToolTipToChildControls(ByVal parent As Control, ByVal toolTip As ToolTip, ByVal message As String)
        For Each control As Control In parent.Controls
            toolTip.SetToolTip(control, message)
            If parent.HasChildren Then
                applyToolTipToChildControls(control, toolTip, message)
            End If
        Next
    End Sub

#End Region

End Class

''' <summary> Trace publisher control base wrapper. </summary>
''' <remarks> This class is required to permit the use of the
''' <see cref="TracePublisherControlBase">base control</see> with the designer. </remarks>
''' <license> (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="1/28/2014" by="David" revision=""> Created. </history>
Public Class TracePublisherControlBaseWrapper
    Inherits TracePublisherControlBase

#Region " I TRACE PUBLISHER "

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overrides Sub DisplayMessage(value As TraceMessage)
        Debug.Assert(False, "Illegal call; reset design mode")
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected Overrides Sub DisplaySynopsis(value As String)
    End Sub

#End Region

End Class

#Region " UNUSED "
#If False Then
#Region " PROPERTY CHANGED EVENT IMPLEMENTATION "

    ''' <summary> Gets the sentinel indicating that multiple synchronization contexts are expected. </summary>
    ''' <remarks> When having multiple user interfaces or a thread running within the user interface,
    ''' the current synchronization context may not reflect the contexts of the current UI causing a
    ''' cross thread exceptions. In this case the more complex
    ''' <see cref="SafeInvokePropertyChanged">thread safe methods</see> must be used. </remarks>
    ''' <value> <c>True</c> if more than one synchronization contexts should be expected. </value>
    Public Property MultipleSyncContextsExpected As Boolean

    ''' <summary> Event that is raised when a property value changes. </summary>
    Public Event PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    ''' <summary> Removes the event handler. </summary>
    ''' <param name="value"> The value. </param>
    Private Sub RemoveEventHandler(value As PropertyChangedEventHandler)
        For Each d As [Delegate] In Me.PropertyChangedEvent.SafeInvocationList
            Try
                RemoveHandler Me.PropertyChanged, CType(d, PropertyChangedEventHandler)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub InvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.PropertyChangedEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. Must be called with the
    ''' <see cref="SynchronizationContext">sync context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokePropertyChanged(ByVal obj As Object)
        Me.InvokePropertyChanged(CType(obj, System.ComponentModel.PropertyChangedEventArgs))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    Protected Sub SafeInvokePropertyChanged(ByVal name As String)
        Me.SafeInvokePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.PropertyChangedEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="name"> The property name. </param>
    ''' <remarks> Use this method to prevent cross thread exceptions when having multiple sync contexts. </remarks>
    Protected Sub SafeBeginInvokePropertyChanged(ByVal name As String)
        Me.SafeBeginInvokePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(name))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes) notifies a change
    ''' <see cref="PropertyChanged">Event</see> in a property value. </summary>
    ''' <param name="e"> The <see cref="PropertyChangedEventArgs" /> instance containing the event
    ''' data. </param>
    Private Sub SafeBeginInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.PropertyChangedEvent.SafeBeginInvoke(Me, e)
    End Sub

#End Region

#Region " SYNCHRONOUS NOTIFICATIONS "

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    Protected Sub SyncNotifyPropertyChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeInvokePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(name))
            Else
                SynchronizationContext.Current.Send(New SendOrPostCallback(AddressOf InvokePropertyChanged),
                                                    New System.ComponentModel.PropertyChangedEventArgs(name))
            End If
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Get value. </summary>
    ''' <remarks> Strips the "get_" prefix derived from using reflection to get the current function
    ''' name from a property Get construct. </remarks>
    ''' <param name="name"> The 'Get' property name. </param>
    Protected Sub SyncNotifyPropertyGetChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafeSendPropertyChanged(name.TrimStart("get_".ToCharArray()))
        End If
    End Sub

    ''' <summary> Synchronously notifies  (<see cref="SynchronizationContext.Send">sends</see>, Invokes
    ''' or Dynamically Invokes) a change <see cref="PropertyChanged">event</see> in Property Set value. </summary>
    ''' <remarks> Strips the "set_" prefix derived from using reflection to Set the current function
    ''' name from a property Set construct. </remarks>
    ''' <param name="name"> The 'Set' property name. </param>
    Protected Sub SyncNotifyPropertySetChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafeSendPropertyChanged(name.TrimStart("set_".ToCharArray()))
        End If
    End Sub

#End Region

#Region " ASYNCHRONOUS NOTIFICATIONS "

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begins
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Value. </summary>
    ''' <param name="name"> The property name. </param>
    Protected Sub SafePostPropertyChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokePropertyChanged(New System.ComponentModel.PropertyChangedEventArgs(name))
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf InvokePropertyChanged),
                                                                  New System.ComponentModel.PropertyChangedEventArgs(name))
            End If
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begin
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Get value. </summary>
    ''' <remarks> Strips the "get_" prefix derived from using reflection to get the current function
    ''' name from a property Get construct. </remarks>
    ''' <param name="name"> The 'Get' property name. </param>
    Protected Sub AsyncNotifyPropertyGetChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafePostPropertyChanged(name.TrimStart("get_".ToCharArray()))
        End If
    End Sub

    ''' <summary> Asynchronously notifies (<see cref="SynchronizationContext.Post">posts</see>, Begin
    ''' Invoke or Dynamically Invokes) a change
    ''' <see cref="PropertyChanged">event</see> in Property Set value. </summary>
    ''' <remarks> Strips the "set_" prefix derived from using reflection to Set the current function
    ''' name from a property Set construct. </remarks>
    ''' <param name="name"> The 'Set' property name. </param>
    Protected Sub AsyncNotifyPropertySetChanged(ByVal name As String)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SafePostPropertyChanged(name.TrimStart("set_".ToCharArray()))
        End If
    End Sub

#End Region

#End Region

#Region " TRACE MESSAGE AVAILABLE EVENT IMPLEMENTATION "

    ''' <summary> Event queue for all listeners interested in TraceMessageAvailable events. </summary>
    Public Event TraceMessageAvailable As EventHandler(Of TraceMessageEventArgs) Implements ITraceMessagePublisher.TraceMessageAvailable

    ''' <summary> Removes event handler. </summary>
    ''' <param name="value"> The handler. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub RemoveEventHandler(ByVal value As EventHandler(Of TraceMessageEventArgs))
        For Each d As [Delegate] In value.SafeInvocationList
            Try
                RemoveHandler Me.TraceMessageAvailable, CType(d, EventHandler(Of TraceMessageEventArgs))
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Next
    End Sub

#Region " INVOKE "

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) the <see cref="TraceMessageAvailable">trace message
    ''' available Event</see>. Must be called with the <see cref="SynchronizationContext">sync
    ''' context</see> </summary>
    ''' <param name="e"> The <see cref="TraceMessageEventArgs" /> instance containing the event data. </param>
    Private Sub InvokeTraceMessageAvailable(ByVal e As TraceMessageEventArgs)
        Dim evt As EventHandler(Of TraceMessageEventArgs) = Me.TraceMessageAvailableEvent
        evt?.Invoke(Me, e)
    End Sub

    ''' <summary> Synchronously notifies (Invokes a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or (Dynamically Invokes) the <see cref="TraceMessageAvailable">trace message
    ''' available Event</see>. Must be called with the <see cref="SynchronizationContext">sync
    ''' context</see> </summary>
    ''' <param name="obj"> The object. </param>
    Private Sub InvokeTraceMessageAvailable(ByVal obj As Object)
        Me.InvokeTraceMessageAvailable(CType(obj, TraceMessageEventArgs))
    End Sub

    ''' <summary> Asynchronously (Begins Invoke a <see cref="ISynchronizeInvoke">sync enabled
    ''' entity</see>) or synchronously (Dynamically Invokes)  the
    ''' <see cref="TraceMessageAvailable">trace message available Event</see>. </summary>
    ''' <param name="e"> The <see cref="TraceMessageEventArgs" /> instance containing the event data. </param>
    Private Sub SafeBeginInvokeTraceMessageAvailable(ByVal e As TraceMessageEventArgs)
        Me.TraceMessageAvailableEvent.SafeBeginInvoke(Me, e)
    End Sub

#End Region

#Region " I TRACE MESSAGE PUBLISHER "

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <remarks> Override this method in cases the trace message needs to be used (e.g., displayed or logged) before it is raised. </remarks>
    ''' <param name="value"> The Trace Message to process. </param>
    Protected Overridable Sub OnTraceMessageAvailable(ByVal value As TraceMessage) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If value IsNot Nothing Then
            Me.DisplayTraceMessage(value)
            If Me.MultipleSyncContextsExpected OrElse SynchronizationContext.Current Is Nothing Then
                ' Even though the current sync context is nothing, one of the targets might 
                ' still require invocation. Therefore, save invoke is implemented.
                Me.SafeBeginInvokeTraceMessageAvailable(New TraceMessageEventArgs(value))
            Else
                SynchronizationContext.Current.Post(New SendOrPostCallback(AddressOf Me.InvokeTraceMessageAvailable),
                                                    New TraceMessageEventArgs(value))
            End If
        End If
    End Sub

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> The event arguments. </returns>
    Protected Function OnTraceMessageAvailable(eventType As TraceEventType, ByVal id As Integer,
                                               format As String, ParamArray args() As Object) As TraceMessage Implements ITraceMessagePublisher.OnTraceMessageAvailable
        Dim e As New TraceMessage(eventType, id, format, args)
        Me.OnTraceMessageAvailable(e)
        Return e
    End Function

    ''' <summary> Asynchronously posts a trace message available event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Sub OnTraceMessageAvailable(ByVal e As TraceMessageEventArgs) Implements ITraceMessagePublisher.OnTraceMessageAvailable
        If e IsNot Nothing Then
            Me.OnTraceMessageAvailable(e.TraceMessage)
        End If
    End Sub

#End Region

#Region " TRACE MESSAGE PUBLISHER OVERRIDABLE "

    ''' <summary> Displays a trace message described by value. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overridable Sub DisplayTraceMessage(ByVal value As TraceMessage)
        If value IsNot Nothing Then
            Dim s As String = value.ExtractSynopsis()
            If Not String.IsNullOrEmpty(s) Then DisplaySynopsis(s)
            Me.DisplayMessage(value)
        End If
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected MustOverride Sub DisplaySynopsis(ByVal value As String)

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected MustOverride Sub DisplayMessage(ByVal value As TraceMessage)

#End Region

#End Region

#End If
#End Region