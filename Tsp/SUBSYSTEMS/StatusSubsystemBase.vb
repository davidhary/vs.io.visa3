Imports NationalInstruments
Imports isr.Core.Agnostic.StackTraceExtensions
''' <summary> Defines a Status Subsystem for a TSP System. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/7/2013" by="David" revision=""> Created. </history>
Public MustInherit Class StatusSubsystemBase
    Inherits Visa.SCPI.StatusSubsystemBase

#Region " CONSTRUCTION + CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="StatusSubsystemBase" /> class. </summary>
    ''' <param name="session"> A reference to a <see cref="Session">message based TSP session</see>. </param>
    Protected Sub New(ByVal session As TspSession)
        MyBase.New(session)

        Me.VersionInfo = New VersionInfo


        ' check for query and other errors reported by the standard event register
        Me.StandardDeviceErrorAvailableBits = StandardEvents.CommandError Or StandardEvents.DeviceDependentError Or
                                              StandardEvents.ExecutionError Or StandardEvents.QueryError

        ' Me.StandardEventStatusQueryCommand = TspSyntax.StandardEventQueryCommand
        ' Me.StandardServiceEnableCommandFormat = TspSyntax.StandardServiceEnableCommandFormat
        ' Me.StandardServiceEnableCompleteCommandFormat = TspSyntax.StandardServiceEnableCompleteCommandFormat

        'Me.MeasurementStatusQueryCommand = TspSyntax.MeasurementEventQueryCommand
        'Me.MeasurementEventConditionQueryCommand = TspSyntax.MeasurementEventConditionQueryCommand

        ' Me.OperationCompletedQueryCommand = TspSyntax.OperationCompletedQueryCommand
        'Me.OperationEventEnableCommandFormat = TspSyntax.OperationEventEnableCommandFormat
        'Me.OperationEventEnableCommandFormat = TspSyntax.OperationEventEnableCommandFormat
        'Me.OperationEventStatusQueryCommand = TspSyntax.OperationEventQueryCommand

        ' Me.QuestionableStatusQueryCommand = Scpi.Syntax.QuestionableEventQueryCommand

        Me.showErrorsStack = New System.Collections.Generic.Stack(Of Boolean?)
        Me.showPromptsStack = New System.Collections.Generic.Stack(Of Boolean?)
        Me._TspSession = session
        Me.TspLinkResetTimeout = TimeSpan.FromMilliseconds(5000)

    End Sub

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks> Executes in two distinct scenarios as determined by its disposing parameter.  If True,
    ''' the method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed. </remarks>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged resources;
    ''' <c>False</c> if this method releases only unmanaged resources. </param>
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)

        Try

            If Not MyBase.IsDisposed Then

                If disposing Then

                    ' Free managed resources when explicitly called

                End If

                ' Free shared unmanaged resources
                If Me.showErrorsStack IsNot Nothing Then
                    Me.showErrorsStack.Clear()
                    Me.showErrorsStack = Nothing
                End If

                If Me.showPromptsStack IsNot Nothing Then
                    Me.showPromptsStack.Clear()
                    Me.showPromptsStack = Nothing
                End If

            End If

        Finally

            MyBase.Dispose(disposing)

        End Try

    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Clears the active state. Issues selective device clear. </summary>
    Public Overrides Sub ClearActiveState()
        MyBase.ClearActiveState()
        Me.ExecutionState = TspExecutionState.IdleReady
        Me.OperationCompleted = Me.QueryOperationCompleted
    End Sub

    ''' <summary> Sets subsystem values to their known execution clear state. </summary>
    Public Overrides Sub ClearExecutionState()
        Me.ReadExecutionState()
        ' Set all cached values that get reset by CLS
        Me.ClearStatus()
        Me.OperationCompleted = Me.QueryOperationCompleted
    End Sub

    ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
    ''' <remarks> Override this method to customize the reset.<para>
    '''           Additional Actions: </para><para>
    '''           Clears Error Queue.
    '''           </para></remarks>
    Public Overrides Sub InitializeKnownState()
        MyBase.InitializeKnownState()
        Me._newProgramRequired = ""
    End Sub

    ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
    Public Overrides Sub ResetKnownState()

        ' clear elements.
        Me.ClearStatus()

        MyBase.ResetKnownState()

        ' get prompt and errors status
        If Me.IsSessionOpen Then

            ' enable processing of execution state.
            Me.ProcessExecutionStateEnabled = True

            ' read the prompts status
            Me.QueryShowPrompts()

            ' read the errors status
            Me.QueryShowErrors()

            ' enable service request on all events
            Me.EnableServiceRequest(StandardEvents.All, ServiceRequests.All)

        End If
        Me._NodeEntities = New NodeEntityCollection()
        Me.ExecutionState = New TspExecutionState?
        Me.ControllerNodeNumber = New Integer?
        Me.ControllerNode = Nothing
        Me.SerialNumber = New Long?
        Me.SerialNumberReading = ""

        ' these values are set upon loading or initializing the framework.
        Me.TspLinkOnlineStateQueryCommand = ""
        Me.TspLinkOfflineStateQueryCommand = ""
        Me.TspLinkResetCommand = ""
        Me.CollectGarbageWaitCompleteCommand = ""
        Me.ResetNodesCommand = ""

        Me.IsTspLinkOnline = New Boolean?
        Me.IsTspLinkOffline = New Boolean?
        Me.OperationCompleted = Me.QueryOperationCompleted

    End Sub

#End Region

#Region " SESSION "

    ''' <summary> Gets or sets the session. </summary>
    Private _TspSession As TspSession

    ''' <summary> Gets the TSP Session. </summary>
    ''' <value> The tsp session. </value>
    Public ReadOnly Property TspSession As TspSession
        Get
            Return Me._TspSession
        End Get
    End Property

    ''' <summary> Handles the property changed event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnSessionPropertyChanged(ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.ProcessExecutionStateEnabled AndAlso e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
            Select Case e.PropertyName
                Case "LastMessageReceived"
                    ' parse the command to get the TSP execution state.
                    Me.ParseExecutionState(Me.TspSession.LastMessageReceived, TspExecutionState.IdleReady)
                Case "LastMessageSent"
                    ' set the TSP status
                    Me.ExecutionState = TspExecutionState.Processing
            End Select
        End If
        MyBase.OnSessionPropertyChanged(e)
    End Sub

#End Region

#Region " ERROR QUEUE: NODE "

    ''' <summary> Queries error count on a remote node. </summary>
    ''' <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <param name="node"> . </param>
    ''' <returns> The error count. </returns>
    Public Function QueryErrorQueueCount(ByVal node As NodeEntityBase) As Integer
        Dim count As Integer?
        If Me.IsSessionOpen Then
            If node Is Nothing Then
                Throw New ArgumentNullException("node")
            End If
            If node.IsController Then
                count = Me.TspSession.QueryPrint(0I, 1, "_G.errorqueue.count")
            Else
                count = Me.TspSession.QueryPrint(0I, 1, "node[{0}].errorqueue.count", node.Number)
            End If
        End If
        Return count.GetValueOrDefault(0)
    End Function

    ''' <summary> Clears the error cache. </summary>
    Public Overrides Sub ClearErrorCache()
        MyBase.ClearErrorCache()
        Me._DeviceErrorQueue = New Queue(Of DeviceError)
    End Sub

    ''' <summary> Clears the error queue for the specified node. </summary>
    ''' <param name="nodeNumber"> The node number. </param>
    Public Overloads Sub ClearErrorQueue(ByVal nodeNumber As Integer)
        If Me.IsSessionOpen Then
            If Not Me.NodeExists(nodeNumber) Then
                Me.Session.WriteLine("node[{0}].errorqueue.clear() waitcomplete({0})", nodeNumber)
            End If
        End If
    End Sub

    ''' <summary> Clears the error queue. </summary>
    ''' <remarks> Sends the <see cref="ClearErrorQueueCommand">clear error queue</see> command. </remarks>
    Public Overrides Sub ClearErrorQueue()
        If Me.NodeEntities Is Nothing Then
            MyBase.ClearErrorQueue()
        Else
            Me.ClearErrorCache()
            For Each node As NodeEntityBase In Me.NodeEntities
                Me.ClearErrorQueue(node.Number)
            Next
        End If
    End Sub

    Private _DeviceErrorQueue As Queue(Of DeviceError)
    ''' <summary> Gets or sets the error queue. </summary>
    ''' <value> A Queue of device errors. </value>
    Protected Shadows ReadOnly Property DeviceErrorQueue As Queue(Of DeviceError)
        Get
            Return Me._DeviceErrorQueue
        End Get
    End Property

    ''' <summary> Returns the queued error. </summary>
    ''' <exception cref="IO.Visa.VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="IO.Visa.DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <remarks> Sends the error print format query and reads back and parses the error. </remarks>
    ''' <returns> The queued error. </returns>
    Public Shadows Function QueryQueuedError(ByVal node As NodeEntityBase) As DeviceError
        Dim err As New DeviceError()
        If Me.IsSessionOpen AndAlso Me.QueryErrorQueueCount(node) > 0 Then
            If node Is Nothing Then
                Throw New ArgumentNullException("node")
            End If
            Dim message As String = ""
            If node.IsController Then
                message = Me.TspSession.QueryPrintStringFormatTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                       "%d,%s,%d,node{0}", node.Number),
                                                                   String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                                 "_G.errorqueue.next()"))
            Else
                message = Me.TspSession.QueryPrintStringFormatTrimEnd(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                       "%d,%s,%d,node{0}", node.Number),
                                                                   String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                                 "node[{0}].errorqueue.next()", node.Number))
            End If
            MyBase.CheckThrowVisaDeviceException(False, "getting queued error;. using {0}.", Me.Session.LastMessageSent)
            err = New DeviceError(message)
        End If
        Return err
    End Function

    ''' <summary> Reads the device errors. </summary>
    ''' <returns> <c>True</c> if device has errors, <c>False</c> otherwise. </returns>
    Public Shadows Function QueryDeviceErrors(ByVal node As NodeEntityBase) As String
        If Me.IsSessionOpen Then
            If node Is Nothing Then
                Throw New ArgumentNullException("node")
            End If
            Dim deviceError As DeviceError
            Do
                deviceError = Me.QueryQueuedError(node)
                If deviceError.IsError Then
                    Me.DeviceErrorQueue.Enqueue(deviceError)
                End If
            Loop While Me.ErrorAvailable()
            Dim message As New System.Text.StringBuilder
            If Me.DeviceErrorQueue IsNot Nothing AndAlso Me.DeviceErrorQueue.Count > 0 Then
                message.AppendFormat("Instrument {0} Node {1} Errors:", Me.ResourceName, node.Number)
                message.AppendLine()
                For Each e As DeviceError In Me.DeviceErrorQueue
                    message.AppendLine(e.ErrorMessage)
                Next
            End If
            Me.DeviceErrors = message.ToString
        End If
        Return Me.DeviceErrors
    End Function

    ''' <summary> Reads the device errors. </summary>
    ''' <returns> <c>True</c> if device has errors, <c>False</c> otherwise. </returns>
    Public Shadows Function QueryDeviceErrors() As String
        Me.ClearErrorCache()
        If Me.IsSessionOpen Then
            For Each node As NodeEntityBase In Me.NodeEntities
                Me.QueryDeviceErrors(node)
            Next
        End If
        Return Me.DeviceErrors
    End Function

#End Region

#Region " IDENTITY "

    Private _NewProgramRequired As String

    ''' <summary> Gets the message indicating that a new program is required for the instrument because
    ''' this instrument is not included in the instrument list. </summary>
    ''' <value> The new program required. </value>
    Public ReadOnly Property NewProgramRequired() As String
        Get
            If String.IsNullOrWhiteSpace(Me._newProgramRequired) Then
                Me._newProgramRequired = "A new version of the program is required;. for instrument {0}"
                Me._newProgramRequired = String.Format(Globalization.CultureInfo.CurrentCulture, Me._newProgramRequired, Me.Identity)
            End If
            Return Me._newProgramRequired
        End Get
    End Property

    ''' <summary> Parse version information. </summary>
    ''' <param name="value"> The value. </param>
    Public Overrides Sub ParseVersionInfo(value As String)
        Me.VersionInfo.Parse(value)
        Me.SerialNumberReading = Me.VersionInfo.SerialNumber
    End Sub

    ''' <summary> Gets or sets the information describing the version. </summary>
    ''' <value> Information describing the version. </value>
    Public Property VersionInfo As VersionInfo

#End Region

#Region " REGISTERS "

    ''' <summary> Reads the condition of the measurement register event. </summary>
    ''' <returns> System.Nullable{System.Int32}. </returns>
    Public Overrides Function QueryMeasurementEventCondition() As Integer?
        If Me.IsSessionOpen Then
            Me.MeasurementEventCondition = Me.TspSession.QueryPrint(0I, 1, "_G.status.measurement.condition")
        End If
        Return Me.MeasurementEventCondition
    End Function

#End Region

#Region " SERIAL NUMBER "

    Private _SerialNumberReading As String

    ''' <summary> Gets or sets the serial number reading. </summary>
    ''' <value> The serial number reading. </value>
    Public Property SerialNumberReading As String
        Get
            Return Me._serialNumberReading
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.SerialNumberReading) Then
                Me._serialNumberReading = value
                Me.SafePostPropertyChanged("SerialNumberReading")
                If String.IsNullOrWhiteSpace(value) Then
                    Me.SerialNumber = New Long?
                Else
                    Dim numericValue As Long = 0
                    If Long.TryParse(Me.SerialNumberReading, Globalization.NumberStyles.Number, Globalization.CultureInfo.InvariantCulture, numericValue) Then
                    End If
                    Me.SerialNumber = numericValue
                End If
            End If
        End Set
    End Property

    ''' <summary> Reads and returns the instrument serial number. </summary>
    ''' <returns> The serial number. </returns>
    ''' <exception cref="IO.Visa.VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="IO.Visa.DeviceException"> Thrown when a device error condition occurs. </exception>
    Public Function QuerySerialNumber() As String
        If Me.IsSessionOpen AndAlso String.IsNullOrWhiteSpace(Me.SerialNumberReading) Then
            Dim value As String = Me.TspSession.QueryPrintStringFormatTrimEnd("%d", "_G.localnode.serialno").Trim
            MyBase.CheckThrowVisaDeviceException(False, "getting serial number;. using {0}.", Me.Session.LastMessageSent)
            Me.SerialNumberReading = value
        ElseIf String.IsNullOrWhiteSpace(Me.SerialNumberReading) AndAlso Me.SerialNumber.HasValue Then
            Me.SerialNumberReading = CStr(Me.SerialNumber.Value)
        End If
        Return Me.SerialNumberReading
    End Function

    Private _SerialNumber As Long?
    ''' <summary>
    ''' Reads and returns the instrument serial number
    ''' </summary>
    Public Property SerialNumber() As Long?
        Get
            Return Me._serialNumber
        End Get
        Set(ByVal value As Long?)
            If Not Nullable.Equals(Me.SerialNumber, value) Then
                Me._serialNumber = value
                Me.SafePostPropertyChanged("SerialNumber")
            End If
        End Set
    End Property

#End Region

#Region " OPC "

    ''' <summary> Enables group wait complete. </summary>
    ''' <param name="groupNumber"> Specifies the group number. That would be the same as the TSP
    ''' Link group number for the node. </param>
    Public Overloads Sub EnableWaitComplete(ByVal groupNumber As Integer)
        Me.EnableServiceRequestComplete(Visa.StandardEvents.All And Not Visa.StandardEvents.RequestControl,
                                        Visa.ServiceRequests.StandardEvent)
        If Me.IsSessionOpen AndAlso Me.LastStatus >= VisaNS.VisaStatusCode.Success Then
            Me.Session.WriteLine(TspSyntax.WaitGroupCommandFormat, groupNumber)
        End If
    End Sub

    ''' <summary> Waits completion after command. </summary>
    ''' <exception cref="IO.Visa.VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="IO.Visa.DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <param name="nodeNumber"> Specifies the node number. </param>
    ''' <param name="timeout">    The timeout. </param>
    ''' <param name="isQuery">    Specifies the condition indicating if the command that preceded the wait is a query, which
    ''' determines how errors are fetched. </param>
    Public Sub WaitComplete(ByVal nodeNumber As Integer, ByVal timeout As TimeSpan, ByVal isQuery As Boolean)

        Me.EnableWaitComplete(0)
        Me.CheckThrowVisaDeviceException(nodeNumber, Not isQuery, "enabled wait complete group '{0}';. ", 0)
        Me.AwaitOperationCompleted(timeout)
        Me.CheckThrowVisaDeviceException(nodeNumber, Not isQuery, "waiting completion;. ")

    End Sub

#End Region

#Region " ASSET TRIGGER "

    ''' <summary> Issues a hardware trigger. </summary>
    Public Sub AssertTrigger()

        Me.ExecutionState = TspExecutionState.IdleReady
        If Me.IsSessionOpen Then
            Me.Session.AssertTrigger()
        End If

    End Sub

#End Region

#Region " EXECUTION STATE "

    Private _ProcessExecutionStateEnabled As Boolean

    ''' <summary> Gets or sets the process execution state enabled. </summary>
    ''' <value> The process execution state enabled. </value>
    Public Property ProcessExecutionStateEnabled As Boolean
        Get
            Return Me._ProcessExecutionStateEnabled
        End Get
        Set(value As Boolean)
            If Not value.Equals(Me._ProcessExecutionStateEnabled) Then
                Me._ProcessExecutionStateEnabled = value
                Me.SafePostPropertyChanged("ProcessExecutionStateEnabled ")
            End If
        End Set
    End Property


    Private _ExecutionState As TspExecutionState?

    ''' <summary> Gets or sets the last TSP execution state. Setting the last state is useful when
    ''' closing the Tsp System. </summary>
    ''' <value> The last state. </value>
    Public Property ExecutionState() As TspExecutionState?
        Get
            Return Me._ExecutionState
        End Get
        Set(ByVal Value As TspExecutionState?)
            If (Value.HasValue AndAlso Not Me.ExecutionState.HasValue) OrElse
                (Not Value.HasValue AndAlso Me.ExecutionState.HasValue) OrElse Not Value.Equals(Me.ExecutionState) Then
                Me._ExecutionState = Value
                Me.SafePostPropertyChanged(NameOf(Me.ExecutionState))
            End If
        End Set
    End Property

    ''' <summary> Gets the instrument Execution State caption. </summary>
    ''' <value> The state caption. </value>
    Public ReadOnly Property ExecutionStateCaption() As String
        Get
            If Me.ExecutionState.HasValue Then
                Return isr.Core.Agnostic.EnumExtensions.Description(Me.ExecutionState.Value)
            Else
                Return "N/A"
            End If
        End Get
    End Property

    ''' <summary> Parses the state of the TSP prompt and saves it in the state cache value. </summary>
    ''' <param name="value"> Specifies the read buffer. </param>
    ''' <returns> The instrument Execution State. </returns>
    Public Function ParseExecutionState(ByVal value As String, ByVal defaultValue As TspExecutionState) As TspExecutionState
        Dim state As TspExecutionState = defaultValue
        If String.IsNullOrWhiteSpace(value) OrElse value.Length < 4 Then
        Else
            value = value.Substring(0, 4)
            If value.StartsWith(TspSyntax.ReadyPrompt, True, Globalization.CultureInfo.CurrentCulture) Then
                state = TspExecutionState.IdleReady
            ElseIf value.StartsWith(TspSyntax.ContinuationPrompt, True, Globalization.CultureInfo.CurrentCulture) Then
                state = TspExecutionState.IdleContinuation
            ElseIf value.StartsWith(TspSyntax.ErrorPrompt, True, Globalization.CultureInfo.CurrentCulture) Then
                state = TspExecutionState.IdleError
            Else
                ' no prompt -- set to the default state
                state = defaultValue
            End If
        End If
        Me.ExecutionState = state
        Return state
    End Function

    ''' <summary> Reads the state of the TSP prompt and saves it in the state cache value. </summary>
    ''' <returns> The instrument Execution State. </returns>
    Public Function ReadExecutionState() As TspExecutionState?
        ' check status of the prompt flag.
        If Me.ShowPrompts.HasValue Then

            ' if prompts are on, 
            If Me.ShowPrompts.Value Then

                ' do a read. This raises an event that parses the state
                If Me.IsMessageAvailable(TimeSpan.FromMilliseconds(1), 3) Then
                    If Me.IsSessionOpen Then
                        Me.Session.ReadLine()
                    End If
                End If

            Else

                Me.ExecutionState = TspExecutionState.Unknown

            End If

        Else

            ' check if we have data in the output buffer.  
            If Me.IsMessageAvailable(TimeSpan.FromMilliseconds(1), 3) Then

                ' if data exists in the buffer, it may indicate that the prompts are already on 
                ' so just go read the output buffer. Once read, the status will be parsed.
                If Me.IsSessionOpen Then
                    Me.Session.ReadLine()
                End If

            Else

                ' if we have no value then we must first read the prompt status
                ' once read, the status will be parsed.
                Me.QueryShowPrompts()

            End If

        End If

        Return Me.ExecutionState

    End Function

#End Region

#Region " SHOW ERRORS "

    Private _ShowErrors As Nullable(Of Boolean)

    ''' <summary> Gets or sets the Show Errors sentinel. </summary>
    ''' <remarks> When true, the unit will automatically display the errors stored in the error queue,
    ''' and then clear the queue. Errors will be processed at the end of executing a command message
    ''' (just prior to issuing a prompt if prompts are enabled). When false, errors will not display.
    ''' Errors will be left in the error queue and must be explicitly read or cleared. The error
    ''' prompt (TSP?) is enabled. </remarks>
    ''' <value> <c>True</c> to show errors; otherwise <c>False</c>. </value>
    Public Property ShowErrors() As Boolean?
        Get
            Return Me._showErrors
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Nullable.Equals(value, Me.ShowErrors) Then
                Me._showErrors = value
                Me.SafePostPropertyChanged(NameOf(Me.ShowErrors))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the show errors sentinel. </summary>
    ''' <param name="value"> <c>True</c> to show errors; otherwise, <c>False</c>. </param>
    ''' <returns> <c>True</c> if on; otherwise <c>False</c>. </returns>
    Public Function ApplyShowErrors(ByVal value As Boolean) As Boolean?
        Me.WriteShowErrors(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.ShowErrors
        Else
            Return Me.QueryShowErrors()
        End If
    End Function

    ''' <summary> Reads the condition for showing errors. </summary>
    ''' <returns> <c>True</c> to show errors; otherwise <c>False</c>. </returns>
    Public Function QueryShowErrors() As Boolean?
        If Me.IsSessionOpen Then
            Me.ShowErrors = Me.TspSession.QueryPrint(True, TspSyntax.ShowErrors)
            If Not Me.ProcessExecutionStateEnabled Then
                ' read execution state explicitly, because session events are disabled.
                Me.ReadExecutionState()
            End If
        End If
        Return Me.ShowErrors
    End Function

    ''' <summary> Sets the condition for showing errors. </summary>
    ''' <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <param name="value"> true to value. </param>
    ''' <returns> <c>True</c> to show errors; otherwise <c>False</c>. </returns>
    Public Function WriteShowErrors(ByVal value As Boolean) As Boolean?
        If Me.IsSessionOpen Then
            Me.TspSession.WriteLine(TspSyntax.ShowErrorsSetterCommand, CType(value, Integer))
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Throw New VisaException(Me.LastStatus, Me.ResourceName, "failed;. executing '{0}'", Me.Session.LastMessageSent)
            Else
                Me.ShowErrors = value
            End If
            If Not Me.ProcessExecutionStateEnabled Then
                ' read execution state explicitly, because session events are disabled.
                Me.ReadExecutionState()
            End If
        End If
        Return Me.ShowErrors
    End Function

#End Region

#Region " SHOW PROMPTS "

    Private _ShowPrompts As Boolean?

    ''' <summary> Gets or sets the Show Prompts sentinel. </summary>
    ''' <remarks> When true, prompts are issued after each command message is processed by the
    ''' instrument.<para>
    ''' When false prompts are not issued.</para><para>
    ''' Command messages do not generate prompts. Rather, the TSP instrument generates prompts in
    ''' response to command messages. When prompting is enabled, the instrument generates prompts in
    ''' response to command messages. There are three prompts that might be returned:</para><para>
    ''' �TSP&gt;� is the standard prompt. This prompt indicates that everything is normal and the
    ''' command is done processing.</para><para>
    ''' �TSP?� is issued if there are entries in the error queue when the prompt is issued. Like the
    ''' �TSP&gt;� prompt, it indicates the command is done processing. It does not mean the previous
    ''' command generated an error, only that there are still errors in the queue when the command
    ''' was done processing.</para><para>
    ''' �&gt;&gt;&gt;&gt;� is the continuation prompt. This prompt is used when downloading scripts
    ''' or flash images. When downloading scripts or flash images, many command messages must be sent
    ''' as a unit. The continuation prompt indicates that the instrument is expecting more messages
    ''' as part of the current command.</para> </remarks>
    ''' <value> The show prompts. </value>
    Public Property ShowPrompts() As Boolean?
        Get
            Return Me._showPrompts
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Nullable.Equals(value, Me.ShowErrors) Then
                Me._showPrompts = value
                Me.SafePostPropertyChanged(NameOf(Me.ShowPrompts))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the show Prompts sentinel. </summary>
    ''' <param name="value"> <c>True</c> to show Prompts; otherwise, <c>False</c>. </param>
    ''' <returns> <c>True</c> if on; otherwise <c>False</c>. </returns>
    Public Function ApplyShowPrompts(ByVal value As Boolean) As Boolean?
        Me.WriteShowPrompts(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.ShowPrompts
        Else
            Return Me.QueryShowPrompts()
        End If
    End Function

    ''' <summary> Queries the condition for showing prompts. Controls prompting.
    ''' </summary>
    ''' <returns> <c>True</c> to show prompts; otherwise <c>False</c>. </returns>
    Public Function QueryShowPrompts() As Boolean?
        If Me.IsSessionOpen Then
            Me.ShowPrompts = Me.TspSession.QueryPrint(True, TspSyntax.ShowPrompts)
            If Not Me.ProcessExecutionStateEnabled Then
                ' read execution state explicitly, because session events are disabled.
                Me.ReadExecutionState()
            End If
        End If
        Return Me.ShowPrompts
    End Function

    ''' <summary> Sets the condition for showing prompts. Controls prompting. </summary>
    ''' <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <param name="value"> true to value. </param>
    ''' <returns> <c>True</c> to show prompts; otherwise <c>False</c>. </returns>
    Public Function WriteShowPrompts(ByVal value As Boolean) As Boolean?
        If Me.IsSessionOpen Then
            Dim previousValue As Nullable(Of Boolean) = Me.ShowErrors
            Me.TspSession.WriteLine(TspSyntax.ShowPromptsSetterCommand, CType(value, Integer))
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.ShowErrors = previousValue
                Throw New VisaException(Me.LastStatus, Me.ResourceName, "failed;. executing '{0}'", Me.Session.LastMessageSent)
            Else
                Me.ShowPrompts = value
            End If
            If Not Me.ProcessExecutionStateEnabled Then
                ' read execution state explicitly, because session events are disabled.
                Me.ReadExecutionState()
            End If
        End If
        Return Me.ShowPrompts
    End Function

    ''' <summary> Turns off prompts and errors. It seems that the new systems come with prompts and
    ''' errors off when the instrument is started or reset so this is not needed. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Sub TurnPromptsErrorsOff()

        If Me.IsSessionOpen Then
            ' flush the input buffer in case the instrument has some leftovers.
            Me.TspSession.DiscardUnreadData()

            Dim showPromptsCommand As String = "<failed to issue>"
            Try
                ' turn off prompt transmissions
                Me.WriteShowPrompts(False)
                showPromptsCommand = Me.Session.LastMessageSent
            Catch
            End Try

            ' flush again in case turning off prompts added stuff to the buffer.
            Me.TspSession.DiscardUnreadData()
            If Not String.IsNullOrWhiteSpace(Me.Session.DiscardedData) Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "Unread data discarded after turning prompts off;. Data: {0}.", Me.Session.DiscardedData)
            End If

            Dim showErrorsCommand As String = "<failed to issue>"
            Try
                ' turn off error transmissions
                Me.WriteShowErrors(False)
                showErrorsCommand = Me.Session.LastMessageSent
            Catch
            End Try

            ' flush again in case turning off errors added stuff to the buffer.
            Me.TspSession.DiscardUnreadData()
            If Not String.IsNullOrWhiteSpace(Me.Session.DiscardedData) Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "Unread data discarded after turning errors off;. Data: {0}.", Me.Session.DiscardedData)
            End If

            ' now validate
            If Me.QueryShowErrors.GetValueOrDefault(True) Then
                Throw New OperationFailedException(Me.ResourceName, showErrorsCommand, "turning off automatic error display--still on.")
            ElseIf Me.QueryShowPrompts.GetValueOrDefault(True) Then
                Throw New OperationFailedException(Me.ResourceName, showPromptsCommand, "turning off test script prompts--still on.")
            End If

        End If

    End Sub

#End Region

#Region " STATUS "

    ''' <summary> Gets or sets the stack for storing the show errors states. </summary>
    Private showErrorsStack As System.Collections.Generic.Stack(Of Boolean?)

    ''' <summary> Gets or sets the stack for storing the show prompts states. </summary>
    Private showPromptsStack As System.Collections.Generic.Stack(Of Boolean?)

    ''' <summary> Clears the status. </summary>
    Public Sub ClearStatus()

        ' clear the stacks
        Me.showErrorsStack.Clear()
        Me.showPromptsStack.Clear()

        If Me.IsSessionOpen Then
            Me.ExecutionState = TspExecutionState.IdleReady
        Else
            Me.ExecutionState = TspExecutionState.Closed
        End If

    End Sub

    ''' <summary> Restores the status of errors and prompts. </summary>
    Public Sub RestoreStatus()

        Dim lastValue As Boolean? = Me.showErrorsStack.Pop
        If lastValue.HasValue Then
            Me.WriteShowErrors(lastValue.Value)
        End If
        lastValue = Me.showPromptsStack.Pop
        If lastValue.HasValue Then
            Me.WriteShowPrompts(lastValue.Value)
        End If

    End Sub

    ''' <summary> Saves the current status of errors and prompts. </summary>
    Public Sub StoreStatus()
        Me.showErrorsStack.Push(Me.QueryShowErrors())
        Me.showPromptsStack.Push(Me.QueryShowPrompts())
    End Sub

#End Region

#Region " COLLECT GARBAGE "

    Public Property CollectGarbageWaitCompleteCommand As String

    ''' <summary> Sends a collect garbage command. </summary>
    Public Sub CollectGarbage()
        If Me.IsSessionOpen Then
            Me.Session.WriteLine(TspSyntax.CollectGarbageCommand)
        End If
    End Sub

    ''' <summary> Collects garbage on the specified node. </summary>
    ''' <param name="nodeNumber"> Specifies the node number. </param>
    ''' <returns><c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function CollectGarbage(ByVal nodeNumber As Integer) As Boolean
        If Me.IsSessionOpen Then
            Me.Session.WriteLine(TspSyntax.CollectNodeGarbageFormat, nodeNumber)
        End If
    End Function

    ''' <summary> Does garbage collection. Reports operations synopsis. </summary>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    ''' completed. </param>
    ''' <param name="format">  Describes the format to use. </param>
    ''' <param name="args">    A variable-length parameters list containing arguments. </param>
    Public Sub CollectGarbage(ByVal timeout As TimeSpan, ByVal format As String, ByVal ParamArray args() As Object)
        If Me.IsSessionOpen Then
            Me.EnableWaitComplete()
            If String.IsNullOrWhiteSpace(Me.CollectGarbageWaitCompleteCommand) Then
                Me.Session.WriteLine(TspSyntax.CollectGarbageWaitCompleteCommand)
            Else
                Me.Session.WriteLine(Me.CollectGarbageWaitCompleteCommand)
            End If
            Me.CheckThrowVisaDeviceException(False, "collecting garbage after {0};. ",
                                                 String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            Me.AwaitOperationCompleted(timeout)
        End If
    End Sub

    ''' <summary> Does garbage collection. Reports operations synopsis. </summary>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    ''' completed. </param>
    ''' <param name="format">  Describes the format to use. </param>
    ''' <param name="args">    A variable-length parameters list containing arguments. </param>
    ''' <returns><c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function CollectGarbageWaitComplete(ByVal timeout As TimeSpan, ByVal format As String, ByVal ParamArray args() As Object) As Boolean

        ' do a garbage collection
        Me.EnableWaitComplete()
        If Me.IsSessionOpen Then
            If String.IsNullOrWhiteSpace(Me.CollectGarbageWaitCompleteCommand) Then
                Me.Session.WriteLine(TspSyntax.CollectGarbageWaitCompleteCommand)
            Else
                Me.Session.WriteLine(Me.CollectGarbageWaitCompleteCommand)
            End If
        End If
        If Me.ReportVisaDeviceOperationOkay(True, "collecting garbage after {0};. ",
                                            String.Format(Globalization.CultureInfo.CurrentCulture, format, args)) Then
            Me.AwaitOperationCompleted(timeout)
            Return Me.ReportVisaDeviceOperationOkay(True, "awaiting timeout after collecting garbage after {0};. ",
                                                    String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        Else
            Return False
        End If

    End Function

    ''' <summary> Does garbage collection. Reports operations synopsis. </summary>
    ''' <param name="node">    Specifies the remote node number to validate. </param>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    ''' completed. </param>
    ''' <param name="format">  Describes the format to use. </param>
    ''' <param name="args">    A variable-length parameters list containing arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function CollectGarbageWaitComplete(ByVal node As NodeEntityBase, ByVal timeout As TimeSpan,
                                               ByVal format As String, ByVal ParamArray args() As Object) As Boolean

        If node Is Nothing Then
            Throw New ArgumentNullException("node")
        End If
        If node.IsController Then
            Return Me.CollectGarbageWaitComplete(timeout, format, args)
        End If

        ' do a garbage collection
        Me.CollectGarbage(node.Number)
        If Me.ReportVisaDeviceOperationOkay(node.Number, True, "collecting garbage after {0};. ",
                                            String.Format(Globalization.CultureInfo.CurrentCulture, format, args)) Then
            Me.EnableWaitComplete(0)
            Me.AwaitOperationCompleted(timeout)
            Return Me.ReportVisaDeviceOperationOkay(True, "awaiting timeout after collecting garbage after {0};. ",
                                                    String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        Else
            Return False
        End If

    End Function

#End Region

#Region " DATA QUEUE "

    ''' <summary> clears the data queue for the specified node. </summary>
    ''' <param name="node">                Specifies the node. </param>
    ''' <param name="reportQueueNotEmpty"> true to report queue not empty. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function ClearDataQueue(ByVal node As NodeEntityBase, ByVal reportQueueNotEmpty As Boolean) As Boolean
        If node Is Nothing Then
            Throw New ArgumentNullException("node")
        End If
        If Me.IsSessionOpen Then
            If Not Me.NodeExists(node.Number) Then
                Return True
            Else
                If reportQueueNotEmpty Then
                    If Me.QueryDataQueueCount(node) > 0 Then
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Data queue not empty on node {0};. ", node.Number)
                    End If
                End If
                Me.Session.WriteLine("node[{0}].dataqueue.clear() waitcomplete({0})", node.Number)
                Return Me.LastStatus >= NationalInstruments.VisaNS.VisaStatusCode.Success
            End If
        Else
            Return True
        End If
    End Function

    ''' <summary> clears the data queue for the specified node. </summary>
    ''' <param name="nodeNumber"> The node number. </param>
    Public Sub ClearDataQueue(ByVal nodeNumber As Integer)
        If Me.IsSessionOpen Then
            If Me.NodeExists(nodeNumber) Then
                Me.Session.WriteLine("node[{0}].dataqueue.clear() waitcomplete({0})", nodeNumber)
            End If
        End If
    End Sub

    ''' <summary> Clears data queue on all nodes. </summary>
    ''' <param name="nodeEntities">        The node entities. </param>
    ''' <param name="reportQueueNotEmpty"> true to report queue not empty. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Function ClearDataQueue(ByVal nodeEntities As NodeEntityCollection, ByVal reportQueueNotEmpty As Boolean) As Boolean

        Dim success As Boolean = True
        If nodeEntities IsNot Nothing Then
            For Each node As NodeEntityBase In nodeEntities
                If Not Me.ClearDataQueue(node, reportQueueNotEmpty) Then
                    success = False
                End If
            Next
        End If
        Return success
    End Function

    ''' <summary> Clears data queue on the nodes. </summary>
    ''' <param name="nodes"> The nodes. </param>
    Public Sub ClearDataQueue(ByVal nodes As NodeEntityCollection)
        If nodes IsNot Nothing Then
            For Each node As NodeEntityBase In nodes
                If node IsNot Nothing Then
                    Me.ClearDataQueue(node.Number)
                End If
            Next
        End If
    End Sub

    ''' <summary> Clears data queue on all nodes. </summary>
    Public Sub ClearDataQueue()
        Me.ClearDataQueue(Me.NodeEntities)
    End Sub

#Region " CAPACITY "

    ''' <summary> Queries the capacity of the data queue. </summary>
    ''' <returns> Capacity. </returns>
    Public Function QueryDataQueueCapacity(ByVal node As NodeEntityBase) As Integer
        If node Is Nothing Then
            Throw New ArgumentNullException("node")
        End If
        Return Me.QueryDataQueueCapacity(node.Number)
    End Function

    ''' <summary> Queries the capacity of the data queue. </summary>
    ''' <exception cref="IO.Visa.VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="IO.Visa.DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <param name="nodeNumber"> The node number. </param>
    ''' <returns> Capacity. </returns>
    Public Function QueryDataQueueCapacity(ByVal nodeNumber As Integer) As Integer
        If Me.IsSessionOpen Then
            If Me.NodeExists(nodeNumber) Then
                Return Me.TspSession.QueryPrint(0I, 1, "node[{0}].dataqueue.capacity", nodeNumber)
            Else
                Return 0
            End If
        End If
    End Function

#End Region

#Region " COUNT "

    ''' <summary> Queries the data queue count. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node"> . </param>
    ''' <returns> Count. </returns>
    Public Function QueryDataQueueCount(ByVal node As NodeEntityBase) As Integer
        If node Is Nothing Then
            Throw New ArgumentNullException("node")
        End If
        Return Me.QueryDataQueueCount(node.Number)
    End Function

    ''' <summary> Queries the data queue count. </summary>
    ''' <exception cref="IO.Visa.VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="IO.Visa.DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <param name="nodeNumber"> The node number. </param>
    ''' <returns> Count. </returns>
    Public Function QueryDataQueueCount(ByVal nodeNumber As Integer) As Integer
        If Me.IsSessionOpen Then
            If Me.NodeExists(nodeNumber) Then
                Return Me.TspSession.QueryPrint(0I, 1, "node[{0}].dataqueue.count", nodeNumber)
            End If
        End If
    End Function

#End Region

#End Region

#Region " FUNCTION "

    ''' <summary> Returns a string from the parameter array of arguments for use when running the
    ''' function. </summary>
    ''' <param name="args"> Specifies a parameter array of arguments. </param>
    ''' <returns> A comma-separated string. </returns>
    Public Shared Function Parameterize(ByVal ParamArray args() As String) As String

        Dim arguments As New System.Text.StringBuilder
        Dim i As Integer
        If args IsNot Nothing AndAlso args.Length >= 0 Then
            For i = 0 To args.Length - 1
                If (i > 0) Then
                    arguments.Append(",")
                End If
                arguments.Append(args(i))
            Next i

        End If
        Return arguments.ToString

    End Function


    ''' <summary> Calls the function with the given arguments in protected mode. </summary>
    ''' <remarks> Protected mode means that any error inside the function is not propagated; instead,
    ''' the call (Lua pcall) catches the error and returns a status code. Its first result is the
    ''' status code (a Boolean), which is <c>True</c> if the call succeeds without errors. In such case,
    ''' pcall also returns all results from the call, after this first result. In case of any error,
    ''' pcall returns false plus the error message. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="functionName"> Specifies the function name. </param>
    ''' <param name="args">         Specifies the function arguments. </param>
    Public Sub CallFunction(ByVal functionName As String, ByVal args As String)

        ' Check name
        Dim callStatement As String
        If String.IsNullOrWhiteSpace(functionName) Then
            Throw New ArgumentNullException("functionName")
        Else
            callStatement = TspSyntax.CallFunctionCommand(functionName, args)
            callStatement = TspSyntax.PrintCommand(callStatement)
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(callStatement)
            End If
        End If

    End Sub

#End Region

#Region " NODE "

    ''' <summary> Resets the local TSP node. </summary>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False></c>. </returns>
    Public Function ResetNode() As Boolean
        If Me.IsSessionOpen Then
            Me.Session.WriteLine("localnode.reset()")
        End If
        Return Me.ReportVisaDeviceOperationOkay(False, "resetting local TSP node;. ")
    End Function

    ''' <summary> Resets a node. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="node"> The node. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False></c>. </returns>
    Public Function ResetNode(ByVal node As NodeEntityBase) As Boolean

        If node Is Nothing Then
            Throw New ArgumentNullException("node")
        End If
        If node.IsController Then
            Return Me.ResetNode()
        Else
            If Me.IsSessionOpen Then
                Me.Session.WriteLine("node[{0}].reset()", node.Number)
            End If
            Return Me.ReportVisaDeviceOperationOkay(node.Number, "resetting TSP node {0};. ", node.Number)
        End If

    End Function

    ''' <summary> Sets the connect rule on the specified node. </summary>
    ''' <param name="nodeNumber"> Specifies the remote node number. </param>
    ''' <param name="value">      true to value. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False&gt;</c>. </returns>
    Public Function ConnectRuleSetter(ByVal nodeNumber As Integer, ByVal value As Integer) As Boolean

        If Me.IsSessionOpen Then
            Me.Session.WriteLine(TspSyntax.NodeConnectRuleSetterCommandFormat, nodeNumber, value)
        End If
        Return Me.LastStatus >= VisaNS.VisaStatusCode.Success

    End Function

#Region " RESET NODES "

    ''' <summary> Gets the reset nodes command. </summary>
    ''' <value> The reset nodes command. </value>
    Public Property ResetNodesCommand As String

    ''' <summary> Resets the TSP nodes. </summary>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    ''' completed. </param>
    ''' <exception cref="IO.Visa.VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="IO.Visa.DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <exception cref="TimeoutException"> Thrown when a Timeout error condition occurs. </exception>
    Public Sub ResetNodes(ByVal timeout As TimeSpan)
        If Not String.IsNullOrWhiteSpace(Me.ResetNodesCommand) Then
            Me.EnableWaitComplete()
            If Me.IsSessionOpen Then
                Me.Session.WriteLine("isr.node.reset() waitcomplete(0)")
            End If
            Me.AwaitOperationCompleted(timeout)
        End If
    End Sub

    ''' <summary> Try reset nodes. </summary>
    ''' <param name="timeout"> Specifies the time to wait for the instrument to return operation
    ''' completed. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False&gt;</c>. </returns>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Function TryResetNodes(ByVal timeout As TimeSpan) As Boolean
        Try
            Me.ResetNodes(timeout)
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                       "Instrument '{0}' timed out resetting nodes;. Ignored.",
                                       Me.Session.ResourceName)
            Return False
        End Try
    End Function

#End Region
#End Region

#Region " STANDARD REGISTER EVENTS "

    ''' <summary> Programs the Operation register event enable bit mask. </summary>
    ''' <param name="value"> The bitmask. </param>
    ''' <returns> The mask to use for enabling the events; nothing if unknown. </returns>
    Public Overrides Function WriteOperationEventEnableBitmask(ByVal value As Integer) As Integer?
        If (value And OperationEventBits.UserRegister) <> 0 Then
            ' if enabling the user register, enable all events on the user register. 
            value = &H4FFF
        End If
        Return MyBase.WriteOperationEventEnableBitmask(value)
    End Function

#End Region

#Region " CONTROLLER NODE "

    ''' <summary> Gets or sets the controller node model. </summary>
    ''' <value> The controller node model. </value>
    Public ReadOnly Property ControllerNodeModel As String
        Get
            If Me.ControllerNode Is Nothing Then
                Return ""
            Else
                Return Me.ControllerNode.ModelNumber
            End If
        End Get
    End Property

    Private _ControllerNode As NodeEntityBase

    ''' <summary> Gets reference to the controller node. </summary>
    ''' <value> The controller node. </value>
    Public Property ControllerNode() As NodeEntityBase
        Get
            Return Me._controllerNode
        End Get
        Set(ByVal value As NodeEntityBase)
            If value Is Nothing Then
                If Me.ControllerNode IsNot Nothing Then
                    Me._controllerNode = value
                    Me.SafePostPropertyChanged("ControllerNode")
                End If
            ElseIf Me.ControllerNode Is Nothing OrElse Not value.Equals(Me.ControllerNode) Then
                Me._controllerNode = value
                Me.SafePostPropertyChanged("ControllerNode")
            End If
        End Set
    End Property

    ''' <summary> Initiates the controller node. </summary>
    ''' <remarks> This also clears the <see cref="NodeEntities">collection of nodes</see>. </remarks>
    ''' <exception cref="FormatException"> Thrown when the format of the model number cannot be
    ''' parse to a valid instrument family. </exception>
    Public Sub InitiateControllerNode()
        If Me.ControllerNode Is Nothing AndAlso Me.IsSessionOpen Then
            Me.QueryControllerNodeNumber()
            Me.ControllerNode = New NodeEntity(Me.ControllerNodeNumber.Value, Me.ControllerNodeNumber.Value)
            Me.ControllerNode.InitializeKnownState(Me.TspSession)
            Me.SafePostPropertyChanged("ControllerNodeModel")
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "Initiated controller node #{3};. Instrument model {0} S/N={1} Firmware={2} enumerated on node.",
                                       Me.ControllerNode.ModelNumber, Me.ControllerNode.SerialNumber,
                                       Me.ControllerNode.FirmwareVersion, Me.ControllerNode.Number)
        End If
        Me._NodeEntities = New NodeEntityCollection()
        Me.NodeEntities.Add(Me.ControllerNode)
    End Sub

#Region " CONTROLLER NODE NUMBER "

    Private _ControllerNodeNumber As Integer?

    ''' <summary> Gets or sets the Controller (local) node number. </summary>
    ''' <value> The Controller (local) node number. </value>
    Public Property ControllerNodeNumber As Integer?
        Get
            Return Me._ControllerNodeNumber
        End Get
        Set(ByVal value As Integer?)
            If Not Nullable.Equals(value, Me.ControllerNodeNumber) Then
                Me._ControllerNodeNumber = value
                Me.SafePostPropertyChanged("ControllerNodeNumber")
            End If
        End Set
    End Property

    ''' <summary> Reads the Controller node number. </summary>
    ''' <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <exception cref="InvalidCastException">     Thrown when an object cannot be cast to a
    ''' required type. </exception>
    ''' <exception cref="IO.Visa.VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="IO.Visa.DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <returns> An Integer or Null of failed. </returns>
    Public Function QueryControllerNodeNumber() As Integer?
        If Me.IsSessionOpen AndAlso Not Me.ControllerNodeNumber.HasValue Then
            Me.ControllerNodeNumber = Me.TspSession.QueryPrint(0I, 1, "_G.tsplink.node")
        End If
        Return Me.ControllerNodeNumber
    End Function

    ''' <summary> Reads the Controller node number. </summary>
    ''' <returns> An Integer or Null of failed. </returns>
    Public Function TryQueryControllerNodeNumber() As Integer?
        Try
            Me.QueryControllerNodeNumber()
        Catch ex As OperationFailedException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, ex.ToString)
        Catch ex As InvalidCastException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, ex.ToString)
        End Try
        Return Me.ControllerNodeNumber
    End Function

#End Region
#End Region

#Region " TSP: NODE ENTITIES "

    ''' <summary> Gets or sets the node entities. </summary>
    ''' <remarks> Required for reading the system errors. </remarks>
    Private _NodeEntities As NodeEntityCollection

    ''' <summary> Returns the enumerated list of node entities. </summary>
    ''' <returns> A list of. </returns>
    Public Function NodeEntities() As NodeEntityCollection
        Return Me._NodeEntities
    End Function

    ''' <summary> Gets the number of nodes detected in the system. </summary>
    ''' <value> The number of nodes. </value>
    Public ReadOnly Property NodeCount() As Integer
        Get
            If Me._NodeEntities Is Nothing Then
                Return 0
            Else
                Return Me._NodeEntities.Count
            End If
        End Get
    End Property

    ''' <summary> Adds a node entity. </summary>
    ''' <param name="nodeNumber"> The node number. </param>
    Private Sub AddNodeEntity(ByVal nodeNumber As Integer)
        If nodeNumber = Me.ControllerNodeNumber Then
            Me._NodeEntities.Add(Me.ControllerNode)
        Else
            Dim node As Tsp.NodeEntity = New NodeEntity(nodeNumber, Me.ControllerNodeNumber.Value)
            node.InitializeKnownState(Me.TspSession)
            Me._NodeEntities.Add(node)
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "Added node #{3};. Instrument model {0} S/N={1} Firmware={2} enumerated on node.",
                                       node.ModelNumber, node.SerialNumber, node.FirmwareVersion, node.Number)
        End If
    End Sub

    ''' <summary> Queries if a given node exists. </summary>
    ''' <remarks> Uses the <see cref="NodeEntities">node entities</see> as a cache and add to the cache
    ''' is not is not cached. </remarks>
    ''' <param name="nodeNumber"> Specifies the node number. </param>
    ''' <returns> <c>True</c> if node exists; otherwise, false. </returns>
    Public Function NodeExists(ByVal nodeNumber As Integer) As Boolean
        If Me._NodeEntities.Count > 0 AndAlso Me._NodeEntities.Contains(NodeEntityBase.BuildKey(nodeNumber)) Then
            Return True
        Else
            If Me.IsSessionOpen Then
                If NodeEntity.NodeExists(Me.TspSession, nodeNumber) Then
                    If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                        Me.AddNodeEntity(nodeNumber)
                        Return Me._NodeEntities.Count > 0 AndAlso Me._NodeEntities.Contains(NodeEntityBase.BuildKey(nodeNumber))
                    Else
                        Return False
                    End If
                Else
                    Return False
                End If
            Else
                Return False
            End If
            Return False
        End If
    End Function

    ''' <summary> Enumerates the collection of nodes on the TSP Link net. </summary>
    ''' <param name="maximumCount"> Specifies the maximum expected node number. There could be up to
    ''' 64 nodes on the TSP link. Specify 0 to use the maximum node count. </param>
    Public Sub EnumerateNodes(ByVal maximumCount As Integer)

        Me.InitiateControllerNode()
        If maximumCount > 1 Then
            For i As Integer = 1 To maximumCount
                If Me.IsSessionOpen AndAlso Not NodeEntity.NodeExists(Me.TspSession, i) Then
                    If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                        Me.AddNodeEntity(i)
                    End If
                End If
            Next
        End If
        Me.SafePostPropertyChanged("NodeCount")
    End Sub

#End Region

#Region " TSP LINK "

    Private _UsingTspLink As Boolean

    ''' <summary> Gets or sets the condition for using TSP Link. Must be affirmative otherwise TSP link
    ''' reset commands are ignored. </summary>
    ''' <value> The using tsp link. </value>
    Public Property UsingTspLink() As Boolean
        Get
            Return Me._usingTspLink
        End Get
        Set(ByVal value As Boolean)
            If value <> Me.UsingTspLink Then
                Me._usingTspLink = value
                Me.SafePostPropertyChanged("UsingTspLink")
            End If
        End Set
    End Property

#Region " TSP LINK ONLINE STATE "

    Private _IsTspLinkOnline As Boolean?

    ''' <summary> gets or sets the sentinel indicating if the TSP Link System is ready. </summary>
    ''' <value> <c>null</c> if Not known; <c>True</c> if the tsp link is online; otherwise <c>False</c>. </value>
    Public Property IsTspLinkOnline() As Boolean?
        Get
            Return Me._isTspLinkOnline
        End Get
        Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(value, Me.IsTspLinkOnline) Then
                Me._isTspLinkOnline = value
                Me.SafePostPropertyChanged("IsTspLinkOnline")
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the tsp link online state query command. </summary>
    ''' <value> The tsp link online state query command. </value>
    Public Property TspLinkOnlineStateQueryCommand As String

    ''' <summary> Reads tsp link online state. </summary>
    ''' <returns> <c>null</c> if Not known; <c>True</c> if the tsp link is online; otherwise
    ''' <c>False</c>. </returns>
    Public Function ReadTspLinkOnlineState() As Boolean?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.TspLinkOnlineStateQueryCommand) Then
            Me.IsTspLinkOnline = Me.TspSession.IsStatementTrue(Me.TspLinkOnlineStateQueryCommand)
        End If
        Return Me.IsTspLinkOnline
    End Function

#End Region

#Region " TSP LINK OFFLINE STATE "

    Private _IsTspLinkOffline As Boolean?

    ''' <summary> gets or sets the sentinel indicating if the TSP Link System is ready. </summary>
    ''' <value> <c>null</c> if Not known; <c>True</c> if the tsp link is Offline; otherwise <c>False</c>. </value>
    Public Property IsTspLinkOffline() As Boolean?
        Get
            Return Me._isTspLinkOffline
        End Get
        Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(value, Me.IsTspLinkOffline) Then
                Me._isTspLinkOffline = value
                Me.SafePostPropertyChanged("IsTspLinkOffline")
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the tsp link Offline state query command. </summary>
    ''' <value> The tsp link Offline state query command. </value>
    Public Property TspLinkOfflineStateQueryCommand As String

    ''' <summary> Reads tsp link Offline state. </summary>
    ''' <returns> <c>null</c> if Not known; <c>True</c> if the tsp link is Offline; otherwise
    ''' <c>False</c>. </returns>
    Public Function ReadTspLinkOfflineState() As Boolean?
        If Me.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(Me.TspLinkOfflineStateQueryCommand) Then
            Me.IsTspLinkOnline = Me.TspSession.IsStatementTrue(Me.TspLinkOfflineStateQueryCommand)
        End If
        Return Me.IsTspLinkOffline
    End Function

#End Region

#Region " RESET "

    ''' <summary> Gets or sets the tsp link reset command. </summary>
    ''' <value> The tsp link reset command. </value>
    Public Property TspLinkResetCommand As String

    ''' <summary> Resets the TSP link and the ISR support framework. </summary>
    ''' <remarks> Requires loading the 'isr.tsplink' scripts. </remarks>
    ''' <param name="timeout">          The timeout. </param>
    ''' <param name="maximumNodeCount"> Number of maximum nodes. </param>
    Public Sub ResetTspLinkWaitComplete(ByVal timeout As TimeSpan, ByVal maximumNodeCount As Integer)

        Try

            Me.Session.StoreTimeout(timeout)

            If Not String.IsNullOrWhiteSpace(Me.TspLinkResetCommand) Then
                ' do not condition the reset upon a previous reset.
                Me.EnableWaitComplete()
                Me.Session.WriteLine(Me.TspLinkResetCommand)
                Me.AwaitOperationCompleted(timeout)
                Me.CheckThrowVisaDeviceException(False, "resetting TSP Link")
            End If

            ' clear the reset status
            Me.IsTspLinkOffline = New Boolean?
            Me.IsTspLinkOnline = New Boolean?
            Me.ReadTspLinkOnlineState()
            Me.EnumerateNodes(maximumNodeCount)

        Catch

            Throw

        Finally

            Me.Session.RestoreTimeout()

        End Try

    End Sub

    ''' <summary> Reset the TSP Link or just the first node if TSP link not defined. </summary>
    ''' <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="timeout">          The timeout. </param>
    ''' <param name="maximumNodeCount"> Number of maximum nodes. </param>
    ''' <param name="displaySubsystem"> The display subsystem. </param>
    ''' <param name="frameworkName">    Name of the framework. </param>
    Public Sub ResetTspLink(ByVal timeout As TimeSpan, ByVal maximumNodeCount As Integer,
                            ByVal displaySubsystem As DisplaySubsystemBase, ByVal frameworkName As String)
        If displaySubsystem Is Nothing Then
            Throw New ArgumentNullException("displaySubsystem")
        End If
        displaySubsystem.DisplayLine(1, "Resetting  {0}", frameworkName)
        displaySubsystem.DisplayLine(2, "Resetting TSP Link")
        Me.ResetTspLinkWaitComplete(timeout, maximumNodeCount)
        If Me.NodeCount <= 0 Then
            If Me.UsingTspLink Then
                Throw New IO.Visa.OperationFailedException("Instrument '{0}' failed resetting TSP Link--no nodes;. ",
                                                           Me.Session.ResourceName)
            Else
                Throw New IO.Visa.OperationFailedException("Instrument '{0}' failed setting master node;. ",
                                                           Me.Session.ResourceName)
            End If
        ElseIf Me.UsingTspLink AndAlso Not Me.IsTspLinkOnline Then
            Throw New IO.Visa.OperationFailedException("Instrument '{0}' failed resetting TSP Link;. TSP Link is not on line.",
                                                       Me.Session.ResourceName)
        End If
    End Sub

#End Region

#Region " TSP LINK STATE "

    Private _TspLinkState As String

    ''' <summary> Gets or sets the state of the tsp link. </summary>
    ''' <value> The tsp state. </value>
    Public Property TspLinkState As String
        Get
            Return Me._TspLinkState
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not value.Equals(Me.TspLinkState) Then
                Me._TspLinkState = value
                Me.SafePostPropertyChanged("TspLinkState")
                Me.IsTspLinkOnline = Me.TspLinkState.Equals("online", StringComparison.OrdinalIgnoreCase)
                Me.IsTspLinkOffline = Not Me.IsTspLinkOnline.Value
            End If
        End Set
    End Property

    ''' <summary> Reads tsp link state. </summary>
    ''' <returns> The tsp link state. </returns>
    ''' <exception cref="IO.Visa.VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="IO.Visa.DeviceException"> Thrown when a device error condition occurs. </exception>
    Public Function QueryTspLinkState() As String
        If Me.IsSessionOpen Then
            Me.TspLinkState = Me.TspSession.QueryPrintStringFormatTrimEnd("tsplink.state")
            MyBase.CheckThrowVisaDeviceException(False, "getting tsp link state;. using {0}.", Me.Session.LastMessageSent)
        End If
        Return Me.TspLinkState
    End Function

#End Region

#Region " TSP LINK GROUP NUMBERS "

    ''' <summary> Assigns group numbers to the nodes. A unique group number is required for executing
    ''' concurrent code on all nodes. </summary>
    ''' <returns> <c>True</c> of okay; otherwise, <c>False</c>. </returns>
    ''' <history date="09/08/2009" by="David" revision="3.0.3538.x"> Allows setting groups
    ''' even if TSP Link is not on line. </history>
    Public Function AssignNodeGroupNumbers() As Boolean

        If Not Me.IsSessionOpen Then Return True
        Dim affirmative As Boolean = True
        If Me.NodeEntities IsNot Nothing Then
            For Each node As NodeEntityBase In Me._NodeEntities
                If Me.IsTspLinkOnline Then
                    Me.Session.WriteLine("node[{0}].tsplink.group = {0}", node.Number)
                Else
                    Me.Session.WriteLine("localnode.tsplink.group = {0}", node.Number)
                End If
                If Not Me.ReportVisaDeviceOperationOkay(False, "assigning group to node number {0};. ", node.Number) Then
                    affirmative = False
                    Exit For
                End If
            Next
            'If Me.IsTspLinkOnline Then
            'End If
        End If
        Return affirmative

    End Function

#End Region

#Region " TSP LINK RESET "

    Public Property TspLinkResetTimeout As TimeSpan

    ''' <summary> Reset TSP Link with error reporting. </summary>
    ''' <history date="09/22/2009" by="David" revision="3.0.3552.x"> The procedure caused
    ''' error 1220 - TSP link failure on the remote instrument. The error occurred only if the
    ''' program was stopped and restarted without toggling power on the instruments. Waiting
    ''' completion of the previous task helped even though that task did not access the remote node! </history>
    Private Sub ResetTspLinkIgnoreError()

        If Not Me.IsSessionOpen Then Return
        Me.IsTspLinkOnline = New Boolean?
        Me.EnableWaitComplete(0)
        Me.AwaitOperationCompleted(Me.TspLinkResetTimeout)
        Me.EnableWaitComplete()
        Me.Session.WriteLine("tsplink.reset() waitcomplete(0) errorqueue.clear() waitcomplete()")
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' failed sending the TSP Link reset command;. {1}{2}",
                                             Me.ResourceName,
                                             Environment.NewLine, New StackFrame(True).UserCallStack())
        End If
        Me.AwaitOperationCompleted(Me.TspLinkResetTimeout)

    End Sub

    ''' <summary> Reset TSP Link with error reporting. </summary>
    ''' <returns> <c>True</c> of okay; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>
    Private Function TryResetTspLinkReportError() As Boolean

        If Not Me.IsSessionOpen Then Return True
        Me.IsTspLinkOnline = New Boolean?
        Me.EnableWaitComplete()
        Me.Session.WriteLine("tsplink.reset() waitcomplete(0)")
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            If Not Me.ReportVisaDeviceOperationOkay(False, "resetting TSP Link;. ") Then
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                           "Instrument '{0}' failed resetting TSP Link;. {1}{2}",
                                           Me.ResourceName,
                                           Environment.NewLine, New StackFrame(True).UserCallStack())
            End If
            Return False
        Else
            Me.AwaitServiceRequest(ServiceRequests.RequestingService, TimeSpan.FromMilliseconds(1000))
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                           "Instrument '{0}' failed awaiting completion after resetting TSP Link;. {1}{2}",
                                           Me.ResourceName,
                                           Environment.NewLine, New StackFrame(True).UserCallStack())
                Return False
            Else
                Return Me.ReportVisaDeviceOperationOkay(False, "resetting TSP Link;. ")
            End If
        End If
        Return True

    End Function

    ''' <summary> Resets the TSP link if not on line. </summary>
    ''' <history date="09/08/2009" by="David" revision="3.0.3538.x"> Allows to complete TSP
    ''' Link reset even on failure in case we have a single node. </history>
    Public Sub ResetTspLink(ByVal maximumNodeCount As Integer)

        If Me.UsingTspLink Then
            Me.ResetTspLinkIgnoreError()
        End If

        If Me.IsTspLinkOnline Then
            ' enumerate all nodes.
            Me.EnumerateNodes(maximumNodeCount)
        Else
            ' enumerate the controller node.
            Me.EnumerateNodes(1)
        End If

        ' assign node group numbers.
        Me.AssignNodeGroupNumbers()

        ' clear the error queue on all nodes.
        Me.ClearErrorQueue()

        ' clear data queues.
        Me.ClearDataQueue(Me.NodeEntities)

    End Sub

#End Region

#End Region

#Region " CHECK AND REPORT "

    ''' <summary> Check and reports visa or device error occurred. Can only be used after receiving a
    ''' full reply from the instrument. </summary>
    ''' <param name="nodeNumber"> Specifies the remote node number to validate. </param>
    ''' <param name="format">     Specifies the report format. </param>
    ''' <param name="args">       Specifies the report arguments. </param>
    ''' <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
    Public Overrides Function ReportVisaDeviceOperationOkay(ByVal nodeNumber As Integer, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Dim success As Boolean = Me.ReportVisaDeviceOperationOkay(nodeNumber, False, format, args)
        If success AndAlso Me.IsSessionOpen AndAlso (nodeNumber <> Me.ControllerNode.ControllerNodeNumber) Then
            success = Me.QueryErrorQueueCount(Me.ControllerNode) = 0
            Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
            If success Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "Instrument {0} node {1} done {2}",
                                           Me.ResourceName, nodeNumber, details)
            Else
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                           "Instrument {0} node {1} encountered errors {2}Details: {3}{4}{5}",
                                           Me.ResourceName, nodeNumber, Me.DeviceErrors, details,
                                           Environment.NewLine, New StackFrame(True).UserCallStack())
            End If
        End If
        Return success
    End Function

#End Region

End Class
