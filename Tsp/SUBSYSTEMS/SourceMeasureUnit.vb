Imports NationalInstruments
Imports System.ComponentModel
Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.EnumExtensions
''' <summary> Manages TSP SMU subsystem. </summary>
''' <license> (c) 2007 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/7/2013" by="David" revision="">                  Uses new core. </history>
''' <history date="01/28/2008" by="David" revision="2.0.2949.x">  Use .NET Framework. </history>
''' <history date="03/12/2007" by="David" revision="1.15.2627.x"> Created. </history>
Public MustInherit Class SourceMeasureUnit
    Inherits SourceMeasureUnitBase

#Region " CONSTRUCTION + CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SourceMeasureUnit" /> class. </summary>
    ''' <remarks> Note that the local node status clear command only clears the SMU status.  So, issue
    ''' a CLS and RST as necessary when adding an SMU. </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystem">TSP status
    ''' Subsystem</see>. </param>
    Protected Sub New(ByVal statusSubsystem As StatusSubsystemBase)
        Me.New(statusSubsystem, 0, TspSyntax.SourceMeasureUnitNumberA)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="SourceMeasureUnit" /> class. </summary>
    ''' <remarks> Note that the local node status clear command only clears the SMU status.  So, issue
    ''' a CLS and RST as necessary when adding an SMU. </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="statusSubsystem">TSP status
    ''' Subsystem</see>. </param>
    ''' <param name="nodeNumber">      Specifies the node number. </param>
    ''' <param name="smuNumber">       Specifies the SMU (either 'a' or 'b'. </param>
    Protected Sub New(ByVal statusSubsystem As StatusSubsystemBase, ByVal nodeNumber As Integer, ByVal smuNumber As String)
        MyBase.New(statusSubsystem, nodeNumber, smuNumber)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
    Public Overrides Sub ResetKnownState()
        Me.ContactCheckOkay = New Boolean?
        Me.ContactCheckThreshold = New Integer?
        Me.ContactCheckSpeedMode = New Tsp.ContactCheckSpeedMode?
        Me.ContactResistances = ""
        Me.SourceFunction = SourceFunctionMode.VoltageDC
        Me.SenseMode = SenseActionMode.Local
    End Sub

#End Region

#Region " CONTACT CHECK "

#Region " CONTACT CHECK SPEED MODE "

    ''' <summary> The Contact Check Speed Mode. </summary>
    Private _ContactCheckSpeedMode As ContactCheckSpeedMode?

    ''' <summary> Gets or sets the cached Contact Check Speed Mode. </summary>
    ''' <value> The <see cref="ContactCheckSpeedMode">Contact Check Speed Mode</see> or none if not set or
    ''' unknown. </value>
    Public Overloads Property ContactCheckSpeedMode As ContactCheckSpeedMode?
        Get
            Return Me._ContactCheckSpeedMode
        End Get
        Protected Set(ByVal value As ContactCheckSpeedMode?)
            If Not Nullable.Equals(Me.ContactCheckSpeedMode, value) Then
                Me._ContactCheckSpeedMode = value
                Me.SafePostPropertyChanged(NameOf(Me.ContactCheckSpeedMode))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Contact Check Speed Mode. </summary>
    ''' <param name="value"> The  Contact Check Speed Mode. </param>
    ''' <returns> The <see cref="ContactCheckSpeedMode">Contact Check Speed Mode</see> or none if unknown. </returns>
    Public Function ApplyContactCheckSpeedMode(ByVal value As ContactCheckSpeedMode) As ContactCheckSpeedMode?
        Me.WriteContactCheckSpeedMode(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.ContactCheckSpeedMode
        Else
            Return Me.QueryContactCheckSpeedMode()
        End If
    End Function

    ''' <summary> Queries the Contact Check Speed Mode. </summary>
    ''' <returns> The <see cref="ContactCheckSpeedMode">Contact Check Speed Mode</see> or none if unknown. </returns>
    Public Function QueryContactCheckSpeedMode() As ContactCheckSpeedMode?
        Dim currentValue As String = Me.ContactCheckSpeedMode.ToString
        If Me.IsSessionOpen Then
            currentValue = Me.TspSession.QueryPrintTrimEnd("{0}.contact.speed()", Me.SourceMeasureUnitReference)
        End If
        If String.IsNullOrWhiteSpace(currentValue) Then
            Dim message As String = "Failed fetching Contact Check Speed Mode"
            Debug.Assert(Not Debugger.IsAttached, message)
            Me.ContactCheckSpeedMode = New Tsp.ContactCheckSpeedMode?
        Else
            Dim se As New StringEnumerator(Of ContactCheckSpeedMode)
            ' strip the SMU reference.
            currentValue = currentValue.Substring(currentValue.LastIndexOf(".", StringComparison.OrdinalIgnoreCase) + 1).Trim("."c)
            Me.ContactCheckSpeedMode = se.ParseContained(currentValue.Substring(4))
        End If
        Return Me.ContactCheckSpeedMode
    End Function

    ''' <summary> Writes the Contact Check Speed Mode without reading back the value from the device. </summary>
    ''' <param name="value"> The Contact Check Speed Mode. </param>
    ''' <returns> The <see cref="ContactCheckSpeedMode">Contact Check Speed Mode</see> or none if unknown. </returns>
    Public Function WriteContactCheckSpeedMode(ByVal value As ContactCheckSpeedMode) As ContactCheckSpeedMode?
        If Me.IsSessionOpen Then
            Me.Session.WriteLine("{0}.contact.speed={0}.{1}", Me.SourceMeasureUnitReference, value.ExtractBetween())
        End If
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Me.ContactCheckSpeedMode = New ContactCheckSpeedMode?
        Else
            Me.ContactCheckSpeedMode = value
        End If
        Return Me.ContactCheckSpeedMode
    End Function

#End Region

#Region " THRESHOLD "

    Private _ContactCheckThreshold As Integer?

    ''' <summary> Gets or sets (Protected) the contact check threshold. </summary>
    ''' <value> The contact check threshold. </value>
    Public Property ContactCheckThreshold As Integer?
        Get
            Return Me._ContactCheckThreshold
        End Get
        Set(ByVal value As Integer?)
            If Not Nullable.Equals(value, Me.ContactCheckThreshold) Then
                Me._ContactCheckThreshold = value
                Me.SafePostPropertyChanged("ContactCheckThreshold")
            End If
        End Set
    End Property

    ''' <summary> Programs and reads back the Contact Check Threshold Level. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> The <see cref="ContactCheckThreshold">Contact Check Threshold</see> or nothing if not known. </returns>
    Public Function ApplyContactCheckThreshold(ByVal value As Integer) As Integer?
        Me.WriteContactCheckThreshold(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.ContactCheckThreshold
        Else
            Return Me.QueryContactCheckThreshold()
        End If
    End Function

    ''' <summary> Reads back the Contact Check Threshold Level. </summary>
    ''' <returns> The <see cref="ContactCheckThreshold">Contact Check Threshold</see> or nothing if not known. </returns>
    Public Overridable Function QueryContactCheckThreshold() As Integer?
        If Me.IsSessionOpen Then
            Me.ContactCheckThreshold = Me.TspSession.QueryPrint(0I, "{0}.contact.threshold()", Me.SourceMeasureUnitReference)
        End If
        Return Me.ContactCheckThreshold
    End Function

    ''' <summary> Programs the Contact Check Threshold Level without updating the value from the device. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> The <see cref="ContactCheckThreshold">Contact Check Threshold</see> or nothing if not known. </returns>
    Public Overridable Function WriteContactCheckThreshold(ByVal value As Integer) As Integer?
        If Me.IsSessionOpen Then
            Me.Session.WriteLine("{0}.contact.threshold={1}", Me.SourceMeasureUnitReference, value)
        End If
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Me.ContactCheckThreshold = New Integer?
        Else
            Me.ContactCheckThreshold = value
        End If
        Return Me.ContactCheckThreshold
    End Function

#End Region

#Region " RESISTANCES "

    Dim _ContactResistances As String

    ''' <summary> Gets or sets (Protected) the contact resistances. </summary>
    ''' <value> The contact resistances. </value>
    Public Property ContactResistances() As String
        Get
            Return Me._contactResistances
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not value.Equals(Me.ContactResistances) Then
                Me._contactResistances = value
                Me.SafePostPropertyChanged("ContactResistances")
            End If
        End Set

    End Property

    ''' <summary> Reads the Contact Resistances. </summary>
    ''' <returns> The <see cref="ContactResistances">Contact Resistances</see> or nothing if not known. </returns>
    Public Overridable Function QueryContactResistances() As String
        If Me.IsSessionOpen Then
            Me.ContactResistances = Me.TspSession.QueryPrintTrimEnd("{0}.contact.r()", Me.SourceMeasureUnitReference)
        End If
        Return Me.ContactResistances
    End Function

#End Region

#Region " CONTACT CHECK OKAY "

    ''' <summary> ContactCheckOkay. </summary>
    Private _ContactCheckOkay As Boolean?

    ''' <summary> Gets or sets the cached Contact Check Okay sentinel. </summary>
    ''' <value> <c>null</c> if Contact Check Okay is not known; <c>True</c> if output is on; otherwise,
    ''' <c>False</c>. </value>
    Public Property ContactCheckOkay As Boolean?
        Get
            Return Me._ContactCheckOkay
        End Get
        Protected Set(ByVal value As Boolean?)
            If Not Boolean?.Equals(Me.ContactCheckOkay, value) Then
                Me._ContactCheckOkay = value
                Me.SafePostPropertyChanged(NameOf(Me.ContactCheckOkay))
            End If
        End Set
    End Property

    ''' <summary> Queries the Contact Check status. Also sets the <see cref="ContactCheckOkay">contact check</see> sentinel. </summary>
    ''' <returns> <c>null</c> if not known; <c>True</c> if ContactCheckOkay; otherwise, <c>False</c>. </returns>
    Public Function QueryContactCheckOkay() As Boolean?
        If Me.IsSessionOpen Then
            Me.ContactCheckOkay = Me.TspSession.IsStatementTrue("{0}.contact.check()", Me.SourceMeasureUnitReference)
        End If
        Return Me.ContactCheckOkay
    End Function

#Region " CONTACT CHECK "

    ''' <summary> Determines whether contact resistances are below the specified threshold. </summary>
    ''' <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="DeviceException"> Thrown when a device error condition occurs. </exception>
    ''' <param name="threshold"> The threshold. </param>
    ''' <returns> <c>True</c> if passed, <c>False</c> if failed, <c>True</c> if passed. Exception is
    ''' thrown if failed configuring contact check. </returns>
    Public Function CheckContacts(ByVal threshold As Integer) As Boolean?

        Me.ContactResistances = "-1,-1"
        If Me.IsSessionOpen AndAlso Not threshold.Equals(Me.ContactCheckThreshold) Then
            Me.WriteContactCheckThreshold(threshold)
            Me.CheckThrowVisaDeviceException(False, "setting up contact check threshold;. using '{0}'", Me.Session.LastMessageSent)
        End If

        If Me.IsSessionOpen AndAlso Not Me.ContactCheckSpeedMode.Equals(Tsp.ContactCheckSpeedMode.Fast) Then
            Me.WriteContactCheckSpeedMode(Tsp.ContactCheckSpeedMode.Fast)
            Me.CheckThrowVisaDeviceException(False, "setting up contact check speed;. using '{0}'", Me.Session.LastMessageSent)
        End If

        If Me.IsSessionOpen Then
            Me.QueryContactCheckOkay()
            Me.CheckThrowVisaDeviceException(True, "checking contact;. using '{0}'", Me.Session.LastMessageSent)
            If Me.ContactCheckOkay.HasValue AndAlso Not Me.ContactCheckOkay.Value Then
                Me.QueryContactResistances()
                Me.CheckThrowVisaDeviceException(True, "reading contacts;. using '{0}'", Me.Session.LastMessageSent)
                If String.IsNullOrWhiteSpace(Me._contactResistances) Then
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Contact check failed;. Failed fetching contact resistances using  '{0}'", Me.Session.LastMessageSent)
                Else
                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Contact check failed;. Contact resistance {0} exceeded the limit {1}",
                                               Me.ContactResistances, Me.ContactCheckThreshold)
                End If
            End If
        End If

        If Me.ContactCheckOkay.HasValue Then
            Return Me.ContactCheckOkay.Value
        Else
            Return New Boolean?
        End If
    End Function
#End Region

#End Region

#End Region

#Region " SOURCE FUNCTION "

    ''' <summary> The Source Function. </summary>
    Private _SourceFunction As SourceFunctionMode?

    ''' <summary> Gets or sets the cached Source Function. </summary>
    ''' <value> The <see cref="SourceFunctionMode">Source Function</see> or none if not set or
    ''' unknown. </value>
    Public Overloads Property SourceFunction As SourceFunctionMode?
        Get
            Return Me._SourceFunction
        End Get
        Protected Set(ByVal value As SourceFunctionMode?)
            If Not Nullable.Equals(Me.SourceFunction, value) Then
                Me._SourceFunction = value
                Me.SafePostPropertyChanged(NameOf(Me.SourceFunction))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Source Function. </summary>
    ''' <param name="value"> The  Source Function. </param>
    ''' <returns> The <see cref="SourceFunctionMode">Source Function</see> or none if unknown. </returns>
    Public Function ApplySourceFunction(ByVal value As SourceFunctionMode) As SourceFunctionMode?
        Me.WriteSourceFunction(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.SourceFunction
        Else
            Return Me.QuerySourceFunction()
        End If
    End Function

    ''' <summary> Queries the Source Function. </summary>
    ''' <returns> The <see cref="SourceFunctionMode">Source Function</see> or none if unknown. </returns>
    Public Function QuerySourceFunction() As SourceFunctionMode?
        Dim currentValue As String = Me.SourceFunction.ToString
        If Me.IsSessionOpen Then
            currentValue = Me.TspSession.QueryPrintTrimEnd("{0}.source.func", Me.SourceMeasureUnitReference)
        End If
        If String.IsNullOrWhiteSpace(currentValue) Then
            Dim message As String = "Failed fetching Source Function"
            Debug.Assert(Not Debugger.IsAttached, message)
            Me.SourceFunction = New SourceFunctionMode?
        Else
            Dim se As New StringEnumerator(Of SourceFunctionMode)
            Me.SourceFunction = se.ParseContained(currentValue.BuildDelimitedValue)
        End If
        Return Me.SourceFunction
    End Function

    ''' <summary> Writes the Source Function Without reading back the value from the device. </summary>
    ''' <param name="value"> The Source Function. </param>
    ''' <returns> The <see cref="SourceFunctionMode">Source Function</see> or none if unknown. </returns>
    Public Function WriteSourceFunction(ByVal value As SourceFunctionMode) As SourceFunctionMode?
        If Me.IsSessionOpen Then
            Me.Session.WriteLine("{0}.source.func = {0}.{1}", Me.SourceMeasureUnitReference, value.ExtractBetween())
        End If
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Me.SourceFunction = New SourceFunctionMode?
        Else
            Me.SourceFunction = value
        End If
        Return Me.SourceFunction
    End Function

#End Region

#Region " SENSE MODE "

    ''' <summary> The Sense Action. </summary>
    Private _SenseMode As SenseActionMode?

    ''' <summary> Gets or sets the cached Sense Action. </summary>
    ''' <value> The <see cref="SenseActionMode">Sense Action</see> or none if not set or
    ''' unknown. </value>
    Public Overloads Property SenseMode As SenseActionMode?
        Get
            Return Me._SenseMode
        End Get
        Protected Set(ByVal value As SenseActionMode?)
            If Not Nullable.Equals(Me.SenseMode, value) Then
                Me._SenseMode = value
                Me.SafePostPropertyChanged(NameOf(Me.SenseMode))
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Sense Action. </summary>
    ''' <param name="value"> The  Sense Action. </param>
    ''' <returns> The <see cref="SenseActionMode">Sense Action</see> or none if unknown. </returns>
    Public Function ApplySenseMode(ByVal value As SenseActionMode) As SenseActionMode?
        Me.WriteSenseMode(value)
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Return Me.SenseMode
        Else
            Return Me.QuerySenseMode()
        End If
    End Function

    ''' <summary> Queries the Sense Action. </summary>
    ''' <returns> The <see cref="SenseActionMode">Sense Action</see> or none if unknown. </returns>
    Public Function QuerySenseMode() As SenseActionMode?
        Dim currentValue As String = Me.SenseMode.ToString
        If Me.IsSessionOpen Then
            currentValue = Me.TspSession.QueryPrintTrimEnd("{0}.sense", Me.SourceMeasureUnitReference)
        End If
        If String.IsNullOrWhiteSpace(currentValue) Then
            Dim message As String = "Failed fetching Sense Action"
            Debug.Assert(Not Debugger.IsAttached, message)
            Me.SenseMode = New SenseActionMode?
        Else
            Dim se As New StringEnumerator(Of SenseActionMode)
            Me.SenseMode = se.ParseContained(currentValue.BuildDelimitedValue)
        End If
        Return Me.SenseMode
    End Function

    ''' <summary> Writes the Sense Action without reading back the value from the device. </summary>
    ''' <param name="value"> The Sense Action. </param>
    ''' <returns> The <see cref="SenseActionMode">Sense Action</see> or none if unknown. </returns>
    Public Function WriteSenseMode(ByVal value As SenseActionMode) As SenseActionMode?
        If Me.IsSessionOpen Then
            Me.Session.WriteLine("{0}.sense = {0}.{1}", Me.SourceMeasureUnitReference, value.ExtractBetween())
        End If
        If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
            Me.SenseMode = New SenseActionMode?
        Else
            Me.SenseMode = value
        End If
        Return Me.SenseMode
    End Function

#End Region

End Class

''' <summary> Specifies the source function modes. </summary>
Public Enum SourceFunctionMode
    <Description("None")> None
    <Description("DC Voltage (OUTPUT_DCVOLTS)")> VoltageDC
    <Description("DC Current (OUTPUT_DCAMPS)")> CurrentDC
End Enum

''' <summary> Specifies the sense modes. </summary>
Public Enum SenseActionMode
    <Description("None")> None
    <Description("Remote (SENSE_REMOTE)")> Remote
    <Description("Local (SENSE_LOCAL)")> Local
End Enum
