﻿
''' <summary> Information about the version of a Keithley 2700 instrument. </summary>
''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="9/22/2013" by="David" revision="3.0.5013"> Created. </history>
Public Class VersionInfo
    Inherits isr.IO.Visa.VersionInfoBase

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.new()
        Me._boardRevisions = New System.Collections.Specialized.StringDictionary
        Me._FirmwareVersion = New System.Version
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    Public Overrides Sub Clear()
        MyBase.Clear()
        Me._boardRevisions = New System.Collections.Specialized.StringDictionary
        Me.ParseFirmwareRevision("")
    End Sub

    Private _BoardRevisions As System.Collections.Specialized.StringDictionary
    ''' <summary>Returns the list of board revisions.</summary>
    Public ReadOnly Property BoardRevisions() As System.Collections.Specialized.StringDictionary
        Get
            Return Me._boardRevisions
        End Get
    End Property

    ''' <summary> Gets or sets the firmware version. </summary>
    ''' <value> The firmware version. </value>
    Public Property FirmwareVersion As System.Version

    ''' <summary> Parses the instrument firmware revision. </summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
    ''' <param name="revision"> Specifies the instrument revision
    ''' e.g., <c>2.1.6</c>. The source meter identity includes no board specs. </param>
    Public Overrides Sub ParseFirmwareRevision(ByVal revision As String)

        If revision Is Nothing Then
            Throw New ArgumentNullException("revision")
        ElseIf String.IsNullOrWhiteSpace(revision) Then
            Me.FirmwareVersion = New System.Version
        Else
            Me.FirmwareVersion = System.Version.Parse(revision)
        End If

    End Sub

End Class

''' <summary>Boards included in the instrument.</summary>
Public Enum BoardType
    <System.ComponentModel.Description("None")> None = 0
    <System.ComponentModel.Description("Digital")> Digital = 1
    <System.ComponentModel.Description("Display")> Display = 2
End Enum

