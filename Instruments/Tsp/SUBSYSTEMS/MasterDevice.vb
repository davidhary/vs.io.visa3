﻿Imports isr.Core.Agnostic.ExceptionExtensions
''' <summary> The Thermal Transient Meter device. </summary>
''' <remarks> An instrument is defined, for the purpose of this library, as a device with a front
''' panel. </remarks>
''' <license> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
''' SOFTWARE.</para> </license>
''' <history date="12/12/2013" by="David" revision=""> Created. </history>
Public Class MasterDevice
    Inherits isr.IO.Visa.Tsp.MasterDeviceBase

#Region " CONSTRUCTION + CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.new()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Clears the active state. Issues selective device clear. </summary>
    Public Overrides Sub ClearActiveState()
        Me.StatusSubsystem.ClearActiveState()
    End Sub

    ''' <summary> Initializes the Device. Performs a reset and additional custom setting by the parent
    ''' Devices. </summary>
    Public Overrides Sub InitializeKnownState()

        MyBase.InitializeKnownState()

        ' disable TSP link for the system
        Me.StatusSubsystem.UsingTspLink = False

    End Sub

    ''' <summary> Resets the Device to its known state. </summary>
    Public Overrides Sub ResetKnownState()
        MyBase.ResetKnownState()
    End Sub

#End Region

#Region " PUBLISHER "

    ''' <summary> Publishes all values by raising the property changed events. </summary>
    Public Overrides Sub Publish()
        Me.Subsystems.Publish()
        If Me.Publishable Then
            For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                Me.SafePostPropertyChanged(p.Name)
            Next
        End If
    End Sub

#End Region

#Region " SESSION "

    ''' <summary> Allows the derived device to take actions after closing. </summary>
    Protected Overrides Sub OnClosed()
        MyBase.OnClosed()
    End Sub

    ''' <summary> Allows the derived device to take actions before closing. Removes subsystems and
    ''' event handlers. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnClosing(e As System.ComponentModel.CancelEventArgs)
        If e Is Nothing OrElse Not e.Cancel Then

            If Me.DisplaySubsystem IsNot Nothing Then
                Me.DisplaySubsystem.RestoreDisplay(Me.InitializeTimeout)
            End If

            If Me.SourceMeasureUnit IsNot Nothing Then
                RemoveHandler Me.SourceMeasureUnit.PropertyChanged, AddressOf SourceMeasureSubsystemPropertyChanged
            End If

            If Me.ScriptManager IsNot Nothing Then
                RemoveHandler Me.ScriptManager.PropertyChanged, AddressOf ScriptManagerPropertyChanged
            End If

            If Me.DisplaySubsystem IsNot Nothing Then
                RemoveHandler Me.DisplaySubsystem.PropertyChanged, AddressOf DisplaySubsystemPropertyChanged
            End If

            If Me.SystemSubsystem IsNot Nothing Then
                RemoveHandler Me.SystemSubsystem.PropertyChanged, AddressOf SystemSubsystemPropertyChanged
            End If

        End If
        MyBase.OnClosing(e)
    End Sub

    ''' <summary> Allows the derived device to take actions after opening. Adds subsystems and event
    ''' handlers. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnOpened()

        ' prevent adding multiple times
        If MyBase.Subsystems IsNot Nothing AndAlso MyBase.Subsystems.Count <> 0 Then
            Debug.Assert(Not Debugger.IsAttached, "Unexpected attempt to add subsystems on top of subsystems")
        Else

            ' STATUS must be the first subsystem.
            Me.StatusSubsystem = New StatusSubsystem(Me.Session)
            MyBase.AddSubsystem(Me.StatusSubsystem)
            AddHandler Me.StatusSubsystem.PropertyChanged, AddressOf StatusSubsystemPropertyChanged

            Me.SystemSubsystem = New SystemSubsystem(Me.StatusSubsystem)
            MyBase.AddSubsystem(Me.SystemSubsystem)
            AddHandler Me.SystemSubsystem.PropertyChanged, AddressOf SystemSubsystemPropertyChanged

            Me.DisplaySubsystem = New DisplaySubsystem(Me.StatusSubsystem)
            MyBase.AddSubsystem(Me.DisplaySubsystem)
            AddHandler Me.DisplaySubsystem.PropertyChanged, AddressOf DisplaySubsystemPropertyChanged

            Me.ScriptManager = New ScriptManager(Me.StatusSubsystem, Me.DisplaySubsystem)
            MyBase.AddSubsystem(Me.ScriptManager)
            AddHandler Me.ScriptManager.PropertyChanged, AddressOf ScriptManagerPropertyChanged

            Me.SourceMeasureUnit = New SourceMeasureUnit(Me.StatusSubsystem)
            MyBase.AddSubsystem(Me.SourceMeasureUnit)
            AddHandler Me.SourceMeasureUnit.PropertyChanged, AddressOf SourceMeasureSubsystemPropertyChanged

            ' set the default file path for scripts.
            Me.ScriptManager.FilePath = System.IO.Path.GetFullPath(System.IO.Path.Combine(My.Application.Info.DirectoryPath, "Scripts"))
            MyBase.OnOpened()

        End If

    End Sub

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpening(e As System.ComponentModel.CancelEventArgs)
        MyBase.OnOpening(e)
    End Sub

#End Region

#Region " SUBSYSTEMS "

#Region " DISPLAY "

    ''' <summary> Display subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub DisplaySubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Try
            If sender IsNot Nothing AndAlso
                e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                ' Dim subsystem As DisplaySubsystem = CType(sender, DisplaySubsystem)
                Select Case e.PropertyName
                    Case "Exists"
                End Select
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#Region " SCRIPT MANAGER "

    ''' <summary> Script Manager property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub ScriptManagerPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Try
            If sender IsNot Nothing AndAlso
                e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                ' Dim subsystem As DisplaySubsystem = CType(sender, DisplaySubsystem)
                Select Case e.PropertyName
                    Case "Exists"
                End Select
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#Region " STATUS "

    ''' <summary> Status subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub StatusSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Try
            If sender IsNot Nothing AndAlso
                e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                ' Dim subsystem As StatusSubsystem = CType(sender, StatusSubsystem)
                Select Case e.PropertyName
#If False Then
                    Case "Identity"
                        If Not String.IsNullOrWhiteSpace(subsystem.Identity) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceId, "{0} identified as {1}.",
                                                       Me.ResourceName, subsystem.Identity)

                        End If
                    Case "DeviceErrors"
                    Case "ErrorAvailable"
                    Case "ErrorAvailableBits"
                    Case "MeasurementAvailable"
                    Case "MeasurementAvailableBits"
                    Case "MeasurementEventCondition"
                    Case "MeasurementEventEnableBitmask"
                    Case "MeasurementEventStatus"
                    Case "MessageAvailable"
                    Case "MessageAvailableBits"
                    Case "OperationEventCondition"
                    Case "OperationEventEnableBitmask"
                    Case "OperationEventStatus"
                    Case "OperationNegativeTransitionEventEnableBitmask"
                    Case "OperationPositiveTransitionEventEnableBitmask"
                    Case "QuestionableEventCondition"
                    Case "QuestionableEventEnableBitmask"
                    Case "QuestionableEventStatus"
                    Case "ServiceRequestEnableBitmask"
                    Case "ServiceRequestStatus"
                    Case "StandardDeviceErrorAvailable"
                    Case "StandardDeviceErrorAvailableBits"
                    Case "StandardEventEnableBitmask"
                    Case "StandardEventStatus"
#End If
                End Select
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#Region " SYSTEM "

    ''' <summary> System subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Try
            If sender IsNot Nothing AndAlso
                e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                ' Dim subsystem As SystemSubsystem = CType(sender, SystemSubsystem)
                Select Case e.PropertyName
                    Case "LastError"
                    Case "LineFrequency"
                    Case "StationLineFrequency"
                    Case "VersionInfo"
                End Select
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#Region " SMU "

    ''' <summary> Smu Subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub SourceMeasureSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Try
            If sender IsNot Nothing AndAlso
                e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                ' Dim subsystem As IO.Visa.Tsp.SourceMeasureUnitBase = CType(sender, IO.Visa.Tsp.SourceMeasureUnitBase)
                Select Case e.PropertyName
                    Case "SourceMeasureUnit"
                    Case "ContactCheckOkay"
                End Select
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#End Region

End Class

