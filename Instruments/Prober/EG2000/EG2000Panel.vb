Imports System.ComponentModel
Imports isr.Core.Controls.ControlExtensions
Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.EscapeSequencesExtensions
Imports isr.Core.Agnostic.ExceptionExtensions
Namespace EG2000

    ''' <summary> Provides a user interface for the EG2000 Prober Device. </summary>
    ''' <license> (c) 2015 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="10/01/2013" by="David" revision="3.0.5022"> Created. </history>
    <System.ComponentModel.DisplayName("EG2000 Panel"),
      System.ComponentModel.Description("EG2000 Prober Panel"),
      System.Drawing.ToolboxBitmap(GetType(EG2000.EG2000Panel))>
    Public Class EG2000Panel
        #Const designMode1 = True
#Region " BASE FROM WRAPPER "
        ' Designing requires changing the condition to True.
#If designMode Then
        Inherits Visa.Instrument.ResourcePanelBaseWrapper
#Else
        Inherits Visa.Instrument.ResourcePanelBase
#End If
#End Region

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Default constructor. </summary>
        Public Sub New()
            MyBase.New()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' instantiate the reference to the Device
            Me.Device = New EG2000.Device()

            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            Me.initializeUserInterface()
            Me.onLastReadingAvailable(Nothing)

#If designMode Then
        Debug.Assert(False, "Illegal call; reset design mode")
#End If

        End Sub

        ''' <summary> Disposes managed resources. </summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub OnDisposeManagedResources()
            If Me.Device IsNot Nothing Then
                Try
                    ' remove event handlers.
                    Me.DeviceClosing(Me, System.EventArgs.Empty)
                    Me._Device.Dispose()
                    Me._Device = Nothing
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, "Exception occurred disposing Prober panel resources", "Exception {0}", ex.ToFullBlownString)
                End Try
            End If
        End Sub

        ''' <summary> Enables or disables controls based on the device open state. </summary>
        ''' <param name="deviceIsOpen"> true if device is open. </param>
        Private Sub OnDisplayDeviceOpenChanged(ByVal deviceIsOpen As Boolean)
            Me._Tabs.Enabled = True
            For Each t As Windows.Forms.TabPage In Me._Tabs.TabPages
                If t IsNot Me._MessagesTabPage Then
                    For Each c As Windows.Forms.Control In t.Controls : c.RecursivelyEnable(deviceIsOpen) : Next
                End If
            Next
        End Sub

        ''' <summary> Initialize User interface. </summary>
        Private Sub InitializeUserInterface()
            Me.IdentityToolStripStatusLabel.Visible = False
            Me.StandardRegisterToolStripStatusLabel.Visible = False
            Me.StatusRegisterToolStripStatusLabel.Visible = True
            Me.StatusRegisterToolStripStatusLabel.Text = "0x.."
        End Sub

#End Region

#Region " DEVICE "

        Private _Device As EG2000.Device

        ''' <summary> Gets or sets a reference to the Device. </summary>
        ''' <value> The device. </value>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Public Overloads Property Device() As EG2000.Device
            Get
                Return Me._Device
            End Get
            Friend Set(ByVal value As EG2000.Device)
                If value IsNot Nothing Then
                    Me._Device = value
                    MyBase.Device = value
                End If
            End Set
        End Property


#End Region

#Region " DEVICE EVENT HANDLERS "

        ''' <summary> Handle the device property changed event. </summary>
        ''' <param name="device">    The device. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnDevicePropertyChanged(ByVal device As Device, ByVal propertyName As String)
            If device Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "Enabled"
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                                   "Hardware {0}.", IIf(device.Enabled, "Enabled", "Disabled"))
                    Case "ResourcesSearchPattern"
                        MyBase.Connector.ResourcesSearchPattern = device.ResourcesSearchPattern
                    Case "ServiceRequestFailureMessage"
                        If Not String.IsNullOrWhiteSpace(device.ServiceRequestFailureMessage) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, device.ServiceRequestFailureMessage)
                        End If
#If False Then
                    Case "IsSessionOpen"
                    Case "IsDeviceOpen"
#End If
                End Select
            End If
        End Sub

        ''' <summary> Event handler. Called for property changed events. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information to send to registered event handlers. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Protected Overrides Sub DevicePropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnDevicePropertyChanged(TryCast(sender, Device), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling property changed Event;. Failed property {0}. {1}",
                                           e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub

        ''' <summary> Device service requested. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Message based session event information. </param>
        Protected Overrides Sub DeviceServiceRequested(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs)
            MyBase.DeviceServiceRequested(sender, e)
        End Sub

        ''' <summary> Event handler. Called upon device opening. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceOpening(ByVal sender As Object, ByVal e As System.EventArgs)
            MyBase.DeviceOpening(sender, e)
        End Sub

        ''' <summary> Event handler. Called when device opened. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)

            ' populate the supported commands. 
            With Me._CommandComboBox
                .DataSource = Nothing
                .Items.Clear()
                .DataSource = Me.Device.ProberSubsystem.SupportedCommands
                .SelectedIndex = 0
            End With

            ' populate the emulated reply combo.
            With Me._EmulatedReplyComboBox
                .DataSource = Nothing
                .Items.Clear()
                .DataSource = Me.Device.ProberSubsystem.SupportedEmulationCommands
                .SelectedIndex = 0
            End With

            Me.EnableServiceRequestEventHandler()
            AddHandler Me.Device.ProberSubsystem.PropertyChanged, AddressOf Me.ProberSubsystemPropertyChanged
            AddHandler Me.Device.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
            AddHandler Me.Device.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            MyBase.DeviceOpened(sender, e)
            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            ' moved to the controls. 
            ' Me.Device.InitializeKnownState()
        End Sub

        ''' <summary> Event handler. Called when device is closing. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceClosing(ByVal sender As Object, ByVal e As System.EventArgs)
            MyBase.DeviceClosing(sender, e)
            If Me.IsDeviceOpen Then
                RemoveHandler Me.Device.ProberSubsystem.PropertyChanged, AddressOf Me.ProberSubsystemPropertyChanged
                RemoveHandler Me.Device.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
                RemoveHandler Me.Device.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            End If
        End Sub

        ''' <summary> Event handler. Called when device is closed. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            MyBase.DeviceClosed(sender, e)
        End Sub

#End Region

#Region " SUBSYSTEMS "

        ''' <summary> Updates the indicator. </summary>
        ''' <param name="label">    The label. </param>
        ''' <param name="sentinel"> The sentinel. </param>
        Private Shared Sub UpdateIndicator(ByVal label As Windows.Forms.Label, ByVal sentinel As Boolean?)
            If sentinel.GetValueOrDefault(False) Then
                label.BackColor = Drawing.Color.LightGreen
            Else
                label.BackColor = Drawing.Color.White
            End If
        End Sub

        ''' <summary> Updates the indicator. </summary>
        ''' <param name="label">    The label. </param>
        ''' <param name="value"> The value. </param>
        Private Shared Sub UpdateIndicator(ByVal label As Windows.Forms.Label, ByVal prefix As String, ByVal value As String, ByVal sentinel As Boolean?)
            updateIndicator(label, sentinel)
            If sentinel.GetValueOrDefault(False) Then
                label.Text = prefix & value
            Else
                label.Text = prefix
            End If
        End Sub

#Region " PROBER "

        ''' <summary> Handles the last reading available action. </summary>
        ''' <param name="reading"> The reading. </param>
        Private Sub OnLastReadingAvailable(ByVal reading As String)
            If reading Is Nothing Then
                Me._ReadingTextBox.Text = ""
                Me._LastMessageTextBox.Text = ""
            Else
                reading = reading.InsertCommonEscapeSequences
                Me._ReadingTextBox.Text = reading
                Me._LastMessageTextBox.Text = reading
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Prober message: '{0}'", reading)
            End If
        End Sub

        ''' <summary> Handles the test start requested action. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub OnTestStartRequested(ByVal subsystem As ProberSubsystem)
            If subsystem Is Nothing Then
            ElseIf subsystem.IsFirstTestStart.GetValueOrDefault(False) Then
                EG2000Panel.updateIndicator(Me._TestStartAttributeLabel, "..", "First Test", subsystem.IsFirstTestStart)
            ElseIf subsystem.RetestRequested.GetValueOrDefault(False) Then
                EG2000Panel.updateIndicator(Me._TestStartAttributeLabel, "..", "Retest", subsystem.RetestRequested)
            ElseIf subsystem.TestAgainRequested.GetValueOrDefault(False) Then
                EG2000Panel.updateIndicator(Me._TestStartAttributeLabel, "..", "Test Again", subsystem.TestAgainRequested)
            Else
                EG2000Panel.updateIndicator(Me._TestStartAttributeLabel, "..", "TS", New Boolean?)
            End If
        End Sub

        ''' <summary> Handles the message received action. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub OnMessageReceived(ByVal subsystem As ProberSubsystemBase)
            If subsystem Is Nothing Then
            End If
            If subsystem Is Nothing Then
            Else
                Me._ReceivedMessageLabel.Text = subsystem.LastReading
            End If
        End Sub

        ''' <summary> Handle the Prober subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As ProberSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "IdentityRead", "ErrorRead", "MessageCompleted", "MessageFailed"
                        Me.onMessageReceived(subsystem)
                    Case "IsFirstTestStart", "RetestRequested"
                        Me.onTestStartRequested(subsystem)
                    Case "LastMessageSent"
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                                   "Controller message: '{0}'", subsystem.LastMessageSent)
                    Case "LastReading"
                        Me.onLastReadingAvailable(subsystem.LastMessageSent)
                    Case "PatternCompleteReceived"
                        EG2000Panel.updateIndicator(Me._PatternCompleteLabel, subsystem.PatternCompleteReceived)
                    Case "SetModeSent"
                        Me._SendMessageLabel.Text = subsystem.LastMessageSent
                    Case "TestCompleteSent"
                        EG2000Panel.updateIndicator(Me._TestCompleteLabel, subsystem.TestCompleteSent)
                    Case "TestStartReceived"
                        EG2000Panel.updateIndicator(Me._TestStartedLabel, subsystem.TestStartReceived)
                    Case "UnhandledMessageReceived"
                        EG2000Panel.updateIndicator(Me._UnhandledMessageLabel, "? ", subsystem.LastReading,
                                              subsystem.UnhandledMessageReceived)
                    Case "UnhandledMessageSent"
                        EG2000Panel.updateIndicator(Me._UnhandledSendLabel, "? ", subsystem.LastMessageSent,
                                              subsystem.UnhandledMessageSent)
                    Case "WaferStartReceived"
                        EG2000Panel.updateIndicator(Me._WaferStartReceivedLabel, subsystem.WaferStartReceived)
                End Select
            End If
        End Sub

        ''' <summary> Prober subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub ProberSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, ProberSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling property changed Event;. Failed property {0}. {1}",
                                           e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub

#End Region

#Region " STATUS "

        ''' <summary> Handle the Status subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As StatusSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "Identity"
                        If Not String.IsNullOrWhiteSpace(subsystem.Identity) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                                       "{0} identified as {1}.", Me.ResourceName, subsystem.Identity)

                        End If
                    Case "MessageAvailable"
                        If subsystem.MessageAvailable Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Message available.")
                        End If
                    Case "MeasurementAvailable"
                        If subsystem.MeasurementAvailable Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Measurement available.")
                        End If
                    Case "ErrorAvailable"
                        If subsystem.ErrorAvailable Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Error available.")
                        End If
                    Case "ServiceRequestStatus"
                        Me.DisplayStatusRegisterStatus(subsystem.ServiceRequestStatus)
                End Select
            End If
        End Sub

        ''' <summary> Status subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub StatusSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, StatusSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception handling property changed Event;. Failed property {0}. {1}",
                                           e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub

#End Region

#Region " SYSTEM "

        ''' <summary> Handle the System subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As SystemSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "LastError"
                        If subsystem.LastError.ErrorNumber > 0 Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                                       "Last error: {0}", subsystem.LastError.CompoundErrorMessage)
                        End If
                        Me._LastMessageTextBox.Text = subsystem.LastError.CompoundErrorMessage
                End Select
            End If
        End Sub

        ''' <summary> System subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, SystemSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception handling property changed Event;. Failed property {0}. {1}",
                                           e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub
#End Region

#End Region

#Region " DISPLAY: TITLE "

        ''' <summary> Gets or sets the title. </summary>
        ''' <value> The title. </value>
        <Category("Appearance"), Description("The title of this panel"),
            Browsable(True),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            DefaultValue("EG2000")>
        Public Property Title As String
            Get
                Return Me._TitleLabel.Text
            End Get
            Set(value As String)
                Me._TitleLabel.Text = value
                Me._TitleLabel.Visible = Not String.IsNullOrWhiteSpace(value)
            End Set
        End Property

#End Region

#Region " TRACE MESSAGES HANDLERS "

        ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
        ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        Protected Overrides Sub DisplayMessage(value As TraceMessage)
            If value IsNot Nothing AndAlso MyBase.ShouldShow(value.EventType) Then
                Me._MessagesBox.AddMessage(value)
            End If
        End Sub

        ''' <summary> Gets the trace display level. </summary>
        ''' <value> The trace display level. </value>
        Public Property TraceDisplayLevel As TraceEventType = TraceEventType.Verbose

        Protected Overrides ReadOnly Property TraceShowLevel As System.Diagnostics.TraceEventType
            Get
                Return Me.TraceDisplayLevel
            End Get
        End Property

#End Region

#Region " CONTROL EVENT HANDLERS: RESET "

        ''' <summary> Event handler. Called by interfaceClearButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _InterfaceClearButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _InterfaceClearButton.Click
            Try
                ' Turn on the form hourglass
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen AndAlso Me.Device.IsSessionOpen AndAlso Me.Device.SystemSubsystem.SupportsClearInterface Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Clearing interface '{0}';. ", Me.Device.Session.InterfaceResourceName)
                    Me.Device.SystemSubsystem.ClearInterface()
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred clearing interface;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by _SelectiveDeviceClearButton for click events. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _SelectiveDeviceClearButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SelectiveDeviceClearButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen AndAlso Me.Device.IsSessionOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "'{0}' Clearing Device '{1}';. ",
                                                Me.Device.Session.InterfaceResourceName, Me.ResourceName)
                    Me.Device.SystemSubsystem.ClearDevice()
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred sending SDC;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Issue RST. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ResetButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ResetButton.Click
            Try
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Resetting known state for '{0}';. ", Me.ResourceName)
                    Me.Device.ResetKnownState()
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred resetting known state;. {0}", ex.ToFullBlownString)
            End Try
        End Sub

        ''' <summary> Event handler. Called by _InitializeKnownStateButton for click events. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _InitializeKnownStateButton_Click(sender As System.Object, ByVal e As System.EventArgs) Handles _InitializeKnownStateButton.Click
            Try
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Initializing known state for '{0}';. ", Me.ResourceName)
                    Me.Device.InitializeKnownState()
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred initializing known state;. {0}", ex.ToFullBlownString)
            End Try
        End Sub

#End Region

#Region " CONTROL EVENTS: READ and WRITE "

        ''' <summary> Event handler. Called by _SessionTraceEnableCheckBox for checked changed events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        Private Sub _SessionTraceEnableCheckBox_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles _SessionTraceEnableCheckBox.CheckedChanged
            If Not Me.DesignMode AndAlso sender IsNot Nothing Then
                Dim checkBox As Windows.Forms.CheckBox = CType(sender, Windows.Forms.CheckBox)
                If checkBox.Enabled Then
                    Me.Device.SessionPropertyChangeHandlerEnabled = checkBox.Checked
                    If Me.Device.SessionPropertyChangeHandlerEnabled = checkBox.Checked Then
                        Me.Device.SessionMessagesTraceEnabled = checkBox.Checked
                    Else
                        Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed to toggle the session property handler")
                    End If
                End If
            End If
        End Sub

        ''' <summary> Event handler. Called by _EmulateButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _EmulateButton_Click(sender As System.Object, ByVal e As System.EventArgs) Handles _EmulateButton.Click
            Dim message As String = Me._EmulatedReplyComboBox.Text.Trim
            If Not String.IsNullOrWhiteSpace(message) Then
                Try
                    Me.ErrorProvider.Clear()
                    Me.Device.ProberSubsystem.LastReading = message
                    Me.Device.ProberSubsystem.ParseReading(message)
                Catch ex As Exception
                    Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred sending emulation message;. '{0}'. {1}",
                                               message, ex.ToFullBlownString)
                End Try
            End If
        End Sub

        ''' <summary> Event handler. Called by _WriteButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _WriteButton_Click(sender As System.Object, ByVal e As System.EventArgs) Handles _WriteButton.Click
            Dim c As Windows.Forms.Control = TryCast(sender, Windows.Forms.Control)
            If c IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me._CommandComboBox.Text) Then
                Dim message As String = Me._CommandComboBox.Text.Trim
                Try
                    Me.ErrorProvider.Clear()
                    Me.Cursor = Windows.Forms.Cursors.WaitCursor
                    Me.Device.ProberSubsystem.CommandTimeoutInterval = TimeSpan.FromMilliseconds(400)
                    If Me._CommandComboBox.Text.StartsWith(Me.Device.ProberSubsystem.TestCompleteCommand) Then
                        Me.Device.ProberSubsystem.CommandTimeoutInterval = TimeSpan.FromMilliseconds(1000)
                    End If
                    If Me.Device.ProberSubsystem.TrySend(message) Then
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Message sent;. Sent: '{0}'; Received: '{1}'.", Me.Device.ProberSubsystem.LastMessageSent, Me.Device.ProberSubsystem.LastReading)
                    Else
                        If Me.Device.ProberSubsystem.UnhandledMessageSent Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed sending message--unknown message sent;. Sent: {0}", message)
                            Me.ErrorProvider.SetError(c, "Failed sending message--unknown message sent.")
                        ElseIf Me.Device.ProberSubsystem.UnhandledMessageReceived Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed sending message--unknown message received;. Sent: {0}", message)
                            Me.ErrorProvider.SetError(c, "Failed sending message--unknown message received.")
                        Else
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed sending message;. Sent: {0}", message)
                            Me.ErrorProvider.SetError(c, "Failed sending message.")
                        End If
                    End If
                Catch ex As Exception
                    Me.ErrorProvider.SetError(c, ex.ToString)
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred sending message;. '{0}'. {1}", message, ex.ToFullBlownString)
                Finally
                    Me.Cursor = Windows.Forms.Cursors.Default
                End Try
            End If
        End Sub

#End Region

    End Class

End Namespace
