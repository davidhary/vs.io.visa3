Imports System.ComponentModel
Imports isr.Core.Controls.ControlExtensions
Imports isr.Core.Controls.ComboBoxExtensions
Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.EnumExtensions
Imports isr.Core.Agnostic.EscapeSequencesExtensions
Imports isr.Core.Agnostic.ExceptionExtensions
Namespace T1750

    ''' <summary> Provides a user interface for the a Tegam 1750 Resistance Measuring System. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="10/7/2013" by="David" revision=""> Created. </history>
    <System.ComponentModel.DisplayName("T1750 Panel"),
      System.ComponentModel.Description("Tegam 1750 Device Panel"),
      System.Drawing.ToolboxBitmap(GetType(T1750.T1750Panel))>
    Public Class T1750Panel
        #Const designMode1 = True
#Region " BASE FROM WRAPPER "
        ' Designing requires changing the condition to True.
#If designMode Then
        Inherits Visa.Instrument.ResourcePanelBaseWrapper
#Else
        Inherits Visa.Instrument.ResourcePanelBase
#End If
#End Region

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Default constructor. </summary>
        Public Sub New()
            MyBase.New()

            ' This call is required by the Windows Form Designer.
            InitializeComponent()

            ' instantiate the reference to the Device
            Me.Device = New T1750.Device()

            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            Me.initializeUserInterface()
            Me.onMeasurementAvailable(Nothing)

#If designMode Then
        Debug.Assert(False, "Illegal call; reset design mode")
#End If

        End Sub

        ''' <summary> Disposes managed resources. </summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub OnDisposeManagedResources()
            If Me.Device IsNot Nothing Then
                Try
                    ' remove event handlers.
                    Me.DeviceClosing(Me, System.EventArgs.Empty)
                    Me._Device.Dispose()
                    Me._Device = Nothing
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, "Exception occurred disposing Prober panel resources", "Exception {0}", ex.ToFullBlownString)
                End Try
            End If
        End Sub

        Private Sub EnableControls(ByVal control As Windows.Forms.Control, ByVal value As Boolean)
            If control IsNot Nothing Then
                control.Enabled = value
                If control.Controls IsNot Nothing AndAlso control.Controls.Count > 0 Then
                    For Each c As Windows.Forms.Control In control.Controls
                        Me.enableControls(c, value)
                    Next
                End If
            End If
        End Sub

        ''' <summary> Enables or disables controls based on the device open state. </summary>
        ''' <param name="deviceIsOpen"> <c>true</c> if device is open. </param>
        Private Sub OnDisplayDeviceOpenChanged(ByVal deviceIsOpen As Boolean)
            Me._Tabs.Enabled = True
            For Each t As Windows.Forms.TabPage In Me._Tabs.TabPages
                If t IsNot Me._MessagesTabPage Then
                    For Each c As Windows.Forms.Control In t.Controls : c.RecursivelyEnable(deviceIsOpen) : Next
                End If
            Next
        End Sub

        ''' <summary> Initialize User interface. </summary>
        Private Sub InitializeUserInterface()

            ' populate the supported commands. 
            With Me._RangeComboBox
                .DataSource = Nothing
                .Items.Clear()
                .DataSource = GetType(RangeMode).ValueDescriptionPairs
                .SelectedIndex = 0
                .ValueMember = "Key"
                .DisplayMember = "Value"
            End With

            ' populate the emulated reply combo.
            With Me._TriggerCombo
                .DataSource = Nothing
                .Items.Clear()
                .DataSource = GetType(TriggerMode).ValueDescriptionPairs
                .SelectedIndex = 0
                .ValueMember = "Key"
                .DisplayMember = "Value"
            End With

            Me.StatusRegisterToolStripStatusLabel.Visible = True
            Me.StandardRegisterToolStripStatusLabel.Visible = False
            Me.IdentityToolStripStatusLabel.Visible = False

        End Sub

#End Region

#Region " DEVICE "

        Private _Device As T1750.Device

        ''' <summary> Gets or sets a reference to the T1750 Device. </summary>
        ''' <value> The device. </value>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
        Public Overloads Property Device() As T1750.Device
            Get
                Return Me._Device
            End Get
            Friend Set(ByVal value As T1750.Device)
                If value IsNot Nothing Then
                    Me._Device = value
                    MyBase.Device = value
                End If
            End Set
        End Property

#End Region

#Region " DEVICE EVENT HANDLERS "

        ''' <summary> Handle the device property changed event. </summary>
        ''' <param name="device">    The device. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnDevicePropertyChanged(ByVal device As Device, ByVal propertyName As String)
            If device Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "Enabled"
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Hardware {0};. ",
                                                   IIf(device.Enabled, "Enabled", "Disabled"))
                    Case "ResourcesSearchPattern"
                        MyBase.Connector.ResourcesSearchPattern = device.ResourcesSearchPattern
                    Case "ServiceRequestFailureMessage"
                        If Not String.IsNullOrWhiteSpace(device.ServiceRequestFailureMessage) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, device.ServiceRequestFailureMessage)
                        End If
                End Select
            End If
        End Sub

        ''' <summary> Device property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Protected Overrides Sub DevicePropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnDevicePropertyChanged(TryCast(sender, Device), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling property changed Event;. Failed property {0}. {1}",
                                           e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub

        ''' <summary> Device service requested. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Message based session event information. </param>
        Protected Overrides Sub DeviceServiceRequested(ByVal sender As Object, ByVal e As NationalInstruments.VisaNS.MessageBasedSessionEventArgs)
            MyBase.DeviceServiceRequested(sender, e)
        End Sub

        ''' <summary> Event handler. Called upon device opening. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceOpening(ByVal sender As Object, ByVal e As System.EventArgs)
            MyBase.DeviceOpening(sender, e)
        End Sub

        ''' <summary> Event handler. Called when device opened. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)

            AddHandler Me.Device.MeasureSubsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
            AddHandler Me.Device.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
            AddHandler Me.Device.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            MyBase.DeviceOpened(sender, e)
            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            ' moved to the calling application
            ' Me.Device.InitializeKnownState()
        End Sub


        ''' <summary> Event handler. Called when device is closing. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceClosing(ByVal sender As Object, ByVal e As System.EventArgs)
            MyBase.DeviceClosing(sender, e)
            If Me.IsDeviceOpen Then
                RemoveHandler Me.Device.MeasureSubsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
                RemoveHandler Me.Device.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
                RemoveHandler Me.Device.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            End If
        End Sub

        ''' <summary> Event handler. Called when device is closed. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        Protected Overrides Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
            Me.onDisplayDeviceOpenChanged(Me.IsDeviceOpen)
            MyBase.DeviceClosed(sender, e)
        End Sub

#End Region

#Region " SUBSYSTEMS "

#Region " MEASURE "

        ''' <summary> handles the last reading available action. </summary>
        Private Sub OnLastReadingAvailable(ByVal subsystem As MeasureSubsystem)
            If subsystem Is Nothing Then
                Me.onMeasurementAvailable(subsystem)
            Else
                Me._ReadingToolStripStatusLabel.Text = subsystem.LastReading
                Me._ComplianceToolStripStatusLabel.Text = "  "
                Me._TbdToolStripStatusLabel.Text = "  "
                Windows.Forms.Application.DoEvents()
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Measure message: {0}.",
                                           subsystem.LastReading.InsertCommonEscapeSequences)
            End If
        End Sub

        ''' <summary> Handles the measurement available action. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub OnMeasurementAvailable(ByVal subsystem As MeasureSubsystem)
            If subsystem Is Nothing Then
                Me._ReadingToolStripStatusLabel.Text = "-.---- Ohm"
                Me._ComplianceToolStripStatusLabel.Text = "  "
                Me._TbdToolStripStatusLabel.Text = "  "
            ElseIf Me.Device.MeasureSubsystem.MeasurementAvailable AndAlso Me._ReadContinuouslyCheckBox.Checked Then
                Windows.Forms.Application.DoEvents()
                Me.Device.StatusSubsystem.ReadRegisters()
                Windows.Forms.Application.DoEvents()
                If Not Me.Device.StatusSubsystem.ErrorAvailable Then
                    If Me._PostReadingDelayNumeric.Value > 0 Then
                        Dim startTime As DateTime = DateTime.Now
                        Do
                            Windows.Forms.Application.DoEvents()
                        Loop Until DateTime.Now.Subtract(startTime).TotalMilliseconds > Me._PostReadingDelayNumeric.Value
                    End If
                    Windows.Forms.Application.DoEvents()
                    Me.Device.MeasureSubsystem.Read(False)
                End If
            ElseIf Me.Device.MeasureSubsystem.OverRangeOpenWire.GetValueOrDefault(False) Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Measurement over range or open wire detected;. ")
                Me._ReadingToolStripStatusLabel.Text = Me.Device.MeasureSubsystem.LastReading
                Me._ComplianceToolStripStatusLabel.Text = "O.R."
                Windows.Forms.Application.DoEvents()
            End If
        End Sub

        ''' <summary> Handles the measurement available action. </summary>
        <Obsolete("replaced with measurementAvailable(MeasureSubsystem)")>
        Private Sub OnMeasurementAvailable()
            If Me.Device.MeasureSubsystem.MeasurementAvailable AndAlso Me._ReadContinuouslyCheckBox.Checked Then
                Windows.Forms.Application.DoEvents()
                Me.Device.StatusSubsystem.ReadRegisters()
                Windows.Forms.Application.DoEvents()
                If Not Me.Device.StatusSubsystem.ErrorAvailable Then
                    If Me._PostReadingDelayNumeric.Value > 0 Then
                        Dim startTime As DateTime = DateTime.Now
                        Do
                            Windows.Forms.Application.DoEvents()
                        Loop Until DateTime.Now.Subtract(startTime).TotalMilliseconds > Me._PostReadingDelayNumeric.Value
                    End If
                    Windows.Forms.Application.DoEvents()
                    Me.Device.MeasureSubsystem.Read(False)
                End If
            End If
        End Sub

        ''' <summary> Executes the over range open wire action. </summary>
        <Obsolete("replaced with measurementAvailable(MeasureSubsystem)")>
        Private Sub OnOverRangeOpenWire()
            If Me.Device.MeasureSubsystem.OverRangeOpenWire.GetValueOrDefault(False) Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Measurement over range or open wire detected;. ")
                Me._ReadingToolStripStatusLabel.Text = Me.Device.MeasureSubsystem.LastReading
                Me._ComplianceToolStripStatusLabel.Text = "O.R."
                Windows.Forms.Application.DoEvents()
            End If
        End Sub

        ''' <summary> Handles the supported commands changed action. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub OnSupportedCommandsChanged(ByVal subsystem As MeasureSubsystem)
            If subsystem IsNot Nothing Then
                With Me._CommandComboBox
                    .DataSource = Nothing
                    .Items.Clear()
                    .DataSource = subsystem.SupportedCommands
                    .SelectedIndex = 0
                End With
            End If
        End Sub

        ''' <summary> Handles the visa status available action. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub OnVisaStatusAvailable(ByVal subsystem As MeasureSubsystem)
            If subsystem Is Nothing Then
            ElseIf subsystem.LastStatus > NationalInstruments.VisaNS.VisaStatusCode.Success Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Visa operation warning;. Visa Status: {0}",
                                               VisaException.BuildVisaStatusDetails(subsystem.LastStatus))
            ElseIf subsystem.LastStatus < NationalInstruments.VisaNS.VisaStatusCode.Success Then
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                           "Visa operation failed;. Visa Status: {0}",
                                           VisaException.BuildVisaStatusDetails(subsystem.LastStatus))
            End If
        End Sub

        ''' <summary> Updates the display of measurement settings. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub OnMeasureSettingsChanged(ByVal subsystem As MeasureSubsystem)
            If subsystem IsNot Nothing Then
                Me._measureSettingsLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                          "Trials: {0}; Initial Delay: {1} ms; Measurement Delay: {2} ms; Delta: {3:0.0%}",
                                                          subsystem.MaximumTrialsCount, subsystem.InitialDelay.TotalMilliseconds,
                                                          subsystem.MeasurementDelay.TotalMilliseconds, subsystem.MaximumDifference)
            End If
        End Sub

        ''' <summary> Handles the Measure subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        <CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As MeasureSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
            Select Case propertyName
                Case "MaximumTrialsCount", "InitialDelay", "MeasurementDelay", "MaximumDifference"
                    Me.onMeasureSettingsChanged(subsystem)
                Case "LastReading"
                    Me.onLastReadingAvailable(subsystem)
                Case "LastStatus"
                    Me.onVisaStatusAvailable(subsystem)
                Case "OverRangeOpenWire"
                    ' Me.onOverRangeOpenWire()
                    Me.onMeasurementAvailable(subsystem)
                Case "RangeMode"
                    If subsystem.RangeMode.HasValue Then
                        Me._RangeComboBox.SafeSilentSelectItem(subsystem.RangeMode.Value.Description)
                        Me._RangeComboBox.Refresh()
                    End If
                Case "Resistance"
                    If subsystem.Resistance.HasValue AndAlso Not subsystem?.OverRangeOpenWire.GetValueOrDefault(False) Then
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                                   "Parsed resistance value;. Resistance = {0}", subsystem.Resistance.Value)
                    End If
                Case "TriggerMode"
                    If subsystem.TriggerMode.HasValue Then
                        Me._TriggerCombo.SafeSilentSelectItem(subsystem.TriggerMode.Value.Description)
                        Me._TriggerCombo.Refresh()
                    End If
                Case "MeasurementAvailable"
                    ' Me.onMeasurementAvailable()
                    Me.onMeasurementAvailable(subsystem)
                Case "SupportedCommands"
                    Me.onSupportedCommandsChanged(subsystem)
            End Select
            Windows.Forms.Application.DoEvents()
        End Sub

        ''' <summary> Measure subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub MeasureSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, MeasureSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception handling property changed Event;. Failed property {0}. {1}",
                                           e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub

#End Region

#Region " STATUS "

        ''' <summary> Handle the Status subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As StatusSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "Identity"
                        If Not String.IsNullOrWhiteSpace(subsystem.Identity) Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                                       "{0} identified as {1}.", Me.ResourceName, subsystem.Identity)

                        End If
                    Case "MeasurementAvailable"
                        If Me.IsDeviceOpen AndAlso Me.Device.StatusSubsystem.MeasurementAvailable Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Measurement available;. ")
                        End If
                    Case "ErrorAvailable"
                        If Me.IsDeviceOpen AndAlso Me.Device.StatusSubsystem.ErrorAvailable Then
                            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Error available;. ")
                        End If
                        Me.Device.SystemSubsystem.QueryLastError()
                    Case "ServiceRequestStatus"
                        Me.DisplayStatusRegisterStatus(Me.Device.StatusSubsystem.ServiceRequestStatus)
                End Select
            End If
        End Sub

        ''' <summary> Status subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub StatusSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, StatusSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception handling property changed Event;. Failed property {0}. {1}",
                                           e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub

#End Region

#Region " SYSTEM "

        ''' <summary> Reports the last error. </summary>
        ''' <param name="subsystem"> The subsystem. </param>
        Private Sub OnLastError(ByVal subsystem As SystemSubsystem)
            If subsystem Is Nothing Then
                Throw New ArgumentNullException("subsystem")
            End If
            If Me.IsDeviceOpen AndAlso subsystem.LastError IsNot Nothing Then
                If subsystem.LastError.ErrorNumber = 0 Then
                    Me._LastErrorTextBox.ForeColor = Drawing.Color.Aquamarine
                Else
                    Me._LastErrorTextBox.ForeColor = Drawing.Color.OrangeRed
                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Error;. Last error: {0}",
                                               subsystem.LastError.CompoundErrorMessage)
                End If
                Me._LastErrorTextBox.Text = subsystem.LastError.CompoundErrorMessage
            Else
                Me._LastErrorTextBox.ForeColor = Drawing.Color.Aquamarine
                Me._LastErrorTextBox.Text = ""
            End If

        End Sub

        ''' <summary> Handle the System subsystem property changed event. </summary>
        ''' <param name="subsystem">    The subsystem. </param>
        ''' <param name="propertyName"> Name of the property. </param>
        Private Sub OnSubsystemPropertyChanged(ByVal subsystem As SystemSubsystem, ByVal propertyName As String)
            If subsystem Is Nothing OrElse propertyName Is Nothing Then
                Return
            Else
                Select Case propertyName
                    Case "LastErrorReading"
                    Case "LastError"
                        Me.onLastError(subsystem)
                End Select
            End If
        End Sub

        ''' <summary> System subsystem property changed. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Property Changed event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
            Try
                If sender IsNot Nothing AndAlso e IsNot Nothing Then
                    Me.OnSubsystemPropertyChanged(TryCast(sender, SystemSubsystem), e.PropertyName)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception handling property changed Event;. Failed property {0}. {1}",
                                           e.PropertyName, ex.ToFullBlownString)
            End Try
        End Sub
#End Region

#End Region

#Region " DISPLAY: TITLE "

        ''' <summary> Gets or sets the title. </summary>
        ''' <value> The title. </value>
        <Category("Appearance"), Description("The title of this panel"),
            Browsable(True),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Visible),
            DefaultValue("T1750")>
        Public Property Title As String
            Get
                Return Me._TitleLabel.Text
            End Get
            Set(value As String)
                Me._TitleLabel.Text = value
                Me._TitleLabel.Visible = Not String.IsNullOrWhiteSpace(value)
            End Set
        End Property

#End Region

#Region " TRACE MESSAGES HANDLERS "

        ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
        ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        Protected Overrides Sub DisplayMessage(value As TraceMessage)
            If value IsNot Nothing AndAlso MyBase.ShouldShow(value.EventType) Then
                Me._MessagesBox.AddMessage(value)
            End If
        End Sub

        ''' <summary> Gets or sets the trace display level. </summary>
        ''' <value> The trace display level. </value>
        Public Property TraceDisplayLevel As TraceEventType = TraceEventType.Verbose

        Protected Overrides ReadOnly Property TraceShowLevel As System.Diagnostics.TraceEventType
            Get
                Return Me.TraceDisplayLevel
            End Get
        End Property

#End Region

#Region " CONTROL EVENT HANDLERS: RESET "

        ''' <summary> Event handler. Called by _SessionTraceEnableCheckBox for checked changed events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        Private Sub _SessionTraceEnableCheckBox_CheckedChanged(ByVal sender As Object, e As System.EventArgs) Handles _SessionTraceEnableCheckBox.CheckedChanged
            If Not Me.DesignMode AndAlso sender IsNot Nothing Then
                Dim checkBox As Windows.Forms.CheckBox = CType(sender, Windows.Forms.CheckBox)
                If checkBox.Enabled Then
                    Me.Device.SessionPropertyChangeHandlerEnabled = checkBox.Checked
                    If Me.Device.SessionPropertyChangeHandlerEnabled = checkBox.Checked Then
                        Me.Device.SessionMessagesTraceEnabled = checkBox.Checked
                    Else
                        Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed to toggle the session property handler")
                    End If
                End If
            End If
        End Sub

        ''' <summary> Event handler. Called by interfaceClearButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _InterfaceClearButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InterfaceClearButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen AndAlso Me.Device.IsSessionOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Clearing interface '{0}';. ", Me.Device.Session.InterfaceResourceName)
                    Me.Device.SystemSubsystem.ClearInterface()
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred clearing interface;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by _SelectiveDeviceClearButton for click events. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _SelectiveDeviceClearButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SelectiveDeviceClearButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen AndAlso Me.Device.IsSessionOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "'{0}' Clearing Device '{1}';. ",
                                                Me.Device.Session.InterfaceResourceName, Me.ResourceName)
                    Me.Device.SystemSubsystem.ClearDevice()
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred sending SDC;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Issue RST. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ResetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ResetButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Resetting known state for '{0}';. ", Me.ResourceName)
                    Me.Device.ResetKnownState()
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred resetting known state;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by _InitializeKnownStateButton for click events. </summary>
        ''' <param name="sender"> <see cref="System.Object"/> instance of this
        ''' <see cref="System.Windows.Forms.Control"/> </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _InitializeKnownStateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InitializeKnownStateButton.Click
            Try
                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                Me.ErrorProvider.Clear()
                If Me.IsDeviceOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Initializing known state for '{0}';. ", Me.ResourceName)
                    Me.Device.InitializeKnownState()
                End If
            Catch ex As Exception
                Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred initializing known state;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = System.Windows.Forms.Cursors.Default
            End Try
        End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

        ''' <summary>Initiates a reading for retrieval by way of the service request event.</summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ReadSRQButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ReadSRQButton.Click
            Try
                Me.Cursor = Windows.Forms.Cursors.WaitCursor
                If Me.IsDeviceOpen Then
                    Me.Device.StatusSubsystem.ReadRegisters()
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception reading registers;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary>Selects a new reading to display.</summary>
        Private Sub _RangeComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _RangeComboBox.SelectedIndexChanged
            If Me.IsDeviceOpen Then
                If Me._RangeComboBox.Enabled AndAlso Me._RangeComboBox.SelectedIndex >= 0 AndAlso
                    Not String.IsNullOrWhiteSpace(Me._RangeComboBox.Text) Then
                End If
            End If
        End Sub

        ''' <summary>Query the Device for a reading.</summary>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub ReadButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ReadButton.Click
            Try
                Me.Cursor = Windows.Forms.Cursors.WaitCursor
                If Me.IsDeviceOpen Then
                    ' update display modalities if changed.
                    Me.Device.MeasureSubsystem.Read(False)
                End If
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception reading;. {0}", ex.ToFullBlownString)
            Finally
                Me.Cursor = Windows.Forms.Cursors.Default
            End Try
        End Sub

        ''' <summary> Event handler. Called by _ConfigureButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _ConfigureButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ConfigureButton.Click
            If Me.IsDeviceOpen Then
                Dim range As RangeMode = RangeMode.R0
                Dim trigger As TriggerMode = TriggerMode.T0
                Try
                    Me.ErrorProvider.Clear()
                    Me.Cursor = Windows.Forms.Cursors.WaitCursor
                    range = CType([Enum].Parse(GetType(RangeMode), Me._RangeComboBox.SelectedValue.ToString), RangeMode)
                    Me.Device.MeasureSubsystem.ApplyRangeMode(range)
                    trigger = CType([Enum].Parse(GetType(TriggerMode), Me._TriggerCombo.SelectedValue.ToString), TriggerMode)
                    Me.Device.MeasureSubsystem.ApplyTriggerMode(trigger)
                    Me.Device.StatusSubsystem.ReadRegisters()
                Catch ex As Exception
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception configuring;. Range {0} Trigger {1}. {2}",
                                               range, trigger, ex.ToFullBlownString)
                    Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToFullBlownString)
                Finally
                    Me.Cursor = Windows.Forms.Cursors.Default
                End Try
            End If
        End Sub

        ''' <summary> Event handler. Called by _WriteButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _WriteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _WriteButton.Click
            If Not String.IsNullOrWhiteSpace(Me._CommandComboBox.Text) Then
                Dim dataToWrite As String = Me._CommandComboBox.Text.Trim
                Try
                    Me.ErrorProvider.Clear()
                    Me.Cursor = Windows.Forms.Cursors.WaitCursor
                    If Me.IsSessionOpen Then
                        If dataToWrite.StartsWith("U", StringComparison.OrdinalIgnoreCase) Then
                            Me.Device.MeasureSubsystem.LastReading = Me.Device.Session.QueryTrimEnd(dataToWrite)
                        Else
                            Me.Device.Session.WriteLine(dataToWrite)
                        End If
                        Me.Device.StatusSubsystem.ReadRegisters()
                    End If
                Catch ex As Exception
                    Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred sending message;. '{0}'. {1}", dataToWrite, ex.ToFullBlownString)
                Finally
                    Me.Cursor = Windows.Forms.Cursors.Default
                End Try
            End If
        End Sub

        ''' <summary> Event handler. Called by _MeasureButton for click events. </summary>
        ''' <param name="sender"> Source of the event. </param>
        ''' <param name="e">      Event information. </param>
        <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Sub _MeasureButton_Click(sender As System.Object, e As System.EventArgs) Handles _MeasureButton.Click
            If Not String.IsNullOrWhiteSpace(Me._CommandComboBox.Text) Then
                Try
                    Me.ErrorProvider.Clear()
                    Me.Cursor = Windows.Forms.Cursors.WaitCursor
                    If Me.IsSessionOpen Then
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Started measuring;. ")
                        Me.Device.MeasureSubsystem.Measure()
                    End If
                Catch ex As Exception
                    Me.ErrorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
                    Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred measuring;. ")
                Finally
                    Me.Cursor = Windows.Forms.Cursors.Default
                End Try
            End If

        End Sub

#End Region

    End Class

End Namespace
