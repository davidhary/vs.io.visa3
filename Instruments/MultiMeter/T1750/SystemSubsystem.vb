Imports NationalInstruments
Namespace T1750

    ''' <summary> Defines a System Subsystem for a Tegam 1750 Resistance Measuring System. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="10/7/2013" by="David" revision=""> Created. </history>
    Public Class SystemSubsystem
        Inherits Visa.R2D2.SystemSubsystemBase

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
        ''' <remarks> Sets termination character to line feed. </remarks>
        Public Overrides Sub InitializeKnownState()
            MyBase.InitializeKnownState()
            If Me.IsSessionOpen Then
                Me.Session.WriteLine("Y3x")
            End If
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.SafePostPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the clear error queue command. </summary>
        ''' <value> The clear error queue command. </value>
        Protected Overrides ReadOnly Property ClearErrorQueueCommand As String
            Get
                Return ""
            End Get
        End Property

        ''' <summary> Gets the initialize memory command. </summary>
        ''' <value> The initialize memory command. </value>
        Protected Overrides ReadOnly Property InitializeMemoryCommand As String
            Get
                Return ""
            End Get
        End Property

        ''' <summary> Gets the last error query command. </summary>
        ''' <value> The last error query command. </value>
        Protected Overrides ReadOnly Property LastErrorQueryCommand As String
            Get
                Return "U1x"
            End Get
        End Property

        ''' <summary> Gets line frequency query command. </summary>
        ''' <value> The line frequency query command. </value>
        Protected Overrides ReadOnly Property LineFrequencyQueryCommand As String
            Get
                Return ""
            End Get
        End Property

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return Visa.Scpi.Syntax.SystemPresetCommand
            End Get
        End Property

#End Region

#Region " LAST ERROR "

        ''' <summary> Parses the last error. </summary>
        ''' <param name="value">     The value. </param>
        Public Overrides Sub ParseLastError(value As String)
            Me.LastError = New DeviceError(value)
        End Sub

#End Region

    End Class

End Namespace
