Namespace K2000

    ''' <summary> Defines a Trace Subsystem for a Keithley 2000 instrument. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    Public Class TraceSubsystem
        Inherits Visa.Scpi.TraceSubsystemBase

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="TraceSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
        Public Overrides Sub ResetKnownState()
            MyBase.ResetKnownState()
            Me.PointsCount = 100
            Me.FeedSource = Visa.Scpi.FeedSource.Calculate1
            Me.FeedControl = Visa.Scpi.FeedControl.Never
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.SafePostPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return ""
            End Get
        End Property

        ''' <summary> Gets the initiate command. </summary>
        ''' <value> The initiate command. </value>
        Protected Overrides ReadOnly Property InitiateCommand As String
            Get
                Return ":INIT"
            End Get
        End Property

        ''' <summary> Gets the Abort command. </summary>
        ''' <value> The Abort command. </value>
        Protected Overrides ReadOnly Property AbortCommand As String
            Get
                Return ":ABOR"
            End Get
        End Property

        ''' <summary> Gets the points count query command. </summary>
        ''' <value> The points count query command. </value>
        Protected Overrides ReadOnly Property PointsCountQueryCommand As String
            Get
                Return ":TRAC:POIN:COUN?"
            End Get
        End Property

        ''' <summary> Gets the points count command format. </summary>
        ''' <value> The points count command format. </value>
        Protected Overrides ReadOnly Property PointsCountCommandFormat As String
            Get
                Return ":TRAC:POIN:COUN {0}"
            End Get
        End Property

        ''' <summary> Gets the Feed Control command. </summary>
        ''' <value> The Feed Control command. </value>
        Protected Overrides ReadOnly Property FeedControlCommand As Command
            Get
                Static cmd As Command
                If cmd Is Nothing Then
                    cmd = New Command(":TRAC:FEED:CONTROL", CommandFormats.Standard And CommandFormats.QuerySupported And CommandFormats.WriteSupported)
                End If
                Return cmd
            End Get
        End Property

        ''' <summary> Gets the Feed Source command. </summary>
        ''' <value> The Feed Source command. </value>
        Protected Overrides ReadOnly Property FeedSourceCommand As Command
            Get
                Static cmd As Command
                If cmd Is Nothing Then
                    cmd = New Command(":TRAC:FEED", CommandFormats.Standard And CommandFormats.QuerySupported And CommandFormats.WriteSupported)
                End If
                Return cmd
            End Get
        End Property

#End Region

    End Class

End Namespace
