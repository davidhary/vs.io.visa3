Imports isr.Core.Agnostic.TrimExtensions
Namespace K2000

    ''' <summary> Defines a SCPI Sense Subsystem for a Keithley 2000 instrument. </summary>
    ''' <license> (c) 2013 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/22/2013" by="David" revision="3.0.5013"> Created. </history>
    Public Class SenseSubsystem
        Inherits Visa.Scpi.SenseSubsystemBase

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
            Me._readings = New Readings
            Me.Readings.Elements = ReadingElements.Reading Or ReadingElements.Units Or
                                   ReadingElements.ReadingNumber Or ReadingElements.Timestamp
            Me.SupportedFunctionModes = Visa.Scpi.SenseFunctionModes.CurrentDC Or
                                        Visa.Scpi.SenseFunctionModes.VoltageDC Or
                                        Visa.Scpi.SenseFunctionModes.Resistance Or
                                        Visa.Scpi.SenseFunctionModes.FourWireResistance
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
        Public Overrides Sub ResetKnownState()
            MyBase.ResetKnownState()
            Me.FunctionMode = Visa.Scpi.SenseFunctionModes.VoltageDC
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.SafePostPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return ""
            End Get
        End Property

#Region " LATEST DATA "

        ''' <summary> Gets the latest data query command. </summary>
        ''' <value> The latest data query command. </value>
        Protected Overrides ReadOnly Property LatestDataQueryCommand As String
            Get
                Return ":SENSE:DATA:LAT?"
            End Get
        End Property

#End Region

#End Region

#Region " LATEST DATA "

        Private _Readings As Readings

        ''' <summary> Returns the readings. </summary>
        ''' <returns> The readings. </returns>
        Public Function Readings() As Readings
            Return Me._readings
        End Function

        ''' <summary> Parses a new set of reading elements. </summary>
        ''' <param name="reading"> Specifies the measurement text to parse into the new reading. </param>
        Public Overrides Sub ParseReading(ByVal reading As String)

            ' check if we have units suffixes.
            If (Me._readings.Elements And isr.IO.Visa.ReadingElements.Units) <> 0 Then
                reading = reading.TrimUnits
            End If

            ' Take a reading and parse the results
            Me.Readings.TryParse(reading)

        End Sub

#End Region

    End Class

End Namespace
