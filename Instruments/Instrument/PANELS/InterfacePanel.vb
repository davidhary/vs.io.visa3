Imports System.ComponentModel
Imports NationalInstruments
Imports isr.IO.Visa.GpibInterfaceExtensions
Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.ExceptionExtensions
''' <summary> Provides a user interface for a
''' <see cref="NationalInstruments.VisaNS.GpibInterface">GPIB interface</see>
''' such as a VISA Interface. </summary>
''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/04/2013" by="David" revision="3.0.4955"> created based on the legacy
''' interface panel. </history>
<System.ComponentModel.Description("GPIB Interface Panel")>
<System.Drawing.ToolboxBitmap(GetType(InterfacePanel), "InterfacePanel"), ToolboxItem(True)>
Public Class InterfacePanel
#Const designMode1 = True
#Region " BASE FROM WRAPPER "
    ' Designing requires changing the condition to True.
#If designMode Then
    Inherits TracePublisherControlBaseWrapper
#Else
    Inherits TracePublisherControlBase
#End If
#End Region

    Implements ITraceMessageObserver

#Region " CONSTRUCTION + CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

        ' disable until the connectible interface is set
        Me.Enabled = False
        Me._InterfaceChooser.Connectible = True
        Me._InterfaceChooser.Clearable = True
        Me._InterfaceChooser.Searchable = True
        AddHandler Me._InterfaceChooser.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable

        Me._InstrumentChooser.Connectible = True
        Me._InstrumentChooser.Clearable = True
        Me._InstrumentChooser.Searchable = True
        AddHandler Me._InstrumentChooser.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable

#If designMode Then
        Debug.Assert(False, "Illegal call; reset design mode")
#End If

    End Sub

    ''' <summary> Executes the dispose managed resources action. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub OnDisposeManagedResources()

        Try
            RemoveHandler Me._InterfaceChooser.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try
        Try
            RemoveHandler Me._InstrumentChooser.TraceMessageAvailable, AddressOf Me.OnTraceMessageAvailable
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        End Try

        ' Traps the VISA error because the interface might be disposed and the dispose sentinel is not exposed by the interface.
        Try
            If Me.VisaInterface IsNot Nothing Then
                Me.VisaInterface.Dispose()
                Me._VisaInterface = Nothing
            End If
        Catch ex As ObjectDisposedException
        End Try

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Event handler. Called by form for load events. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me._StatusToolStripStatusLabel.Text = "Find and select an interface."
    End Sub

#End Region

#Region " INTERFACE PROPERTIES AND METHODS "

    ''' <summary> Gets a value indicating whether the interface is open. </summary>
    ''' <value> <c>True</c> if the interface session is defined; otherwise, <c>False</c>. </value>
    Public ReadOnly Property IsInterfaceOpen As Boolean
        Get
            Return Me.VisaInterface IsNot Nothing
        End Get
    End Property

    ''' <summary> Try open session. </summary>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <c>True</c> if open; <c>False</c> otherwise. </returns>
    Public Overridable Function TryOpenInterfaceSession(ByVal resourceName As String) As Boolean
        Try
            Me.OpenInterfaceSession(resourceName)
        Catch ex As OperationFailedException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                       "Exception occurred opening interface;. {0}", ex.ToFullBlownString)
            Return False
        End Try
        Return Me.IsInterfaceOpen
    End Function

    ''' <summary> Opens the interface session. </summary>
    ''' <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="resourceName"> Name of the resource. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overridable Sub OpenInterfaceSession(ByVal resourceName As String)
        If Me.Enabled Then
            Try
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Opening interface to {0};. ", resourceName)
                Me.VisaInterface = New VisaNS.GpibInterface(resourceName)

                If Me.IsInterfaceOpen Then
                    Dim lastStatus As VisaNS.VisaStatusCode = Me.VisaInterface.LastStatus
                    If lastStatus >= VisaNS.VisaStatusCode.Success Then
                        ' connect the connector.
                        Me._InterfaceChooser.IsConnected = True
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Interface '{0}' opened;. ", resourceName)
                        Me.displayResourceNames()
                    Else
                        Me.TryCloseInterfaceSession()
                        Throw New OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                         "Failed opening interface '{0}'; VISA status: '{1}'", resourceName,
                                                                         VisaException.BuildVisaStatusDetails(lastStatus)))
                    End If
                Else
                    Me.TryCloseInterfaceSession()
                    Throw New OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                   "Failed opening interface '{0}'.", resourceName))
                End If

            Catch ex As OperationFailedException
                Throw
            Catch ex As VisaException
                Me.TryCloseInterfaceSession()
                Throw New OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                 "VISA exception occurred opening interface '{0}'.", resourceName),
                                                             ex)
            Catch ex As Exception
                Me.TryCloseInterfaceSession()
                Throw New OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                 "Exception occurred opening interface '{0}'.", resourceName),
                                                             ex)
            Finally
                With Me._InterfaceChooser
                    .Enabled = Me.IsInterfaceOpen
                    .IsConnected = Me.IsInterfaceOpen
                    .Visible = True
                    .Invalidate()
                End With
                With Me._InstrumentChooser
                    .Enabled = Me.IsInterfaceOpen
                    .Visible = True
                    .Invalidate()
                End With
                With Me._ClearSelectedResourceButton
                    .Enabled = Me.IsInterfaceOpen AndAlso Me._InstrumentChooser.HasResources AndAlso
                        Not String.IsNullOrWhiteSpace(Me._InstrumentChooser.SelectedResourceName)
                    .Visible = True
                    .Invalidate()
                End With
                With Me._ClearAllResourcesButton
                    .Enabled = Me.IsInterfaceOpen AndAlso Me._InstrumentChooser.HasResources
                    .Visible = True
                    .Invalidate()
                End With
                Me.Cursor = System.Windows.Forms.Cursors.Default
                Me.SafePostPropertyChanged("IsOpen")
            End Try
        End If
    End Sub

    ''' <summary> Try close session. </summary>
    ''' <returns> <c>True</c> if session closed; otherwise <c>False</c>. </returns>
    Public Overridable Function TryCloseInterfaceSession() As Boolean
        Try
            Me.CloseInterfaceSession()
        Catch ex As OperationFailedException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred closing interface;. {0}", ex.ToFullBlownString)
            Return False
        End Try
        Return Not Me.IsInterfaceOpen
    End Function

    ''' <summary> Closes the session. </summary>
    ''' <exception cref="OperationFailedException"> Thrown when operation failed to execute. </exception>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Public Overridable Sub CloseInterfaceSession()
        If Me.Enabled Then
            If Me.VisaInterface IsNot Nothing Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Closing interface to {0};. ",
                                           Me._InterfaceChooser.SelectedResourceName)
                Try
                    Me.VisaInterface.Dispose()
                    Try
                        ' Trying to null the session raises an ObjectDisposedException 
                        ' if session service request handler was not released. 
                        Me.VisaInterface = Nothing
                    Catch ex As ObjectDisposedException
                        Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                    End Try
                Catch ex As VisaException
                    Throw New OperationFailedException("VISA Exception occurred closing the interface.", ex)
                Catch ex As Exception
                    Throw New OperationFailedException("Exception occurred closing the interface.", ex)
                Finally
                    With Me._InterfaceChooser
                        .IsConnected = Me.IsInterfaceOpen
                    End With
                    With Me._InstrumentChooser
                        .IsConnected = Me.IsInterfaceOpen
                        .Enabled = False
                        .Visible = True
                        .Invalidate()
                    End With
                    With Me._ClearSelectedResourceButton
                        .Enabled = False
                        .Visible = True
                        .Invalidate()
                    End With
                    With Me._ClearAllResourcesButton
                        .Enabled = False
                        .Visible = True
                        .Invalidate()
                    End With
                    Me._InterfaceChooser.IsConnected = False
                    Me.SafePostPropertyChanged("IsOpen")
                End Try
            End If
        End If
    End Sub

    ''' <summary> Gets or sets the visa interface. </summary>
    ''' <value> The visa interface. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property VisaInterface As NationalInstruments.VisaNS.GpibInterface

#End Region

#Region " INTERFACE CHOOSER EVENT HANDLERS "

    Private _InterfaceResourceName As String

    ''' <summary> Gets or sets the name of the interface resource. </summary>
    ''' <value> The name of the interface resource. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property InterfaceResourceName As String
        Get
            Return Me._InterfaceResourceName
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not value.Equals(Me.InterfaceResourceName) Then
                Me._InterfaceResourceName = value
                Me.SafePostPropertyChanged("InterfaceResourceName")
            End If
        End Set
    End Property

    ''' <summary> Display the interface names based on the last interface type. </summary>
    Public Sub DisplayInterfaceNames()
        Try
            ' display the selected resources.
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._InterfaceChooser.ResourcesSearchPattern = IO.Visa.ResourceManagerExtensions.InterfaceSearchPattern()
            Me._InterfaceChooser.DisplayResourceNames()
            If Me._InterfaceChooser.HasResources Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "Interfaces available--select and connect;. Found interfaces.")
            Else
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "NO INTERFACES;. No interfaces were found. Connect the interface(s) and click Find.")
            End If
            Me.displayResourceNames()
            Me.Enabled = True
        Catch ex As System.ArgumentException
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "EXCEPTION FINDING INTERFACES;. Failed finding or listing interfaces. Connect the interface(s) and click Find. {0}", ex.ToFullBlownString)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Clears the interface by issuing an interface clear. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub InterfaceChooser_Clear(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InterfaceChooser.Clear
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If Me.IsInterfaceOpen Then
                Me.VisaInterface.SendInterfaceClear()
            End If
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred clearing;. {0}", ex.ToFullBlownString)
            Me._MessagesBox.AddMessage(ex)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

    ''' <summary> Connects the interface. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub InterfaceChooser_Connect(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InterfaceChooser.Connect
        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Connecting {0};. ", Me._InterfaceChooser.SelectedResourceName)
        Dim resourcename As String = Me._InterfaceChooser.SelectedResourceName
        Me.OpenInterfaceSession(resourcename)
    End Sub

    ''' <summary> Disconnects the interface. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    Private Sub InterfaceChooser_Disconnect(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InterfaceChooser.Disconnect
        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Disconnecting {0};. ", Me._InterfaceChooser.SelectedResourceName)
        Me.TryCloseInterfaceSession()
    End Sub

    ''' <summary> Displays available interface names. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    Private Sub InterfaceChooser_FindNames(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InterfaceChooser.FindNames
        Me.DisplayInterfaceNames()
    End Sub

    ''' <summary> Event handler. Called by _InterfaceChooser for property changed events. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _InterfaceChooser_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _InterfaceChooser.PropertyChanged
        If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me._InterfaceChooser_PropertyChanged), New Object() {sender, e})
        End If
        Try
            Select Case e.PropertyName
                Case "SelectedResourceName"
                    Me.InterfaceResourceName = Me._InterfaceChooser.SelectedResourceName
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Interface selected--connect;. Selected interface {0}", Me._InterfaceChooser.SelectedResourceName)
                Case "SelectedResourceExists"
                Case "ResourcesSearchPattern"
                Case "IsConnected"
                Case "Clearable"
                Case "Connectible"
                Case "Searchable"
                Case "HasResources"
                Case Else
                    ' Debug.Assert(Not Debugger.IsAttached, "Unhandled Property Name", "Unhandled Property Name {0}", e.PropertyName)
            End Select
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception handling property changed Event;. Failed property {0}. {1}",
                                       e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Event handler. Called by _clearSelectedResourceButton for click events. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ClearSelectedResourceButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ClearSelectedResourceButton.Click
        ' Transmit the SDC command to the interface.
        If Not String.IsNullOrWhiteSpace(Me._InstrumentChooser.SelectedResourceName) Then
            Try
                Me.Cursor = Windows.Forms.Cursors.WaitCursor
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "Clearing selected device;. Clearing '{0}'...", Me._InstrumentChooser.SelectedResourceName)
                If Me.VisaInterface IsNot Nothing Then
                    Me._VisaInterface.SelectiveDeviceClear(Me._InstrumentChooser.SelectedResourceName)
                End If
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "Cleared selected device;. Cleared '{0}'.", Me._InstrumentChooser.SelectedResourceName)
            Catch ex As Exception
                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                           "Exception occurred clearing '{0}';. {1}", Me._InstrumentChooser.SelectedResourceName, ex.ToFullBlownString)
            Finally
                Me.Cursor = Windows.Forms.Cursors.Default
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called by _clearAllResourcesButton for click events. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ClearAllResourcesButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ClearAllResourcesButton.Click
        Try
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "Clearing devices;. Clearing devices at '{0}'...", Me.InterfaceResourceName)
            Me._VisaInterface.ClearDevices()
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "Cleared devices;. Cleared devices at '{0}'.", Me.InterfaceResourceName)
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                       "Exception occurred clearing devices at '{0}';. {1}", Me.InterfaceResourceName, ex.ToFullBlownString)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " INSTRUMENT PROPERTIES AND METHODS "

    ''' <summary> Gets the instrument chooser. </summary>
    ''' <value> The instrument chooser. </value>
    Public ReadOnly Property InstrumentChooser As ResourceSelectorConnectorBase
        Get
            Return Me._InstrumentChooser
        End Get
    End Property

    ''' <summary> Displays instrument resource names. </summary>
    Private Sub DisplayResourceNames()
        If Me.VisaInterface Is Nothing Then
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "Finding resources;. Finding resources {0}", IO.Visa.ResourceManagerExtensions.InstrumentSearchPattern())
            Me._InstrumentChooser.ResourcesSearchPattern = IO.Visa.ResourceManagerExtensions.InstrumentSearchPattern()
        Else
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "Finding resources;. Finding resources for interface {0}", Me._InterfaceChooser.SelectedResourceName)
            Me._InstrumentChooser.ResourcesSearchPattern = IO.Visa.ResourceManagerExtensions.InstrumentSearchPattern(Me.VisaInterface.HardwareInterfaceType, Me.VisaInterface.HardwareInterfaceNumber)
        End If
        Me._InstrumentChooser.DisplayResourceNames()
        Me._InstrumentChooser.Enabled = Me._InstrumentChooser.HasResources
        If Me._InstrumentChooser.HasResources Then
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "Instruments available--select and connect;. Found Instruments.")
        Else
            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                       "No Instruments;. No Instruments were found. Connect the Instrument(s) and click Find.")
        End If
    End Sub

    ''' <summary> Displays resource names. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _InstrumentChooser_FindNames(ByVal sender As Object, ByVal e As System.EventArgs) Handles _InstrumentChooser.FindNames
        Me.displayResourceNames()
    End Sub

    ''' <summary> Event handler. Called by _ResourceChooser for property changed events. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _InstrumentChooser_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _InstrumentChooser.PropertyChanged
        If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me._InterfaceChooser_PropertyChanged), New Object() {sender, e})
        End If
        Try
            Select Case e.PropertyName
                Case "SelectedResourceName"
                    Me._ClearSelectedResourceButton.Enabled = Not String.IsNullOrWhiteSpace(Me._InstrumentChooser.SelectedResourceName)
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument resource selected--connect;. Selected resource {0}", Me._InstrumentChooser.SelectedResourceName)
                Case "SelectedResourceExists"
                Case "ResourcesSearchPattern"
                Case "IsConnected"
                Case "Clearable"
                Case "Connectible"
                Case "Searchable"
                Case "HasResources"
                    Me._ClearSelectedResourceButton.Enabled = Me._InstrumentChooser.HasResources AndAlso Not String.IsNullOrWhiteSpace(Me._InstrumentChooser.SelectedResourceName)
                    Me._ClearAllResourcesButton.Enabled = Me._InstrumentChooser.HasResources
                Case Else
                    ' Debug.Assert(Not Debugger.IsAttached, "Unhandled Property Name", "Unhandled Property Name {0}", e.PropertyName)
            End Select
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception handling property changed Event;. Failed property {0}. {1}",
                                       e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#Region " I TRACE MESSAGE OBSERVER "

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overrides Sub DisplayMessage(value As TraceMessage)
        If value IsNot Nothing AndAlso Me.ShouldShow(value.EventType) Then
            Me._MessagesBox.AddMessage(value)
        End If
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected Overrides Sub DisplaySynopsis(value As String)
        Me._StatusToolStripStatusLabel.Text = value
    End Sub

    ''' <summary> Observes the Trace event published by <see cref="ITraceMessagePublisher">trace publishers</see>. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="TraceMessageEventArgs" /> instance containing the event data. </param>
    Protected Overloads Sub OnTraceMessageAvailable(ByVal sender As Object, ByVal e As TraceMessageEventArgs) Implements ITraceMessageObserver.OnTraceMessageAvailable
        If sender IsNot Nothing AndAlso e IsNot Nothing Then
            Me.OnTraceMessageAvailable(e.TraceMessage)
        End If
    End Sub

    ''' <summary> Gets the trace display level. </summary>
    ''' <value> The trace display level. </value>
    Public Property TraceDisplayLevel As TraceEventType = TraceEventType.Verbose

    ''' <summary> Gets or sets the trace show level. The trace message is displayed if the trace level is lower
    ''' than this value. </summary>
    ''' <value> The trace show level. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected ReadOnly Property TraceShowLevel As Diagnostics.TraceEventType Implements ITraceMessageObserver.TraceShowLevel
        Get
            Return Me.TraceDisplayLevel
        End Get
    End Property

    ''' <summary> Determines if the trace event type should be displayed. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> if the trace event type should be displayed. </returns>
    Protected Function ShouldShow(ByVal eventType As TraceEventType) As Boolean Implements ITraceMessageObserver.ShouldShow
        Return eventType <= Me.TraceShowLevel
    End Function

#End Region

End Class

