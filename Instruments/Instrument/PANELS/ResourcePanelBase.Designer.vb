Imports System.Drawing
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ResourcePanelBase

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me.onDisposeManagedResources()
                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="StatusPanel")> <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="IdentityPanel")> <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Connector = New isr.IO.Visa.Instrument.ResourceSelectorConnectorWrapper()
        Me.TipsToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.StatusToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.IdentityToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusRegisterToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StandardRegisterToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.StatusStrip.SuspendLayout()
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Connector
        '
        Me.Connector.BackColor = System.Drawing.Color.Transparent
        Me.Connector.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Connector.Location = New System.Drawing.Point(0, 10)
        Me.Connector.Name = "Connector"
        Me.Connector.Size = New System.Drawing.Size(364, 40)
        Me.Connector.TabIndex = 14
        '
        'StatusStrip
        '
        Me.StatusStrip.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusToolStripStatusLabel, Me.IdentityToolStripStatusLabel, Me.StatusRegisterToolStripStatusLabel, Me.StandardRegisterToolStripStatusLabel})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 50)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Padding = New System.Windows.Forms.Padding(1, 0, 16, 0)
        Me.StatusStrip.ShowItemToolTips = True
        Me.StatusStrip.Size = New System.Drawing.Size(364, 22)
        Me.StatusStrip.TabIndex = 15
        Me.StatusStrip.Text = "StatusStrip1"
        '
        'StatusToolStripStatusLabel
        '
        Me.StatusToolStripStatusLabel.Font = New System.Drawing.Font(Me.Font, System.Drawing.FontStyle.Bold)
        Me.StatusToolStripStatusLabel.Name = "StatusToolStripStatusLabel"
        Me.StatusToolStripStatusLabel.Size = New System.Drawing.Size(245, 17)
        Me.StatusToolStripStatusLabel.Spring = True
        Me.StatusToolStripStatusLabel.Text = "<Status>"
        Me.StatusToolStripStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.StatusToolStripStatusLabel.ToolTipText = "Status"
        '
        'IdentityToolStripStatusLabel
        '
        Me.IdentityToolStripStatusLabel.Name = "IdentityToolStripStatusLabel"
        Me.IdentityToolStripStatusLabel.Size = New System.Drawing.Size(30, 17)
        Me.IdentityToolStripStatusLabel.Text = "<I>"
        Me.IdentityToolStripStatusLabel.ToolTipText = "Identity"
        '
        'StatusRegisterToolStripStatusLabel
        '
        Me.StatusRegisterToolStripStatusLabel.Name = "StatusRegisterToolStripStatusLabel"
        Me.StatusRegisterToolStripStatusLabel.Size = New System.Drawing.Size(36, 17)
        Me.StatusRegisterToolStripStatusLabel.Text = "0x00"
        Me.StatusRegisterToolStripStatusLabel.ToolTipText = "Status Register Value"
        '
        'StandardRegisterToolStripStatusLabel
        '
        Me.StandardRegisterToolStripStatusLabel.Name = "StandardRegisterToolStripStatusLabel"
        Me.StandardRegisterToolStripStatusLabel.Size = New System.Drawing.Size(36, 17)
        Me.StandardRegisterToolStripStatusLabel.Text = "0x00"
        Me.StandardRegisterToolStripStatusLabel.ToolTipText = "Standard Register Value"
        '
        'ErrorProvider
        '
        Me.ErrorProvider.ContainerControl = Me
        '
        'ResourcePanelBase
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.Connector)
        Me.Controls.Add(Me.StatusStrip)
        Me.Name = "ResourcePanelBase"
        Me.Size = New System.Drawing.Size(364, 72)
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Protected WithEvents Connector As ResourceSelectorConnectorBase
    Protected WithEvents TipsToolTip As System.Windows.Forms.ToolTip
    Protected WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Protected WithEvents StatusToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Protected WithEvents IdentityToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Protected WithEvents StatusRegisterToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Protected WithEvents StandardRegisterToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Protected WithEvents ErrorProvider As System.Windows.Forms.ErrorProvider

End Class
