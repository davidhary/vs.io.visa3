Namespace My

    ''' <summary> Defines a singleton class to provide project management for this project.
    ''' This class has the Public Shared Not Creatable instancing property. </summary>
    ''' <license> (c) 2006 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="06/13/2006" by="David" revision="1.0.2355.x"> Created. </history>
    Public NotInheritable Class MyLibrary

        ''' <summary> Identifier for the trace event. </summary>
        Public Const TraceEventId As Integer = Visa.My.ProjectTraceEventId.HandlerLibrary

        Public Const AssemblyTitle As String = "VISA SCPI Handler Library"
        Public Const AssemblyDescription As String = "Visa SCPI Handler Library"
        Public Const AssemblyProduct As String = "IO.Visa.SCPI.Handler.2019"

    End Class

End Namespace

