Imports NationalInstruments
Namespace Scpi

    ''' <summary> Defines the contract that must be implemented by a SCPI Sense Subsystem. </summary>
    ''' <license> (c) 2012 Integrated Scientific ReSenses, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    Public MustInherit Class SenseSubsystemBase
        Inherits SourceMeasure.SenseSubsystemBase

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="SenseSubsystemBase" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">status subsystem</see>. </param>
        Protected Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " CONCURRENT SENSE FUNCTION MODE "

        ''' <summary> Queries the Concurrent Function Mode Enabled sentinel. Also sets the 
        '''           <see cref="ConcurrentSenseEnabled">mode</see> sentinel. </summary>
        ''' <returns> <c>null</c> if mode is not known; <c>True</c> if concurrent; otherwise, <c>False</c>. </returns>
        Public Overrides Function QueryConcurrentSenseEnabled() As Boolean?
            If Me.IsSessionOpen Then
                Me.ConcurrentSenseEnabled = Me.Session.Query(True, ":SENS:FUNC:CONC?")
            End If
            Return Me.ConcurrentSenseEnabled
        End Function

        ''' <summary> Writes the Concurrent Function Mode  Enabled sentinel. Does not read back from the instrument. </summary>
        ''' <param name="value"> if set to <c>True</c> is concurrent. </param>
        ''' <returns> <c>null</c> if mode is not known; <c>True</c> if concurrent; otherwise, <c>False</c>. </returns>
        Public Overrides Function WriteConcurrentSenseEnabled(ByVal value As Boolean) As Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SENS:FUNC:CONC {0:'ON';'ON';'OFF'}", CType(value, Integer))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.ConcurrentSenseEnabled = New Boolean?
            Else
                Me.ConcurrentSenseEnabled = value
            End If
            Return Me.ConcurrentSenseEnabled
        End Function

#End Region

#Region " FUNCTION MODE "

        ''' <summary> Queries the Sense Function Modes. Also sets the <see cref="FunctionModes"></see> cached value. </summary>
        ''' <returns> The Sense Function Mode or null if unknown. </returns>
        Public Overrides Function QueryFunctionModes() As SenseFunctionModes?
            If Me.IsSessionOpen Then
                ' the K2700 expects single quotes when writing the value but sends back items delimited with double quotes.
                Me.FunctionModes = SenseSubsystemBase.ParseSenseFunctionModes(Me.Session.QueryTrimEnd(":SENS:FUNC?").Replace(CChar(""""), "'"c))
            End If
            Return Me.FunctionModes
        End Function

        ''' <summary> Writes the Sense Function Mode. Does not read back from the instrument. </summary>
        ''' <param name="value"> The Function Mode. </param>
        ''' <returns> The Sense Function Mode or null if unknown. </returns>
        Public Overrides Function WriteFunctionModes(ByVal value As SenseFunctionModes) As SenseFunctionModes?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SENS:FUNC {0}", SenseSubsystemBase.BuildRecord(value))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.FunctionModes = New SenseFunctionModes?
            Else
                Me.FunctionModes = value
            End If
            Return Me.FunctionModes
        End Function

#End Region

#Region " LATEST DATA "

        ''' <summary> Fetches the latest data and parses it. </summary>
        ''' <remarks> Issues the ':SENSE:DATA:LAT?' query, which reads data stored in the Sample Buffer.. </remarks>
        Public Overrides Sub FetchLatestData()
            If Me.IsSessionOpen Then
                MyBase.LastReading = Me.Session.QueryTrimEnd(":SENSE:DATA:LAT?")
            End If
            If Not String.IsNullOrWhiteSpace(Me.LastReading) Then
                ' the emulator will set the last reading. 
                Me.ParseReading(Me.LastReading)
                MyBase.MeasurementAvailable = True
            End If
        End Sub

#End Region

#Region " RANGE "

        ''' <summary> Queries the current Range. </summary>
        ''' <returns> The current Range or none if unknown. </returns>
        Public Overrides Function QueryRange() As Double?
            If Me.IsSessionOpen Then
                MyBase.Range = Me.Session.Query(0.0F, ":SOUR:RANG?")
            End If
            Return MyBase.Range
        End Function

        ''' <summary> Writes the sense current Range without reading back the value from the device. </summary>
        ''' <remarks> This command sets the current Range. The value is in Amperes. 
        '''           At *RST, the range is auto and the value is not known. </remarks>
        ''' <param name="value"> The sense current Range. </param>
        ''' <returns> The sense Current Range. </returns>
        Public Overrides Function WriteRange(ByVal value As Double) As Double?
            If value >= (isr.IO.Visa.Scpi.Syntax.Infinity - 1) Then
                If Me.IsSessionOpen Then
                    Me.Session.WriteLine(":SENS:RANG MAX")
                End If
                value = isr.IO.Visa.Scpi.Syntax.Infinity
            ElseIf value <= (isr.IO.Visa.Scpi.Syntax.NegativeInfinity + 1) Then
                If Me.IsSessionOpen Then
                    Me.Session.WriteLine(":SENS:RANG MIN")
                End If
                value = isr.IO.Visa.Scpi.Syntax.NegativeInfinity
            Else
                If Me.IsSessionOpen Then
                    Me.Session.WriteLine(":SENS:RANG {0}", value)
                End If
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                MyBase.Range = New Double?
            Else
                MyBase.Range = value
            End If
            Return MyBase.Range
        End Function

#End Region

    End Class

End Namespace
