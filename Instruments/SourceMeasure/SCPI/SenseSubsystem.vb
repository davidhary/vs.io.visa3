Namespace Scpi

    ''' <summary> Defines a SCPI Sense Subsystem for a generic Source Measure instrument. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    Public Class SenseSubsystem
        Inherits SenseSubsystemBase

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
            MyBase.SupportsMultiFunctions = True
            Me.SupportedFunctionModes = SenseFunctionModes.CurrentDC Or
                                         SenseFunctionModes.VoltageDC Or
                                         SenseFunctionModes.Resistance
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Sets the subsystem values to their known execution reset state. </summary>
        Public Overrides Sub ResetKnownState()
            MyBase.ResetKnownState()
            Me.FunctionModes = SenseFunctionModes.CurrentDC Or SenseFunctionModes.VoltageDC
            Me.ConcurrentSenseEnabled = True
            Me.Range = 0.105
            Me.PowerLineCycles = 5
        End Sub

#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.SafePostPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return ""
            End Get
        End Property

#Region " AUTO RANGE "

        ''' <summary> Gets the automatic Range enabled command Format. </summary>
        ''' <value> The automatic Range enabled query command. </value>
        Protected Overrides ReadOnly Property AutoRangeEnabledCommandFormat As String
            Get
                Return ":SENS:RANG:AUTO {0:'ON';'ON';'OFF'}"
            End Get
        End Property

        ''' <summary> Gets the automatic Range enabled query command. </summary>
        ''' <value> The automatic Range enabled query command. </value>
        Protected Overrides ReadOnly Property AutoRangeEnabledQueryCommand As String
            Get
                Return ":SENS:RANG:AUTO?"
            End Get
        End Property

#End Region

#Region " LATEST DATA "

        Public Overrides Sub ParseReading(reading As String)
        End Sub

#End Region

#Region " POWER LINE CYCLES "

        ''' <summary> Gets The Power Line Cycles command format. </summary>
        ''' <value> The Power Line Cycles command format. </value>
        Protected Overrides ReadOnly Property PowerLineCyclesCommandFormat As String
            Get
                Return ":SENS:NPLC {0}"
            End Get
        End Property

        ''' <summary> Gets The Power Line Cycles query command. </summary>
        ''' <value> The Power Line Cycles query command. </value>
        Protected Overrides ReadOnly Property PowerLineCyclesQueryCommand As String
            Get
                Return ":SENS:NPLC?"
            End Get
        End Property

#End Region

#Region " PROTECTION LEVEL "

        ''' <summary> Gets the protection level command format. </summary>
        ''' <value> the protection level command format. </value>
        Protected Overrides ReadOnly Property ProtectionLevelCommandFormat As String
            Get
                Return ":SENS:PROT {0}"
            End Get
        End Property

        ''' <summary> Gets the protection level query command. </summary>
        ''' <value> the protection level query command. </value>
        Protected Overrides ReadOnly Property ProtectionLevelQueryCommand As String
            Get
                Return ":SENS:PROT?"
            End Get
        End Property

#End Region

#End Region

    End Class

End Namespace

#If False Then

#Region " CONCURRENT FUNCTION MODE "

        ''' <summary> State of the output on. </summary>
        Private _ConcurrentSenseEnabled As Boolean?

        ''' <summary> Gets or sets the cached Concurrent Function Mode Enabled sentinel. </summary>
        ''' <value> <c>null</c> if mode is not known; <c>True</c> if concurrent; otherwise, <c>False</c>. </value>
        Public Property ConcurrentSenseEnabled As Boolean?
            Get
                Return Me._ConcurrentSenseEnabled
            End Get
            Protected Set(ByVal value As Boolean?)
                If Not Boolean?.Equals(Me.ConcurrentSenseEnabled value) Then
                    Me._ConcurrentSenseEnabled = value
                    Me.AsyncNotifyPropertyChanged(NameOf(Me.))
                End If
            End Set
        End Property

        ''' <summary> Writes and reads back the Concurrent Function Mode Enabled sentinel. </summary>
        ''' <param name="value"> if set to <c>True</c> is concurrent. </param>
        ''' <returns> <c>null</c> if mode is not known; <c>True</c> if concurrent; otherwise, <c>False</c>. </returns>
        Public Function ApplyConcurrentSenseEnabled(ByVal value As Boolean) As Boolean?
            Me.WriteConcurrentSenseEnabled(value)
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Return Me.ConcurrentSenseEnabled
            Else
                Return Me.QueryConcurrentSenseEnabled()
            End If
        End Function

        ''' <summary> Queries the Concurrent Function Mode Enabled sentinel. Also sets the 
        '''           <see cref="ConcurrentSenseEnabled">mode</see> sentinel. </summary>
        ''' <returns> <c>null</c> if mode is not known; <c>True</c> if concurrent; otherwise, <c>False</c>. </returns>
        Public Function QueryConcurrentSenseEnabled() As Boolean?
            If Me.IsSessionOpen Then
                Me.ConcurrentSenseEnabled = Me.Session.Query(BooleanParser.OneZero, ":SENS:FUNC:CONC?")
            End If
            Return Me.ConcurrentSenseEnabled
        End Function

        ''' <summary> Writes the Concurrent Function Mode  Enabled sentinel. Does not read back from the instrument. </summary>
        ''' <param name="value"> if set to <c>True</c> is concurrent. </param>
        ''' <returns> <c>null</c> if mode is not known; <c>True</c> if concurrent; otherwise, <c>False</c>. </returns>
        Public Function WriteConcurrentSenseEnabled(ByVal value As Boolean) As Boolean?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(BooleanParser.OnOff, ":SENS:FUNC:CONC {0}", value)
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.ConcurrentSenseEnabled = New Boolean?
            Else
                Me.ConcurrentSenseEnabled = value
            End If
            Return Me.ConcurrentSenseEnabled
        End Function

#End Region

#Region " FUNCTION MODES "

        ''' <summary> Builds the Modes record for the specified Modes. </summary>
        ''' <param name="Modes"> Sense Function Modes. </param>
        ''' <returns> The record. </returns>
        Public Shared Function BuildRecord(ByVal modes As SenseFunctionModes) As String
            If modes = SenseFunctionModes.None Then
                Return String.Empty
            Else
                Dim reply As New System.Text.StringBuilder
                For Each code As Integer In [Enum].GetValues(GetType(SenseFunctionModes))
                    If (modes And code) <> 0 Then
                        Dim value As String = CType(code, SenseFunctionModes).ExtractBetween()
                        If Not String.IsNullOrWhiteSpace(value) Then
                            reply.AddWord(value)
                        End If
                    End If
                Next
                Return reply.ToString
            End If
        End Function

        ''' <summary> Returns the <see cref="SenseFunctionModes"></see> from the specified value. </summary>
        ''' <param name="value"> The Modes. </param>
        ''' <returns> The sense function mode. </returns>
        Public Shared Function ParseSenseFunctionMode(ByVal value As String) As SenseFunctionModes
            If String.IsNullOrWhiteSpace(value) Then
                Return SenseFunctionModes.None
            Else
                Dim se As New StringEnumerator(Of SenseFunctionModes)
                Return se.ParseContained(value.BuildDelimitedValue)
            End If
        End Function

        ''' <summary> Get the composite Sense Function Modes based on the message from the instrument. </summary>
        ''' <param name="record"> Specifies the comma delimited Modes record. </param>
        ''' <returns> The sense function modes. </returns>
        Public Shared Function ParseSenseFunctionModes(ByVal record As String) As SenseFunctionModes
            Dim parsed As SenseFunctionModes = SenseFunctionModes.None
            If Not String.IsNullOrWhiteSpace(record) Then
                For Each modeValue As String In record.Split(","c)
                    parsed = parsed Or SenseSubsystemBase.ParseSenseFunctionMode(modeValue)
                Next
            End If
            Return parsed
        End Function

        ''' <summary> The Sense Function Modes. </summary>
        Private _FunctionModes As SenseFunctionModes?

        ''' <summary> Gets or sets the cached Sense Function Modes. </summary>
        ''' <value> The Function Modes or null if unknown. </value>
        Public Property FunctionModes As SenseFunctionModes?
            Get
                Return Me._FunctionModes
            End Get
            Protected Set(ByVal value As SenseFunctionModes?)
                If Not Nullable.Equals(Me.FunctionModes, value) Then
                    Me._FunctionModes = value
                    Me.AsyncNotifyPropertyChanged(NameOf(Me.))
                End If
            End Set
        End Property

        ''' <summary> Queries the Sense Function Modes. </summary>
        ''' <returns> The Function Modes or null if unknown. </returns>
        Public MustOverride Function QueryFunctionModes() As SenseFunctionModes?

        ''' <summary> Writes the Sense Function Modes. Does not read back from the instrument. </summary>
        ''' <param name="value"> The Sense Function Mode. </param>
        ''' <returns> The Function Modes or null if unknown. </returns>
        Public MustOverride Function WriteFunctionModes(ByVal value As SenseFunctionModes) As SenseFunctionModes?

        ''' <summary> Writes and reads back the Sense Function Modes. </summary>
        ''' <param name="value"> The <see cref="SenseFunctionModes">Function Modes</see>. </param>
        ''' <returns> The Sense Function Modes or null if unknown. </returns>
        Public Function ApplyFunctionModes(ByVal value As SenseFunctionModes) As SenseFunctionModes?
            Me.WriteFunctionModes(value)
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Return Me.FunctionModes
            Else
                Return Me.QueryFunctionModes()
            End If
        End Function

        Private _SupportsMultiFunctions As Boolean

        ''' <summary> Gets or sets the condition telling if the instrument supports multi-functions. For
        ''' example, the 2400 source-measure instrument support measuring voltage, current, and
        ''' resistance concurrently whereas the 2700 supports a single function at a time. </summary>
        ''' <value> The supports multi functions. </value>
        <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Multi")>
        Public Property SupportsMultiFunctions() As Boolean
            Get
                Return Me._supportsMultiFunctions
            End Get
            Set(ByVal value As Boolean)
                If Not Me.SupportsMultiFunctions.Equals(value) Then
                    Me._supportsMultiFunctions = value
                    Me.AsyncNotifyPropertyChanged(NameOf(Me.))
                End If
            End Set
        End Property

        Private _SupportedFunctionModes As SenseFunctionModes
        ''' <summary>
        ''' Gets or sets the supported function modes.
        ''' This is a subset of the functions supported by the instrument.
        ''' </summary>
        Public Property SupportedFunctionModes() As SenseFunctionModes
            Get
                Return _SupportedFunctionModes
            End Get
            Set(ByVal value As SenseFunctionModes)
                If Not Me.SupportedFunctionModes.Equals(value) Then
                    Me._SupportedFunctionModes = value
                    Me.AsyncNotifyPropertyChanged(NameOf(Me.))
                End If
            End Set
        End Property

        ''' <summary> Queries the Sense Function Modes. Also sets the <see cref="FunctionModes"></see> cached value. </summary>
        ''' <returns> The Sense Function Mode or null if unknown. </returns>
        Public Overrides Function QueryFunctionModes() As SenseFunctionModes?
            If Me.IsSessionOpen Then
                ' the K2700 expects single quotes when writing the value but sends back items delimited with double quotes.
                Me.FunctionModes = SenseSubsystemBase.ParseSenseFunctionModes(Me.Session.QueryTrimEnd(":SENS:FUNC?").Replace(CChar(""""), "'"c))
            End If
            Return Me.FunctionModes
        End Function

        ''' <summary> Writes the Sense Function Mode. Does not read back from the instrument. </summary>
        ''' <param name="value"> The Function Mode. </param>
        ''' <returns> The Sense Function Mode or null if unknown. </returns>
        Public Overrides Function WriteFunctionModes(ByVal value As SenseFunctionModes) As SenseFunctionModes?
            If Me.IsSessionOpen Then
                Me.Session.WriteLine(":SENS:FUNC {0}", SenseSubsystemBase.BuildRecord(value))
            End If
            If Me.LastStatus < VisaNS.VisaStatusCode.Success Then
                Me.FunctionModes = New SenseFunctionModes?
            Else
                Me.FunctionModes = value
            End If
            Return Me.FunctionModes
        End Function

#End Region


''' <summary>Specifies the sense function modes.</summary>
<System.Flags()> Public Enum SenseFunctionModes
    <ComponentModel.Description("Not specified")> None = 0
    <ComponentModel.Description("Voltage ('VOLT')")> Voltage = 1
    <ComponentModel.Description("Current ('CURR')")> Current = 2 * SenseFunctionModes.Voltage
    <ComponentModel.Description("DC Voltage ('VOLT:DC')")> VoltageDC = 2 * SenseFunctionModes.Current
    <ComponentModel.Description("DC Current ('CURR:DC')")> CurrentDC = 2 * SenseFunctionModes.VoltageDC
    <ComponentModel.Description("AC Voltage ('VOLT:AC')")> VoltageAC = 2 * SenseFunctionModes.CurrentDC
    <ComponentModel.Description("AC Current ('CURR:AC')")> CurrentAC = 2 * SenseFunctionModes.VoltageAC
    <ComponentModel.Description("Resistance ('RES')")> Resistance = 2 * SenseFunctionModes.CurrentAC
    <ComponentModel.Description("Four-Wire Resistance ('FRES')")> FourWireResistance = 2 * SenseFunctionModes.Resistance
    <ComponentModel.Description("Temperature ('TEMP')")> Temperature = 2 * SenseFunctionModes.FourWireResistance
    <ComponentModel.Description("Frequency ('FREQ')")> Frequency = 2 * SenseFunctionModes.Temperature
    <ComponentModel.Description("Period ('PER')")> Period = 2 * SenseFunctionModes.Frequency
    <ComponentModel.Description("Continuity ('CONT')")> Continuity = 2 * SenseFunctionModes.Period
    <ComponentModel.Description("Timestamp element ('TIME')")> TimestampElement = 2 * SenseFunctionModes.Continuity
    <ComponentModel.Description("Status Element ('STAT')")> StatusElement = 2 * SenseFunctionModes.TimestampElement
    <ComponentModel.Description("Memory ('MEM')")> Memory = 2 * SenseFunctionModes.StatusElement
End Enum
#End If
