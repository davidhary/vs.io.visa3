Imports NationalInstruments
Namespace SCPI.PowerSupply

    ''' <summary> Defines a SCPI Measure Subsystem for power supplies. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/7/2013" by="David" revision=""> Created. </history>
    Public Class MeasureSubsystem
        Inherits SCPI.MeasureSubsystemBase

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="MeasureSubsystem" /> class. </summary>
        ''' <param name="visaSession"> A reference to a <see cref="MessageBased.Session">message based
        ''' session</see>. </param>
        Public Sub New(ByVal visaSession As MessageBased.Session)
            MyBase.New(visaSession)
        End Sub

#End Region

#Region " I PRESETTABLE "

        ''' <summary> Sets subsystem values to their known execution clear state. </summary>
        ''' <returns> True if successful <see cref="VisaNS.VisaStatusCode">VISA Status Code</see>
        ''' was returned; False otherwise. </returns>
        Public Overrides Function ClearExecutionState() As Boolean
            Return True
        End Function

        ''' <summary> Performs a reset and additional custom setting for the subsystem. </summary>
        ''' <remarks> Use this method to customize the reset. </remarks>
        ''' <returns> True if successful <see cref="VisaNS.VisaStatusCode">VISA Status Code</see>
        ''' was returned; False otherwise. </returns>
        Public Overrides Function InitializeKnownState() As Boolean
            Return Me.ResetKnownState
        End Function

        ''' <summary> Gets the subsystem value to their known execution preset state. </summary>
        ''' <returns> True if successful <see cref="VisaNS.VisaStatusCode">VISA Status Code</see>
        ''' was returned; False otherwise. </returns>
        Public Overrides Function PresetKnownState() As Boolean
            Return True
        End Function

        ''' <summary> Sets the subsystem value to their known execution reset state. </summary>
        ''' <returns> True if successful <see cref="VisaNS.VisaStatusCode">VISA Status Code</see>
        ''' was returned; False otherwise. </returns>
        Public Overrides Function ResetKnownState() As Boolean
            Me.Level = 0
            Return True
        End Function

#End Region

#Region " LEVEL "

        ''' <summary> The level. </summary>
        Private _Level As Double

        ''' <summary> Gets or sets the cached Current level. </summary>
        ''' <value> The Current. </value>
        Public Property Level As Double
            Get
                Return Me._Level
            End Get
            Protected Set(value As Double)
                If Me.Level <> value Then
                    Me._Level = value
                    MyBase.OnSetPropertyChanged(System.Reflection.MethodInfo.GetCurrentMethod.Name)
                End If
            End Set
        End Property

#End Region

#Region " MEASURE "

        ''' <summary> Measures the Current. </summary>
        ''' <returns> . </returns>
        Public Overloads Function Measure() As Double
            Me.Level = isr.IO.Visa.SCPI.MeasureSubsystemBase.Measure(Me.Session)
            Return Level
        End Function

#End Region

    End Class

End Namespace
