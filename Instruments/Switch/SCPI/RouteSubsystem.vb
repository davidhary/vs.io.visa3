Imports NationalInstruments
Namespace SCPI

    ''' <summary> Defines a SCPI Route Subsystem for Switch SCPI devices. </summary>
    ''' <license> (c) 2012 Integrated Scientific Resources, Inc.<para>
    ''' Licensed under The MIT License. </para><para>
    ''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    ''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    ''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    ''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    ''' </para> </license>
    ''' <history date="9/26/2012" by="David" revision="1.0.4652"> Created. </history>
    Public Class RouteSubsystem
        Inherits Visa.SCPI.RouteSubsystemBase

#Region " CONSTRUCTION + CLEANUP "

        ''' <summary> Initializes a new instance of the <see cref="RouteSubsystem" /> class. </summary>
        ''' <param name="statusSubsystem "> A reference to a <see cref="IO.VISA.StatusSubsystemBase">message based
        ''' session</see>. </param>
        Public Sub New(ByVal statusSubsystem As IO.Visa.StatusSubsystemBase)
            MyBase.New(statusSubsystem)
        End Sub

#End Region

#Region " I PRESETTABLE "


#End Region

#Region " PUBLISHER "

        ''' <summary> Publishes all values by raising the property changed events. </summary>
        Public Overrides Sub Publish()
            If Me.Publishable Then
                For Each p As Reflection.PropertyInfo In Reflection.MethodInfo.GetCurrentMethod.DeclaringType.GetProperties()
                    Me.SafePostPropertyChanged(p.Name)
                Next
            End If
        End Sub

#End Region

#Region " COMMAND SYNTAX "

        ''' <summary> Gets the preset command. </summary>
        ''' <value> The preset command. </value>
        Protected Overrides ReadOnly Property PresetCommand As String
            Get
                Return ""
            End Get
        End Property

#Region " CHANNELS "

        ''' <summary> Gets the close channels command format. </summary>
        ''' <value> The close channels command format. </value>
        Protected Overrides ReadOnly Property CloseChannelsCommandFormat As String
            Get
                Return ":ROUT:CLOS {0}"
            End Get
        End Property

        ''' <summary> Gets the recall channel pattern command format. </summary>
        ''' <value> The recall channel pattern command format. </value>
        Protected Overrides ReadOnly Property RecallChannelPatternCommandFormat As String
            Get
                Return ":ROUT:MEM:REC M{0}"
            End Get
        End Property

        ''' <summary> Gets the save channel pattern command format. </summary>
        ''' <value> The save channel pattern command format. </value>
        Protected Overrides ReadOnly Property SaveChannelPatternCommandFormat As String
            Get
                Return ":ROUT:MEM:SAVE M{0}"
            End Get
        End Property

        ''' <summary> Gets the open channels command. </summary>
        ''' <value> The open channels command. </value>
        Protected Overrides ReadOnly Property OpenChannelsCommand As String
            Get
                Return ":ROUT:OPEN ALL"
            End Get
        End Property

        Protected Overrides ReadOnly Property OpenChannelsCommandFormat As String
            Get
                Return ":ROUT:OPEN {0}"
            End Get
        End Property

#End Region

#Region " SCAN LIST "

        ''' <summary> Gets the scan list command. </summary>
        ''' <value> The scan list command. </value>
        Protected Overrides ReadOnly Property ScanListCommand As Command
            Get
                Static cmd As Command
                If cmd Is Nothing Then
                    cmd = New Command(":ROUT:SCAN", CommandFormats.Standard And CommandFormats.QuerySupported And
                                      CommandFormats.WriteSupported)
                End If
                Return cmd
            End Get
        End Property

#End Region

#Region " SLOT CARD INFO "

        ''' <summary> Gets the slot card type query command format. </summary>
        ''' <value> The slot card type query command format. </value>
        Protected Overrides ReadOnly Property SlotCardTypeQueryCommandFormat As String
            Get
                Return ":ROUT:CONF:SLOT{0}:CTYPE?"
            End Get
        End Property

        ''' <summary> Gets the slot card settling time query command format. </summary>
        ''' <value> The slot card settling time query command format. </value>
        Protected Overrides ReadOnly Property SlotCardSettlingTimeQueryCommandFormat As String
            Get
                Return ":ROUT:CONF:SLOT{0}:STIME?"
            End Get
        End Property


#End Region

#Region " TERMINAL MODE "

        Protected Overrides ReadOnly Property TerminalModeCommand As Command
            Get
                Static cmd As Command
                If cmd Is Nothing Then
                    cmd = New Command(":ROUT:TERM", CommandFormats.Standard And CommandFormats.QuerySupported And CommandFormats.WriteSupported)
                End If
                Return cmd
            End Get
        End Property

#End Region

#End Region

    End Class

End Namespace
