﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = Visa.My.ProjectTraceEventId.MultimeterTester

        Public Const AssemblyTitle As String = "VISA Multimeter Tester"
        Public Const AssemblyDescription As String = "VISA Multimeter Tester"
        Public Const AssemblyProduct As String = "IO.VISA.Multimeter.Tester.2019"

    End Class

End Namespace

