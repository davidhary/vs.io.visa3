﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Visa Multimeter Tester")> 
<Assembly: AssemblyDescription("Visa Multimeter Tester")> 
<Assembly: AssemblyProduct("Visa.Multimeter.Tester.2019")> 

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: ComVisible(False)> 

