﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.IO.Visa.My.ProjectTraceEventId.TspTester

        Public Const AssemblyTitle As String = "TSP Tester"
        Public Const AssemblyDescription As String = "TSP Tester"
        Public Const AssemblyProduct As String = "IO.VISA.TSP.Tester.2019"

    End Class

End Namespace

