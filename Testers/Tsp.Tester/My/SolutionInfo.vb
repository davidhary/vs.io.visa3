﻿Imports System
Imports System.Reflection
Imports System.Resources
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes
<Assembly: AssemblyCompany("Integrated Scientific Resources")> 
<Assembly: AssemblyCopyright("(c) 2012 Integrated Scientific Resources, Inc.")> 
<Assembly: AssemblyTrademark("Licensed under The MIT License.")> 
<Assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)> 
<Assembly: AssemblyCulture("")> 
#If Not NonClsCompliant Then
<Assembly: CLSCompliant(True)> 
#End If

' Version information
<Assembly: AssemblyVersion("3.1.*")> 


