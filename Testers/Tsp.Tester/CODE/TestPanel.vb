Imports NationalInstruments
Imports isr.Core.Controls
Imports isr.Core.Controls.CheckBoxExtensions
Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.ExceptionExtensions
Imports isr.Core.Agnostic.IncludesExtensions
Imports isr.IO.Visa
''' <summary> TSP Test panel. </summary>
''' <license> (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/07/2007" by="David" revision="2.0.2597.x"> Created. </history>
Friend Class TestPanel
    #Const designMode1 = True
#Region " BASE FROM WRAPPER "
    ' Designing requires changing the condition to True.
#If designMode Then
    Inherits TraceObserverUserFormBaseWrapper
#Else
    Inherits TraceObserverUserFormBase
#End If
#End Region
    Implements ITraceMessageLogger

#Region " CONSTRUCTION + CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of this class.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        ' set large font for messages.
        'Me._messagesBox.Font = New Font(Me._messagesBox.Font.FontFamily, 10, FontStyle.Regular)
        'Me._messagesBox.TabCaption = "MESSA&GES"

        Me.queryCommand = ""

        ' instantiate the timing timer
        Me.TimingStopwatch = System.Diagnostics.Stopwatch.StartNew

        ' instantiate the instrument
        Me.TspSystem = New isr.IO.Visa.Tsp.TspSystem(New IO.Visa.Tsp.Instrument.MasterDevice)

        ' hide timer interval information - this could be activated if we use an
        ' service request driven rather than timer driver monitoring.
        Me._executionTimeTextBox.Visible = False
        Me._executionTimeTextBoxLabel.Visible = False

        With Me._ResourceSelectorConnector
            .Searchable = True
            .Clearable = True
            .Connectible = True
        End With

#If designMode Then
        Debug.Assert(False, "Illegal call; reset design mode")
#End If
    End Sub

#Region " DROP SHADOW "

    ''' <summary>
    ''' Defines the Drop Shadow constant.
    ''' </summary>
    Private Const CS_DROPSHADOW As Integer = 131072

    ''' <summary>
    ''' Adds a drop shadow parameter.
    ''' </summary>
    ''' <remarks>
    ''' From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx
    ''' </remarks>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand, Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)>
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
            Return cp
        End Get
    End Property

#End Region

    ''' <summary> Cleans up managed components. </summary>
    ''' <remarks> Use this method to reclaim managed resources used by this class. </remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub OnDisposeManagedResources()

        Try
            ' dispose of the timer.
            Me.DisposeRefreshTimer()
        Finally
        End Try

        Try
            ' dispose of the instrument
            If Me.TspSystem IsNot Nothing Then
                Try
                    If Me.TspSystem.IsDeviceOpen Then
                        Me.TspSystem.Device.CloseSession()
                    End If
                Finally
                End Try
                Me.TspSystem.Dispose()
                Me.TspSystem = Nothing
            End If
        Finally
        End Try

        Me.TimingStopwatch = Nothing

    End Sub

#End Region

#Region " TYPES "

    ''' <summary>Gets or sets Tab index values.</summary>
    Private Enum MainTabsIndex
        ConsoleTabIndex = 0
        TspScripts = 1
        TspFunctions = 2
        TbdTabIndex = 3
        MessagesTabIndex = 4
    End Enum

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all
    ''' publishers. </summary>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    ''' event data. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Unloading..;. ")

            ' stop the timer
            Me.StopRefreshTimer()

            ' dispose of the timer.
            Me.DisposeRefreshTimer()

            ' dispose of the instrument
            If Me.TspSystem IsNot Nothing Then
                If Me.TspSystem.IsDeviceOpen Then
                    Me.TspSystem.Device.CloseSession()
                End If
                Me.TspSystem.Dispose()
                Me.TspSystem = Nothing
            End If

            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Saving settings..;. ")
            System.Windows.Forms.Application.DoEvents()

            ' terminate all the singleton objects.
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Closing. Please wait..;. ")

            ' delay a bit longer
            Dim timer As System.Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
            Do Until timer.ElapsedMilliseconds > 200
                Windows.Forms.Application.DoEvents()
                Threading.Thread.Sleep(10)
            Loop

            If e IsNot Nothing Then
                e.Cancel = False
            End If

        Catch ex As Exception

            Debug.Assert(Not Debugger.IsAttached, ex.ToString)

        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Application.DoEvents()
            MyBase.OnClosing(e)
        End Try
    End Sub

    ''' <summary> Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            ' Add any initialization after the InitializeComponent() call
            ' Me.OnInstantiate()

            ' set the form caption
            Me.Text = My.Application.Info.ProductName & " release " & My.Application.Info.Version.ToString

            ' build the navigator tree.
            ' Me.BuildNavigatorTreeView()

            ' default to center screen.
            Me.CenterToScreen()

            MyBase.OnLoad(e)

        Catch ex As Exception

            ex.Data.Add("@isr", "Exception loading the form.")
            My.Application.MyLog.TraceSource.TraceEvent(ex, My.MyApplication.TraceEventId)
            If Core.MyMessageBox.ShowDialogAbortIgnore(Me, ex) = Windows.Forms.DialogResult.Abort Then
                Application.Exit()
            End If

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default
            Trace.CorrelationManager.StopLogicalOperation()

        End Try

    End Sub

    ''' <summary> Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnShown(e As System.EventArgs)

        Try

            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' allow form rendering time to complete: process all messages currently in the queue.
            Application.DoEvents()

            ' display the log file location
            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Logging to: '{0}'", My.Application.Log.DefaultFileLogWriter.CustomLocation)

            ' allow form rendering time to complete: process all messages currently in the queue.
            Application.DoEvents()

            If Not Me.DesignMode Then
                Me._ResourceSelectorConnector.Enabled = False
                Me._clearInterfaceButton.Enabled = False

                ' disable controls
                Me.IsOpen = False

                ' clear the SRQ value.
                Me._srqStatusLabel.Text = "0x.."

                ' allow some events to occur for refreshing the display.
                Application.DoEvents()

                Me._ResourceSelectorConnector.Clearable = True
                Me._ResourceSelectorConnector.Searchable = True
                Me._ResourceSelectorConnector.Connectible = True
                Me._ResourceSelectorConnector.Enabled = True

                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Ready to open Visa Session;. ")
            End If

            MyBase.OnShown(e)

        Catch ex As Exception

            ex.Data.Add("@isr", "Exception showing the form.")
            My.MyLibrary.MyLog.TraceSource.TraceEvent(ex, My.MyApplication.TraceEventId)
            If isr.Core.MyMessageBox.ShowDialogAbortIgnore(Me, ex) = Windows.Forms.DialogResult.Abort Then
                Application.Exit()
            End If

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default
            Trace.CorrelationManager.StopLogicalOperation()

        End Try

    End Sub


#End Region

#Region " RESOURCE CONNECTOR "

    Private _ResourceName As String

    ''' <summary> Gets or sets the name of the resource. </summary>
    ''' <value> The name of the resource. </value>
    Public Property ResourceName As String
        Get
            Return Me._ResourceName
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not value.Equals(Me.ResourceName) Then
                Me._ResourceName = value
            End If
            Me._ResourceSelectorConnector.SelectedResourceName = value
        End Set
    End Property

    ''' <summary> Gets or sets the Search Pattern of the resource. </summary>
    ''' <value> The Search Pattern of the resource. </value>
    Public Property ResourceSearchPattern As String
        Get
            Return Me._ResourceSelectorConnector.ResourcesSearchPattern
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = ""
            If Not value.Equals(Me.ResourceSearchPattern) Then
                Me._ResourceSelectorConnector.ResourcesSearchPattern = value
            End If
        End Set
    End Property

    ''' <summary> Displays the resource names based on the device resource search pattern. </summary> 
    Public Sub DisplayNames()
        ' get the list of available resources
        If Me.TspSystem IsNot Nothing AndAlso Me.TspSystem.Device IsNot Nothing AndAlso
            Me.TspSystem.Device.ResourcesSearchPattern IsNot Nothing Then
            Me._ResourceSelectorConnector.ResourcesSearchPattern = Me.TspSystem.Device.ResourcesSearchPattern
        End If
        Me._ResourceSelectorConnector.DisplayResourceNames()
    End Sub

    ''' <summary> Clears the instrument by calling a propagating clear command. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    Private Sub Connector_Clear(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ResourceSelectorConnector.Clear
        Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId,
                                   "Clearing resource;. Clearing the resource {0}", Me._ResourceSelectorConnector.SelectedResourceName)
        Me.TspSystem.Device.ResetAndClear()
        Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId,
                                   "Resource cleared;. Resource {0} cleared", Me._ResourceSelectorConnector.SelectedResourceName)
    End Sub

    ''' <summary> Connects the instrument by calling a propagating connect command. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub Connector_Connect(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ResourceSelectorConnector.Connect

        Try
            Me.Cursor = Cursors.WaitCursor
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "Connecting;. Opening VISA Session to {0}", Me._ResourceSelectorConnector.SelectedResourceName)
            Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
            Me.TspSystem.Device.OpenSession(Me._ResourceSelectorConnector.SelectedResourceName)
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "Connected;. Opened VISA Session to {0}", Me._ResourceSelectorConnector.SelectedResourceName)

            Me._clearInterfaceButton.Enabled = Me.TspSystem.Device.IsDeviceOpen AndAlso Me.TspSystem.Device.IsSessionOpen

        Catch ex As OperationFailedException
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "Failed connecting;. Failed opening VISA Session to {0}. {1}",
                                       Me._ResourceSelectorConnector.SelectedResourceName, ex.ToFullBlownString)
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                       "Exception occurred connecting;. {0}. {1}",
                                       Me._ResourceSelectorConnector.SelectedResourceName, ex.ToFullBlownString)
        Finally
            Me._ResourceSelectorConnector.IsConnected = Me.TspSystem.Device.IsDeviceOpen
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Disconnects the instrument by calling a propagating disconnect command. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub Connector_Disconnect(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ResourceSelectorConnector.Disconnect
        Try
            If Me.TspSystem.Device.IsDeviceOpen Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "Disconnecting;. Ending access to {0}", Me._ResourceSelectorConnector.SelectedResourceName)
            End If
            Me.TspSystem.Device.CloseSession()
        Catch ex As OperationFailedException
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                       "Failed disconnecting;. Failed closing VISA Session to {0}. {1}",
                                       Me._ResourceSelectorConnector.SelectedResourceName, ex.ToFullBlownString)
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                       "Exception occurred disconnecting;. {0}. {1}",
                                       Me._ResourceSelectorConnector.SelectedResourceName, ex.ToFullBlownString)
        Finally
            If Me.TspSystem.Device.IsDeviceOpen Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "Failed disconnecting;. Failed ending access to {0}", Me._ResourceSelectorConnector.SelectedResourceName)
            Else
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                           "Disconnected;. Ended access to {0}", Me._ResourceSelectorConnector.SelectedResourceName)
            End If
            Me._ResourceSelectorConnector.IsConnected = Me.TspSystem.Device.IsDeviceOpen
            Me._clearInterfaceButton.Enabled = Me.TspSystem.Device.IsDeviceOpen AndAlso Me.TspSystem.Device.IsSessionOpen
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Displays available instrument names. </summary>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Specifies the event arguments provided with the call. </param>
    Private Sub Connector_FindNames(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ResourceSelectorConnector.FindNames
        Me.DisplayNames()
    End Sub

    ''' <summary> Event handler. Called by _ResourceNameSelectorConnector for property changed
    ''' events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub Connector_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _ResourceSelectorConnector.PropertyChanged
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.Connector_PropertyChanged), New Object() {sender, e})
        End If
        Try
            Select Case e.PropertyName
                Case "SelectedResourceName"
                    Me.ResourceName = Me._ResourceSelectorConnector.SelectedResourceName
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId,
                                               "Selected {0};. ", Me._ResourceSelectorConnector.SelectedResourceName)
            End Select
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception handling property changed Event;. Failed property {0}. {1}", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Event handler. Called by Connector for trace message available events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Trace message event information. </param>
    Private Sub Connector_TraceMessageAvailable(ByVal sender As Object, ByVal e As TraceMessageEventArgs) Handles _ResourceSelectorConnector.TraceMessageAvailable
        Me.OnTraceMessageAvailable(sender, e)
    End Sub

#End Region

#Region " TSP SYSTEM "

    Private _IsOpen As Boolean

    ''' <summary> Gets or sets the Open condition enabling or disable panels. </summary>
    ''' <value> The is open. </value>
    Private Property IsOpen() As Boolean
        Get
            Return Me._isOpen
        End Get
        Set(ByVal value As Boolean)
            Me._isOpen = value
            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, If(value, "Connected;. ", "Not connected;. "))
            Me._ResourceSelectorConnector.IsConnected = value
            Me._inputTextBox.Enabled = value
            Me._outputTextBox.Enabled = value
            Me._scriptsPanel1.Enabled = value
            Me._scriptsPanel2.Enabled = value
            Me._scriptsPanel3.Enabled = value
            Me._scriptsPanel4.Enabled = value
            Me._functionsPanel1.Enabled = value
            Me._loadFunctionButton.Enabled = value
            Me._instrumentPanel1.Enabled = value
            Me._instrumentPanel2.Enabled = value
            Me._instrumentPanel3.Enabled = value
        End Set
    End Property

    ''' <summary> Executes the connection changed action. </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Mobility", "CA1601:DoNotUseTimersThatPreventPowerStateChanges")>
    Private Sub OnConnectionChanged()

        If Me.TspSystem Is Nothing Then
            Return
        End If

        Dim wasEnabled As Boolean = Me._ResourceSelectorConnector.Enabled
        Dim value As Boolean = Me.TspSystem.IsDeviceOpen

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId,
                                       If(value, "Connection changed--connecting;. ",
                                                  "Connection changed--disconnecting;. "))

            If value Then

                ' get the instrument ID
                If Me.TspSystem IsNot Nothing AndAlso Me.TspSystem.IsDeviceOpen Then

                    If Me.TspSystem.IsSessionOpen Then
                        Me._ResourceSelectorConnector.SelectedResourceName = Me.TspSystem.Device.ResourceName
                    End If

                    AddHandler Me.TspSystem.Device.StatusSubsystem.PropertyChanged, AddressOf StatusSubsystemPropertyChanged
                    AddHandler Me.TspSystem.Device.SystemSubsystem.PropertyChanged, AddressOf SystemSubsystemPropertyChanged

                    ' flush the input buffer in case the instrument has some leftovers.
                    Me.TspSystem.Device.StatusSubsystem.ReadServiceRequestStatus()
                    If Me.TspSystem.Device.StatusSubsystem.MessageAvailable Then
                        Me.receive(True)
                    End If

                    ' initialize the master instrument to its known state.
                    Me.TspSystem.Device.InitializeKnownState()

                    ' set the error and prompt check boxes.
                    Me.TspSystem.Device.StatusSubsystem.QueryIdentity()
                    If Not Me.TspSystem.Device.StatusSubsystem.ProcessExecutionStateEnabled Then
                        ' read execution state explicitly, because session events are disabled.
                        Me.TspSystem.Device.StatusSubsystem.ReadExecutionState()
                    End If

                    ' list any user scripts.
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Listing user scripts;. ")
                    Try
                        Me.listUserScripts()
                    Catch ex As Exception
                        Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId,
                                                   "Exception occurred {0};. {1}", Me._statusPanel.Text, ex.ToFullBlownString)
                    End Try

                    Me.StartRefreshTimer(TimeSpan.FromMilliseconds(500))

                End If

            Else
                If Me.TspSystem IsNot Nothing AndAlso Not Me.TspSystem.IsDeviceOpen Then
                    RemoveHandler Me.TspSystem.Device.StatusSubsystem.PropertyChanged, AddressOf StatusSubsystemPropertyChanged
                    RemoveHandler Me.TspSystem.Device.SystemSubsystem.PropertyChanged, AddressOf SystemSubsystemPropertyChanged
                End If
                Me._srqStatusLabel.Text = "0x.."
                Me.StopRefreshTimer()
            End If


        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred {0};. {1}", Me._statusPanel.Text, ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

            Me._ResourceSelectorConnector.Enabled = wasEnabled

            ' update status
            Me.IsOpen = Me.TspSystem.IsDeviceOpen

        End Try

    End Sub

    ''' <summary>Gets or sets the currently built query command string.</summary>
    Dim queryCommand As String

    ''' <summary> Gets or sets reference to the timing stop watch. </summary>
    Dim timingStopwatch As System.Diagnostics.Stopwatch

    ''' <summary> Gets or sets reference to the TSP system. </summary>
    Private WithEvents TspSystem As isr.IO.Visa.Tsp.TspSystem

    ''' <summary> Event handler. Called by TspSystem for trace message available events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Trace message event information. </param>
    Private Sub TspSystem_TraceMessageAvailable(ByVal sender As Object, ByVal e As TraceMessageEventArgs) Handles TspSystem.TraceMessageAvailable
        Me.OnTraceMessageAvailable(sender, e)
    End Sub

    ''' <summary> Event handler. Called by TspSystem for connection changed events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub TspSystem_ConnectionChanged(ByVal sender As Object, ByVal e As EventArgs) Handles TspSystem.ConnectionChanged

        ' handle the change in connection.
        Me.OnConnectionChanged()

    End Sub

#Region " STATUS "

    ''' <summary> Status subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub StatusSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Try
            If sender IsNot Nothing AndAlso
                e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                Dim subsystem As Tsp.StatusSubsystemBase = CType(sender, Tsp.StatusSubsystemBase)
                Select Case e.PropertyName
                    Case "ExecutionState"
                        Me._tspStatusLabel.Text = subsystem.ExecutionStateCaption
                    Case "ShowErrors"
                        Me._showErrorsCheckBox.SafeSilentCheckStateSetter(subsystem.ShowErrors.ToCheckState)
                    Case "ShowPrompts"
                        Me._showPromptsCheckBox.SafeSilentCheckStateSetter(subsystem.ShowPrompts.ToCheckState)
#If False Then
                    Case "DeviceErrors"
                    Case "ErrorAvailable"
                    Case "ErrorAvailableBits"
                    Case "MeasurementAvailable"
                    Case "MeasurementAvailableBits"
                    Case "MeasurementEventCondition"
                    Case "MeasurementEventEnableBitmask"
                    Case "MeasurementEventStatus"
                    Case "MessageAvailable"
                    Case "MessageAvailableBits"
                    Case "OperationEventCondition"
                    Case "OperationEventEnableBitmask"
                    Case "OperationEventStatus"
                    Case "OperationNegativeTransitionEventEnableBitmask"
                    Case "OperationPositiveTransitionEventEnableBitmask"
                    Case "QuestionableEventCondition"
                    Case "QuestionableEventEnableBitmask"
                    Case "QuestionableEventStatus"
                    Case "ServiceRequestEnableBitmask"
                    Case "ServiceRequestStatus"
                    Case "StandardDeviceErrorAvailable"
                    Case "StandardDeviceErrorAvailableBits"
                    Case "StandardEventEnableBitmask"
                    Case "StandardEventStatus"
#End If
                End Select
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#Region " SYSTEM "

    ''' <summary> System subsystem property changed. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Try
            If sender IsNot Nothing AndAlso
                e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
                Dim subsystem As Tsp.SystemSubsystemBase = CType(sender, Tsp.SystemSubsystemBase)
                Select Case e.PropertyName
                    Case "LastError"
                        Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId,
                                                   "Last error: {0}", subsystem.LastError.CompoundErrorMessage)
#If False Then
                    Case "LastError"
                    Case "LineFrequency"
                    Case "StationLineFrequency"
                    Case "VersionInfo"
#End If
                End Select
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#End Region

#Region " METHODS "

    ''' <summary>Gets the current line of the text box.
    ''' </summary>
    ''' <param name="control">Specifies reference to a text box.</param>
    Private Shared Function GetLineOld(ByVal control As System.Windows.Forms.TextBoxBase) As String
        Dim commandText As String
        Dim commandStart As Integer
        Dim commandEnd As Integer
        Dim commandLength As Integer

        ' save the text
        commandText = control.Text

        ' lookup the command start
        commandStart = control.SelectionStart

        If commandStart > 0 Then
            Do Until (commandStart = 1) Or (Asc(Mid(commandText, commandStart, 1)) = 10)
                commandStart = commandStart - 1
            Loop
            If commandStart > 1 Then
                commandStart = commandStart + 1
            End If
            ' check if operator is entering returns
            If commandStart < commandText.Length Then
                commandLength = commandText.Length
                commandEnd = control.SelectionStart
                Do Until (commandEnd = commandLength) Or (Asc(Mid(commandText, commandEnd, 1)) = 13)
                    commandEnd = commandEnd + 1
                Loop
                If commandEnd < commandLength Then
                    commandEnd = commandEnd - 1
                End If
                commandLength = commandEnd - commandStart + 1
                getLineOld = Mid(commandText, commandStart, commandLength)
            Else
                getLineOld = ""
            End If
        Else
            getLineOld = ""
        End If

    End Function

    ''' <summary>List the user scripts.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Function ListUserScripts() As Boolean

        Dim refreshTimeState As TimerStates = TimerStates.None
        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If Me._refreshTimer IsNot Nothing Then
                refreshTimeState = Me._refreshTimer.TimerState
                Me.StopRefreshTimer()
            End If

            If Me.TspSystem IsNot Nothing AndAlso Me.TspSystem.IsDeviceOpen Then
                If Me.TspSystem.IsSessionOpen Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Flushing the read buffer...;. ")
                    Me.TspSystem.Device.Session.DiscardUnreadData()
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Fetching script names...;. ")
                    Dim scriptCount As Integer = Me.TspSystem.Device.ScriptManager.FetchUserScriptNames()
                    If scriptCount > 0 Then
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Listing script names...;. ")
                        Me._userScriptsList.DataSource = Nothing
                        Me._userScriptsList.Items.Clear()
                        Me._userScriptsList.DataSource = Me.TspSystem.Device.ScriptManager.UserScriptNames
                        If Me._userScriptsList.Items.Count > 0 Then
                            Me._userScriptsList.SelectedIndex = 0
                        End If
                    Else
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "No Scripts;. ")
                        Me._userScriptsList.DataSource = Nothing
                        Me._userScriptsList.Items.Clear()
                    End If
                End If
            End If

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred listing scripts;. {0}", ex.ToFullBlownString)

        Finally

            If refreshTimeState <> TimerStates.None Then
                Me.StartRefreshTimer(TimeSpan.FromMilliseconds(500))
            End If

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Function

    Private receiveLock As New Object
    ''' <summary>Receive a message from the instrument.
    ''' </summary>
    ''' <param name="updateConsole">True to update the instrument output text box.</param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub Receive(ByVal updateConsole As Boolean)

        SyncLock (receiveLock)

            Try

                ' read a message if we have messages.
                Dim receiveBuffer As String = ""
                If updateConsole AndAlso Me.TspSystem.Device.StatusSubsystem.IsMessageAvailable(TimeSpan.FromMilliseconds(50), 3) Then

                    ' Turn on the form hourglass
                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Reading..;. ")

                    If Me.TspSystem IsNot Nothing AndAlso Me.TspSystem.IsSessionOpen Then
                        receiveBuffer = Me.TspSystem.Device.Session.ReadLine()
                    End If

                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Done Reading;. ")

                    Me._outputTextBox.SelectionStart = Me._outputTextBox.Text.Length
                    Me._outputTextBox.SelectionLength = 0
                    Me._outputTextBox.SelectedText = receiveBuffer & vbCrLf
                    Me._outputTextBox.SelectionStart = Me._outputTextBox.Text.Length

                    ' stop the timer and get execution time in ms.
                    Me.TimingStopwatch.Stop()
                    Me._executionTimeTextBox.Text = Me.TimingStopwatch.Elapsed.TotalMilliseconds.ToString("0.0", Globalization.CultureInfo.CurrentCulture)

                End If

            Catch ex As Exception

                Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred receiving;. {0}", ex.ToFullBlownString)

            Finally

                ' Turn off the form hourglass
                Me.Cursor = System.Windows.Forms.Cursors.Default

            End Try

        End SyncLock

    End Sub

    ''' <summary> Sends a message to the instrument. </summary>
    ''' <param name="sendBuffer">         Specifies the message to send. </param>
    ''' <param name="updateInputConsole"> True to update the instrument input text box. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub Send(ByVal sendBuffer As String, ByVal updateInputConsole As Boolean)

        ' Turn on the form hourglass
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Try

            If Not String.IsNullOrWhiteSpace(sendBuffer) Then
                Me._refreshTimer.Enabled = False

                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Sending query/command..;. ")

                ' start the timing timer
                Me.TimingStopwatch = System.Diagnostics.Stopwatch.StartNew

                If Me.TspSystem.Device.IsSessionOpen Then
                    Me.TspSystem.Device.Session.WriteLine(sendBuffer)
                    Me.TspSystem.Device.StatusSubsystem.ReportVisaOperationOkay("sending query/command;. '{0}'", sendBuffer)
                End If

                If updateInputConsole Then
                    Me._inputTextBox.SelectionStart = Me._inputTextBox.Text.Length
                    Me._inputTextBox.SelectionLength = 0
                    Me._inputTextBox.SelectedText = sendBuffer & vbCrLf
                    Me._inputTextBox.SelectionStart = Me._inputTextBox.Text.Length
                End If

            End If

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred sending;. {0}", ex.ToFullBlownString)

        Finally

            Me._refreshTimer.Enabled = True

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private _ValidatedGpibAddress As Nullable(Of Integer)
    ''' <summary>
    ''' Enables connecting to the GPIB board.
    ''' </summary>
    Private Function ValidatedGpibAddress(ByVal address As String) As Nullable(Of Integer)
        Dim value As Integer
        If Integer.TryParse(address, System.Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture, value) Then
            If Not Me._validatedGpibAddress.HasValue OrElse Me._validatedGpibAddress.Value <> value OrElse Not Me._ResourceSelectorConnector.Enabled Then
                Dim resourceName As String = BuildGpibInstrumentResource("0", address)
                Me._ResourceSelectorConnector.Enabled = VisaNS.ResourceManager.GetLocalManager.FindInstrument(resourceName)
            End If
            Me._validatedGpibAddress = value
        Else
            Me._validatedGpibAddress = New Nullable(Of Integer)()
        End If
        Return Me._validatedGpibAddress
    End Function

#End Region

#Region " REFRESH TIMER "

    ''' <summary> Dispose refresh timer. </summary>
    Private Sub DisposeRefreshTimer()
        ' dispose of the timer.
        If Me._refreshTimer IsNot Nothing Then
            Me._refreshTimer.Close()
            Me._refreshTimer.Dispose()
            Me._refreshTimer = Nothing
        End If
    End Sub

    ''' <summary> Starts refresh timer. </summary>
    ''' <param name="interval"> The interval. </param>
    Private Sub StartRefreshTimer(ByVal interval As TimeSpan)
        Me._refreshTimer = New isr.Core.Controls.StateAwareTimer
        Me._refreshTimer.SynchronizingObject = Me
        Me.RestartRefreshTimer(interval)
    End Sub

    ''' <summary> Restarts refresh timer. </summary>
    ''' <param name="interval"> The interval. </param>
    Private Sub RestartRefreshTimer(ByVal interval As TimeSpan)
        If Me._refreshTimer IsNot Nothing Then
            Me._refreshTimer.Interval = interval.TotalMilliseconds
            Me._refreshTimer.AutoReset = True
            Me._refreshTimer.Start()
        End If
    End Sub

    ''' <summary> Stops the refresh timer. </summary>
    Private Sub StopRefreshTimer()
        If Me._refreshTimer IsNot Nothing Then
            Me._refreshTimer.Stop()
            Me._refreshTimer.AutoReset = False
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Timer stopped;. ")
        End If
    End Sub

    ''' <summary> Status register visible setter. </summary>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub StatusRegisterVisibleSetter(ByVal value As Boolean)
        isr.Core.Controls.ToolStripExtensions.SafeVisibleSetter(Me._srqStatusLabel, value)
    End Sub

    ''' <summary> Displays the status register status using hex format. </summary>
    ''' <param name="value"> The register value. </param>
    Public Sub DisplayStatusRegisterStatus(ByVal value As Integer?)
        Me.DisplayStatusRegisterStatus(value, "0x{0:X}")
    End Sub

    ''' <summary> Displays the status register status. </summary>
    ''' <param name="value">  The register value. </param>
    ''' <param name="format"> The format. </param>
    Public Sub DisplayStatusRegisterStatus(ByVal value As Integer?, ByVal format As String)
        isr.Core.Controls.ToolStripExtensions.SafeTextSetter(Me._srqStatusLabel, value, format)
    End Sub

    Private WithEvents _RefreshTimer As isr.Core.Controls.StateAwareTimer
    ''' <summary>
    ''' Serial polls and receives.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _RefreshTimer_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _refreshTimer.Tick

        Try

            If Me.TspSystem IsNot Nothing AndAlso Me.TspSystem.IsDeviceOpen Then
                If Me.TspSystem.IsSessionOpen Then
                    Me.TspSystem.Device.StatusSubsystem.ReadServiceRequestStatus()
                End If
                Me.DisplayStatusRegisterStatus(Me.TspSystem.Device.StatusSubsystem.ServiceRequestStatus)

                ' check if the console tab is open
                If Me._tabControl.SelectedIndex = MainTabsIndex.ConsoleTabIndex Then

                    Me.receive(True)

                End If

            End If

            Me._tspStatusLabel.Text = Me.TspSystem.Device.StatusSubsystem.ExecutionStateCaption

        Catch ex As Exception

            ' stop the time on error
            Me.StopRefreshTimer()

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred receiving;. {0}", ex.ToFullBlownString)


        Finally

        End Try

    End Sub

#End Region

#Region " TESTING TIMER "

    Private WithEvents _TestingTimer As isr.Core.Controls.StateAwareTimer

    ''' <summary>
    ''' Serial polls and receives.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> Private Sub _TestingTimer_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _testingTimer.Tick

        Try

            Me._srqStatusLabel.Text = DateTime.Now.ToLongTimeString

        Catch ex As Exception

            ' stop the time on error
            Me._testingTimer.Stop()

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred timing;. {0}", ex.ToFullBlownString)

        Finally

        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>
    ''' Terminates script execution.
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _AbortButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _abortButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' Terminates script execution when called from a script that is being
            ' executed. This command will not wait for overlapped commands to complete
            ' before terminating script execution. If overlapped commands are required
            ' to finish, use the wait complete function prior to calling exit.
            send("exit() ", True)

            If Not Me.TspSystem.Device.StatusSubsystem.ProcessExecutionStateEnabled Then
                ' read execution state explicitly, because session events are disabled.
                ' update the instrument status.
                Me.TspSystem.Device.StatusSubsystem.ReadExecutionState()
            End If


            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Aborted;. ")

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred clearing device;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary>
    ''' Displays the application information
    ''' </summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _AboutButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _aboutButton.Click

        ' Display the application information
        Using aboutScreen As New isr.Core.WindowsForms.About
            aboutScreen.Image = Me.Icon
            aboutScreen.ShowDialog(System.Reflection.Assembly.GetExecutingAssembly,
              "", "", "", "")
        End Using

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred displaying about panel;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Toggle the font on the control to highlight selection of this control. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub _Button_Enter(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _exitButton.Enter, _abortButton.Enter, _aboutButton.Enter, _callFunctionButton.Enter,
      _clearInterfaceButton.Enter, _deviceClearButton.Enter, _exitButton.Enter, _groupTriggerButton.Enter,
       _removeScriptButton.Enter, _resetLocalNodeButton.Enter, _resetLocalNodeButton.Enter, _refreshUserScriptsListButton.Enter
        Dim thisButton As Control = CType(eventSender, Control)
        thisButton.Font = New Font(thisButton.Font, FontStyle.Bold)
    End Sub

    ''' <summary> Toggle the font on the control to highlight leaving this control. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub _Button_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _exitButton.Leave, _abortButton.Leave, _aboutButton.Leave, _callFunctionButton.Leave,
      _clearInterfaceButton.Leave, _deviceClearButton.Leave, _exitButton.Leave, _groupTriggerButton.Leave,
       _removeScriptButton.Leave, _resetLocalNodeButton.Leave, _resetLocalNodeButton.Leave, _refreshUserScriptsListButton.Leave
        Dim thisButton As Control = CType(eventSender, Control)
        thisButton.Font = New Font(thisButton.Font, FontStyle.Regular)
    End Sub

    ''' <summary> Event handler. Called by _clearInterfaceButton for click events. </summary>
    ''' <param name="sender"> The event sender. </param>
    ''' <param name="e">      Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ClearInterfaceButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _clearInterfaceButton.Click

        Try
            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._errorProvider.Clear()
            If Me.TspSystem.Device.IsDeviceOpen AndAlso Me.TspSystem.Device.IsSessionOpen Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Clearing interface '{0}';. ", Me.TspSystem.Device.Session.InterfaceResourceName)
                Me.TspSystem.Device.SystemSubsystem.ClearInterface()
            End If
        Catch ex As Exception
            Me._errorProvider.SetError(CType(sender, Windows.Forms.Control), ex.ToString)
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred clearing interface;. {0}", ex.ToFullBlownString)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary> Event handler. Called by _callFunctionButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _CallFunctionButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _callFunctionButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Dim functionName As String
            functionName = Me._functionNameTextBox.Text

            Dim functionArgs As String
            functionArgs = Me._functionArgsTextBox.Text

            If Not String.IsNullOrWhiteSpace(functionName) Then
                Me.TspSystem.Device.StatusSubsystem.CallFunction(functionName, functionArgs)
                Me.TspSystem.Device.StatusSubsystem.ReportVisaOperationOkay("calling function '{0}({1})';. ", functionName, functionArgs)
            End If

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred calling function;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by _deviceClearButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _DeviceClearButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _deviceClearButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If Me.TspSystem.IsDeviceOpen Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Clearing device..;. ")
                Me.TspSystem.Device.StatusSubsystem.ClearActiveState()
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Device cleared.;. ")
            End If

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred during device clear;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by _exitButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub _ExitButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _exitButton.Click
        Me.Close()
    End Sub

    ''' <summary> Event handler. Called by _groupTriggerButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _GroupTriggerButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _groupTriggerButton.Click,
                                                                                                            _groupTriggerButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If TspSystem.IsSessionOpen Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Asserting trigger..;. ")
                Me.TspSystem.Device.Session.AssertTrigger()
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Trigger asserted.;. ")
            End If

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred triggering device;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try
    End Sub

    ''' <summary> Event handler. Called by _inputTextBox for key press events. 
    '''           Handles key at the command console.
    '''           </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Key press event information. </param>
    Private Sub _InputTextBox_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs) Handles _inputTextBox.KeyPress

        Dim keyAscii As Integer = Asc(eventArgs.KeyChar)

        Select Case KeyAscii

            Case System.Windows.Forms.Keys.Return ' enter key

                ' User pressed enter key.  Send the string to the instrument.

                ' Disable the command console while waiting for command to process.
                Me._inputTextBox.Enabled = False

                ' Enable the abort button.
                Me._abortButton.Enabled = False

                ' dh 6.0.02 use command line at cursor
                If Me._inputTextBox.Lines IsNot Nothing AndAlso Me._inputTextBox.Lines.Length > 0 Then
                    Dim line As Integer = Me._inputTextBox.GetLineFromCharIndex(Me._inputTextBox.SelectionStart)
                    Me.queryCommand = Me._inputTextBox.Lines(line)
                Else
                    Me.queryCommand = ""
                End If
                'queryCommand = getLine(Me._inputTextBox)

                ' Send the string to the instrument. Receive is invoked by the timer.
                Me.send(Me.queryCommand, False)

                ' TO_DO: this is a perfect place for setting a service request before read and
                ' letting the service request invoke the next read.

                ' clear the command
                Me.queryCommand = ""

                ' Turn command console back on and display response.
                Me._abortButton.Enabled = False
                Me._inputTextBox.Enabled = True
                Me._inputTextBox.Focus()

            Case System.Windows.Forms.Keys.Back ' backspace key
                If queryCommand.Length < 2 Then
                    Me.queryCommand = ""
                Else
                    Me.queryCommand = queryCommand.Substring(0, queryCommand.Length - 1)
                End If

            Case Else ' normal key
                ' Stash key in buffer.
                Me.queryCommand &= Chr(KeyAscii)
        End Select

        eventArgs.KeyChar = Chr(KeyAscii)
        If KeyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub

    ''' <summary>
    ''' _loads the and run script.
    ''' </summary>
    ''' <param name="scriptName">Name of the script.</param>
    ''' <param name="filePath">The file path.</param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _LoadAndRunScript(ByVal scriptName As String, ByVal filePath As String)

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If Me.TspSystem.IsDeviceOpen Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Loading script '{0}';. from '{1}'.", scriptName, filePath)
                Me.TspSystem.Device.ScriptManager.ScriptNameSetter(scriptName)
                Me.TspSystem.Device.ScriptManager.FilePath = filePath
                Me.TspSystem.Device.ScriptManager.LoadScriptFile(True, True, Me._retainCodeOutlineToggle.Checked)
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Script '{0}' loaded;. from '{1}'.", scriptName, filePath)
                listUserScripts()
                Me.TspSystem.Device.ScriptManager.RunScript(TimeSpan.FromMilliseconds(3000))
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Script '{0}' run okay;. ", scriptName, filePath)

            End If

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred loading or running script;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by _loadAndRunButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub _LoadAndRunButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _loadAndRunButton.Click

        Me._loadAndRunScript(Me._scriptNameTextBox.Text, Me._tspScriptSelector.FilePath)

    End Sub

    ''' <summary> Event handler. Called by _loadFunctionButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _LoadFunctionButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _loadFunctionButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Dim functionCode As String
            functionCode = Me._functionCodeTextBox.Text.Replace(vbCrLf, Space(1))
            If Me.TspSystem.IsSessionOpen AndAlso Not String.IsNullOrWhiteSpace(functionCode) Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Loading function code..;. ")
                Me.TspSystem.Device.Session.WriteLine(functionCode)
                Me.TspSystem.Device.StatusSubsystem.ReportVisaOperationOkay("loading function code;. ")
            End If

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred loading function;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by _loadScriptButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _LoadScriptButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _loadScriptButton.Click

        If Me._scriptNameTextBox.Text.IncludesIllegalFileCharacters() Then
            Return
        End If
        ' Turn on the form hourglass
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Try

            Me.TspSystem.Device.ScriptManager.ScriptNameSetter(Me._scriptNameTextBox.Text)
            Me.TspSystem.Device.ScriptManager.FilePath = Me._tspScriptSelector.FilePath
            If Me.TspSystem.IsDeviceOpen Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Loading script '{0}';. from '{1}'.",
                                           Me.TspSystem.Device.ScriptManager.Name, Me.TspSystem.Device.ScriptManager.FilePath)
                Me.TspSystem.Device.ScriptManager.LoadScriptFile(True, True, Me._retainCodeOutlineToggle.Checked)
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Script '{0}' loaded;. from '{1}'.",
                                           Me.TspSystem.Device.ScriptManager.Name, Me.TspSystem.Device.ScriptManager.FilePath)
                listUserScripts()
            End If

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred loading script;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try


    End Sub

    ''' <summary> Event handler. Called by _MessagesBox for property changed events. Updates the tab
    ''' caption. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Property Changed event information. </param>
    Private Sub _MessagesBox_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _messagesBox.PropertyChanged
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me._MessagesBox_PropertyChanged), New Object() {sender, e})
        End If
        If e.PropertyName = "Caption" Then
            Me._tabControl.TabPages.Item(MainTabsIndex.MessagesTabIndex).Text = Me._messagesBox.Caption()
        End If
    End Sub

#If False Then
    ''' <summary> Locates VISA resources. </summary>
    Private Sub LocateVisaResources()

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceId, "Locating VISA resources...;. ")

            Me._clearInterfaceButton.Enabled = False
            Me._ResourceSelectorConnector.Enabled = False

            ' allow some events to occur for refreshing the display.
            Application.DoEvents()

            Dim interfaces As String() = {}
            If VisaNS.ResourceManager.GetLocalManager.TryFindInterfaces(interfaces) Then

                ' allow some events to occur for refreshing the display.
                Application.DoEvents()

                Dim instruments As String() = {}
                If VisaNS.ResourceManager.GetLocalManager.TryFindInstruments(instruments) Then

                    ' allow some events to occur for refreshing the display.
                    Application.DoEvents()

                    ' resources must be present for enabling the interface clear otherwise it times out.
                    Me._clearInterfaceButton.Enabled = True
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceId, "Ready;. ")
                Else
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceId, "No Instruments are connected at address;. ")
                End If
            Else
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceId, "No interfaces found;. ")
            End If

        Catch

            Throw

        Finally

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by _refreshVisaButton for click events. 
    ''' Tries finding the resources after connecting the instrument.
    '''           </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _RefreshVisaButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _refreshVisaButton.Click
        Me.locateVisaResources()
    End Sub
#End If

    ''' <summary> Event handler. Called by _refreshUserScriptsListButton for click events. Updates the
    ''' list of user scripts. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _RefreshUserScriptsListButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _refreshUserScriptsListButton.Click
        Me.listUserScripts()
    End Sub

    ''' <summary> Event handler. Called by _removeScriptButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _RemoveScriptButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _removeScriptButton.Click

        ' Turn on the form hourglass
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try

            If Me._userScriptsList.SelectedItems.Count > 0 Then
                For Each scriptName As String In Me._userScriptsList.SelectedItems
                    If Me.TspSystem.IsSessionOpen Then
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Removing script '{0}'..;. ", scriptName)
                        Me.TspSystem.Device.ScriptManager.RemoveScript(scriptName)
                        Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Removed script '{0}';. ", scriptName)
                    End If
                Next
                listUserScripts()
            End If

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred removing script;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by _resetLocalNodeButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ResetLocalNodeButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _resetLocalNodeButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If Me.TspSystem IsNot Nothing AndAlso Me.TspSystem.IsDeviceOpen Then

                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Resetting local node..;. ")
                send("localnode.reset() ", True)
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Local node was reset;. ")

                If Not Me.TspSystem.Device.StatusSubsystem.ProcessExecutionStateEnabled Then
                    ' read execution state explicitly, because session events are disabled.
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Updating instrument state..;. ")
                    Me.TspSystem.Device.StatusSubsystem.ReadExecutionState()

                End If

                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Clearing error queue..;. ")
                send("localnode.errorqueue.clear() ", True)
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Error queue cleared;. ")

                ' update the instrument state.
                If Not Me.TspSystem.Device.StatusSubsystem.ProcessExecutionStateEnabled Then
                    ' read execution state explicitly, because session events are disabled.
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Updating instrument state..;. ")
                    Me.TspSystem.Device.StatusSubsystem.ReadExecutionState()
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument state updated;. ")
                End If

            End If

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred setting prompts;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by _runScriptButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _RunScriptButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _runScriptButton.Click

        Try

            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me.TspSystem.Device.ScriptManager.ScriptNameSetter(Me._scriptNameTextBox.Text)
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Running script '{0}';. ", Me.TspSystem.Device.ScriptManager.Name)
            Me.TspSystem.Device.ScriptManager.RunScript(TimeSpan.FromMilliseconds(3000))
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Script '{0}' ran okay;. ", Me.TspSystem.Device.ScriptManager.Name)

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred running;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by _showErrorsCheckBox for check state changed events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ShowErrorsCheckBox_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _showErrorsCheckBox.CheckStateChanged

        Try
            ' Turn on the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            If Me._showErrorsCheckBox.Enabled AndAlso Me.TspSystem IsNot Nothing AndAlso Me.TspSystem.IsDeviceOpen Then
                If Me.TspSystem.Device.StatusSubsystem.QueryShowErrors() <> Me._showErrorsCheckBox.Checked Then
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Toggling showing errors..;. ")
                    Me.TspSystem.Device.StatusSubsystem.WriteShowErrors(Me._showErrorsCheckBox.Checked)
                    If Not Me.TspSystem.Device.StatusSubsystem.ShowErrors.HasValue Then
                        Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed toggling showing errors--value not set;. ")
                    ElseIf Me.TspSystem.Device.StatusSubsystem.ShowErrors.Value <> Me._showErrorsCheckBox.Checked Then
                        Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed toggling showing errors--incorrect value;. ")
                    End If
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Showing errors: {0};. ",
                                               IIf(Me.TspSystem.Device.StatusSubsystem.ShowErrors.Value, "ON", "OFF"))
                End If
            End If

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred showing errors;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by _showPromptsCheckBox for check state changed events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ShowPromptsCheckBox_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _showPromptsCheckBox.CheckStateChanged

        Try

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If Me._showPromptsCheckBox.Enabled AndAlso Me.TspSystem IsNot Nothing AndAlso Me.TspSystem.IsDeviceOpen Then
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Toggling showing prompts..;. ")

                If Me.TspSystem.Device.StatusSubsystem.QueryShowPrompts() <> Me._showPromptsCheckBox.Checked Then
                    Me.TspSystem.Device.StatusSubsystem.WriteShowPrompts(Me._showPromptsCheckBox.Checked)

                    If Not Me.TspSystem.Device.StatusSubsystem.ShowPrompts.HasValue Then
                        Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed toggling showing Prompts--value not set;. ")
                    ElseIf Me.TspSystem.Device.StatusSubsystem.ShowPrompts.Value <> Me._showPromptsCheckBox.Checked Then
                        Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed toggling showing Prompts--incorrect value;. ")
                    End If
                    Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Showing Prompts: {0};. ",
                                               IIf(Me.TspSystem.Device.StatusSubsystem.ShowPrompts.Value, "ON", "OFF"))
                End If
            End If

        Catch ex As Exception

            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred showing prompts;. {0}", ex.ToFullBlownString)

        Finally

            ' Turn off the form hourglass
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by _tspScriptSelector for selected changed events. </summary>
    ''' <param name="Sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _TspScriptSelector_SelectedChanged(ByVal Sender As System.Object, ByVal e As EventArgs) Handles _tspScriptSelector.SelectedChanged
        If Not String.IsNullOrWhiteSpace(Me._tspScriptSelector.FileTitle) Then
            Me._scriptNameTextBox.Text = Me._tspScriptSelector.FileTitle.Replace(".", "_")
            Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Selected Script;. '{0}'.", Me._scriptNameTextBox.Text)
        End If
    End Sub

#End Region

#Region " MESSAGE MANAGERS "

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overrides Sub DisplayMessage(value As TraceMessage)
        If value IsNot Nothing AndAlso MyBase.ShouldShow(value.EventType) Then
            Me._messagesBox.AddMessage(value)
        End If
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected Overrides Sub DisplaySynopsis(value As String)
        Me._statusPanel.Text = value
    End Sub

    ''' <summary> Gets the trace show level. The trace message is displayed if the trace level is lower
    ''' than this value. </summary>
    ''' <value> The trace show level. </value>
    Protected Overrides ReadOnly Property TraceShowLevel As System.Diagnostics.TraceEventType
        Get
            Return My.MyLibrary.MyLog.TraceLevel
        End Get
    End Property

#End Region

#Region " TRACE MESSAGE LOGGER "

    ''' <summary> Event handler. Called by instrument panels for trace message available events. </summary>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Trace message event information. </param>
    Protected Sub OnTraceMessageAvailable1(sender As Object, e As TraceMessageEventArgs) Implements ITraceMessageLogger.OnTraceMessageAvailable
        MyBase.OnTraceMessageAvailable(sender, e)
    End Sub

    ''' <summary> Displays a trace message described by value. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overrides Sub DisplayTraceMessage(value As TraceMessage)
        MyBase.DisplayTraceMessage(value)
        Me.LogMessage(value)
    End Sub

    ''' <summary> Gets the trace log level. </summary>
    ''' <value> The trace log level. </value>
    Protected ReadOnly Property TraceLogLevel As System.Diagnostics.TraceEventType Implements ITraceMessageLogger.TraceLogLevel
        Get
            Return My.MyLibrary.MyLog.TraceLevel
        End Get
    End Property

    ''' <summary> Determines if the trace event type should be logged. </summary>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> if the trace event type should be logged. </returns>
    Protected Function ShouldLog(ByVal eventType As TraceEventType) As Boolean Implements ITraceMessageLogger.ShouldLog
        Return eventType <= Me.TraceLogLevel
    End Function

    ''' <summary> Logs the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Sub LogMessage(value As TraceMessage)
        If value IsNot Nothing AndAlso Me.ShouldLog(value.EventType) Then
            My.Application.Log.TraceSource.TraceEvent(value.EventType, value.Id, value.Details)
            ' My.Application.Log.WriteLogEntry(value)
            My.Application.DoEvents()
        End If
    End Sub

#End Region

End Class