﻿Imports NationalInstruments.VisaNS
Imports isr.Core.Controls.TextBoxExtensions
Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.EnumExtensions
Imports isr.Core.Agnostic.EscapeSequencesExtensions
Imports isr.Core.Agnostic.ExceptionExtensions
Imports System.ComponentModel
''' <summary> Message Based VISA Panel. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/07/2005" by="David" revision="2.0.2597.x"> Created. </history>
<System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")>
Public Class MessageBasedVisaPanel
#Const designMode1 = True
#Region " BASE FROM WRAPPER "
    ' Designing requires changing the condition to True.
#If designMode Then
    Inherits TraceObserverUserFormBaseWrapper
#Else
    Inherits TraceObserverUserFormBase
#End If
#End Region

#Region " CONSTRUCTION + CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()

        ' Initialize user components that might be affected by resize or paint actions
        'onInitialize()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' allow connecting the resource.
        Me._instrumentChooser = _InterfacePanel.InstrumentChooser
        Me._InterfacePanel.InstrumentChooser.Connectible = True

        ' Add any initialization after the InitializeComponent() call
        Me._InterfacePanel.DataBindings.Add(New System.Windows.Forms.Binding("InterfaceResourceName", My.MySettings.Default,
                                                                             "InterfaceName", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me._InterfacePanel.InterfaceResourceName = My.Settings.InterfaceName
        Me._InterfacePanel.InstrumentChooser.DataBindings.Add(New System.Windows.Forms.Binding("SelectedResourceName", My.MySettings.Default,
                                                                             "ResourceName", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me._InterfacePanel.InstrumentChooser.SelectedResourceName = My.Settings.ResourceName

#If designMode Then
        Debug.Assert(False, "Illegal call; reset design mode")
#End If

    End Sub

    ''' <summary> Cleans up managed components. </summary>
    ''' <remarks> Use this method to reclaim managed resources used by this class. </remarks>
    Private Sub OnDisposeManagedResources()
        If Me.Session IsNot Nothing Then
            Me.CloseInstrumentSession()
        End If
        Me._instrumentChooser = Nothing
    End Sub

#End Region

#Region " MODULE DATA MEMBERS "

    ''' <summary> Gets reference to the
    ''' <see cref="Visa.Session">message based session</see>.
    ''' Was Ivi.Visa.Interop.IGpib. </summary>
    ''' <value> The session. </value>
    Private Property Session As Visa.Session

    ''' <summary> Gets the last message that was received from the instrument. </summary>
    ''' <value> A Buffer for receive data. </value>
    Private Property ReceiveBuffer As String

    ''' <summary> Gets the last message that was sent to the instrument. </summary>
    ''' <value> A Buffer for transmit data. </value>
    Private Property TransmitBuffer As String

    ''' <summary> Gets the last visa
    ''' <see cref="NationalInstruments.VisaNS.VisaStatusCode">result code</see>. </summary>
    ''' <value> The visa result. </value>
    Private Property VisaResult As NationalInstruments.VisaNS.VisaStatusCode

#End Region

#Region " CONNECT / DISCONNECT "

    ''' <summary> Gets the condition for determining if the instrument is connected. </summary>
    ''' <value> <c>True</c> if a VISA session exists. </value>
    Private ReadOnly Property IsOpen() As Boolean
        Get
            Return Me.Session IsNot Nothing
        End Get
    End Property

    ''' <summary> Opens a visa session to the instrument. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub OpenInstrumentSession()

        Dim wasOpen As Boolean
        wasOpen = Me.isOpen

        ' close the device if open
        If wasOpen Then
            Me.CloseInstrumentSession()
        End If

        Try

            ' clear values
            Me.receiveBuffer = String.Empty
            Me.transmitBuffer = String.Empty
            Me.visaResult = NationalInstruments.VisaNS.VisaStatusCode.Success

            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Initializing driver;. ")
            Dim resourceName As String = ""
            resourceName = Me._InterfacePanel.InstrumentChooser.SelectedResourceName

            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Opening a VISA Session to {0};. ", resourceName)
            Me.Session = New Visa.Session(resourceName) With {.Timeout = 3000}

            If Me.isOpen Then

                visaResult = Me.Session.LastStatus
                If visaResult <> NationalInstruments.VisaNS.VisaStatusCode.Success Then
                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Visa operation failed {0};. Details {1}.", visaResult.Description(), VisaException.BuildVisaStatusDetails(visaResult))
                End If

                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Clearing the device;. ")
                Me.Session.Clear()

                visaResult = Me.Session.LastStatus
                If visaResult <> NationalInstruments.VisaNS.VisaStatusCode.Success Then
                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Visa Clear operation failed {0};. Details {1}.", visaResult.Description(), VisaException.BuildVisaStatusDetails(visaResult))
                End If

                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Connected to {0};. ", Me.Session.ResourceName)

            Else

                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Failed opening a session to {0};. ", resourceName)

            End If

        Catch ex As Exception
            ex.Data.Add("@isr", "Connect Error")
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId, ex.ToString)
            isr.Core.MyMessageBox.ShowDialog(Me, ex)
            Try
                Me.CloseInstrumentSession()
            Finally
            End Try

        Finally

            ' check if ExecutionState changed.
            If wasOpen <> Me.isOpen Then
                ' if so, alert on connection changed.
                Me.OnConnectionChanged()
            End If

            Me._InterfacePanel.InstrumentChooser.IsConnected = Me.isOpen

        End Try

    End Sub

    ''' <summary> Closes the device. Performs the necessary termination functions, which will cleanup
    ''' and disconnect the interface connection. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub CloseInstrumentSession()

        Dim wasOpen As Boolean
        wasOpen = Me.isOpen
        stopPollTimer()

        Try

            If Me.isOpen Then

                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Disconnecting Instrument;. ")

                If _SendDisconnectCommandsCheckBox.Checked Then

                    If Me._DisconnectCommandsTextBox.Lines.Length > 0 Then
                        For Each Command As String In Me._DisconnectCommandsTextBox.Lines
                            Command = Command.Trim
                            If Not String.IsNullOrWhiteSpace(Command) Then
                                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Sending commands;. {0}", Command)
                                Me.Session.Write(Command)
                                visaResult = Me.Session.LastStatus
                                If visaResult <> NationalInstruments.VisaNS.VisaStatusCode.Success Then
                                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Visa commands failed {0};. Details {1}.", visaResult.Description(), VisaException.BuildVisaStatusDetails(visaResult))
                                End If
                            End If
                        Next
                    End If
                End If


                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Clearing the device;. ")
                Me.Session.Clear()

                visaResult = Me.Session.LastStatus
                If visaResult <> NationalInstruments.VisaNS.VisaStatusCode.Success Then
                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Visa Clear operation failed {0};. Details {1}.", visaResult.Description(), VisaException.BuildVisaStatusDetails(visaResult))
                End If

                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Disabling service request events if any;. ")
                Me.DisableServiceRequestEventHandler()

                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Ending the VISA session;. ")
                Me.Session.Dispose()
                Try
                    ' Trying to null the session raises an ObjectDisposedException 
                    ' if session service request handler was not released. 
                    Me.Session = Nothing
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                End Try

                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Session closed;. ")

            End If

        Catch

            Throw

        Finally

            ' check if ExecutionState changed.
            If wasOpen <> Me.isOpen Then
                ' if so, alert on connection changed.
                Me.OnConnectionChanged()
            End If
            Me._InterfacePanel.InstrumentChooser.IsConnected = Me.isOpen

        End Try

    End Sub

    ''' <summary> Updates the connection status. </summary>
    Private Sub OnConnectionChanged()

        If Not Me.isOpen Then
            Me.stopPollTimer()
        End If

        Me._SendButton.Enabled = Me.isOpen
        Me._ReadStatusRegisterButton.Enabled = Me.isOpen
        Me._SendComboCommandButton.Enabled = Me.isOpen
        Me._sendReceiveControlPanel.Enabled = Me.isOpen

        ' enable by reading SRQ
        Me._ReceiveButton.Enabled = False

        Me._StatusRegisterLabel.Text = String.Empty

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all
    ''' publishers. </summary>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
    ''' event data. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Me._StatusToolStripLabel.Text = "CLOSING."
            Me.CloseInstrumentSession()
            Me.stopPollTimer()
            My.Application.Destroy()
            If e IsNot Nothing Then
                e.Cancel = False
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToString)
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Application.DoEvents()
            MyBase.OnClosing(e)
        End Try
    End Sub

    ''' <summary> Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event. </summary>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            ' Add any initialization after the InitializeComponent() call
            ' Me.OnInstantiate()

            ' set the form caption
            Me.Text = My.Application.Info.ProductName & " release " & My.Application.Info.Version.ToString

            ' build the navigator tree.
            ' Me.BuildNavigatorTreeView()

            ' default to center screen.
            Me.CenterToScreen()

            MyBase.OnLoad(e)

        Catch ex As Exception

            ex.Data.Add("@isr", "Exception loading the form.")
            If DialogResult.Abort = isr.Core.MyMessageBox.ShowDialogAbortIgnore(Me, ex, MessageBoxIcon.Error) Then
                Application.Exit()
            End If

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default
            Trace.CorrelationManager.StopLogicalOperation()

        End Try

    End Sub

    ''' <summary> Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event. </summary>
    ''' <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Protected Overrides Sub OnShown(e As System.EventArgs)

        Try

            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' allow form rendering time to complete: process all messages currently in the queue.
            Application.DoEvents()

            ' display the log file location
            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Logging to: '{0}'", My.Application.Log.DefaultFileLogWriter.CustomLocation)

            ' allow form rendering time to complete: process all messages currently in the queue.
            Application.DoEvents()

            If Not Me.DesignMode Then
                Me._InterfacePanel.DisplayInterfaceNames()
                Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Ready to open Visa Session;. ")
                ' select the first item
                If Me._CommandsComboBox.Items.Count > 0 Then
                    Me._CommandsComboBox.SelectedIndex = 0
                End If
            End If

            MyBase.OnShown(e)

        Catch ex As Exception

            ex.Data.Add("@isr", "Exception showing the form.")
            My.MyLibrary.MyLog.TraceSource.TraceEvent(ex, My.MyApplication.TraceEventId)
            If DialogResult.Abort = isr.Core.MyMessageBox.ShowDialogAbortIgnore(Me, ex, MessageBoxIcon.Error) Then
                Application.Exit()
            End If

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default
            Trace.CorrelationManager.StopLogicalOperation()

        End Try

    End Sub


#End Region

#Region " INTERFACE EVENT HANDLERS "

    Private WithEvents _PollTimer As Windows.Forms.Timer

    ''' <summary> Event handler. Called by _InterfacePanel for clear events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _InterfacePanel_Clear(ByVal sender As Object, ByVal e As System.EventArgs)

        Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Clearing the device;. ")
        Me.Session.Clear()

        visaResult = Me.Session.LastStatus
        If visaResult <> NationalInstruments.VisaNS.VisaStatusCode.Success Then
            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Visa Clear operation failed {0};. Details {1}.", visaResult.Description(), VisaException.BuildVisaStatusDetails(visaResult))
        End If

    End Sub

    ''' <summary> Event handler. Called by _InterfacePanel for connect events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _InterfacePanel_Connect(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        ' exception handling is done in the resource connector.
        Me.OpenInstrumentSession()
    End Sub

    ''' <summary> Event handler. Called by _InterfacePanel for disconnect events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _InterfacePanel_Disconnect(ByVal sender As Object, ByVal e As System.EventArgs)
        ' exception handling is done in the resource connector.
        Me.CloseInstrumentSession()
    End Sub

    Private WithEvents _InstrumentChooser As isr.IO.Visa.Instrument.ResourceSelectorConnectorBase

    Private Sub _InstrumentChooser_Clear(ByVal sender As Object, ByVal e As System.EventArgs) Handles _instrumentChooser.Clear
        Me._InterfacePanel_Clear(sender, e)
    End Sub

    Private Sub _InstrumentChooser_Connect(ByVal sender As Object, ByVal e As System.EventArgs) Handles _instrumentChooser.Connect
        Me._InterfacePanel_Connect(sender, e)
    End Sub

    Private Sub _InstrumentChooser_Disconnect(ByVal sender As Object, ByVal e As System.EventArgs) Handles _instrumentChooser.Disconnect
        Me._InterfacePanel_Disconnect(sender, e)
    End Sub

    ''' <summary> Event handler. Called by _InterfacePanel for property changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _InterfacePanel_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _InterfacePanel.PropertyChanged
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me._InterfacePanel_PropertyChanged), New Object() {sender, e})
        End If
        Try
            Select Case e.PropertyName
                Case "InterfaceResourceName"
                    Me._StatusToolStripLabel.Text = "Selected " & Me._InterfacePanel.InterfaceResourceName
                Case "IsOpen"

                    If Me._InterfacePanel.IsInterfaceOpen Then
                        Me._StatusToolStripLabel.Text = "Select Instrument Resource"
                        'AddHandler Me._InterfacePanel.InstrumentChooser.Clear, AddressOf Me._InterfacePanel_Clear
                        'AddHandler Me._InterfacePanel.InstrumentChooser.Connect, AddressOf Me._InterfacePanel_Connect
                        'AddHandler Me._InterfacePanel.InstrumentChooser.Disconnect, AddressOf Me._InterfacePanel_Disconnect
                    Else
                        Me._StatusToolStripLabel.Text = "Interface Closed"
                        'RemoveHandler Me._InterfacePanel.InstrumentChooser.Clear, AddressOf Me._InterfacePanel_Clear
                        'RemoveHandler Me._InterfacePanel.InstrumentChooser.Connect, AddressOf Me._InterfacePanel_Connect
                        'RemoveHandler Me._InterfacePanel.InstrumentChooser.Disconnect, AddressOf Me._InterfacePanel_Disconnect
                    End If
                Case "ResourceName"
                    Me._StatusToolStripLabel.Text = "Selected " & Me._InterfacePanel.InstrumentChooser.SelectedResourceName

            End Select
        Catch ex As Exception
            Me.OnTraceMessageAvailable(TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception handling property changed Event;. Failed property {0}. {1}", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

    ''' <summary> Event handler. Called by _InterfacePanel for trace message available events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Trace message event information. </param>
    Private Sub _InterfacePanel_TraceMessageAvailable(ByVal sender As Object, ByVal e As TraceMessageEventArgs) Handles _InterfacePanel.TraceMessageAvailable
        MyBase.OnTraceMessageAvailable(sender, e)
    End Sub

#End Region

#Region " SERVICE REQUEST MANAGEMENT "

    Private _ServiceRequestBits As Integer

    ''' <summary> Gets or sets the service request bits. </summary>
    ''' <value> The service request bits. </value>
    Private Property ServiceRequestBits As Integer
        Get
            Return Me._ServiceRequestBits
        End Get
        Set(value As Integer)
            If value <> Me.ServiceRequestBits Then
                Me._ServiceRequestBits = value
                Me._StatusRegisterLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture,
                                                             "0x{0:X}", CByte(value And &HFF))
                Dim mAV As Integer = CInt(Me._MessageAvailableBitsNumeric.Value)
                Me.MessageAvailable = (value And MAV) <> 0
            End If
        End Set
    End Property

    Private _MessageAvailable As Boolean

    ''' <summary> Gets or sets the message available sentinel. </summary>
    ''' <value> The message available. </value>
    Private Property MessageAvailable As Boolean
        Get
            Return Me._MessageAvailable
        End Get
        Set(value As Boolean)
            If value <> Me.MessageAvailable OrElse value <> (Me._ReadManualRadioButton.Checked AndAlso value) Then
                Me._MessageAvailable = value
                Me._ReceiveButton.Enabled = Me._ReadManualRadioButton.Checked AndAlso value
            End If
        End Set
    End Property

#End Region

#Region " TIMER HANDLERS "

    ''' <summary> Stops poll timer. </summary>
    Private Sub StopPollTimer()

        If Me._pollTimer IsNot Nothing Then
            Me._pollTimer.Enabled = False
            RemoveHandler Me._pollTimer.Tick, AddressOf Me.onPollTimerTick
            Me._pollTimer.Dispose()
            Me._pollTimer = Nothing
        End If

        ' wait for timer to terminate all is actions
        Dim timer As System.Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
        Do Until timer.ElapsedMilliseconds > 200
            Windows.Forms.Application.DoEvents()
            Threading.Thread.Sleep(10)
        Loop

        If Me._PollRadioButton.Checked Then
            Me._ReadManualRadioButton.Checked = True
        End If

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Event handler. Called by exitButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub ExitButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        Me.Close()
    End Sub

    ''' <summary> Reads status register. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub ReadStatusRegister()

        Try

            Me._StatusRegisterLabel.Text = ""
            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Reading SRQ;. ")
            Me.ServiceRequestBits = CType(Me.Session.ReadStatusByte, Visa.ServiceRequests)

            visaResult = Me.Session.LastStatus
            If visaResult <> NationalInstruments.VisaNS.VisaStatusCode.Success Then
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Visa read status failed {0};. Details {1}.", visaResult.Description(), VisaException.BuildVisaStatusDetails(visaResult))
            End If

        Catch ex As Exception
            ex.Data.Add("@isr", "Exception reading status register.")
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId, ex.ToString)
            Core.MyMessageBox.ShowDialog(Me, ex)
        End Try

    End Sub

    ''' <summary> Event handler. Called by readStatusRegisterButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub ReadStatusRegisterButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _ReadStatusRegisterButton.Click
        Me.readStatusRegister()
    End Sub

    ''' <summary> Receives this object. </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub Receive()

        Try
            Me.OnTraceMessageAvailable(TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Receiving data;. ")

            receiveBuffer = Me.Session.ReadString
            If Not String.IsNullOrWhiteSpace(receiveBuffer) Then
                receiveBuffer = receiveBuffer.InsertCommonEscapeSequences
            End If

            visaResult = Me.Session.LastStatus
            If visaResult <> NationalInstruments.VisaNS.VisaStatusCode.Success Then
                Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Visa read failed {0};. Details {1}.", visaResult.Description(), VisaException.BuildVisaStatusDetails(visaResult))
            End If

            If Not String.IsNullOrWhiteSpace(receiveBuffer) Then
                _OutputTextBox.Append(MessageBasedVisaPanel.BuildTimeStampLine(receiveBuffer))
                Me._ReceiveButton.Enabled = False
                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Received '{0}'.", receiveBuffer)
            End If

            ' update the status register information
            Me.readStatusRegister()

        Catch ex As Exception
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId, ex.ToString)
            Core.MyMessageBox.ShowDialog(Me, ex)
        End Try

    End Sub

    ''' <summary> Event handler. Called by receiveButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub ReceiveButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _ReceiveButton.Click
        Me.receive()
    End Sub

    ''' <summary> Send this message. </summary>
    ''' <param name="value"> The value. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub Send(ByVal value As String)

        Try

            transmitBuffer = value.ReplaceCommonEscapeSequences
            If Not String.IsNullOrWhiteSpace(transmitBuffer) Then

                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Sending command='{0}'.", transmitBuffer)

                Me.Session.Write(transmitBuffer)

                visaResult = Me.Session.LastStatus
                If visaResult <> NationalInstruments.VisaNS.VisaStatusCode.Success Then
                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Visa send failed {0};. Details {1}.", visaResult.Description(), VisaException.BuildVisaStatusDetails(visaResult))
                End If

            End If

        Catch ex As Exception
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId, ex.ToString)
            Core.MyMessageBox.ShowDialog(Me, ex)
        End Try
    End Sub

    ''' <summary> Event handler. Called by sendButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    Private Sub SendButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _SendButton.Click
        Me.send(_InputTextBox.Text.Trim)
    End Sub

    ''' <summary> Event handler. Called by sendComboCommandButton for click events. </summary>
    ''' <param name="eventSender"> The event sender. </param>
    ''' <param name="eventArgs">   Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub SendComboCommandButton_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _SendComboCommandButton.Click

        Try

            transmitBuffer = Me._CommandsComboBox.Text.Trim
            If Not String.IsNullOrWhiteSpace(transmitBuffer) Then

                Me.OnTraceMessageAvailable(TraceEventType.Information, My.MyLibrary.TraceEventId, "Sending command='{0}'.", transmitBuffer)

                Me.Session.Write(transmitBuffer)

                visaResult = Me.Session.LastStatus
                If visaResult <> NationalInstruments.VisaNS.VisaStatusCode.Success Then
                    Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Sending failed {1};. Sent {0}. Details {2}.", transmitBuffer, visaResult.Description(), VisaException.BuildVisaStatusDetails(visaResult))
                End If

            End If

        Catch ex As Exception
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId, ex.ToString)
            Core.MyMessageBox.ShowDialog(Me, ex)
        End Try

    End Sub

    ''' <summary> Event handler. Called by _readManualRadioButton for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ReadManualRadioButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ReadManualRadioButton.CheckedChanged
        If Not Me._ReadManualRadioButton.Checked Then
            Exit Sub
        End If
        Try
            Me._ReceiveButton.Enabled = False
            Me._ReadStatusRegisterButton.Enabled = True
            If Me.Session IsNot Nothing Then
                RemoveHandler Me.Session.ServiceRequest, AddressOf Me.OnSessionServiceRequest
            End If
            stopPollTimer()
        Catch ex As Exception
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId, ex.ToString)
            Core.MyMessageBox.ShowDialog(Me, ex)
        End Try

    End Sub

    ''' <summary> Event handler. Called by _pollRadioButton for checked changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _PollRadioButton_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _PollRadioButton.CheckedChanged
        If Not Me._PollRadioButton.Checked Then
            Exit Sub
        End If
        Try
            Me._ReceiveButton.Enabled = False
            Me._ReadStatusRegisterButton.Enabled = False
            If Me.Session IsNot Nothing Then
                Me.DisableServiceRequestEventHandler()
            End If
            Me._PollTimer = New Windows.Forms.Timer With {.Enabled = False, .Interval = CInt(Me._PollIntervalNumericUpDown.Value)}
            AddHandler Me._pollTimer.Tick, AddressOf Me.onPollTimerTick
            Me._pollTimer.Enabled = True
        Catch ex As Exception
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId, ex.ToString)
            Core.MyMessageBox.ShowDialog(Me, ex)
        End Try

    End Sub

    ''' <summary> Raises the system. event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Sub OnPollTimerTick(ByVal sender As Object, ByVal e As System.EventArgs)
        Static pollTimerLocker As New Object
        SyncLock pollTimerLocker
            Me.readStatusRegister()
            If Me.MessageAvailable Then
                Me.receive()
            End If
        End SyncLock

    End Sub

    ''' <summary> Raises the message based session event. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub OnSessionServiceRequest(ByVal sender As Object, ByVal e As MessageBasedSessionEventArgs)
        Dim mbs As MessageBasedSession = CType(sender, MessageBasedSession)
        If mbs IsNot Nothing Then
            Try
                Me.readStatusRegister()
                If Me.MessageAvailable Then
                    Me.receive()
                End If
            Catch ex As Exception
                ex.Data.Add("@isr", "failed service request")
                My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId, ex.ToString)
                If DialogResult.Abort = isr.Core.MyMessageBox.ShowDialogAbortIgnore(Me, ex, MessageBoxIcon.Error) Then
                    Me.Close()
                End If
            End Try
        Else
            Me.OnTraceMessageAvailable(TraceEventType.Warning, My.MyLibrary.TraceEventId, "Sender is not a message-based session")
        End If
    End Sub

    Private _ServiceRequestEventHandlerEnabled As Boolean

    ''' <summary> Enable service request event handler. </summary>
    Public Sub EnableServiceRequestEventHandler()
        If Me.isOpen AndAlso Not Me._ServiceRequestEventHandlerEnabled Then
            Me._ServiceRequestEventHandlerEnabled = True
            AddHandler Me.Session.ServiceRequest, AddressOf Me.OnSessionServiceRequest
            Me.Session.EnableEvent(MessageBasedSessionEventType.ServiceRequest, EventMechanism.Handler)
        End If
    End Sub

    ''' <summary> Disable service request event handler. </summary>
    Public Sub DisableServiceRequestEventHandler()
        If Me.isOpen AndAlso Me._ServiceRequestEventHandlerEnabled Then
            Me._ServiceRequestEventHandlerEnabled = False
            Me.Session.DisableEvent(MessageBasedSessionEventType.ServiceRequest, EventMechanism.Handler)
            RemoveHandler Me.Session.ServiceRequest, AddressOf Me.OnSessionServiceRequest
        End If
    End Sub

    ''' <summary> Event handler. Called by _serviceRequestReceiveOptionRadioButton for checked changed
    ''' events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub _ServiceRequestReceiveOptionRadioButton_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ServiceRequestReceiveOptionRadioButton.CheckedChanged
        If Not Me._ServiceRequestReceiveOptionRadioButton.Checked Then
            Exit Sub
        End If
        Try
            If Me._pollTimer IsNot Nothing Then
                Me._pollTimer.Enabled = False
                RemoveHandler Me._pollTimer.Tick, AddressOf Me.onPollTimerTick
                Me._pollTimer.Dispose()
                Me._pollTimer = Nothing
            End If
            If Me.Session IsNot Nothing Then
                Me.EnableServiceRequestEventHandler()
                If Not String.IsNullOrWhiteSpace(Me._SreCommandComboBox.Text) Then
                    Me.send(Me._SreCommandComboBox.Text & "\n")
                End If
                Me._ReceiveButton.Enabled = False
                Me._ReadStatusRegisterButton.Enabled = False
            End If
        Catch ex As Exception
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, My.MyApplication.TraceEventId, ex.ToString)
            Core.MyMessageBox.ShowDialog(Me, ex)
        End Try

    End Sub

    ''' <summary> Event handler. Called by _pollIntervalNumericUpDown for value changed events. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub _PollIntervalNumericUpDown_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _PollIntervalNumericUpDown.ValueChanged

        If Me._pollTimer IsNot Nothing Then
            Me._pollTimer.Interval = CInt(Me._PollIntervalNumericUpDown.Value)
        End If

    End Sub

#End Region

#Region " TRACE MESSAGE OBSERVER "

    ''' <summary> Builds a message for the message log appending a line. </summary>
    ''' <param name="message"> Specifies the message to append. </param>
    ''' <returns> The time stamped message. </returns>
    Friend Shared Function BuildTimeStampLine(ByVal message As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:HH:mm:ss.fff} {1}{2}", DateTime.Now, message, Environment.NewLine)
    End Function

    ''' <summary> Displays the <paramref name="value">message</paramref>. </summary>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Protected Overrides Sub DisplayMessage(value As TraceMessage)
        If value IsNot Nothing AndAlso MyBase.ShouldShow(value.EventType) Then
            Me._MessagesTextBox.Prepend(MessageBasedVisaPanel.BuildTimeStampLine(value.Details))
        End If
    End Sub

    ''' <summary> Displays the <paramref name="value">synopsis</paramref>. </summary>
    ''' <param name="value"> The synopsis to display. </param>
    Protected Overrides Sub DisplaySynopsis(value As String)
        Me._StatusToolStripLabel.Text = value
    End Sub

    ''' <summary> Gets the trace display level. </summary>
    ''' <value> The trace display level. </value>
    Public Property TraceDisplayLevel As TraceEventType = TraceEventType.Verbose

    ''' <summary> Gets or sets the trace show level. The trace message is displayed if the trace level is lower
    ''' than this value. </summary>
    ''' <value> The trace show level. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Protected Overrides ReadOnly Property TraceShowLevel As Diagnostics.TraceEventType
        Get
            Return Me.TraceDisplayLevel
        End Get
    End Property

#End Region

End Class