Imports isr.Core.Agnostic
Imports isr.Core.Agnostic.EnumExtensions
''' <summary> Includes code for Switchboard Form, which serves as a switchboard for this program. </summary>
''' <license> (c) 2005 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="10/11/95" by="David" revision="1.0.2110.x"> Created. </history>
Public Class Switchboard

#Region " CONSTRUCTION + CLEANUP "

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New()

        ' This method is required by the Windows Form Designer.
        InitializeComponent()

        ' turn off saving to prevent exception reported due to access from two programs
        Me.SaveSettingsOnClosing = False

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Event handler. Called by form for load events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' set the form caption
            Me.Text = ApplicationInfo.BuildApplicationDescriptionCaption("SWITCH BOARD")

            ' center the form
            Me.CenterToScreen()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Event handler. Called by form for shown events. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' populate the action list
            Me.populateActiveList()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default


        End Try
    End Sub


#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary> Enumerates the action options. </summary>
    Private Enum ActionOption
        <System.ComponentModel.Description("Simple Read and Write")> SimpleReadAndWrite
        <System.ComponentModel.Description("Message-Based VISA Test Panel")> MessageBasedVisaTestPanel
        <System.ComponentModel.Description("Service Request Events Requester")> ServiceRequester
    End Enum

    ''' <summary> Populates the list of options in the action combo box. </summary>
    ''' <remarks> It seems that out enumerated list does not work very well with this list. </remarks>
    Private Sub PopulateActiveList()

        ' set the action list
        Me.applicationsListBox.DataSource = Nothing
        Me.applicationsListBox.Items.Clear()
        Me.applicationsListBox.DataSource = GetType(ActionOption).ValueDescriptionPairs()
        Me.applicationsListBox.ValueMember = "Key"
        Me.applicationsListBox.DisplayMember = "Value"

    End Sub

    ''' <summary> Gets the selected action. </summary>
    ''' <value> The selected action. </value>
    Private ReadOnly Property SelectedAction() As ActionOption
        Get
            Return CType(CType(Me.applicationsListBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, ActionOption)
        End Get
    End Property

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Closes the form and exits the application. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitButton.Click
        Me.Close()
    End Sub

    ''' <summary> Open selected items. </summary>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    ''' <see cref="System.Windows.Forms.Form"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openButton.Click
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Select Case Me.selectedAction
            Case ActionOption.MessageBasedVisaTestPanel
                Using testPanel As MessageBasedVisaPanel = New MessageBasedVisaPanel
                    ' both cannot save to the settings file.
                    testPanel.SaveSettingsOnClosing = Not Me.SaveSettingsOnClosing
                    testPanel.ShowDialog(Me)
                End Using
            Case ActionOption.SimpleReadAndWrite
                Using testPanel As SimpleReadWrite = New SimpleReadWrite
                    testPanel.ShowDialog(Me)
                End Using
            Case ActionOption.ServiceRequester
                Using testPanel As ServiceRequester = New ServiceRequester
                    testPanel.ShowDialog(Me)
                End Using
        End Select
        Me.Cursor = System.Windows.Forms.Cursors.Default
    End Sub

#End Region

End Class