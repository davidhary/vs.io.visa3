<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class Switchboard
    Inherits Core.Agnostic.UserFormBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Switchboard))
        Me.applicationsListBoxLabel = New System.Windows.Forms.Label()
        Me.openButton = New System.Windows.Forms.Button()
        Me.exitButton = New System.Windows.Forms.Button()
        Me.applicationsListBox = New System.Windows.Forms.ListBox()
        Me.tipsTooltip = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'applicationsListBoxLabel
        '
        Me.applicationsListBoxLabel.Location = New System.Drawing.Point(15, 15)
        Me.applicationsListBoxLabel.Name = "applicationsListBoxLabel"
        Me.applicationsListBoxLabel.Size = New System.Drawing.Size(345, 21)
        Me.applicationsListBoxLabel.TabIndex = 15
        Me.applicationsListBoxLabel.Text = "Select an item from the list and click Open"
        '
        'openButton
        '
        Me.openButton.Font = New System.Drawing.Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.openButton.Location = New System.Drawing.Point(414, 38)
        Me.openButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.openButton.Name = "openButton"
        Me.openButton.Size = New System.Drawing.Size(87, 30)
        Me.openButton.TabIndex = 13
        Me.openButton.Text = "&Open..."
        '
        'exitButton
        '
        Me.exitButton.Font = New System.Drawing.Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.exitButton.Location = New System.Drawing.Point(414, 165)
        Me.exitButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.exitButton.Name = "exitButton"
        Me.exitButton.Size = New System.Drawing.Size(87, 30)
        Me.exitButton.TabIndex = 12
        Me.exitButton.Text = "E&xit"
        '
        'applicationsListBox
        '
        Me.applicationsListBox.Font = New System.Drawing.Font(SystemFonts.MessageBoxFont.FontFamily, 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.applicationsListBox.ItemHeight = 17
        Me.applicationsListBox.Location = New System.Drawing.Point(13, 38)
        Me.applicationsListBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.applicationsListBox.Name = "applicationsListBox"
        Me.applicationsListBox.Size = New System.Drawing.Size(382, 157)
        Me.applicationsListBox.TabIndex = 14
        Me.tipsTooltip.SetToolTip(Me.applicationsListBox, "Select an application to test")
        '
        'Switchboard
        '
        Me.ClientSize = New System.Drawing.Size(513, 217)
        Me.Controls.Add(Me.applicationsListBoxLabel)
        Me.Controls.Add(Me.openButton)
        Me.Controls.Add(Me.exitButton)
        Me.Controls.Add(Me.applicationsListBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Switchboard"
        Me.Text = "Switchboard"
        Me.ResumeLayout(False)

    End Sub
  Private WithEvents ApplicationsListBoxLabel As System.Windows.Forms.Label
  Private WithEvents OpenButton As System.Windows.Forms.Button
  Private WithEvents ExitButton As System.Windows.Forms.Button
  Private WithEvents ApplicationsListBox As System.Windows.Forms.ListBox
  Private WithEvents TipsTooltip As System.Windows.Forms.ToolTip
End Class
