﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = Visa.My.ProjectTraceEventId.CoreTester

        Public Const AssemblyTitle As String = "Visa Core Tester"
        Public Const AssemblyDescription As String = "Visa Core Tester"
        Public Const AssemblyProduct As String = "Visa.Core.Tester.2019"

    End Class

End Namespace

