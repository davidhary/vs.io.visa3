﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TestPanel

#Region "Windows Form Designer generated code "
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then

                ' Free managed resources when explicitly called
                Me.onDisposeManagedResources()

                If components IsNot Nothing Then
                    components.Dispose()
                End If

            End If

            ' Free shared unmanaged resources
            'onDisposeUnManagedResources()

        Finally

            ' Invoke the base class dispose method

            MyBase.Dispose(disposing)

        End Try
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private WithEvents _T1750TabPage As System.Windows.Forms.TabPage
    Private WithEvents _MessagesTextBox As System.Windows.Forms.TextBox
    Private WithEvents _MessagesTabPage As System.Windows.Forms.TabPage
    Private WithEvents _Tabs As System.Windows.Forms.TabControl
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TestPanel))
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._Tabs = New System.Windows.Forms.TabControl()
        Me._InterfaceTabPage = New System.Windows.Forms.TabPage()
        Me._ConnectTabLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._InterfaceGroupBox = New System.Windows.Forms.GroupBox()
        Me._InterfacePanel = New isr.IO.Visa.Instrument.InterfacePanel()
        Me._K2002TabPage = New System.Windows.Forms.TabPage()
        Me._K2002Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._K2000Panel = New isr.IO.Visa.Multimeter.K2000.K2000Panel()
        Me._K2700TabPage = New System.Windows.Forms.TabPage()
        Me._K2700Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._K2700Panel = New isr.IO.Visa.Multimeter.K2700.K2700Panel()
        Me._T1750TabPage = New System.Windows.Forms.TabPage()
        Me._T1750Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._ThermoStreamPanel = New isr.IO.Visa.Handler.ThermoStream.ThermoStreamPanel()
        Me._MessagesTabPage = New System.Windows.Forms.TabPage()
        Me._MessagesTextBox = New System.Windows.Forms.TextBox()
        Me._StatusStrip = New System.Windows.Forms.StatusStrip()
        Me._StatusToolStripLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._Tabs.SuspendLayout()
        Me._InterfaceTabPage.SuspendLayout()
        Me._ConnectTabLayout.SuspendLayout()
        Me._InterfaceGroupBox.SuspendLayout()
        Me._K2002TabPage.SuspendLayout()
        Me._K2002Layout.SuspendLayout()
        Me._K2700TabPage.SuspendLayout()
        Me._K2700Layout.SuspendLayout()
        Me._T1750TabPage.SuspendLayout()
        Me._T1750Layout.SuspendLayout()
        Me._MessagesTabPage.SuspendLayout()
        Me._StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ToolTip
        '
        Me._ToolTip.IsBalloon = True
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._InterfaceTabPage)
        Me._Tabs.Controls.Add(Me._K2002TabPage)
        Me._Tabs.Controls.Add(Me._K2700TabPage)
        Me._Tabs.Controls.Add(Me._T1750TabPage)
        Me._Tabs.Controls.Add(Me._MessagesTabPage)
        Me._Tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Tabs.ItemSize = New System.Drawing.Size(42, 22)
        Me._Tabs.Location = New System.Drawing.Point(0, 0)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(466, 541)
        Me._Tabs.TabIndex = 1

        '
        '_InterfaceTabPage
        '
        Me._InterfaceTabPage.Controls.Add(Me._ConnectTabLayout)
        Me._InterfaceTabPage.Location = New System.Drawing.Point(4, 22)
        Me._InterfaceTabPage.Name = "_InterfaceTabPage"
        Me._InterfaceTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._InterfaceTabPage.Size = New System.Drawing.Size(458, 515)
        Me._InterfaceTabPage.TabIndex = 3
        Me._InterfaceTabPage.Text = "INTERFACE"
        Me._InterfaceTabPage.UseVisualStyleBackColor = True
        '
        '_ConnectTabLayout
        '
        Me._ConnectTabLayout.ColumnCount = 3
        Me._ConnectTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ConnectTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ConnectTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ConnectTabLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._ConnectTabLayout.Controls.Add(Me._InterfaceGroupBox, 1, 1)
        Me._ConnectTabLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ConnectTabLayout.Location = New System.Drawing.Point(3, 3)
        Me._ConnectTabLayout.Name = "_ConnectTabLayout"
        Me._ConnectTabLayout.RowCount = 3
        Me._ConnectTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ConnectTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._ConnectTabLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ConnectTabLayout.Size = New System.Drawing.Size(452, 509)
        Me._ConnectTabLayout.TabIndex = 26
        '
        '_InterfaceGroupBox
        '
        Me._InterfaceGroupBox.Controls.Add(Me._InterfacePanel)
        Me._InterfaceGroupBox.Location = New System.Drawing.Point(32, 81)
        Me._InterfaceGroupBox.Name = "_InterfaceGroupBox"
        Me._InterfaceGroupBox.Size = New System.Drawing.Size(387, 347)
        Me._InterfaceGroupBox.TabIndex = 27
        Me._InterfaceGroupBox.TabStop = False
        Me._InterfaceGroupBox.Text = "INTERFACE:"
        '
        '_InterfacePanel
        '
        Me._InterfacePanel.BackColor = System.Drawing.Color.Transparent
        Me._InterfacePanel.Enabled = False
        Me._InterfacePanel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._InterfacePanel.Location = New System.Drawing.Point(13, 23)
        Me._InterfacePanel.Name = "_InterfacePanel"
        Me._InterfacePanel.Size = New System.Drawing.Size(368, 311)
        Me._InterfacePanel.TabIndex = 0
        Me._InterfacePanel.TraceDisplayLevel = System.Diagnostics.TraceEventType.Verbose
        '
        '_K2002TabPage
        '
        Me._K2002TabPage.Controls.Add(Me._K2002Layout)
        Me._K2002TabPage.Location = New System.Drawing.Point(4, 22)
        Me._K2002TabPage.Name = "_K2002TabPage"
        Me._K2002TabPage.Size = New System.Drawing.Size(458, 515)
        Me._K2002TabPage.TabIndex = 5
        Me._K2002TabPage.Text = "K2002"
        Me._K2002TabPage.ToolTipText = "Keithley 2002 Multimeter"
        Me._K2002TabPage.UseVisualStyleBackColor = True
        '
        '_K2002Layout
        '
        Me._K2002Layout.ColumnCount = 3
        Me._K2002Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._K2002Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._K2002Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._K2002Layout.Controls.Add(Me._K2000Panel, 1, 1)
        Me._K2002Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._K2002Layout.Location = New System.Drawing.Point(0, 0)
        Me._K2002Layout.Name = "_K2002Layout"
        Me._K2002Layout.RowCount = 3
        Me._K2002Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._K2002Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._K2002Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._K2002Layout.Size = New System.Drawing.Size(458, 515)
        Me._K2002Layout.TabIndex = 1
        '
        '_K2000Panel
        '
        Me._K2000Panel.BackColor = System.Drawing.Color.Transparent
        Me._K2000Panel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._K2000Panel.Location = New System.Drawing.Point(47, 33)
        Me._K2000Panel.Name = "_K2000Panel"
        Me._K2000Panel.Size = New System.Drawing.Size(364, 449)
        Me._K2000Panel.TabIndex = 0
        Me._K2000Panel.TraceDisplayLevel = System.Diagnostics.TraceEventType.Verbose
        '
        '_K2700TabPage
        '
        Me._K2700TabPage.Controls.Add(Me._K2700Layout)
        Me._K2700TabPage.Location = New System.Drawing.Point(4, 22)
        Me._K2700TabPage.Name = "_K2700TabPage"
        Me._K2700TabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._K2700TabPage.Size = New System.Drawing.Size(458, 515)
        Me._K2700TabPage.TabIndex = 4
        Me._K2700TabPage.Text = "K2700"
        Me._K2700TabPage.ToolTipText = "Keithley 2700 Meter and Switch"
        Me._K2700TabPage.UseVisualStyleBackColor = True
        '
        '_K2700Layout
        '
        Me._K2700Layout.ColumnCount = 3
        Me._K2700Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._K2700Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._K2700Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._K2700Layout.Controls.Add(Me._K2700Panel, 1, 1)
        Me._K2700Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._K2700Layout.Location = New System.Drawing.Point(3, 3)
        Me._K2700Layout.Name = "_K2700Layout"
        Me._K2700Layout.RowCount = 3
        Me._K2700Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._K2700Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._K2700Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._K2700Layout.Size = New System.Drawing.Size(452, 509)
        Me._K2700Layout.TabIndex = 0
        '
        '_K2700Panel
        '
        Me._K2700Panel.BackColor = System.Drawing.Color.Transparent
        Me._K2700Panel.ClosedChannels = ""
        Me._K2700Panel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._K2700Panel.Location = New System.Drawing.Point(44, 30)
        Me._K2700Panel.Name = "_K2700Panel"
        Me._K2700Panel.Size = New System.Drawing.Size(364, 449)
        Me._K2700Panel.TabIndex = 0
        Me._K2700Panel.TraceDisplayLevel = System.Diagnostics.TraceEventType.Verbose
        '
        '_T1750TabPage
        '
        Me._T1750TabPage.Controls.Add(Me._T1750Layout)
        Me._T1750TabPage.Location = New System.Drawing.Point(4, 22)
        Me._T1750TabPage.Name = "_T1750TabPage"
        Me._T1750TabPage.Size = New System.Drawing.Size(458, 515)
        Me._T1750TabPage.TabIndex = 0
        Me._T1750TabPage.Text = "THERMO"
        Me._T1750TabPage.ToolTipText = "Tegam Ohm Meter"
        Me._T1750TabPage.UseVisualStyleBackColor = True
        '
        '_T1750Layout
        '
        Me._T1750Layout.ColumnCount = 3
        Me._T1750Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._T1750Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._T1750Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._T1750Layout.Controls.Add(Me._ThermoStreamPanel, 1, 1)
        Me._T1750Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._T1750Layout.Location = New System.Drawing.Point(0, 0)
        Me._T1750Layout.Name = "_T1750Layout"
        Me._T1750Layout.RowCount = 3
        Me._T1750Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._T1750Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._T1750Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._T1750Layout.Size = New System.Drawing.Size(458, 515)
        Me._T1750Layout.TabIndex = 0
        '
        '_ThermoStreamPanel
        '
        Me._ThermoStreamPanel.BackColor = System.Drawing.Color.Transparent
        Me._ThermoStreamPanel.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ThermoStreamPanel.Location = New System.Drawing.Point(47, 33)
        Me._ThermoStreamPanel.Name = "_ThermoStreamPanel"
        Me._ThermoStreamPanel.Size = New System.Drawing.Size(364, 449)
        Me._ThermoStreamPanel.TabIndex = 0
        Me._ThermoStreamPanel.TraceDisplayLevel = System.Diagnostics.TraceEventType.Verbose
        '
        '_MessagesTabPage
        '
        Me._MessagesTabPage.Controls.Add(Me._MessagesTextBox)
        Me._MessagesTabPage.Location = New System.Drawing.Point(4, 22)
        Me._MessagesTabPage.Name = "_MessagesTabPage"
        Me._MessagesTabPage.Size = New System.Drawing.Size(458, 515)
        Me._MessagesTabPage.TabIndex = 2
        Me._MessagesTabPage.Text = "Log"
        Me._MessagesTabPage.UseVisualStyleBackColor = True
        '
        '_MessagesTextBox
        '
        Me._MessagesTextBox.AcceptsReturn = True
        Me._MessagesTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._MessagesTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._MessagesTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._MessagesTextBox.Location = New System.Drawing.Point(0, 0)
        Me._MessagesTextBox.MaxLength = 0
        Me._MessagesTextBox.Multiline = True
        Me._MessagesTextBox.Name = "_MessagesTextBox"
        Me._MessagesTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._MessagesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me._MessagesTextBox.Size = New System.Drawing.Size(458, 515)
        Me._MessagesTextBox.TabIndex = 15
        '
        '_StatusStrip
        '
        Me._StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StatusToolStripLabel})
        Me._StatusStrip.Location = New System.Drawing.Point(0, 541)
        Me._StatusStrip.Name = "_StatusStrip"
        Me._StatusStrip.Size = New System.Drawing.Size(466, 22)
        Me._StatusStrip.TabIndex = 2
        Me._StatusStrip.Text = "StatusStrip1"
        '
        '_StatusToolStripLabel
        '
        Me._StatusToolStripLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._StatusToolStripLabel.Name = "_StatusToolStripLabel"
        Me._StatusToolStripLabel.Size = New System.Drawing.Size(451, 17)
        Me._StatusToolStripLabel.Spring = True
        Me._StatusToolStripLabel.Text = "Ready"
        '
        'TestPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(466, 563)
        Me.Controls.Add(Me._Tabs)
        Me.Controls.Add(Me._StatusStrip)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(297, 150)
        Me.MaximizeBox = False
        Me.Name = "TestPanel"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Mini VISA Tester"
        Me._Tabs.ResumeLayout(False)
        Me._InterfaceTabPage.ResumeLayout(False)
        Me._ConnectTabLayout.ResumeLayout(False)
        Me._InterfaceGroupBox.ResumeLayout(False)
        Me._K2002TabPage.ResumeLayout(False)
        Me._K2002Layout.ResumeLayout(False)
        Me._K2700TabPage.ResumeLayout(False)
        Me._K2700Layout.ResumeLayout(False)
        Me._T1750TabPage.ResumeLayout(False)
        Me._T1750Layout.ResumeLayout(False)
        Me._MessagesTabPage.ResumeLayout(False)
        Me._MessagesTabPage.PerformLayout()
        Me._StatusStrip.ResumeLayout(False)
        Me._StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _InterfaceTabPage As System.Windows.Forms.TabPage
    Private WithEvents _ConnectTabLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _InterfaceGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents _T1750Layout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _InterfacePanel As isr.IO.Visa.Instrument.InterfacePanel
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents _StatusStrip As System.Windows.Forms.StatusStrip
    Private WithEvents _StatusToolStripLabel As System.Windows.Forms.ToolStripStatusLabel
    Private WithEvents _K2002TabPage As System.Windows.Forms.TabPage
    Private WithEvents _K2700TabPage As System.Windows.Forms.TabPage
    Private WithEvents _K2700Layout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _K2002Layout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _K2700Panel As isr.IO.Visa.Multimeter.K2700.K2700Panel
    Private WithEvents _K2000Panel As isr.IO.Visa.Multimeter.K2000.K2000Panel
    Private WithEvents _ThermoStreamPanel As isr.IO.Visa.Handler.ThermoStream.ThermoStreamPanel

#End Region

End Class

